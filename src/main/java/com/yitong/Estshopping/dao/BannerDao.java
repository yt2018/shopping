package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.Banner;
import com.yitong.Estshopping.entity.User;

public interface BannerDao extends BaseDao<Banner>{


	Banner getbanner(Banner banner);

	Integer listcount(User user, Integer state, Integer page, Integer limit);

	List<Banner> list(User user, Integer state, Integer page, Integer limit);

	List<Banner> indexbannerlist(int i);
	
}
