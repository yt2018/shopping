package com.yitong.Estshopping.controller;




import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.Pinglun;
import com.yitong.Estshopping.entity.Product;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.AdminService;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.PinglunService;
import com.yitong.Estshopping.service.ProductService;
import com.yitong.Estshopping.service.UserService;

/**
 * 个人中心
 * @author YT
 *
 */

@Controller
public class UserInfoController {
	@Autowired
	private UserService userService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	@Autowired
	private ImageService imageService;
	
	@RequestMapping(value= "/userinfo")
	public String showIndex(HttpSession session,Model model){
		User user = (User) session.getAttribute("user");
		Integer uid = user.getUid();
		User userfind= userService.findUserId(uid);
		List<Product> productList =productService.getMyAllproduct(uid,1,10,null);
		//查询总数
		//通过uid查询评论
		List<Map> pinglist=pinglunService.getUidlist(uid,"on");
		Integer count=productService.getMyAllproductCount(uid,null);
		model.addAttribute("u", userfind);
		model.addAttribute("myproduct", productList);
		model.addAttribute("count", count);
		return "user/index";
	}
	@RequestMapping(value= "/userinfo/set")
	public String userset(HttpSession session){
		User user = (User) session.getAttribute("user");
		Integer uid = user.getUid();
		User userfind= userService.findUserId(uid);
		session.setAttribute("user", userfind);
		return "user/set";
	}
	@RequestMapping(value= "/userinfo/icon",method = RequestMethod.POST)
	public  @ResponseBody Map<String,Object> updateicon(HttpSession session,String icon){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		Integer uid = ((User) session.getAttribute("user")).getUid();
		User user2 = new User();
		user2.setUid(uid);
		user2.setIcon(icon);
		userService.updateUser(user2);
		resultMap.put("state", 1);
		resultMap.put("result", "更新成功");
		return resultMap;
	}
	@RequestMapping(value= "/userinfo/update",method = RequestMethod.POST)
	public  @ResponseBody Map<String,Object> update(HttpSession session,@RequestBody User user){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		Integer uid = ((User) session.getAttribute("user")).getUid();
		user.setUid(uid);
		userService.updateUser(user);
		resultMap.put("state", 1);
		resultMap.put("result", "更新成功");
		return resultMap;
	}
	@RequestMapping(value= "/userinfo/password",method = RequestMethod.GET)
	public  @ResponseBody Map<String,Object> updatePassword(HttpSession session,@RequestParam(name="oldpassword") String oldpassword,@RequestParam(name="newpassword") String newpassword){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		Integer uid = ((User) session.getAttribute("user")).getUid();
		
		boolean flag= userService.changePassage(uid,oldpassword,newpassword);
		if(flag) {
			resultMap.put("state", 1);
			resultMap.put("result", "密码修改成功");
			return resultMap;
			
		}else {
			resultMap.put("state", 2);
			resultMap.put("result", "当前密码错误");
			return resultMap;
		}
	}
	@RequestMapping(value= "/userinfo/relieveQQ",method = RequestMethod.GET)
	public  @ResponseBody Map<String,Object>  relieveQQ(HttpSession session){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		User user = (User) session.getAttribute("user");
		Integer uid = user.getUid();
		user.setQquser("");
		userService.relieveQQ(uid);
		resultMap.put("state", 0);
		resultMap.put("result", "解除绑定成功");
		session.setAttribute("user", userService.findUserId(uid));
		return resultMap;
	}
	@RequestMapping(value= "/userinfo/message")
	public String message(HttpSession session){
		
		return "user/message";
	}
	/**
	 * 根据用户查询商品
	 * @param session
	 * @return
	 */
	@RequestMapping(value= "/userinfo/allproduct")
	public String allproduct(HttpSession session,@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit){
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		User user = (User) session.getAttribute("user");
		Integer uid = user.getUid();
		List<Product> productList =productService.getMyAllproduct(uid,page,limit,null);
		//查询总数
		Integer count=productService.getMyAllproductCount(uid,null);
		session.setAttribute("myproduct", productList);
		session.setAttribute("page", page);
		session.setAttribute("limit", limit);
		session.setAttribute("count", count);
		
		return "user/allproduct";
	}
	@Autowired
	private PinglunService pinglunService;
	@RequestMapping(value= "/u/{uid}")
	public String uString(HttpSession session,Model model,@PathVariable Integer uid,@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit){
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		/**
		 * 自己访问自己
		 */
		User user = (User) session.getAttribute("user");
		if(!ObjectUtils.isEmpty(user)) {
			if(user.getUid()==uid) {
				return "user/index";
			}
		}
		User userfind= userService.findUserId(uid);
		if (ObjectUtils.isEmpty(userfind)||userfind.getState()==2) {
			//被禁的用户,访问不存在的页面
			return "user/uno";
		}
		List<Product> productList =productService.getMyAllproduct(uid,page,limit,"on");
		//查询总数
		//通过uid查询评论
		List<Map> pinglist=pinglunService.getUidlist(uid,"on");
		Integer count=productService.getMyAllproductCount(uid,"on");
		model.addAttribute("u", userfind);
		model.addAttribute("myproduct", productList);
		model.addAttribute("page", page);
		model.addAttribute("limit", limit);
		model.addAttribute("count", count);
		
		return "user/u";
	}
	
}
