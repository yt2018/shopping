package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.entity.Walletlog;

/**
 * 钱包日志记录
 * @author YT
 *
 */

public interface WalletLogService{
	public void seve(Walletlog walletlog);
	/**
	 * 查询记录列表
	 * @param user
	 * @param page
	 * @return
	 */

	public List<Walletlog> listorder(User user, Integer page, Integer limit);
	public Integer listorder(User user);
	/**
	 * 查询总资产
	 * @param user
	 * @return
	 */
	public Float usercount(User user);
	/**
	 * 查询今日消费
	 * @param user
	 * @param i
	 * @return
	 */
	public Float usercount(User user, Integer i);
	/**
	 * 取消订单
	 * @param walletlog
	 */
	public void sevequxiao(Walletlog walletlog);
	public void seveShouhuo(Walletlog walletlog);
	
}
