package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.User;

/**
 * 客户的Dao接口
 * @author YT
 *
 */
public interface UserDao extends BaseDao<User>{
	
	/**
	 * 根据用户名查询用户
	 */
	public User findByUserName(String userName);
	
	/**
	 * 根据用户名和密码查询用户
	 */
	public User findByUserNameAndPassword(String userName, String password);
	/**
	 * qq登录查找登录
	 * @param openID
	 * @return
	 */
	public User findByUserQQ(String openID);
	/**
	 * 更新user
	 * @param user
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */

	public void updateUser(User user) throws IllegalArgumentException, IllegalAccessException;
	/**
	 * 修改密码
	 * @param uid
	 * @param oldpassword
	 * @param newpassword
	 * @return 
	 */
	public boolean changePassage(Integer uid, String oldpassword, String newpassword);
	
	/**
	 * 管理员修改密码
	 * @param uid
	 * @param newpassword
	 * @return
	 */
	boolean resetPassage(Integer uid, String newpassword);
	/**
	 * 解除QQ
	 * @param uid 
	 */
	public void relieveQQ(Integer uid);
	
	/**
	 * 管理员查询用户
	 * @param page
	 * @param limit
	 * @param states
	 * @return
	 */
	public List<User> userList(Integer page, Integer limit, Integer states);

	public Integer countUsers(Integer states);

}
