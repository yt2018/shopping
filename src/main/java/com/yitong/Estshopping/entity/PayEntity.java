package com.yitong.Estshopping.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
@Table(name = "payentity")
@Entity
public class PayEntity {
	@Override
	public String toString() {
		return "PayEntity [payid=" + payid + ", state=" + state + ", out_trade_no=" + out_trade_no + ", uname=" + uname
				+ ", uid=" + uid + ", subject=" + subject + ", total_amount=" + total_amount + ", body=" + body
				+ ", timeout_express=" + timeout_express + ", product_code=" + product_code + ", timestamp=" + timestamp
				+ ", method=" + method + ", trade_no=" + trade_no + ", seller_id=" + seller_id + "]";
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "payid")
    private Integer payid;
	//状态
	/**
	 * 1 赞助
	 * 2 在线交易
	 * 3 充值交易
	 */
	
	@Column(name = "state")
	private Integer state;
	
	// 商户订单号，商户网站订单系统中唯一订单号，必填
	@Column(name = "out_trade_no")
	private String out_trade_no;
	// 商户
	@Column(name = "uname")
	private String uname;
	// 商户
	@Column(name = "uid")
	private String uid;
	// 订单名称，必填
	@Column(name = "subject")
	private String subject;
	// 付款金额，必填
	@Column(name = "total_amount")
	private String total_amount;
	// 商品描述，可空
	@Column(name = "body")
	private String body;
	// 超时时间 可空
	@Column(name = "timeout_express")
	private String timeout_express;
	// 销售产品码 必填
	@Column(name = "product_code")
	private String product_code;
	// 支付时间
	@Column(name = "timestamp")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  
	private Date timestamp;
	//支付方式
	@Column(name = "method")
	private String method;
	//trade_no/该交易在支付宝系统中的交易流水号。最长64位。
	@Column(name = "trade_no")
	private String trade_no;
	//收款支付宝账号对应的支付宝唯一用户号。 以2088开头的纯16位数字
	@Column(name = "seller_id")
	private String seller_id;
	/*http://tong118.com/playreturn?
		total_amount=1.00
		&timestamp=2018-03-27+17%3A21%3A53
		&sign=n16sFvUlmttSx3WU3rKQcaFsM9uR8NOy3LnrhC2Bvx8mprui6aGxEENJ4AN7MtWGTngbPAHoJqRLCJidItuadi6eUH%2ByUqsnrua9CLJ%2FZj0GGWUZSQi%2FfZ9XS7UzoluehV2J2wzMTJ%2BMz%2B29bo%2BBiiUZA7CeC2MsKTifuNERj%2Ff0cUa4qwOit5b3NRpNqCiobgLkwVRNSb2hXjeR6vz7n%2F7OsHC2SoP0ppoCyS%2BaNzWFamlf1gHBQcFZExYDRab4HlW4GZua%2Ftnbe016kWvB27xwzi6L6SJdFtIBvxhEg6%2FbXkGIzmEnwXrm505PVWwLv1wEzHkT8QL6M%2BgpSbUldQ%3D%3D
		&trade_no=2018032721001004080572259857
		&sign_type=RSA2
		&auth_app_id=2018011201818651
		&charset=UTF-8
		&seller_id=2088512167067317
		&method=alipay.trade.page.pay.return
		&app_id=2018011201818651
		&out_trade_no=201803271522142473825
		&version=1.0*/
	
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.timestamp = simpleDateFormat.parse(timestamp);
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getTrade_no() {
		return trade_no;
	}
	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}
	public String getSeller_id() {
		return seller_id;
	}
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTimeout_express() {
		return timeout_express;
	}
	public Integer getPayid() {
		return payid;
	}
	public void setPayid(Integer payid) {
		this.payid = payid;
	}
	public void setTimeout_express(String timeout_express) {
		this.timeout_express = timeout_express;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	
	
	
}
