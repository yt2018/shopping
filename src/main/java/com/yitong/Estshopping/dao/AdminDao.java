package com.yitong.Estshopping.dao;

import com.yitong.Estshopping.entity.Adminuser;

public interface AdminDao extends BaseDao<Adminuser>{
	/**
	 * 查看管理权限 
	 */
	public Adminuser adminRole(int uid);
}
