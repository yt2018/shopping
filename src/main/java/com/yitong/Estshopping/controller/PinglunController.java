package com.yitong.Estshopping.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.dao.PinglunDao;
import com.yitong.Estshopping.entity.Pinglun;
import com.yitong.Estshopping.entity.Product;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.PinglunService;
import com.yitong.Estshopping.service.ProductService;

/**
 * 评论
 * @author YT
 *
 */

@Controller
public class PinglunController {
	@Autowired
	private PinglunService pinglunService;
	@Autowired
	private ProductService productService;
	@RequestMapping(value="/userinfo/p/{pid}")
	public  @ResponseBody Map<String, Object>  ping(@PathVariable("pid") Integer pid,
			HttpServletRequest request,
			HttpSession session, 
			HttpServletResponse response,
			@RequestParam(required=false) String pinglun,
			@RequestParam(required=false) String niming,
			@RequestParam(required=false) Integer zid
			){
		User user = (User) session.getAttribute("user");
		Map<String,Object> map = new HashMap<String,Object>();
		if(user.getQq()==null||user.getPhone()==null||user.getName()==null) {
			map.put("code", 3);
			map.put("msg", "请完善资料在评论");
			return map;
		}
		/*String pinglun = map.get("pinglun").toString();
	    String niming = map.get("niming").toString();
	    Object object = map.get("zid");*/
	    Pinglun pinglun3=null;
	    if(!ObjectUtils.isEmpty(zid)) {
	    	System.out.println("追评ID----"+zid);
	    	pinglun3=pinglunService.getpingid(zid);
	    }
	    if(niming=="on") {
	    	System.out.println("匿名评论");
	    }
	    Product product = productService.getProduct(pid); 
	    Pinglun pinglun2 = new Pinglun(pinglun, 1, niming, pinglun3, user, product);
	    pinglunService.save(pinglun2);
		System.out.println(pinglun+"===="+niming);
		map.put("code", 1);
		map.put("msg", "评论成功");
		
		return map;
	}
	/**
	 * 评论
	 * @param pid
	 * @param request
	 * @param session
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/p/plist/{pid}")
	public  @ResponseBody Map<String, Object>  pinglist(@PathVariable("pid") Integer pid,
			HttpServletRequest request,
			HttpSession session, 
			HttpServletResponse response,
			@RequestParam(required=false) Integer page,
			@RequestParam(required=false) Integer limit
			){
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=50;
		}
		Map<String,Object> map1 = new HashMap<String,Object>();
		List<Map> map =pinglunService.maplist(pid,page,limit);
		map1.put("code", 1);
		map1.put("msg", "加载成功");
		map1.put("data", map);
		map1.put("count", pinglunService.countlist(pid));
		
		return map1;
	}
	
}
