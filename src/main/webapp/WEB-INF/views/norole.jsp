<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta.jsp"></jsp:include>
	<link rel="stylesheet" href="${basePath}/res/css/login.css" media="all" />
	<title>你权限不足,请联系网站管理员</title>
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
</head>
<body>
<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<div class="login_bg">
		<div class="login_content">
			<p style="font-size:30px;text-align: right;padding-top: 200px;color: red">你权限不足,请联系网站管理员</p>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/login.js"></script>
</body>
</html>