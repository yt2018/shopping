package com.yitong.Estshopping.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.AdminDao;
import com.yitong.Estshopping.entity.Adminuser;
import com.yitong.Estshopping.service.AdminService;
@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminDao adminDao;
	
	public Adminuser adminRole(int uid) {
		// TODO Auto-generated method stub
		return adminDao.adminRole(uid);
	}

}
