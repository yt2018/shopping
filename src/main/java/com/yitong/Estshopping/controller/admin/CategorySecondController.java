package com.yitong.Estshopping.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.CategorySecond;
import com.yitong.Estshopping.entity.Categorys;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;

/**
 * 菜单管理
 * @author YT
 *
 */

@Controller
public class CategorySecondController {
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	/**
	 * 二级目录页面
	 * @return
	 */
	
	@RequestMapping(value = "/admin/listCategorySecond/{page}")
	public String listCategorys(@PathVariable("page") Integer page, Map<String, Object> map){
		List<Categorys> categoryPage = categorysService.listCategory(page);
		map.put("categorys", categoryPage);
		map.put("page", page);
		//查询一级分类的页数
		Integer count = categorysService.countCategory();
		map.put("count", count);
		return "admin/categorySecond/list";
	 }
	@RequestMapping(value = "/admin/listCs")
	public @ResponseBody List<Categorys> listCs(){
		List<Categorys> categoryPage = categorysService.findAll();
		return categoryPage;
	 }
	/**
	 * 删除二级菜单
	 */
	@RequestMapping(value = "/admin/deleteCategorySecond/{csid}/{page}")
    public @ResponseBody Map<String, Object> deleteCategorySecond(@PathVariable("csid") Integer csid, @PathVariable("page") Integer page) {
		Map<String, Object> map =new HashMap<>();
		categorySecondService.deleteCategorySecond(csid);
		
		map.put("rest", "删除成功");
        return map;
    }
	/**
	 * 跳转到添加二级分类的界面
	 */
	@RequestMapping(value = "/admin/gotoAddCategorySecond")
	public String gotoAddCategorySecond(Map<String, Object> map) {
		List<Categorys> categorys = categorysService.findAll();
		for(Categorys cs : categorys){
        	System.out.println("====" + cs.getCname());
        }
		map.put("categorys", categorys);
		return "admin/categorySecond/add";
	}
	/**
	 * 添加二级分类
	 */
	@RequestMapping(value = "/admin/addCategorySecond/{cid}/{csname}")
    public @ResponseBody Map<String, Object> addCategorySecond(@PathVariable Integer cid,@PathVariable String csname,@RequestParam(required=false) String href) {
		Map<String, Object> map =new HashMap<>();
		System.out.println("===" + csname);
		System.out.println("==" + cid);
		//先查询是否存在
		CategorySecond findCategorySecond = categorySecondService.findCategorySecond(csname);
		if(findCategorySecond!=null) {
			System.out.println(findCategorySecond);
			String cname = findCategorySecond.getCategory().getCname();
			map.put("flag", false);
			map.put("rest", "[csname]-一级菜单["+cname+"]-名称被占用请修改");
			return map;
		}
        //查找一级分类对象
        Categorys category = categorysService.findCategory(cid);
        //创建二级分类对象
        CategorySecond categorySecond = new CategorySecond();
        //设置一级分类和二级分类的关联关系
        categorySecond.setCategory(category);
        categorySecond.setCsName(csname);
        categorySecond.setHref(href);;
        //保存对象
        categorySecondService.addCategorySecond(categorySecond);
        map.put("flag", true);
		map.put("rest", "["+csname+"]--名称创建成功");
		return map;
    }
	/**
	 * 跳转到修改二级分类界面
	 */
	@RequestMapping(value = "/admin/gotoEditCategorySecond/{csId}")
    public String gotoEditCategorySecond(@PathVariable("csId") Integer csId,
                                         Map<String, Object> map) {
        System.out.println("===" + csId);
        //查找对应的二级分类
        CategorySecond categorySecond = categorySecondService.findCategorySecond(csId);
        map.put("categorySecond", categorySecond);
        //查找所有一级分类
        List<Categorys> categorys = categorysService.findAll();
        map.put("categorys", categorys);
        return "admin/categorySecond/edit";
    }
	/**
	 * 更新二级分类
	 * updataCategorySecond/${categorySecond.csId }/"+cid+"/"+data.field.csname+"
	 */
	@RequestMapping(value = "/admin/updateCategorySecond/{csid}/{cid}/{csname}")
    public @ResponseBody Map<String, Object> updateCategorySecond(@PathVariable Integer csid, @PathVariable String csname,
    		@PathVariable Integer cid) {
		System.out.println(csid+"-----"+csname+"----"+cid+"--"+this.getClass());
		Map<String, Object> map =new HashMap<>();
        Categorys category = categorysService.findCategory(cid);
        CategorySecond categorySecond = categorySecondService.findCategorySecond(csid);
        String csName2 = categorySecond.getCsName();
        if (csName2.equals(csname)) {
        	map.put("flag", false);
    		map.put("rest", "["+csname+"]--名称没有修改");
    		return map;
		}
        //先查询是否存在
  		CategorySecond findCategorySecond = categorySecondService.findCategorySecond(csname);
  		if(findCategorySecond!=null) {
  			System.out.println(findCategorySecond);
  			String cname = findCategorySecond.getCategory().getCname();
  			map.put("flag", false);
  			map.put("rest", "["+csname+"]-一级菜单["+cname+"]-名称被占用请修改");
  			return map;
  		}
        categorySecond.setCategory(category);
        categorySecond.setCsName(csname);
        categorySecondService.updateCategorySecond(categorySecond);
        map.put("flag", true);
		map.put("rest", "["+csname+"]--名称修改成功");
        return map;
    }
	/**
	 * 根据ID查询二级分类
	 * @param cid
	 * @return
	 */
	@RequestMapping(value="/listcategory2/{cid}")
	public @ResponseBody List<CategorySecond> listProduct(@PathVariable("cid") Integer cid){
		Map<String, Object> map =new HashMap<>();
		List<CategorySecond> listcategory2 = categorySecondService.findCategorySecondlist(cid);
		for (CategorySecond categorySecond : listcategory2) {
			System.out.println(categorySecond.getCsName());
		}
		//map.put("listcategory2", listcategory2);
		return listcategory2;
	}
}
