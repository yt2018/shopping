<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>钱包</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",8); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
				<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
					<div class="layui-row layui-timeline-content" style="line-height: 50px;">
						<div class="layui-col-xs12 layui-col-md4">
							<div class="layui-col-xs6 layui-col-md12">
								
								总金额: <span style="font-size: 30px; color: #E6162D;">${money}</span><span>元</span>
							</div>
							<div class="layui-col-xs6 layui-col-md12">
								<div class="layui-col-xs4">
									<a href="javascript:layer.msg('正在和第三方支付协商.......');" class="layui-btn layui-btn-danger layui-btn-mini">充值</a>
								</div>
								<div class="layui-col-xs4">
									<a href="javascript:layer.msg('正在和第三方支付协商.......');" class="layui-btn layui-btn-warm layui-btn-mini">充值卡</a>
								</div>	
								<div class="layui-col-xs4">
									<a href="javascript:layer.msg('正在和第三方支付协商.......');" class="layui-btn layui-btn-mini">提现</a>
								</div>
							</div>
						</div>
						<div class="layui-col-xs6 layui-col-md4">
							<div class="grid-demo">
								今日消费: <span style="font-size: 30px; color:green;">${money1}</span><span>元</span>
							</div>
						</div>
						<div class="layui-col-xs6 layui-col-md4">
							<div class="grid-demo grid-demo-bg2">
								今日收益: <span style="font-size: 30px; color:green;">${money2}</span><span>元</span>
							</div>
						</div>
					</div>
				</div>
				<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
					<legend>公告</legend>
				</fieldset>
				<blockquote class="layui-elem-quote">系统公告</blockquote>
				<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
					<legend>消费记录</legend>
				</fieldset>
				<table class="layui-table">
					<colgroup>
						<col width="200">
						<col>
						<col width="80">
						<col width="200">
						<col width="80">
					</colgroup>
					<thead>
						<tr>
							<th>订单号</th>
							<th>内容</th>
							<th>状态</th>
							<th>日期</th>
							<th>金额</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${empty listorder }">
						<tr>
							<th colspan="5">暂无数据</th>
						</tr>
					</c:if>
					<c:if test="${not empty listorder }">
						<c:forEach var="w" items="${listorder}">
							<tr>
								<th>${w.wif }</th>
								<th class="cy"><div style="display: none">${ w.content }</div></th>
								<c:if test="${w.states ==1}">
									<th>消费</th>
								</c:if>
								<c:if test="${w.states ==2}">
									<th>收入</th>
								</c:if>
								<c:if test="${w.states ==3}">
									<th>充值(第三方)</th>
								</c:if>
								<c:if test="${w.states ==4}">
									<th>充值卡</th>
								</c:if>
								<c:if test="${w.states ==5}">
									<th>退款</th>
								</c:if>
								<th>${w.logtime }</th>
								<th>${w.money }元</th>
							</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
				<div id="pages"></div>
			</div>
		
						<script type="text/javascript">
							layui.config({
								  //base: 'res/js/jquery-zclip-master/' //假设这是你存放拓展模块的根目录
								}).use(['layer', 'laytpl', 'form', 'element', 'upload', 'util','jquery','laypage'], function(exports){
								  
									var $ = layui.jquery,
										laypage = layui.laypage,
										layer = layui.layer,
										laytpl = layui.laytpl,
										form = layui.form,
										element = layui.element,
										upload = layui.upload,
										util = layui.util,
										device = layui.device();
										$(".cy").each(function(){
											var t=$.parseJSON($(this).text())
											$(this).html("<a target='_block' href='${basePath}/Productinfo/"+t.pid+"'>"+t.name+"</a>")
										})
										var limit=${limit},
										page=${page},
										count=${count};
										laypage.render({ 
											 elem: 'pages'
											,curr: page//选中当前页  
											,limit: limit
											,limits:[10, 20, 30, 40, 50]
											,count: count//数据总数，从服务端得到 
											,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
											,jump: function(obj,first){
											    //obj包含了当前分页的所有参数，比如：
											    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
											    console.log(obj.limit); //得到每页显示的条数
											    if(page!=obj.curr){
											    	window.location.href="?page="+obj.curr+"&limit="+obj.limit;
												}
											}
										})
								})
						</script>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	
</body>
</html>