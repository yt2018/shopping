<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<title>所有商品列表</title>
	<%-- <link rel="stylesheet" href="${basePath}/res/css/categorylist.css" media="all" /> --%>
	<style type="text/css">
		.layui-table-cell ,.layui-table-view .layui-table td {
		    height: 60px;
		}
	</style>
</head>
<body>
	<br/>
	<a class="layui-btn add">添加商品</a>
	<br/>
	<div class="layui-btn-group demoTable">
		<button class="layui-btn" data-type="getCheckData">获取选中行数据</button>
		<button class="layui-btn" data-type="getCheckLength">获取选中数目</button>
		<button class="layui-btn" data-type="isAll">验证是否全选</button>
	</div>
	<table class="layui-table" id="test" lay-data="{
		 height:600
		, url:'${basePath}/admin/getProductlistno'
		, page:true
		, id:'idTest'
		,limits: [10,20,30,60,100]
		,limit: 10
		}"
		lay-filter="demo">
		<thead>
			<tr>
				<th lay-data="{checkbox:true, fixed: true}"></th>
				<th lay-data="{field:'pid', width:80, sort: true, fixed: true}">商品ID</th>
				<th lay-data="{field:'pname', width:300,templet:'#href' }">商品名称</th>
				<th lay-data="{field:'image', width:300,heigth:60,templet:'#imgsrc'}">商品图片</th>
				<th lay-data="{field:'marketPrice', width:80}">市场价</th>
				<th lay-data="{field:'shopPrice', width:177}">促销价</th>
				<th lay-data="{field:'inventory', width:80}">库存量</th>
				<th lay-data="{field:'isHot', width:100, sort: true,fixed: 'right',toolbar:'#isHot'}">是否热门</th>
				<th lay-data="{fixed: 'right', width:200, align:'center', toolbar: '#barDemo'}"></th>
			</tr>
		</thead>
	</table>
	<script type="text/html" id="href">
		 <a href="/detail/{{d.id}}" class="layui-table-link">{{d.pname}}</a>
	</script>
	<script type="text/html" id="isHot">
		 {{#  if(d.isHot == 'on'){ }}
    		<input type="checkbox" data-pid="{{d.pid}}" checked name="is" lay-skin="switch" lay-filter="switchTest" lay-text="是|否" />
  			{{#  }else{ }} 
    		<input type="checkbox" data-pid="{{d.pid}}" name="is" lay-skin="switch" lay-filter="switchTest" lay-text="是|否" />
  			{{#  } }} 
	</script>
	<script type="text/html" id="imgsrc">
		 {{# layui.each(d.image,function(index,item){ }}
			<img height="60" src="{{item.imgsrc}}" alt="{{item.name}}"/>		
		{{# })}}
	</script>
	<script type="text/html" id="barDemo">
		{{#  if(d.isState == 'on'){ }}
    		<input type="checkbox" data-pid="{{d.pid}}" checked name="is" lay-skin="switch" lay-filter="switchTest2" lay-text="正常|停止" />
  			{{#  }else{ }} 
    		<input type="checkbox" data-pid="{{d.pid}}" name="is" lay-skin="switch" lay-filter="switchTest2" lay-text="正常|停止" />
  			{{#  } }} 
		<a class="layui-btn layui-btn-primary layui-btn-mini" lay-event="detail">查看</a>
		<a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="del">删除</a>
	</script>
	<script>
	layui.use(['form','table','layer','jquery'], function(){
		var table = layui.table;
		var form=layui.form;
		
		//监听表格复选框选择
		table.on('checkbox(demo)', function(obj){
		    console.log(obj)
		});
		form.on('switch(switchTest)', function(data) {
			/* layer.msg('是否热门：' + (this.checked ? 'true' : 'false'), {
				offset: '6px'
			}); */
			var dom =$(data.elem);
			var is
			this.checked ? is='on' : isHot='off'
			updata(is,dom,"updateIsHot")
		});
		form.on('switch(switchTest2)', function(data) {
			/* layer.msg('是否热门：' + (this.checked ? 'true' : 'false'), {
				offset: '6px'
			}); */
			var dom =$(data.elem);
			var is
			this.checked ? is='on' : isHot='off'
			updata(is,dom,"updateIsState")
		});
		function updata(is,dom,url){
			
			var index  = layer.msg('修改中，请稍候',{icon: 16,time:false,shade:0.8});
	       	setTimeout(function(){
	        	$.ajax({
					url : "${basePath}/admin/"+url,
					type : "get",
					dataType : "json",
					data:{"is":is,"pid":dom.data("pid")},
					success : function(data){
						layer.close(index);
						layer.msg(data.msg);
					},
					error :function(){
						layer.msg("网络超时");
					}
				})
	            
	        },200);	
		}
		  //监听工具条
		table.on('tool(demo)', function(obj){
			var data = obj.data;
			if(obj.event === 'detail'){
				layer.msg('ID：'+ data.id + ' 的查看操作');
			} else if(obj.event === 'del'){
				layer.confirm('真的删除行么', function(index){
				    obj.del();
				    layer.close(index);
				});
			} else if(obj.event === 'edit'){
				layer.alert('编辑行：<br>'+ JSON.stringify(data))
			}
		});
		 
		var $ = layui.$, active = {
		    getCheckData: function(){ //获取选中数据
			var checkStatus = table.checkStatus('idTest')
				,data = checkStatus.data;
				layer.alert(JSON.stringify(data));
			}
			,getCheckLength: function(){ //获取选中数目
				var checkStatus = table.checkStatus('idTest')
				,data = checkStatus.data;
				layer.msg('选中了：'+ data.length + ' 个');
			}
			,isAll: function(){ //验证是否全选
				var checkStatus = table.checkStatus('idTest');
				layer.msg(checkStatus.isAll ? '全选': '未全选')
			}
		};
		  
		$('.demoTable .layui-btn').on('click', function(){
			var type = $(this).data('type');
			console.info(type)
			active[type] ? active[type].call(this) : '';
		});
		//添加
		$(".add").click(function(){
			var index = layer.open({
				title : "新增商品",
				type : 2,
				area: ['800px', '600px'],
				content : "${basePath}/admin/addProduct",
				cancel: function(index, layero){ 
					if(confirm('商品未保存确定要关闭么')){ //只有当点击confirm框的确定时，该层才会关闭
						//关闭后清除缓存文件
				    	$.get("${basePath}/imagesdelete");
						layer.close(index)
				  	}
				  	return false; 
				}    
			})			
		})
	});
	</script>
</body>
</html>
