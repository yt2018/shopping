package com.yitong.framework.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PayUtil {

	private static Logger logger = LogManager.getLogger();

	public static String UID = "2e60fd991ae07350b7896c1a";

	public static String NOTIFY_URL = "http://ileart.imwork.net/ESTshopping/userinfo/notifyPay";

	public static String RETURN_URL = "http://ileart.imwork.net/ESTshopping/userinfo/returnPay";

	public static String BASE_URL = "https://pay.paysapi.com";

	public static String TOKEN = "bda53a0d8af4e290aaa73a8a3dc68c02";

	public static Map<String, String> payhtml(String price,String istype,String orderid,String orderuid,String goodsname) {  
        Map<String, String> mapParam = new HashMap<String, String>();  
        /*商户uid*/
        mapParam.put("uid", UID);
        /*价格*/
        mapParam.put("price",price);  
        /*支付渠道*//*1：支付宝；2：微信支付*/
        mapParam.put("istype",istype);  
        /*通知回调网址*/
        mapParam.put("notify_url",NOTIFY_URL);
        /*跳转网址*/
        mapParam.put("return_url",RETURN_URL);  
        /*商户自定义订单号*/
        mapParam.put("orderid",orderid);
        /*商户自定义客户号*/
        mapParam.put("orderuid",orderuid);  
        /*商品名称*/
        mapParam.put("goodsname",goodsname);
        /*秘钥*/
        mapParam.put("key",TOKEN);  
		return mapParam;
    } 
	/** 
     * 向指定URL发送POST请求 
     * @param url 
     * @param paramMap 
     * @return 响应结果 
     */  
    public static String sendPost(String url, Map<String, String> paramMap) {  
        PrintWriter out = null;  
        BufferedReader in = null;  
        String result = "";  
        try {  
            URL realUrl = new URL(url);  
            // 打开和URL之间的连接  
            URLConnection conn = realUrl.openConnection();  
            // 设置通用的请求属性  
            conn.setRequestProperty("accept", "*/*");  
            conn.setRequestProperty("connection", "Keep-Alive");  
            conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");  
            // conn.setRequestProperty("Charset", "UTF-8");  
            // 发送POST请求必须设置如下两行  
            conn.setDoOutput(true);  
            conn.setDoInput(true);  
            // 获取URLConnection对象对应的输出流  
            out = new PrintWriter(conn.getOutputStream());  
  
            // 设置请求属性  
            String param = "";  
            if (paramMap != null && paramMap.size() > 0) {  
                Iterator<String> ite = paramMap.keySet().iterator();  
                while (ite.hasNext()) {  
                    String key = ite.next();// key  
                    String value = paramMap.get(key);  
                    param += key + "=" + value + "&";  
                }
                System.out.println(param);
                param = param.substring(0, param.length() - 1);  
            }  
  
            // 发送请求参数  
            out.print(param);  
            // flush输出流的缓冲  
            out.flush();  
            // 定义BufferedReader输入流来读取URL的响应  
            in = new BufferedReader(  
                    new InputStreamReader(conn.getInputStream()));  
            String line;  
            while ((line = in.readLine()) != null) {  
                result += line;  
            }  
        } catch (Exception e) {  
            System.err.println("发送 POST 请求出现异常！" + e);  
            e.printStackTrace();  
        }  
        // 使用finally块来关闭输出流、输入流  
        finally {  
            try {  
                if (out != null) {  
                    out.close();  
                }  
                if (in != null) {  
                    in.close();  
                }  
            } catch (IOException ex) {  
                ex.printStackTrace();  
            }  
        }  
        return result;  
    }  
      
    /**  
     * 数据流post请求  
     * @param urlStr  
     * @param xmlInfo  
     */  
    public static String doPost(String urlStr, String strInfo) {  
        String reStr="";  
        try {  
            URL url = new URL(urlStr);  
            URLConnection con = url.openConnection();  
            con.setDoOutput(true);  
            con.setRequestProperty("Pragma:", "no-cache");  
            con.setRequestProperty("Cache-Control", "no-cache");  
            con.setRequestProperty("Content-Type", "text/xml");  
            OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());  
            out.write(new String(strInfo.getBytes("utf-8")));  
            out.flush();  
            out.close();  
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));  
            String line = "";  
            for (line = br.readLine(); line != null; line = br.readLine()) {  
                reStr += line;  
            }  
        } catch (MalformedURLException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return reStr;  
    }  
     
   
  
    /** 
     * 测试主方法 
     * @param args 
     */  
    public static void main(String[] args) {  
        Map<String, String> mapParam = new HashMap<String, String>();  
        mapParam.put("uid", UID);  
        mapParam.put("price","1");  
        mapParam.put("istype","1");  
        mapParam.put("notify_url",NOTIFY_URL);  
        mapParam.put("return_url",RETURN_URL);  
        mapParam.put("orderid","201802241519445709903");  
        mapParam.put("orderuid","tong118com");  
        mapParam.put("goodsname","韩式大衣");  
        mapParam.put("key",TOKEN);  
        String result = sendPost(BASE_URL, mapParam);  
        System.out.println(result);  
        
    } 

}
