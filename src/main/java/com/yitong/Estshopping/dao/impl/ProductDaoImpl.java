package com.yitong.Estshopping.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.ProductDao;
import com.yitong.Estshopping.entity.Product;

/**
 * 商品的Dao实现类
 * @author YT
 *
 */
@Repository("productDao") 
@Transactional
public class ProductDaoImpl  extends BaseDaoImpl<Product>implements ProductDao{
	final String selecthql = "select p.pid,p.imgsrcs,p.isHot,p.inventory,"
            + "p.marketPrice,p.pdate,p.pdesc,p.pname,p.shopPrice,p.user";
	
	@Override
	public Integer countProductno(String isState) {
		// TODO Auto-generated method stub
		String hql = "select count(*) from Product p where p.isState = '"+isState+"' order by p.pdate desc";
        return count(hql);
	}
	@Override
	public List<Product> getProductListno(Integer page, Integer limit, String isState) {
		String hql = "from Product p where p.isState = '"+isState+"' order by p.pdate desc";
        return find(hql,page, limit);
	}
	@Override
	public Product getPrId(Date pdate) {
		// TODO Auto-generated method stub
		String hql ="from Product p where p.pdate= :pdate";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("pdate", pdate);
		return (Product) query.uniqueResult();
	}

	@Override
	public List<Product> getProductList(Integer page, Integer limit) {
		String hql="from Product p order by p.pdate desc";
		return this.find(hql, page, limit);
	}
	@Override
	public Integer countProduct() {
		// TODO Auto-generated method stub
		String hql = "select count(*) from Product";
        return count(hql);
	}

	@Override
	public void updateProductIsHot(String isHot, Integer pid) {
		// TODO Auto-generated method stub
		Product product = this.get(pid);
		product.setIsHot(isHot);
		this.update(product);
	}


	@Override
	public void updateProductIsState(String isState, Integer pid) {
		// TODO Auto-generated method stub
		Product product = this.get(pid);
		product.setIsState(isState);
		product.setIsHot("off");;
		this.update(product);
	}


	/*@Override
	public List<Product> getProductListno(Integer page, Integer limit,String isState) {
		String hql="from Product p where p.isState = '"+isState+"' order by p.pdate desc";
		return this.find(hql, page, limit);
	}*/
	@Override
	public List<Product> getProductListcid(Integer csid, Integer page,Integer limit) {
		String hql="from Product p where p.isState = 'on' and p.categorySecond.csId = '"+csid+"' order by p.pdate desc";
		return this.find(hql, page,limit);
	}
	@Override
	public Integer countProduct(Integer csid) {
		// TODO Auto-generated method stub
		String hql = "select count(*) from Product p where p.isState = 'on' and p.categorySecond.csId = '"+csid+"'";
        return count(hql);
	}
	@Override
	public List<Product> findByCategoryId(Integer cid, Integer page,Integer limit) {
		// TODO Auto-generated method stub
		String hql="from Product p where p.isState = 'on' and p.categorySecond.category.cid = '"+cid+"' order by p.pdate desc";
		return this.find(hql, page,limit);
	}
	@Override
	public Integer countProductone(Integer cid) {
		// TODO Auto-generated method stub
		String hql = "select count(*) from Product p where p.isState = 'on' and p.categorySecond.category.cid = '"+cid+"' order by p.pdate desc";
        return count(hql);
	}
	@Override
	public List<Product> getProductdate(Integer page, Integer limit) {
		// TODO Auto-generated method stub
		String hql="from Product p where p.isState = 'on' order by p.pdate desc";
		return this.find(hql, page,limit);
	}
	@Override
	public Integer countProductdate() {
		// TODO Auto-generated method stub
		String hql = "select count(*) from Product p where p.isState = 'on' order by p.pdate desc";
        return count(hql);
	}
	@Override
	public void updateProductPnub(Integer pid ,Integer pnub) {
		// TODO Auto-generated method stub
		Product product = this.get(pid);
		Integer pnub2 = product.getPnub();
		if(pnub2==null)pnub2=0;
		product.setPnub(pnub2+pnub);
		this.update(product);
	}
	@Override
	public List<Product> getProductPnub(Integer page, Integer limit) {
		String hql="from Product p where p.isState = 'on' order by p.pnub desc";
		return this.find(hql, page,limit);
	}
	@Override
	public List<Product> getProductPremen(Integer page, Integer limit) {
		// TODO Auto-generated method stub
		String hql="from Product p where p.isState = 'on'and p.isHot ='on' order by p.pdate desc";
		return this.find(hql, page,limit);
	}
	@Override
	public Integer countProductdatePremen() {
		String hql="select count(*) from Product p where p.isState = 'on'and p.isHot ='on' order by p.pdate desc";
		// TODO Auto-generated method stub
		return count(hql);
	}
	@Override
	public List<Product> getProductListRAND() {
		// TODO Auto-generated method stub
		String intlist="select p.pid from Product p where p.isState = 'on'";
		List<Product> list = this.find(intlist);
		Iterator iter = list.iterator();
		List<Integer> arr=new ArrayList<>();
		while (iter.hasNext()) {
          Integer next = (Integer) iter.next();
          arr.add(next);
          //System.out.println(next);
		}
		int index=(int)(Math.random()*arr.size());
		//System.out.println(index);
		int rand = arr.get((int)(Math.random()*arr.size()));
		System.out.println("随机数为"+rand);
		String hql="from Product p where p.isState = 'on' and p.pid>='"+rand+"'";
		List<Product> subList = this.find(hql, 1, 8);
		
		return subList;
	}
	@Override
	public Integer getcount(Integer uid) {
		// TODO Auto-generated method stub
		String hql="select count(*) from Product p where p.isState = 'on'and p.user.uid='"+uid+"'";
		// TODO Auto-generated method stub
		return count(hql);
	}
	@Override
	public List<Product> getcountsum(Integer uid) {
		// TODO Auto-generated method stub
		String hql="from Product p where p.isState = 'on' and p.user.uid='"+uid+"' order by p.pdate desc";
		// TODO Auto-generated method stub
		return find(hql);
	}
	@Override
	public List<Product> getMyAllproduct(Integer uid, Integer page, Integer limit,String on) {
		String hql = null;
		if(on==null) {
			hql="from Product p where p.user.uid='"+uid+"' and p.isState != 'del' order by p.pdate desc";
		}else {
			hql="from Product p where p.user.uid='"+uid+"' and p.isState = 'on' order by p.pdate desc";
		}
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		return find(hql,page, limit);
	}
	@Override
	public Integer getMyAllproductCount(Integer uid,String on) {
		// TODO Auto-generated method stub
		String hql = null;
		if(on==null) {
			hql="select count(*) from Product p where p.user.uid='"+uid+"' and p.isState != 'del'";
		}else {
			hql="select count(*) from Product p where p.user.uid='"+uid+"' and p.isState = 'on'";
		}
		// TODO Auto-generated method stub
		return count(hql);
	}
	@Override
	public void updateProductInventory(Integer pid, Integer nume) {
		// TODO Auto-generated method stub
		Product product = this.get(pid);
		product.setInventory(nume);
		this.update(product);
	}
	//final String selecthql = "select p.pid,p.imgsrcs,p.isHot,p.inventory,"
           // + "p.marketPrice,p.pdate,p.pdesc,p.pname,p.shopPrice,p.user";
	@Override
	public List<Product> findSearch(String key,Integer page, Integer limit) {
		// TODO Auto-generated method stub
		String hql ="from Product p where p.isState = 'on' and p.pname  = :key or p.pnamedesc= :key or p.categorySecond= :key";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("key", "%"+key+"%");
		return query.setFirstResult((page - 1)*limit).setMaxResults(limit).list();
	}
	@Override
	public List<Product> listSearch(String key, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		String hql ="from Product p where p.isState = 'on' and (p.pname like :key or p.pnamedesc like :key or p.categorySecond.csName like :key or p.categorySecond.category.cname like :key) order by p.pdate desc";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("key", "%"+key+"%");
		return query.setFirstResult((page - 1)*limit).setMaxResults(limit).list();
	}
	@Override
	public Integer listSearchcount(String key) {
		// TODO Auto-generated method stub
		String hql="select count(*) from Product p where p.isState = 'on' and (p.pname like :key or p.pnamedesc like :key or p.categorySecond.csName like :key or p.categorySecond.category.cname like :key) order by p.pdate desc";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("key", "%"+key+"%");
		return query.getFirstResult();
	}
	
	

}
