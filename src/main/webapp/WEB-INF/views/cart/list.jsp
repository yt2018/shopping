<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<title>我的购物车</title>
	<%-- <link rel="stylesheet" href="${basePath}/res/css/categorylist.css" media="all" /> --%>
	<link rel="stylesheet" href="${basePath}/res/css/productinfo.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
	<style type="text/css">
		.productbody{min-height: 600px}
		.productbody img{background: url("${basePath}/res/css/img/logo-03.png") no-repeat center center;background-size: cover;}
		.productbody ul li{padding:10px 0;border-bottom: 1px solid #eee;line-height: 24px}
		.productbody ul li p{font-size: 18px}
		.productbody button{float: right;margin-left:15px}
		.pdelete{text-align: right;}
	</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<div class="productbody">
		<ul style="font-size: 14px;" id='carthtml'>
		</ul>
		<div class="cart-jiesuan layui-row" style="padding-top:10px">
			<div class="layui-col-xs9" style="color: green;font-size:22px;line-height: 40px">
				<c:if test="${empty user }"><span>登录查看</span></c:if>
				<c:if test="${not empty user }"><span>总计：￥</span><span id="total"></span></c:if>
			</div>
			<div class="layui-col-xs3" >
				<a class="layui-row layui-btn layui-btn-red" href="${basePath}/userinfo/mycart">
					结 算
				</a>
				<button class="layui-btn layui-btn-danger cartbtn" id="clearcart">
					清 空
				</button>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<script>
			layui.use('layer', function(){
				var layer = parent.layer === undefined ? layui.layer : parent.layer;
				var $ = layui.$;
				cartlist('${basePath}/getcart')
				//清空购物车
				$("#clearcart").on('click',function(e){
					var index  = layer.msg('批量删除中，请稍候',{icon: 16,time:false,shade:0.8});
					layer.confirm('确定要删除吗?', function(index){
						cartlist("${basePath}/clearCart")
						layer.close(index);
					})
				})
				$(document).on('click','.pdelete',function(){
					var pid=$(this).data("id");
					var url='${basePath}/removeCart/'+pid
					cartlist(url)
				})
				function cartlist(url){
					$.get(url,function(d){
						var html;
						//console.info(d.carts)
						if(d.carts==null||d.carts.total==0){
							html="<li>购物车空空的,亲选择你喜欢的商品</li>"
							$("#carthtml").html(html);
							var t=0;
							$("#total").html(t.toFixed(2))
							html='';
							return ;
						}else{	
							html='';
							$.each(d.carts.cartItems,function(i,v,arr){
								html+= '<li class="layui-row">'
										+'	<div class="layui-col-xs1">'
										+'		<img width="60" height="60" src="'+v.imgsrc+'"/>'
										+'	</div>'
										+'	<div class="layui-col-xs9">'
										+'		<p><a href="${basePath}/Productinfo/'+v.product.pid+'" target="_blank">'+v.product.pname+'</a></p>'
										<c:if test="${empty user }">+'		<span class="picard">登录查看</span>'</c:if>
										<c:if test="${not empty user }">+'		<span class="picard">'+v.price+'元 X'+v.count+'件 小计:￥'+v.subtotal+'元</span>'</c:if>
										+'	</div>'
										+'	<div class="layui-col-xs2 pdelete" data-id="'+v.product.pid+'">'
										+'		<i class="layui-icon" style="font-size: 40px; color: #1E9FFF;line-height: 60px">&#xe640;</i>  '
										+'	</div>'
										+'</li>'
							})
						}
						$("#carthtml").html(html);
						$("#total").html(d.carts.total.toFixed(2))
						html='';
						
					})
				}
			})
		</script>
</body>
</html>
