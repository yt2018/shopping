package com.yitong.Estshopping.dao.impl;


import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.AdminDao;
import com.yitong.Estshopping.entity.Adminuser;
@Repository("adminDao")
public class AdminDaoImlp extends BaseDaoImpl<Adminuser> implements AdminDao {


	@Override
	public Adminuser adminRole(int uid) {
		// TODO Auto-generated method stub
		System.out.println(uid+"---"+this.getClass());
		String hql="from Adminuser a where a.user.uid = ?";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(0, uid);
		return (Adminuser) query.uniqueResult();
	}

}
