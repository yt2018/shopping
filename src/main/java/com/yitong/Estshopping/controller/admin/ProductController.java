package com.yitong.Estshopping.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.Categorys;
import com.yitong.Estshopping.entity.Image;
import com.yitong.Estshopping.entity.Product;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.ProductService;

/**
 * 商城首页
 * @author YT
 *
 */
 
@Controller
@RequestMapping("/admin")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	@Autowired
	private ImageService imageService;
	/**
	 * 分页显示商品
	 * @return
	 */
	@RequestMapping(value="/listProduct/1")
	public String listProduct(Model model){
		model.addAttribute("url","getProductlist");
		return "admin/product/list";
	}
	@RequestMapping(value="/listProductoff/1")
	public String listProductoff(Model model){
		model.addAttribute("url","getProductlist/off");
		return "admin/product/list";
	}
	@RequestMapping(value="/listProducton/1")
	public String listProductno(Model model){
		model.addAttribute("url","getProductlist/on");
		return "admin/product/list";
	}
	/**
	 * 添加页面
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/addProduct")
	public String addProduct(Map<String, Object> map){
		//查询一级菜单
		List<Categorys> findAll = categorysService.findAll();
		map.put("Categorys", findAll);
		return "admin/product/add";
	}
	
	
	/**
	 * 查询全部商品
	 * 查询商品列表
	 */
	@RequestMapping(value = "/getProductlist")
	public @ResponseBody Map<String,Object> getProductlist(HttpSession session,@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit) {
		Map<String,Object> map = new HashMap<String,Object>();
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		//分页查询
		//List<Product> productList = productService.getProductList(page,limit);
		List<Product> productList = productService.getProductList(page, limit);
		map.put("code", 0);
		map.put("msg", "加载成功");
		//总数量
		map.put("count", productService.countProduct());
		map.put("data", productList);
		return map;
	}
	/**
	 * 查询商品被停止的商品
	 * 查询商品列表
	 */
	@RequestMapping(value = "/getProductlist/{isState}")
	public @ResponseBody Map<String,Object> getProductlistno(HttpSession session,@PathVariable String isState,@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit) {
		Map<String,Object> map = new HashMap<String,Object>();
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		//分页查询
		List<Product> productList = productService.getProductListno(page, limit, isState);
		
		map.put("code", 0);
		map.put("msg", "加载成功");
		//总数量
		map.put("count", productService.countProductno(isState));
		//System.out.println(productList);
		map.put("data", productList);
		return map;
	}
	/**
	 * 停止的商品
	 * @param session
	 * @param is
	 * @param pid
	 * @return
	 */
	@RequestMapping(value = "/updateIsState")
	public @ResponseBody Map<String,Object> updateIsState(HttpSession session,@RequestParam String is,@RequestParam Integer pid) {
		Map<String,Object> map = new HashMap<String,Object>();
		System.out.println("----------"+is);
		System.out.println("----------"+pid);
		productService.updateProductIsState(is,pid);
		map.put("code", true);
		map.put("msg", "加载成功");
		return map;
	}
	/**
	 * 不是热门商品
	 * @param session
	 * @param is
	 * @param pid
	 * @return
	 */
	@RequestMapping(value = "/updateIsHot")
	public @ResponseBody Map<String,Object> updateIsHot(HttpSession session,@RequestParam String is,@RequestParam Integer pid) {
		Map<String,Object> map = new HashMap<String,Object>();
		System.out.println("----------"+is);
		System.out.println("----------"+pid);
		productService.updateProductIsHot(is,pid);
		map.put("code", true);
		map.put("msg", "加载成功");
		return map;
	}
	@RequestMapping(value = "/delProduct")
	public @ResponseBody Map<String,Object> delProduct(@RequestParam Integer pid){
		Map<String,Object> map = new HashMap<String,Object>();
		Product product = new Product();
		product.setPid(pid);
		/*List<Image> images = imageService.getPid(pid);
		if(images.size()>0) {
			for (Image image : images) {
				imageService.delete(image);
			}
		}*/
		productService.deleteProduct(product);
		map.put("code", true);
		map.put("msg", "删除成功");
		return map;
	}
	@RequestMapping(value = "/delProducts",method = RequestMethod.POST)
	public @ResponseBody Map<String,Object> delProducts(@RequestParam String pids){
		Map<String,Object> map = new HashMap<String,Object>();
		String array[]=new String[pids.length()];
		pids = pids.replace("[", "");
		pids = pids.replace("]", "");
		pids = pids.replace("\"", "");
		String split[] = pids.split(",");
		if(pids==null&&pids=="") {
			map.put("code", false);
			map.put("msg", "删除失败");
			return map;
		}
		for (String spid : split) {
			
			Product product = new Product();
			int pid = Integer.parseInt(spid);
			product.setPid(pid);
			/*List<Image> images = imageService.getPid(pid);
			if(images.size()>0) {
				for (Image image : images) {
					imageService.delete(image);
				}
			}*/
			productService.deleteProduct(product);
		}
		map.put("code", true);
		map.put("msg", "删除成功");
		return map;
	}
}
