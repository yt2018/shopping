<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<title>修改商品</title>
	<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
	<style type="text/css">
		.imageedit{
			display: inline-block;
			position: relative;
		}
		.imageedit a{
			display:inline-block;
			width: 20px;
			height:20px;
			position:absolute;
			line-height:20px;
			text-align:center;
			top:0;right: 0;
			background: red;
			color: #fee
		}
	</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",4); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
			<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
				<form class="layui-form">
					<div class="layui-form-item layui-hide">
						<label class="layui-form-label">商品ID：</label>
						<div class="layui-input-block">
							<input type="hidden" name="pid" value="${product.pid}" >
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">商品名称：</label>
						<div class="layui-input-block">
							<input type="text" name="pname" lay-verify="required" placeholder="请输入" value="${product.pname }" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">商品简述：</label>
						<div class="layui-input-block">
							<input type="text" name="pnamedesc" lay-verify="required" placeholder="请输入商品简述"  value="${product.pnamedesc }"  autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">市场价格：</label>
						<div class="layui-input-block">
							<input type="number" name="marketPrice" lay-verify="number" placeholder="￥" value="${product.marketPrice }" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">商品价格：</label>
						<div class="layui-input-block">
							<input type="number" name="shopPrice" lay-verify="number" placeholder="￥" value="${product.shopPrice }" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">库存量：</label>
						<div class="layui-input-block">
							<input type="number" name="inventory" lay-verify="number" placeholder="库存量" value="${product.inventory }" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">上传图片</label>
						<div class="layui-upload layui-input-block"> 
							<button type="button" class="layui-btn" id="test2">上传商品图片</button>
							<blockquote class="layui-elem-quote layui-quote-nm" style="margin-top: 10px;">
								预览图：
								<div class="layui-upload-list" id="demo2">
									<c:forEach var="i" items="${product.image }" >
										<div class="imageedit">
											<img width="100" alt="${i.title }" src="${i.src}" class="layui-upload-img" data-d='${i }'>
											<a href="javascript:;" data-imgid="${i.imgid }" class="imageeditbtna ">X</a>
										</div>
									</c:forEach>
								</div>
							</blockquote>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">商品分类</label>
						<div class="layui-input-inline">
							<select name="cid" lay-filter="select1">
								<option value="${product.categorySecond.category.cid }">${product.categorySecond.category.cname }</option>
								<c:forEach var="c" items="${Categorys}">
								<option value="${c.cid}">${c.cname }</option>
								</c:forEach>
							</select>
						</div>
						<div class="layui-input-inline">
							<select name="csid" id="category2" lay-filter="select2">
								<option value="${product.categorySecond.csId}">${product.categorySecond.csName}</option>
							</select>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">商品描述：</label>
						<div class="layui-input-block">
							<p>提示:表格目前不支持直接插入,请在Excel编辑好复制粘贴进来</p>
							<textarea id="demo" style="display: none;">${product.pdesc}</textarea>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">商品参数：</label>
						<div class="layui-input-block">
							<p>提示:表格目前不支持直接插入,请在Excel编辑好复制粘贴进来</p>
							<textarea id="parameter" style="display: none;" placeholder="">${product.parameter}</textarea>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn" type="button" lay-submit lay-filter="*">立即提交</button>
						</div>
					</div>
				</form>
			</div>
	</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript">
		layui.use(['form', 'layer', 'upload', 'jquery', 'element', 'laypage', 'layedit'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery,
				layedit = layui.layedit,
				laydate = layui.laydate,
				upload = layui.upload;
			var element = layui.element;
			var layedit = layui.layedit;
			layedit.set({
				uploadImage: {
					url:  path+'/cloud/upload/watermark?fsrc=Image&uname=${user.name}&uid=${user.uid}&title=' //上传接口
					,type: 'post' //默认post
				}
			});
			var edit= layedit.build('demo',{
				tool: [
					'strong' //加粗
					, 'italic' //斜体
					, 'underline' //下划线
					, 'del' //删除线
					, '|'
					, 'left' //左对齐
					, 'center' //居中对齐
					, 'right' //右对齐
					, '|'
					, 'link' //超链接
					, 'unlink' //清除链接
					, '|', 'face' //表情
					, 'image' //插入图片
					//, 'code' //代码
				]
			}); //建立编辑器
			var parameter= layedit.build('parameter',{
				tool: [
					'strong' //加粗
					, 'italic' //斜体
					, 'underline' //下划线
					, 'del' //删除线
					, '|'
					, 'left' //左对齐
					, 'center' //居中对齐
					, 'right' //右对齐
					, '|'
					, 'image' //插入图片
				]
			}); //建立编辑器
			//多图片上传
			var fileobj;
			upload.render({
				elem: '#test2',
				url:  path+'/cloud/upload/watermark?fsrc=Image&uname=${user.name}&uid=${user.uid}&title=' //上传接口
				,multiple: true,
				size:2050,
				number:5,
				before: function(obj) {
					layer.load()
					//console.info(obj)
					/* obj.preview(function(index, file, result) {
						$('#demo2').append('<img src="' + result + '" alt="' + file.name + '" class="layui-upload-img">')
					}); */
				},
				done: function(obj) {
					//上传完毕
					layer.closeAll('loading')
					console.info(obj)
					if(obj.code==2){
						layer.msg(obj.msg)
					}else{
						layer.msg(obj.msg)
						
						$('#demo2').append('<div class="imageedit">'
						+'<img width="100" alt="' + obj.data.title + '" src="' + obj.data.src + '" data-d=\''+ JSON.stringify(obj.data) +'\' class="layui-upload-img">'
						+'<a href="javascript:;" data-imgid="" class="imageeditbtna ">X</a>'
						+'</div>')
						
						fileobj=obj;
					}
					//$.get("${basePath}/uploadsuccess")
				}
			});
			//监听指定开关
			form.on('switch(switchTest)', function(data) {
				layer.msg('是否热门：' + (this.checked ? 'true' : 'false'), {
					offset: '6px'
				});
			});
			form.on('select(select1)', function(data) {
				console.info(data);
				var html
				$.get("${basePath}/listcategory2/"+data.value,function(d){
					console.info(d)
					for(var i in d){
						console.info(d[i].csName)
						html+='<option value="'+d[i].csId+'">'+d[i].csName+'</option>'
					}
					$('#category2').html(html);
					form.render('select')
				})

			});
			form.on('select(select2)', function(data) {
				console.info(data);
				layer.msg('二级菜单：' + data.value, {
					offset: '6px'
				});
			});
			$(document).on("click",".imageeditbtna",function(){
				$(this).parent('.imageedit').remove();
				var src=$(this).siblings("img").attr("src");
				$.get(path+"/userinfo/removeimg?src="+src)
				layer.msg("移除成功")
			})
			//监听提交
			form.on('submit(*)', function(data) {
				if(!$("#demo2").children().length){
					layer.msg("请上传图片")
					return false;
				} 
				if(data.field.csid==0){
					layer.msg("请选择商品分类")
					return false;
				}
				//获取图片地址
				var imgsrcs=new Array();
				$("#demo2 .imageedit").each(function(i){
					imgsrcs.push(JSON.stringify($(this).find("img").data("d")));
				})
				console.info(imgsrcs);
				//获取编辑器的值
				var str=data.field;
				var pdesc=layedit.getContent(edit)
				var parameters=layedit.getContent(parameter)
				str.imgsrcs=imgsrcs.toString();
				str.pdesc=pdesc;
				str.parameter=parameters;
				console.info(JSON.stringify(str))
				console.info('编辑器---'+layedit.getContent(edit))
				console.info(str.csid)
				$.ajax({
					type:"post",
					contentType : "application/json ; charset=utf-8",
					url:"${basePath}/userinfo/addProductlist/"+str.csid,
					data:JSON.stringify(str),
					success:function(data){
						if(data.flag){
							layer.msg(data.rest)
						}else{
							layer.msg("添加失败")
						}
					}
					
				})
				console.info(JSON.stringify(str));
				return false;
			});
		})
	</script>
</body>

</html>