package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.Orderlog;
import com.yitong.Estshopping.entity.User;

/**
 * 订单的Dao接口
 * @author YT
 *
 */
public interface OrderLogDao extends BaseDao<Orderlog> {

	/**
	 * 查询记录列表
	 * @param user
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Orderlog> listorder(User user, Integer page, Integer limit);

	public Integer listorder(User user);
	
}
