package com.yitong.Estshopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "banner")
public class Banner {
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name ="bid")
	private Integer bid;
	/**
	 * 图片地址
	 */
	@Column(name ="bimg_src")
	private String bimgSrc;
	/**
	 * 图片描述
	 */
	@Column(name ="bimg_alt")
	private String bimgAlt;
	/**
	 * 图片主色
	 */
	@Column(name ="bimg_color")
	private String bimgColor;
	/**
	 * 图片排序
	 */
	@Column(name ="bimg_num")
	private Integer bimgNum;
	/**
	 * 图片状态
	 */
	@Column(name ="bimg_states")
	private Integer bimgStates;
	/**
	 * 图片宽度
	 */
	@Column(name ="bimg_width")
	private Integer bimgWidth;
	/**
	 * 图片高度
	 */
	@Column(name ="bimg_height")
	private Integer bimgHeight;
	/**
	 * 合作天数
	 */
	@Column(name ="datenum")
	private Integer datenum;
	/**
	 * 类型
	 */
	@Column(name ="bimg_class")
	private Integer bimgClass;
	/**
	 * 图片状态
	 */
	
	@Column(name ="bimg_date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date bimgDate;
	/**
	 * 图片链接
	 */
	@Column(name ="href")
	private String href;
	/**
	 * 上传者
	 */
	@JoinColumn(name = "uid")
	@ManyToOne
	@JsonIgnore
	public User user;
	public Integer getBid() {
		return bid;
	}
	public void setBid(Integer bid) {
		this.bid = bid;
	}
	public String getBimgSrc() {
		return bimgSrc;
	}
	public void setBimgSrc(String bimgSrc) {
		this.bimgSrc = bimgSrc;
	}
	public String getBimgAlt() {
		return bimgAlt;
	}
	public void setBimgAlt(String bimgAlt) {
		this.bimgAlt = bimgAlt;
	}
	public Integer getBimgNum() {
		return bimgNum;
	}
	public void setBimgNum(Integer bimgNum) {
		this.bimgNum = bimgNum;
	}
	public Integer getBimgStates() {
		return bimgStates;
	}
	public void setBimgStates(Integer bimgStates) {
		this.bimgStates = bimgStates;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public Date getBimgDate() {
		return bimgDate;
	}
	public void setBimgDate(Date bimgDate) {
		this.bimgDate = bimgDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getBimgColor() {
		return bimgColor;
	}
	public void setBimgColor(String bimgColor) {
		this.bimgColor = bimgColor;
	}
	public Integer getBimgWidth() {
		return bimgWidth;
	}
	public void setBimgWidth(Integer bimgWidth) {
		this.bimgWidth = bimgWidth;
	}
	public Integer getBimgClass() {
		return bimgClass;
	}
	public void setBimgClass(Integer bimgClass) {
		this.bimgClass = bimgClass;
	}
	public Integer getBimgHeight() {
		return bimgHeight;
	}
	public void setBimgHeight(Integer bimgHeight) {
		this.bimgHeight = bimgHeight;
	}
	public Integer getDatenum() {
		return datenum;
	}
	public void setDatenum(Integer datenum) {
		this.datenum = datenum;
	}
	
}
