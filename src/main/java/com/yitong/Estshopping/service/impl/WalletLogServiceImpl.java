package com.yitong.Estshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.yitong.Estshopping.dao.WalletDao;
import com.yitong.Estshopping.dao.WalletLogDao;
import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.entity.Wallet;
import com.yitong.Estshopping.entity.Walletlog;
import com.yitong.Estshopping.service.WalletLogService;

/**
 * 订单的Dao接口
 * @author YT
 *
 */
@Service("walletlogservice")
@Transactional
public class WalletLogServiceImpl implements WalletLogService{
	
	@Autowired
	private WalletLogDao walletLogDao;
	@Autowired
	private WalletDao walletDao;

	@Override
	public void seve(Walletlog walletlog) {
		// TODO Auto-generated method stub
		User user = walletlog.getUser();
		Float e = walletlog.getMoney();
		Float money = usercount(user);
		Integer wid = user.getWallet().getWid();
		Wallet wallet =new Wallet(wid, money-e);
		walletDao.update(wallet,wid);
		walletLogDao.save(walletlog);
	}
	@Override
	public void sevequxiao(Walletlog walletlog) {
		// TODO Auto-generated method stub
		User user = walletlog.getUser();
		Float e = walletlog.getMoney();
		Order order= JSON.parseObject(walletlog.getContent(), Order.class);
		Float money = usercount(user);
		Wallet wallet1 = user.getWallet();
		Integer wid;
		if(wallet1==null) {
			Wallet wallet2 = new Wallet(null, 0f, user);
			walletDao.save(wallet2);
		}
		wid = wallet1.getWid();
		
		Wallet wallet =new Wallet(order.getCuser().getWallet().getWid(), money+e, user);
		walletDao.update(wallet);
		//保存客户日志
		walletlog.setUser(order.getCuser());
		walletLogDao.save(walletlog);
	}
	@Override
	public void seveShouhuo(Walletlog walletlog) {
		// TODO Auto-generated method stub
		//商家
		User user = walletlog.getUser();
		//金额
		Float e = walletlog.getMoney();
		//订单对象
		Order order= JSON.parseObject(walletlog.getContent(), Order.class);
		//查询金额
		Float money = usercount(user);
		//查询
		Integer wid = user.getWallet().getWid();
		//
		Wallet wallet =new Wallet(wid, money+e);
		System.out.println(wallet);
		walletLogDao.save(walletlog);
		walletDao.update(wallet, wid);
	}

	@Override
	public List<Walletlog> listorder(User user, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return walletLogDao.listorder(user, page, limit);
	}

	@Override
	public Integer listorder(User user) {
		// TODO Auto-generated method stub
		return walletLogDao.listorder(user);
	}

	@Override
	public Float usercount(User user) {
		// TODO Auto-generated method stub
		return walletDao.usercount(user);
	}
	@Override
	public Float usercount(User user,Integer states) {
		// TODO Auto-generated method stub
		return walletLogDao.usercount(user,states);
	}
	


}
