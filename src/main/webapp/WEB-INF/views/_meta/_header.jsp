<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class='description'>易斯顿校园二手电商网站，易通通实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。</div>
		<header>
			<!--导航-->
			<div class="nav">
				<div class="navbox">
					<div class="lf fl">
						<c:if test="${sessionScope.user == null }">
							<a href="${basePath}" class="pr12"><i class="iconfont">&#xf00a0;</i>&nbsp;网站首页</a>
							<span class="pr12">喵，欢迎来EST商城</span>
							<a href="${basePath}/userLogin" class="pr12">请登录</a>
							<a href="${basePath}/userRegister">免费注册</a>
						</c:if>
						<c:if test="${sessionScope.user != null }">
							<a href="${basePath}" class="pr12"><i class="iconfont">&#xf00a0;</i>&nbsp;网站首页</a>
							<span class="pr12">喵，欢迎<b>${user.name }</b>来EST商城</span>
							<c:if test="${sessionScope.role != null }">
								<span style="color: red">${role.rolename }</span>
								<a href="${basePath}/admin">后台管理</a>
							</c:if>
							<a href="${basePath}/quit">退出</a>
						</c:if>
					</div>
					<ul class="lr fr">
						<li>
							<a href="${basePath}/userinfo?${sessionScope.user.uid}"><i class="iconfont">&#x3433;</i>个人中心<i class="top"></i></a>
							<div class="bx">
								<a href="${basePath}/userinfo/order">已买到的宝贝</a>
								<a href="${basePath}/userinfo/sell">已卖出的宝贝</a>
							</div>
						</li>
						<li>
							<a href="#"><i class="iconfont">&#xf0052;</i>我关注的品牌</a>
						</li>
						<li>
							<%-- <a href="${basePath}/myCart"><i class="iconfont">&#xf003f;</i>我的购物车</a> --%>
						</li>
						<li>
							<a href="#"><i class="iconfont">&#x3438;</i>收藏夹</a>
						</li>
					</ul>
				</div>
			</div>
			<!--logo区域和搜索框-->
			<div class="logobox">
				<h1 class="logo fl">
						<a href="${basePath}"><img src="${basePath}/res/images/logo1.png" width="320"/></a>
					</h1>
				<div class="search fl">
					<p><input type="text" placeholder="请输入关键字..." id="gover_search_key" value="${key }"/>
						<a href="#" class="s_a s_btn">搜&nbsp;&nbsp;索</a>
					</p>
					<div class="search_suggest" id="gov_search_suggest">
		                <ul>
		                </ul>
		            </div>
				</div>
				<div class="img fl">
					<!-- <a href="#"><img src="http://img.alicdn.com/tps/i4/TB1vFvoIpXXXXXBXpXX01gBIXXX-190-80.jpg" /></a> -->
				</div>
			</div>
			<div class="shopping-nav">
				<ul>
					<li class="oneall"><a href="${basePath}">全部商品</a></li>
					<c:forEach var="c" items="${categorys}" begin="4" end="13">
					<c:if test="${empty c.href }">
					<li><a href='${basePath}/classify/${c.cid }'>${c.cname }</a></li>
					</c:if>
					<c:if test="${not empty c.href }">
					<li><a href='${c.href }'>${c.cname }</a></li>
					</c:if>
					</c:forEach>
				</ul>
			</div>
		</header>
		<script src="${basePath}/res/js/search.js"></script>