<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
		<style type="text/css">
			footer{
				background: #101;
				border-top: 2px solid red;
				clear: both;
				margin-top:30px;
				text-align: center;
			}
			footer a{
				color: #fff;
				margin: 10px;
			}
			.footer{
				margin: 0 auto;
				color: #fff;
			}
		</style>
		<footer>
			<div class="footer">
				<div style="margin: 0 auto;">
					<a href="${basePath}">首页</a>
					<a href="${basePath}/zanzhu">众筹项目</a>
					<a href="${basePath}/timeline">时光轴</a>
					<a href="${basePath}/resume">关于我</a>
					<a href="http://wpa.qq.com/msgrd?v=3&uin=1810258114&site=qq&menu=yes">qq联系我</a>
				</div>
				<div style="margin: 0 auto;">
					友情链接：
					<a href="http://www.tanzhouedu.com/">潭州教育</a>
					<a href="http://www.estedu.com/">易斯顿美术学院</a>
				</div>
				<div style="margin: 0 auto;border-bottom: 1px solid #345;">
					合作伙伴：
					<a href="http://www.layui.com/">layui</a>
					<a href="https://connect.qq.com">QQ互联</a>
					<a href="https://www.alipay.com/">支付宝</a>
				</div>
				<p><span>Copyright</span><span>&copy;</span><span>2018</span>
					<a href="${basePath}">易通通</a><span>Design By tong118</span></p>
				<p>
					<a href="http://www.miibeian.gov.cn/" target="_blank">陇ICP备16001964号-1</a>
				</p>
			</div>
		</footer>
		