layui.use(['layer','jquery','element','flow','laypage'],function(){
				var $ =layui.jquery,
						layer = parent.layer === undefined ? layui.layer : parent.layer,
						laypage = layui.laypage,
						$ = layui.jquery,
						element = layui.element;
				
				var productimg =$(".layui-row .p_imgone");
				var productli =$(".layui-row li");
				var imgArr = []; //用来存放图片宽高比属性
 				init()
				function init(){
					var w =($(".layui-row li").width())*1-10;
					productli.css({
						height:w*1.6
					})
					productimg.each(function(i,dom) {
						//获取图片的宽度
						var imgWidth = $(this).attr("width")*1;
						//获取图片的高度
						var imgHeight = $(this).attr("height")*1;
						if(imgWidth >= imgHeight) {
							$(this).css({
								width: w,
								height: w / imgWidth * imgHeight,
								top: (w - w / imgWidth * imgHeight) / 2
							});
							
			 			} else {
							$(this).css({
								width: w / imgHeight * imgWidth,
								height: w,
								left: (w - (w / imgHeight * imgWidth)) / 2+18
							});
						}
					});
				}
				$(window).resize(function(){
					init()
				})
				var flow = layui.flow;
				  flow.lazyimg({
				    elem: 'body img'
				    //,scrollElem: '#LAY_demo3' //一般不用设置，此处只是演示需要。
				  });
				//当前页
				laypage.render({ 
					 elem: 'pagedemo'
					,curr: page//选中当前页 
					,limit: limit
					,limits:[10, 20, 30, 40, 50]
  					,count: count //数据总数，从服务端得到 
  					,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
					,jump: function(obj,first){
					    //obj包含了当前分页的所有参数，比如：
					    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
					    console.log(obj.limit); //得到每页显示的条数
					    if(page!=obj.curr){
					    	window.location.href="?page="+obj.curr+"&limit="+obj.limit;
						}
					}
				})
				$(".productdel").on("click",function(){
					var id=$(this).data("dateleid")
					layer.confirm('您确定要删除该商品吗?', {icon: 3, title:'温馨提示'}, function(index){
						$.post(path+"/userinfo/pdelete/"+id,function(d){
							layer.msg(d.msg)
							window.location.reload();
						})
					})
				})
				
			})