package com.yitong.Estshopping.controller;




import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.Orderlog;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.OrderLogService;
import com.yitong.Estshopping.service.OrderService;
import com.yitong.Estshopping.service.ProductService;
import com.yitong.Estshopping.service.UserService;
import com.yitong.framework.Orderlogtext;

/**
 * 订单管理 商家订单
 * @author YT
 *
 */

@Controller
public class SellController {
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderLogService orderlogservice;
	
	
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	@Autowired
	private ImageService imageService;
	/**
	 * 查询被生成的订单
	 */
	/**
	 * 查询订单
	 * @param session
	 * @return
	 */
	@RequestMapping("/userinfo/sell")
    public String sell(Model model, HttpSession session,@RequestParam(required=false) Integer state,@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit) {
		User user = (User) session.getAttribute("user");
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=12;
		}
		if(state==null) {
			state=1;
		}
		/**
		 * 查询订单
		 */
		List<Order> orderlist= orderService.listsell(user,state,page,limit);
		//查询订单数量
		Integer count1=orderService.countsell(user,1);
		Integer count2=orderService.countsell(user,2);
		Integer count3=orderService.countsell(user,3);
		Integer count4=orderService.countsell(user,4);
		Integer count10=orderService.countsell(user,10);
		session.setAttribute("orderlist", orderlist);
		session.setAttribute("page", page);
		session.setAttribute("limit", limit);
		session.setAttribute("state", state);
		session.setAttribute("count1", count1);
		session.setAttribute("count2", count2);
		session.setAttribute("count3", count3);
		session.setAttribute("count4", count10);
		
		return "user/sell";
		
	}
	/**
	 * 商家修改订单
	 * @param session
	 * @param oif
	 * @param e
	 * @return
	 */
	@RequestMapping(value= "/userinfo/editsell/{oif}",method = RequestMethod.POST)
	public  @ResponseBody Map<String,Object>  editsell(HttpSession session,@PathVariable("oif") String oif,Integer num,Float money){
		Map<String,Object> map = new HashMap<String,Object>();
		User user = (User) session.getAttribute("user");
		/**
		 * 根据订单号查询
		 */
		if(num<1||num==null||money<0) {
			map.put("code", 0);
			map.put("msg", "修改失败,数量不小于1,金额不小于0");
			return map;
		}
		
		Order order=orderService.findOif(oif);
		order.setNum(num);
		order.setTotal(money);
		boolean update = orderService.update(order);
		map.put("code",1);
		map.put("msg", "修改成功");
		//日志记录
		Orderlog textlog = Orderlogtext.textlog(user, order.getOif(), order,9);
		orderlogservice.seve(textlog );
		return map;
		
	}
	/**
	 * 商家发货
	 * @param session
	 * @param oif
	 * @param e
	 * @return
	 */
	@RequestMapping(value= "/userinfo/kuaidi/{oif}",method = RequestMethod.POST)
	public  @ResponseBody Map<String,Object>  kuaidi(HttpSession session,@PathVariable("oif") String oif,String kuaidi){
		Map<String,Object> map = new HashMap<String,Object>();
		User user = (User) session.getAttribute("user");
		/**
		 * 根据订单号查询
		 */
		
		Order order=orderService.findOif(oif);
		System.out.println(kuaidi);
		System.out.println(order);
		if(kuaidi==null)kuaidi="当面交易";
		order.setKuaidi(kuaidi);
		order.setState(3);
		boolean update = orderService.update(order);
		//日志记录
		order=orderService.findOif(oif);
		Orderlog textlog = Orderlogtext.textlog(user, oif, order,3);
		System.out.println("商家发货日志"+textlog.toString());
		orderlogservice.seve(textlog );
		map.put("code",1);
		map.put("msg", "修改成功");
		return map;
	}
}
