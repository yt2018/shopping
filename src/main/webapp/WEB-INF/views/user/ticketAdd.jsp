<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>优惠券</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",9); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
				<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
					<form class="layui-form layui-form-pane">
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">优惠券名称</label>
								<div class="layui-input-inline">
									<input type="tel" name="phone" lay-verify="required|phone" autocomplete="off" class="layui-input">
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">优惠券金额</label>
								<div class="layui-input-inline">
									<input type="text" name="email" lay-verify="email" autocomplete="off" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">优惠券数量</label>
								<div class="layui-input-inline">
									<input type="text" name="email" lay-verify="email" autocomplete="off" class="layui-input">
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">优惠券约束</label>
								<div class="layui-input-inline">
									<input type="text" name="email" lay-verify="email" autocomplete="off" class="layui-input">
								</div>
							</div>
						</div>
						
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">起止时间</label>
								<div class="layui-input-inline">
									<input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">结束时间</label>
								<div class="layui-input-inline">
									<input type="tel" name="date" id="date1"lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<div class="layui-input-inline">
									<a href="javascript:layer.msg('正在开发中......')" class="layui-btn">确定</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
</body>
</html>