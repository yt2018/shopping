package com.yitong.Estshopping.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.PayInfoDao;
import com.yitong.Estshopping.entity.PayEntity;
import com.yitong.Estshopping.service.PayInfoService;

@Service("payInfoService")
@Transactional
public class PayInfoServiceImpl implements PayInfoService {
	@Autowired
	private PayInfoDao payInfoDao;
	@Override
	public void save(PayEntity payEntity) {
		// TODO Auto-generated method stub
		payInfoDao.save(payEntity);
	}
	@Override
	public PayEntity getOut_trade_no(String out_trade_no) {
		// TODO Auto-generated method stub
		return payInfoDao.getOut_trade_no(out_trade_no);
	}
	@Override
	public void update(PayEntity payEntity) {
		// TODO Auto-generated method stub
		PayEntity out_trade_no = getOut_trade_no(payEntity.getOut_trade_no());
		payInfoDao.update(payEntity, out_trade_no.getPayid());
	}

}
