package com.yitong.Estshopping.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.OrderDao;
import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.User;

@Repository("orderDao")
public class OrderDaoImpl extends BaseDaoImpl<Order> implements OrderDao {

	@Override
	public List<Order> listorder(User user,Integer state, Integer page, Integer limit) {
		// TODO Auto-generated method stub  
		String hql="from Order o where o.cuser.uid='"+user.getUid()+"' and o.state='"+state+"' order by o.ordertime desc";
		return this.find(hql, page, limit);
	}

	@Override
	public Order findOif(String iof) {
		// TODO Auto-generated method stub
		String hql="from Order o where o.oif= :iof";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("iof", iof);
		return (Order) query.uniqueResult();
	
	}

	@Override
	public Integer count(User user, Integer state) {
		// TODO Auto-generated method stub
		String hql = "select count(*) from Order o where o.cuser.uid='"+user.getUid()+"' and o.state='"+state+"' order by o.ordertime desc";
		return this.count(hql);
	}

	@Override
	public List<Order> listsell(User user, Integer state, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		String hql="from Order o where o.user.uid='"+user.getUid()+"' and o.state='"+state+"' order by o.ordertime desc";
		return this.find(hql, page, limit);
	}

	@Override
	public Integer countsell(User user, Integer state) {
		String hql = "select count(*) from Order o where o.user.uid='"+user.getUid()+"' and o.state='"+state+"' order by o.ordertime desc";
		return this.count(hql);
	}

	

}
