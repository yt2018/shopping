package com.yitong.framework.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.yitong.Estshopping.entity.Banner;
import com.yitong.Estshopping.entity.Categorys;
import com.yitong.Estshopping.entity.Product;
import com.yitong.Estshopping.service.BannerService;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.ProductService;

public class IndexInterceptor implements HandlerInterceptor {
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private BannerService bannerService;
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String url = request.getRequestURI();
		String serverName = request.getServerName();
		System.out.println(serverName);
		String www = serverName.substring(0, 3);
		System.out.println(www.indexOf("www")!=-1);
		if(www.indexOf("www")!=-1) {
			response.sendRedirect("http://"+serverName.replaceAll("www.", "")+url);
		}
		System.out.println("请求地址=http://"+serverName+url);
		String urler = request.getHeader("Referer");//获取上个页面的url
		HttpSession session = request.getSession();
	   
			List<Categorys> categorys = categorysService.findAll();
			session.setAttribute("categorys", categorys);
			//查询最新商品
			List<Product> productszx=productService.getProductdate(1,10);
			List<Product> productszg=productService.getProductPnub(1,10);
			List<Product> productsrm=productService.getProductPremen(1,10);
			session.setAttribute("productszx", productszx);
			session.setAttribute("productszg", productszg);
			session.setAttribute("productsrm", productsrm);
			List<Banner> banners = bannerService.indexbannerlist(1);
			List<Banner> banners2 = bannerService.indexbannerlist(2);
	        session.setAttribute("ad_banner", banners);
	        session.setAttribute("ad_banner2", banners2);
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("报告---请求完毕");
	}

}
