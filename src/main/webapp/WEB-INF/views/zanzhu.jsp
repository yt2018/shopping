<%@page import="com.yitong.framework.utils.Dingdan"%>
<%@page import="java.util.UUID"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<title>我要支持他--易通通</title>
<meta name="keywords" content="我要支持他,众筹,易通通,全国在校大学生创业平台,大学生创业平台,EST商城,易斯顿美术学院" />
<meta name="description" content="我要支持他,众筹,
校园二手电商网站，实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
<meta http-equiv="Content-Type" content="text/html; Charset=gb2312">
<meta http-equiv="Content-Language" content="zh-CN">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="icon" rel="shortcut icon" type="image/x-icon" href="${basePath}/res/images/logo.png"/>
<title>我要支持他</title>
<!--Layui-->
		<link href="${basePath}/res/timeline/plug/layui/css/layui.css" rel="stylesheet" />
		<!--font-awesome-->
		<link href="${basePath}/res/timeline/plug/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
		<!-- animate.css -->
		<link href="${basePath}/res/timeline/css/animate.min.css" rel="stylesheet" />
		<!--全局样式表-->
		<link href="${basePath}/res/timeline/css/global.css" rel="stylesheet" />
		<!-- 本页样式表 -->
		<link href="${basePath}/res/timeline/css/timeline.css" rel="stylesheet" />
<body>
	<body>
		<!-- canvas -->
		<canvas id="canvas-banner"></canvas>
		<!-- 导航 -->
		<nav class="blog-nav layui-header">
			<div class="blog-container">
				<a href="${basePath}"><img src="${basePath}/res/images/logo1.png" height="50"/></a>
				<a href="${basePath}/timeline" class="layui-btn layui-btn-danger" style="float: right;margin-top: 12px">时光轴</a>
				<a href="${basePath}/resume" class="layui-btn layui-btn" style="float: right;margin-top: 12px">关于我</a>
			</div>
		</nav>
		<!-- 主体（一般只改变这里的内容） -->
		<style>
			.zanzhu{
				background: rgba(255, 255, 255, 0.62)
			}
		</style>
		<div class="blog-body">
			<div class="blog-container">
				<div class="zanzhu shadow" >
					<form class="layui-form layui-form-pane" style="max-width: 644px ;margin: 0 auto;">
						 <div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">众筹者</label>
								<div class="layui-input-inline">
									<% Dingdan dingdan=new Dingdan();
									String diof = dingdan.diof();request.setAttribute("out_trade_no", diof); %>
									<input type="hidden" name="out_trade_no" value="${out_trade_no }">
									<input type="text" name="uname" lay-verify="required" autocomplete="off" class="layui-input" value="${user.name }">
									<input type="hidden" name="subject" value="赞助">
									<input type="hidden" name="uid" value="${user.uid }">
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">众筹金额</label>
								<div class="layui-input-inline">
									<input type="number" name="total_amount" lay-verify="number" autocomplete="off" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-form-item layui-form-text">
							
					          <label class="layui-form-label">留言</label>
					          <div class="layui-input-block">
					            <textarea placeholder="给我一个留言,励志我的梦" class="layui-textarea" name="body"></textarea>
					          </div>
				        </div>
						<div class="layui-form-item">
				          	<a class="layui-btn hongbao" style="background: red"  href="javascript:;">支付宝红包领取</a>
				            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="alipay">支付宝众筹</button>
				        </div>
				        <style>
				        	.hongbaozhuanxiang{
				        		width: 200px;
				        		background: #fff;
				        		text-align: center;
				        		display: none
				        	}
				        	.erwema{
				        		width: 128px;
			    	    		height: 128px;
			    	    		margin: 10px auto;
			    	    		overflow: hidden;
				        	}
				        	.tishi{
				        		border-top: 1px dashed #e5e5e5; 
				        		height: 30px;
				        		line-height: 30px;
				        	}
				        </style>
				        <div class="hongbaozhuanxiang">
				        	<br/>
				        	<img alt="支付宝提示" src="${basePath}/res/images/alipay_hb_icon03.png"/>
				        	<br/>
				        	<div class="erwema">
								<div id="test"></div>
				        	</div>
				        	<div class="tishi">
				        		手机端支付宝扫一扫领取红包
				        	</div>
				        </div>
					</form>
				</div>
			</div>
		</div>
		<!-- 底部 -->
		<footer class="blog-footer">
			<p><span>Copyright</span><span>&copy;</span><span>2018</span>
				<a href="${basePath}">易通通</a><span>Design By tong118</span></p>
			<p>
				<a href="http://www.miibeian.gov.cn/" target="_blank">陇ICP备16001964号-1</a>
			</p>
		</footer>
		<!-- layui.js -->
		<script src="${basePath}/res/layui2/layui.js"></script>
		<!-- 全局脚本 -->
		<script src="${basePath}/res/timeline/js/global.js"></script>
		<!-- 本页脚本 -->
		<script type="text/javascript" src="http://static.runoob.com/assets/qrcode/qrcode.min.js"></script>
		<script type="text/javascript">
		layui.use(['jquery','form'], function() {
			var $ = layui.jquery;
			var form = layui.form;
		    //红包链接
		    var hongbao="https://render.alipay.com/p/f/fd-j6lzqrgm/guiderofmklvtvw.html?shareId=2088512167067317&campStr=p1j%2BdzkZl018zOczaHT4Z5CLdPVCgrEXq89JsWOx1gdt05SIDMPg3PTxZbdPw9dL&sign=3McZJVSVXxkCFzJvkdtRD%2FYAYTHv28AXWq4IZ4yuLig%3D&scene=offlinePaymentNewSns";
		    $(".hongbao").on("click",function(){
		    	//手机端检测QQ,微信 PC
			    if(isWeiXin()||isQQ()){
			    	window.location.href="${basePath}/pay.htm?goto="+encodeURIComponent(hongbao);
			    }else if(isPC()){
			    	var qrcode = new QRCode("test", {
			    	    text: hongbao,
			    	    width: 128,
			    	    height: 128,
			    	    colorDark : "#000000",
			    	    colorLight : "#ffffff",
			    	    correctLevel : QRCode.CorrectLevel.L
			    	});
			    	$(".hongbaozhuanxiang").show()
			    }else{
			    	window.location.href=hongbao;
			    }
		    })
		    form.on('submit(alipay)', function(data){
				  var t=data.field;
				  console.info(t);
				  var queryParam='';
				  var url="${basePath}/playzhifu" +"?";
				 //alert(isQQ())
				 if(isWeiXin()||isQQ()){
					  var bizStr = JSON.stringify(t);
						 //queryParam += 'bizcontent=' + encodeURIComponent(bizStr);
						 for( var attr in t ){
								queryParam+=attr+"="+t[attr]+"&"
						 } 
						 queryParam=queryParam.substr(0, queryParam.length - 1);
						 var dourl=url+queryParam;
						 //console.info("微信版"+dourl);
						 //console.info("微信版"+dourl);
						 var encodeurl=encodeURIComponent(dourl)
						 var decodeurl=decodeURIComponent(dourl)
						 //console.info('加密'+encodeurl);
						 //alert(encodeurl)
						 //console.info('解密'+decodeurl);
						 window.location.href="${basePath}/pay.htm?goto="+encodeurl;
						 //_AP.pay(dourl);
				} else{
						
						// alert(JSON.stringify(t))
							 $.ajax({
									type:"post",
									dataType:"html",
					                contentType: "application/json",
									url:"${basePath}/playzhifu",
									data:JSON.stringify(t),
									success:function(data){
										//window.location.href="${basePath}/playzhifu"
										//console.info(data)
										$("body").html(data)
										
									}
							})
					} 
				  //window.location.href="${basePath}/playzhifu?";
				return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
			});
			
		});
		/**
		* 判断是否在微信里打开
		*/
		function isWeiXin()
		{
			return "micromessenger" == navigator.userAgent.toLowerCase().match(/MicroMessenger/i)
		}

		/**
		* 判断是否在QQ里打开
		*/
		function isQQ()
		{
			return window.navigator.userAgent.indexOf("MQQBrowser/6.2 TBS")>-1
		}
		function isPC()
		{
			var sUserAgent = navigator.userAgent.toLowerCase();  
		    var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";  
		    var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";  
		    var bIsMidp = sUserAgent.match(/midp/i) == "midp";  
		    var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";  
		    var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";  
		    var bIsAndroid = sUserAgent.match(/android/i) == "android";  
		    var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";  
		    var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";  
		    //document.writeln("您的浏览设备为：");  
		    if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {  
		        return false;
		    }else{
		    	return true;
		    }
		}
		
		</script>
		<!--背景-->
		<script src="${basePath}/res/timeline/js/canvas.js"></script>
	</body>
</html>