package com.yitong.Estshopping.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.OrderLogDao;
import com.yitong.Estshopping.entity.Orderlog;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.OrderLogService;

/**
 * 订单的Dao接口
 * @author YT
 *
 */
@Service("orderlogservice")
@Transactional
public class OrderLogServiceImpl implements OrderLogService{
	
	@Autowired
	private OrderLogDao orderLogDao;

	@Override
	public void seve(Orderlog orderlog) {
		// TODO Auto-generated method stub
		Date today = new Date();
		orderlog.setLogtime(today);
		orderLogDao.save(orderlog);
	}

	@Override
	public List<Orderlog> listorder(User user, Integer page, Integer limit) {
		return orderLogDao.listorder(user,page,limit);
		// TODO Auto-generated method stub
	}

	@Override
	public Integer listorder(User user) {
		// TODO Auto-generated method stub
		
		return orderLogDao.listorder(user);
	}

}
