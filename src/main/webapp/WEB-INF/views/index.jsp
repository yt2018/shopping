<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>易通通|tong118.com</title>
	<meta name="keywords" content="易斯顿,大学生,tong118,易通通,全国在校大学生创业平台,大学生创业,大学生创业平台,校园二手电商网站,EST商城,易斯顿美术学院" />
	<meta name="description" content="易斯顿校园二手电商网站，易通通实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/login.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/banner.css" media="all" />
	<style type="text/css">
		.banner img,.items_warp img{background: url("${basePath}/res/css/img/logo-03.png") no-repeat center center;}
		.picon{width: 44px;height: 44px;}
		.items_warp .c_items .cr li .picon img{width: 100%;height:100%;border-radius: 50%}
		.layui-carousel img{width: 100%}
		.layui-carousel,.layui-carousel>[carousel-item]>*{background-color: transparent}
	</style>
</head>
<body>
<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<div class="banner">
			<div class="b_nav">
				<div class="b">
					<div class="b_all">
						<ul class="bb_all">
							<c:forEach var="c" items="${categorys}" begin="0" end="6">
								<li>
									<c:if test="${empty c.href }">
									<h3><a href='${basePath}/classify/${c.cid }'>${c.cname }</a></h3>
									</c:if>
									<c:if test="${not empty c.href }">
									<h3><a href='${c.href }'>${c.cname }</a></h3>
									</c:if>
									<span>›</span>
									<div>
										<c:forEach items="${c.categorySeconds }" var="cat" varStatus="s" begin="0" end="2">
											<c:if test="${empty cat.href }">
												<a href='${basePath}/classify2/${cat.csId }'>${cat.csName }</a>
											</c:if>
											<c:if test="${not empty cat.href }">
												<a href='${cat.href }'>${cat.csName }</a>
											</c:if>
										</c:forEach>
									</div>
									<div class="aaaa">
										<c:forEach items="${c.categorySeconds }" var="cat" varStatus="s">
											<c:if test="${empty cat.href }">
												<a href='${basePath}/classify2/${cat.csId }'>${cat.csName }</a>
											</c:if>
											<c:if test="${not empty cat.href }">
												<a href='${cat.href }'>${cat.csName }</a>
											</c:if>
										</c:forEach>
									</div>
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
			<div class="b_b">
				<div class="b_b_g">
					<div class="b">
						<div class="ig fl">
							<div class="layui-carousel" id="test1" lay-filter="test1">
								<div carousel-item>
									<c:forEach var="ad_b" items="${ad_banner }">
									<div><a target="_blank" href="${ad_b.href }"><img lay-src="${ad_b.bimgSrc}" alt="${ad_b.bimgAlt }" data-color="${ad_b.bimgColor }"></a></div>
									</c:forEach>
								</div>
							</div>
						</div>
						<div class="sao fr">
							<span class="sssa" >App下载</span>
							<span class="sssw" >微信公众号</span>
							<div class="f_sao" style="background-image: url('${basePath}/res/bannerimages/f3.png');">
								
							</div>
						</div>
					</div>
				</div>
				<div class="b_footer">
					<div class="b">
						<div class="img">
							<c:forEach var="ad_b" items="${ad_banner2 }" begin="0" end="2">
								<a target="_blank" href="${ad_b.href }">
									<img lay-src="${ad_b.bimgSrc}" alt="${ad_b.bimgAlt }" data-color="${ad_b.bimgColor }">
								</a>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="items_warp">
			<!--第三层-->
			<div class="mt25 c_items">
				<!--左边-->
				<div class="cl" >
					<a href="${basePath}/remen" target="_blank"><div class="ct" style="background: #d4af59">
						热门商品
					</div></a>
					<!-- <div class="cb">
						<a href="#"><img lay-src="http://img.alicdn.com/tps/i4/TB11nqWIpXXXXbwXVXXSYoGNVXX-400-422.jpg" width="200" height="333" alt="tianmao" />
					</div> -->
				</div>
				<!--右边-->
				<div class="cr">
					<ul class="layui-row ">
						<c:forEach var="p" items="${productsrm }" begin="0" end="9">
							<li class="layui-col-sm4 layui-col-md3 layui-col-lg2">
								<a href="${basePath}/Productinfo/${p.pid }">
									<div class="">
										<c:if test="${not empty p.image}">
											<img lay-src="${p.image[0].src }"/>
										</c:if>
										<c:if test="${empty p.image}">
											<img lay-src="${basePath}/res/images/timg.jpg"/>
										</c:if>
										<p style="color: green">${p.pname }</p>
										<div class="layui-row">
											<div class="layui-col-xs3">
												<div class="picon">
													<img src="${p.user.icon }"/>
												</div>
											</div>
											<div class="layui-col-xs9">
												<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
												<c:if test="${not empty user }">
												<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
												</c:if>
												<c:if test="${empty user }">
												<span style="color: red;font-size: 12px">现价: <b>登录即可查看优惠价</b></span>
												</c:if>
											</div>
										</div>
									</div>
								</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
			<!--第三层结束-->
			<!--第一层-->
			<div class="mt25 c_items">
				<!--左边-->
				<div class="cl" >
					<a href="${basePath}/zuixin" target="_blank"><div class="ct" style="background: #883866">
						最新商品
					</div></a>
					<!-- <div class="cb">
						<a href="#"><img lay-src="http://img.alicdn.com/tps/i4/TB11nqWIpXXXXbwXVXXSYoGNVXX-400-422.jpg" width="200" height="333" alt="tianmao" />
					</div> -->
				</div>
				<!--右边-->
				<div class="cr">
					<ul class="layui-row ">
						<c:forEach var="p" items="${productszx }" begin="0" end="9">
							<li class="layui-col-sm4 layui-col-md3 layui-col-lg2">
								<a href="${basePath}/Productinfo/${p.pid }">
									<div class="">
										<c:if test="${not empty p.image}">
											<img lay-src="${p.image[0].src }"/>
										</c:if>
										<c:if test="${empty p.image}">
											<img lay-src="${basePath}/res/images/timg.jpg"/>
										</c:if>
										<p style="color: green">${p.pname }</p>
										<div class="layui-row">
											<div class="layui-col-xs3">
												<div class="picon">
													<img src="${p.user.icon }"/>
												</div>
											</div>
											<div class="layui-col-xs9">
												<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
												<c:if test="${not empty user }">
												<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
												</c:if>
												<c:if test="${empty user }">
												<span style="color: red;font-size: 12px">现价: <b>登录即可查看优惠价</b></span>
												</c:if>
											</div>
										</div>
									</div>
								</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
			<!--第一层结束-->
			<!--第二层-->
			<div class="mt25 c_items">
				<!--左边-->
				<div class="cl" >
					<a href="${basePath}/zuigao" target="_blank"><div class="ct" style="background: #d4595e">
						大家都在看
					</div></a>
					<!-- <div class="cb">
						<a href="#"><img lay-src="http://img.alicdn.com/tps/i4/TB11nqWIpXXXXbwXVXXSYoGNVXX-400-422.jpg" width="200" height="333" alt="tianmao" />
					</div> -->
				</div>
				<!--右边-->
				<div class="cr">
					<ul class="layui-row ">
						<c:forEach var="p" items="${productszg }" begin="0" end="9">
							<li class="layui-col-sm4 layui-col-md3 layui-col-lg2">
								<a href="${basePath}/Productinfo/${p.pid }">
									<div class="">
										<c:if test="${not empty p.image}">
											<img lay-src="${p.image[0].src }"/>
										</c:if>
										<c:if test="${empty p.image}">
											<img lay-src="${basePath}/res/images/timg.jpg"/>
										</c:if>
										<p style="color: green">${p.pname }</p>
										<div class="layui-row">
											<div class="layui-col-xs3">
												<div class="picon">
													<img src="${p.user.icon }"/>
												</div>
											</div>
											<div class="layui-col-xs9">
												<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
												<c:if test="${not empty user }">
												<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
												</c:if>
												<c:if test="${empty user }">
												<span style="color: red;font-size: 12px">现价: <b>登录即可查看优惠价</b></span>
												</c:if>
											</div>
										</div>
									</div>
								</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
			<!--第二层结束-->
			
		</div>
		<script type="text/javascript"src="${basePath}/res/js/bannerjs.js"></script>
		
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
</body>
</html>