package com.yitong.Estshopping.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.AdminService;
import com.yitong.Estshopping.service.UserService;

/**
 * 网站管理员--用户
 * @author YT
 *
 */

@Controller
@RequestMapping(value="/admin/user")
public class AdminUserController {
	@Autowired
	private AdminService adminService;
	@Autowired
	private UserService userService;
	
	/**
	 * 用户首页
	 * @return
	 */
	@RequestMapping(value= {"/","/index"})
	public String showIndex(HttpSession session){
		session.setAttribute("url", "getuserlist");
		return "admin/user/list";
	}
	@RequestMapping(value= "/jinzhi")
	public String showIndex0(HttpSession session){
		session.setAttribute("url", "getuserlist/0");
		return "admin/user/list";
	}
	@RequestMapping(value= "/zhengchang")
	public String showIndex1(HttpSession session){
		session.setAttribute("url", "getuserlist/1");
		return "admin/user/list";
	}
	@RequestMapping(value= "/xianzhi")
	public String showIndex2(HttpSession session){
		session.setAttribute("url", "getuserlist/2");
		return "admin/user/list";
	}
	/**
	 * 查询所有用户
	 * @return
	 */
	@RequestMapping(value= "/getuserlist")
	public @ResponseBody Map<String,Object> getuserlist(HttpSession session, @RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit){
		Map<String,Object> map = new HashMap<String,Object>();
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		List<User> users=userService.userList(page,limit,null);
		map.put("code", 0);
		map.put("msg", "加载成功");
		//总数量
		map.put("count", userService.countUsers(null));
		//System.out.println(productList);
		map.put("data", users);
		return map;
	}
	@RequestMapping(value= "/getuserlist/{states}")
	public @ResponseBody Map<String,Object> getuserliststates(HttpSession session,@PathVariable("states") Integer states, @RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit){
		Map<String,Object> map = new HashMap<String,Object>();
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		List<User> users=userService.userList(page,limit,states);
		map.put("code", 0);
		map.put("msg", "加载成功");
		//总数量
		map.put("count", userService.countUsers(states));
		//System.out.println(productList);
		map.put("data", users);
		return map;
	}
}
