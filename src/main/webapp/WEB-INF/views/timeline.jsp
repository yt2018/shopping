<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; Charset=gb2312">
<meta http-equiv="Content-Language" content="zh-CN">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="icon" rel="shortcut icon" type="image/x-icon" href="${basePath}/res/images/logo.png"/>
<title>ESTshopping点点滴滴 - 时光轴</title>
<!--Layui-->
		<link href="${basePath}/res/timeline/plug/layui/css/layui.css" rel="stylesheet" />
		<!--font-awesome-->
		<link href="${basePath}/res/timeline/plug/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
		<!-- animate.css -->
		<link href="${basePath}/res/timeline/css/animate.min.css" rel="stylesheet" />
		<!--全局样式表-->
		<link href="${basePath}/res/timeline/css/global.css" rel="stylesheet" />
		<!-- 本页样式表 -->
		<link href="${basePath}/res/timeline/css/timeline.css" rel="stylesheet" />
<body>
	<body>
		<!-- canvas -->
		<canvas id="canvas-banner"></canvas>
		<!-- 导航 -->
		<nav class="blog-nav layui-header">
			<div class="blog-container">
				<a href="${basePath}"><img src="${basePath}/res/images/logo.svg" height="50"/></a>
				<a href="${basePath}/zanzhu" class="layui-btn layui-btn-danger" style="float: right;margin-top: 12px">我要捐助</a>
				<a href="${basePath}/resume" class="layui-btn layui-btn" style="float: right;margin-top: 12px">关于我</a>
			</div>
		</nav>
		<!-- 主体（一般只改变这里的内容） -->
		<div class="blog-body">
			<div class="blog-container">
				<div class="blog-main">
					<div class="child-nav shadow">
						<span class="child-nav-btn child-nav-btn-this">时光轴</span>
						<span class="child-nav-btn">赞助名单</span>
					</div>
					<div class="qiehuan">
						<div class="timeline-box shadow">
							<div class="timeline-main">
								<h1><i class="fa fa-clock-o"></i>时光轴<span> —— 记录生活点点滴滴</span></h1>
								<div class="timeline-line"></div>
								<div class="timeline-year">
									<h2><a class="yearToggle" href="javascript:;">2018年</a><i class="fa fa-caret-down fa-fw"></i></h2>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">5月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">5月31日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">修复了立即购买bug</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">5月21日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">完善个人主页功能</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">5月20日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">完善搜索功能</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">5月4日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">手机端页面发布</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">4月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">4月28日 07:52</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">商品的评论已做完，可以评论图片，追评也可以评论图片，表情</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">4月13日 07:52</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">服务器图片独立处理</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">4月4日 08:08</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">发起网上众筹</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">4月1日 20:35</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">申请支付宝,微信接口完成</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">1月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">01月5日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">毕业开题答辩</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">01月3日 20:35</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">出售听力耳机全部出售完,一共盈利1500元</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
								</div>
								<div class="timeline-year">
									<h2><a class="yearToggle" href="javascript:;">2017年</a><i class="fa fa-caret-down fa-fw"></i></h2>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">12月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">12月30日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">完成购物网站50%</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">12月26日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">接入QQ第三方一键登录网站</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">12月25日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">销售听力耳机150多个,盈利1200元</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">11月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">11月30日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">完成购物网站30%</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">11月11日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">启动出售6月份回收的英语听力耳机</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">10月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">10月6日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">开始构建网站</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">10月5日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">写《校园二手电商网站需求分析文档》完成</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">9月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">09月10日 20:35</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">分析网站需求</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">7月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">07月10日 20:35</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">构思网站平台设计</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="timeline-month">
										<h3 class=" animated fadeInLeft"><a class="monthToggle" href="javascript:;">6月</a><i class="fa fa-caret-down fa-fw"></i></h3>
										<ul>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">06月08日- 06月14日</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">看到毕业生的闲置物品弃如敝屣,校园环境犹如一片狼藉，造成环境的污染，资源的浪费。我的猜测应该毕业生没时间，或不好意思出售，此时我在想如果利用互联网资源做一个闲置物品商城品台</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">06月08日 24:00</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">截止06月08日 24:00回收闲置听力耳机200多个</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">06月02日 18:00</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">主动回收听力耳机</div>
												<div class="clear"></div>
											</li>
											<li class=" ">
												<div class="h4  animated fadeInLeft">
													<p class="date">06月01日18:00</p>
												</div>
												<p class="dot-circle animated "><i class="fa fa-dot-circle-o"></i></p>
												<div class="content animated fadeInRight">发现毕业生闲置物品销售摆设地摊</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
								</div>
								<h1 style="padding-top:4px;padding-bottom:2px;margin-top:40px;"><i class="fa fa-hourglass-end"></i>THE END</h1>
							</div>
						</div>
						<div class="zanzhu shadow" style="display: none">
							<table class="layui-table">
							  <thead>
							    <tr>
							      <th>赞助者</th>
							      <th>捐助方式</th>
							      <th>捐助金额</th>
							      <th>留言</th>
							    </tr> 
							  </thead>
							  <tbody>
							    <tr>
							      <td>贤心</td>
							      <td>支付宝</td>
							      <td>8元</td>
							      <td>人生就像是一场修行</td>
							    </tr>
							  </tbody>
							</table>
      
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 底部 -->
		<footer class="blog-footer">
			<p><span>Copyright</span><span>&copy;</span><span>2018</span>
				<a href="${basePath}">ESTshopping</a><span>Design By ESTshopping</span></p>
			<p>
				<a href="http://www.miibeian.gov.cn/" target="_blank">陇ICP备16001964号-1</a>
			</p>
		</footer>
		<!-- layui.js -->
		<script src="${basePath}/res/timeline/plug/layui/layui.js"></script>
		<!-- 全局脚本 -->
		<script src="${basePath}/res/timeline/js/global.js"></script>
		<!-- 本页脚本 -->
		<script type="text/javascript">
			layui.use('jquery', function() {
				var $ = layui.jquery;
				$(function() {
					$('.monthToggle').click(function() {
						$(this).parent('h3').siblings('ul').slideToggle('slow');
						$(this).siblings('i').toggleClass('fa-caret-down fa-caret-right');
					});
					$('.yearToggle').click(function() {
						$(this).parent('h2').siblings('.timeline-month').slideToggle('slow');
						$(this).siblings('i').toggleClass('fa-caret-down fa-caret-right');
					});
				})
				//子栏目导航点击事件
				$('.child-nav span').click(function() {
					$(this).addClass('child-nav-btn-this').siblings().removeClass('child-nav-btn-this');
					var i=$(this).index();
					$(".qiehuan").children().eq(i).show().siblings('.shadow').hide()
				});
			});
		</script>
		<!--背景-->
		<script src="${basePath}/res/timeline/js/canvas.js"></script>
	</body>
</html>