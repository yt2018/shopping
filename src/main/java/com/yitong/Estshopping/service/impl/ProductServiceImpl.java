package com.yitong.Estshopping.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.ImageDao;
import com.yitong.Estshopping.dao.ProductDao;
import com.yitong.Estshopping.entity.Image;
import com.yitong.Estshopping.entity.Product;
import com.yitong.Estshopping.service.ProductService;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService{
 
	@Autowired
	private ProductDao productDao;
	@Autowired
	private ImageDao imageDao;

	@Override
	public Integer countProduct() {
		// TODO Auto-generated method stub
		return productDao.countProduct();
	}


	@Override
	public void saveProduct(Product product) {
		// TODO Auto-generated method stub
		productDao.save(product);
	}

	@Override
	public Product getProduct(Integer pid) {
		// TODO Auto-generated method stub
		return productDao.get(pid);
	}

	@Override
	public void deleteProduct(Product product) {
		// TODO Auto-generated method stub
		Integer pid = product.getPid();
		List<Image> pid2 = imageDao.getPid(pid);
		/*System.out.println(pid2);
		if(pid2.size()!=0) {
			for (Image image : pid2) {
				System.out.println(image.getImgsrc());
				imageDao.delete(image);
				System.out.println("关联图片删除成功-------"+image.getImgsrc());
			}
		}
		System.out.println("图片删除执行完毕");*/
		productDao.updateProductIsState("del", pid);
		//productDao.delete(product);
	}

	@Override
	public void updateProduct(Product product) {
		// TODO Auto-generated method stub
		productDao.update(product, product.getPid());
	}

	@Override
	public Product getPrId(Date date) {
		// TODO Auto-generated method stub
		return productDao.getPrId(date);
	}

	@Override
	public List<Product> getProductList(Integer page, Integer limit) {
		return productDao.getProductList(page, limit);
		// TODO Auto-generated method stub
	}


	@Override
	public void updateProductIsHot(String isHot, Integer pid) {
		// TODO Auto-generated method stub
		productDao.updateProductIsHot(isHot,pid);
	}


	@Override
	public void updateProductIsState(String isState, Integer pid) {
		// TODO Auto-generated method stub
		productDao.updateProductIsState(isState,pid);
	}


	@Override
	public List<Product> getProductListno(Integer page, Integer limit,String isState){
		// TODO Auto-generated method stub
		List<Product> productListno = productDao.getProductListno(page,limit,isState);
		return productListno;
	}


	@Override
	public Integer countProductno(String on) {
		// TODO Auto-generated method stub
		return productDao.countProductno(on);
	}


	@Override
	public List<Product> getProductListcid(Integer csid, Integer page,Integer limit) {
		// TODO Auto-generated method stub
		return productDao.getProductListcid(csid,page,limit);
	}


	@Override
	public Integer countProduct(Integer csid) {
		// TODO Auto-generated method stub
		return productDao.countProduct(csid);
	}


	@Override
	public List<Product> getProductListone(Integer cid, Integer page,Integer limit) {
		// TODO Auto-generated method stub
		return productDao.findByCategoryId(cid,page,limit);
	}


	@Override
	public Integer countProductone(Integer cid) {
		// TODO Auto-generated method stub
		return productDao.countProductone(cid);
	}


	@Override
	public List<Product> getProductdate( Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return productDao.getProductdate(page,limit);
	}


	@Override
	public Integer countProductdate() {
		// TODO Auto-generated method stub
		return productDao.countProductdate();
	}


	@Override
	public void updateProductPnub(Integer pid ,Integer pnub) {
		// TODO Auto-generated method stub
		productDao.updateProductPnub(pid,pnub);
	}


	@Override
	public List<Product> getProductPnub(Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return productDao.getProductPnub(page,limit);
	}


	@Override
	public List<Product> getProductPremen(Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return productDao.getProductPremen(page,limit);
	}


	@Override
	public Integer countProductdatePremen() {
		// TODO Auto-generated method stub
		return productDao.countProductdatePremen();
	}


	@Override
	public List<Product> getProductListRAND() {
		// TODO Auto-generated method stub
		return productDao.getProductListRAND();
	}


	@Override
	public Integer getcount(Integer uid) {
		// TODO Auto-generated method stub
		return productDao.getcount(uid);
	}


	@Override
	public Integer sumnub(Integer uid) {
		// TODO Auto-generated method stub
		List<Product> getcountsum = productDao.getcountsum(uid);
		Integer sum=0;
		for (Product product : getcountsum) {
			Integer pnub = product.getPnub();
			if(pnub==null)pnub=0;
			sum+=pnub;
		}
		return sum;
	}


	@Override
	public List<Product> getuserProductList(Integer uid) {
		// TODO Auto-generated method stub
		return productDao.getcountsum(uid);
	}


	@Override
	public List<Product> getMyAllproduct(Integer uid, Integer page, Integer limit,String on) {
		// TODO Auto-generated method stub
		return productDao.getMyAllproduct(uid,page,limit,on);
	}


	@Override
	public Integer getMyAllproductCount(Integer uid,String on) {
		// TODO Auto-generated method stub
		return productDao.getMyAllproductCount(uid,on);
	}


	@Override
	public void updateProductInventory(Integer pid, Integer nume) {
		// TODO Auto-generated method stub
		productDao.updateProductInventory(pid, nume);
	}


	@Override
	public List<Product> listSearch(String key, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return productDao.listSearch(key,page,limit);
	}


	@Override
	public Integer listSearchcount(String key) {
		// TODO Auto-generated method stub
		return productDao.listSearchcount(key);
	}

	
}
