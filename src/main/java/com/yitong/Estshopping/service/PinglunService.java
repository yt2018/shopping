package com.yitong.Estshopping.service;

import java.util.List;
import java.util.Map;

import com.yitong.Estshopping.entity.Pinglun;

public interface PinglunService {
	/**
	 * 评论
	 * @param pinglun
	 */
	public void save(Pinglun pinglun);
	
	/**
	 * 评论加载
	 * @param pid
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Map> maplist(Integer pid, Integer page, Integer limit);
	/**
	 * id获取entity
	 * @param zid
	 * @return
	 */
	public Pinglun getpingid(int zid);
	/**
	 * 通过uid查询品论
	 * @param uid
	 * @param string
	 * @return
	 */
	public List<Map> getUidlist(Integer uid, String string);
	/**
	 * 评论数量
	 * @param pid
	 * @return
	 */
	public Integer countlist(Integer pid);

}
