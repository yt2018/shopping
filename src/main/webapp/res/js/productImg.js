layui.use(['form', 'table', 'layer', 'jquery','flow','element'], function() {
	var table = layui.table;
	var form = layui.form;
	var element = layui.element;
	var layer = parent.layer === undefined ? layui.layer : parent.layer;
	var $ = layui.jquery;
	var flow = layui.flow;
	  /*flow.lazyimg({
	    elem: 'body img'
	    //,scrollElem: '#LAY_demo3' //一般不用设置，此处只是演示需要。
	  });*/
	  flow.lazyimg();
	var $tabli = $("#wrap .tab ul li");
	var $picimg = $("#wrap .pic img");
	var $tabimg = $("#wrap .tab ul li img");
	var $picCover = $("#wrap .pic .pic-cover");
	var $cover = $("#wrap .pic .pic-cover .cover");
	var $show = $("#wrap .pic .pic-cover .show");
	var imgArr = []; //用来存放图片宽高比属性
	var index = 0;
	var timeInterval = (null);
	var _index = 0;
	var len = $tabli.length;
	//执行初始化方法
	$tabli.mousemove(function() {
		index = $(this).index();
		//给li添加on样式
		var thisdome = this;
		clearInterval(timeInterval);
		timein(thisdome, index)
	});
	outpaly();
	init()
	//初始化底部小图片的位置
	function init() {
		$tabimg.each(function(i) {
			//获取图片的宽度
			var imgWidth = $(this).attr("w")*1;
			//获取图片的高度
			var imgHeight = $(this).attr("h")*1;
			if(imgWidth >= imgHeight) {
				$(this).css({
					width: 50,
					height: 50 / imgWidth * imgHeight,
					top: (50 - 50 / imgWidth * imgHeight) / 2
				});
				imgArr[i] = {
					width: 1,
					height: imgHeight / imgWidth,
					imgW: imgWidth,
					imgH: imgHeight
				};
			} else {
				$(this).css({
					width: 50 / imgHeight * imgWidth,
					height: 50,
					left: (50 - (50 / imgHeight * imgWidth)) / 2
				});
				imgArr[i] = {
					width: imgWidth / imgHeight,
					height: 1,
					imgW: imgWidth,
					imgH: imgHeight
				};
			}
		});
	};
	
	//鼠标移入pic-cover中
	$picCover.mousemove(function(e) {
		//鼠标画上去停止播放
		clearInterval(timeInterval);
		//更改cover的大小
		var a = $show.width() * $picCover.width() / imgArr[index].imgW;
		$cover.css({
			width: a,
			height: a
		});
		$cover.show();
		$show.show();
		var e = e || window.event;
		var x = e.pageX;
		var y = e.pageY;
		var picCoverX = $picCover.offset().left;
		var picCoverY = $picCover.offset().top;
		var minusX = x - picCoverX - $cover.width() / 2;
		var minusY = y - picCoverY - $cover.height() / 2;
		var maxX = $picCover.width() - $cover.width();
		var maxY = $picCover.height() - $cover.height();
		if(minusX < 0) minusX = 0;
		if(minusY < 0) minusY = 0;
		if(minusX > maxX) minusX = maxX;
		if(minusY > maxY) minusY = maxY;
		$cover.css({
			left: minusX,
			top: minusY
		});
	
		//将show跟随鼠标移动
		var bitX = minusX / $picCover.width();
		var bitY = minusY / $picCover.height();
		var imgWidth = imgArr[index].imgW;
		var imgHeight = imgArr[index].imgH;
		$show.css("background-position", -bitX * imgWidth + "px " + (-bitY * imgHeight) + "px");
		//		});
	}).mouseout(function() {
		//遮盖层隐藏
		$cover.hide();
		$show.hide();
	});
	
	//鼠标移入到show的时候
	$show.mouseover(function() {
		//遮盖层隐藏
		//鼠标画上去停止播放
		clearInterval(timeInterval);
		$cover.hide();
		$show.hide();
	}).mousemove(function() {
		return false;
	});
	$("#wrap").hover(function(){
		clearInterval(timeInterval);
	},function(){
		outpaly();
	})
	function outpaly() {
		timeInterval = setInterval(function() {
			_index++;
			//获取小图框的长度
			if(_index > len - 1) {
				_index = 0
			}
			var thisdome = $tabli.eq(_index)
			index = $(thisdome).index();
			timein(thisdome, index);
		}, 3000)
	}
	
	function timein(thisdome, index) {
		$(thisdome).addClass("on").siblings().removeClass("on");
		//获取小图片的src属性
		var src = $(thisdome).find("img").attr("src"); //getAttribute
		//给show设置src属性
		$show.css("background-image", "url(" + src + ")");
		//给大图设置src属性
		$picimg.attr({
			src: src,
			width: imgArr[index].width * 400,
			height: imgArr[index].height * 400
		});
		$picCover.css({
			top: (400 - imgArr[index].height * 400) / 2,
			left: (400 - imgArr[index].width * 400) / 2
		});
	}
})