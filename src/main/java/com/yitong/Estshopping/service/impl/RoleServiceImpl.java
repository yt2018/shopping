package com.yitong.Estshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.bean.RoleViwe;
import com.yitong.Estshopping.dao.RoleDao;
import com.yitong.Estshopping.entity.Role;
import com.yitong.Estshopping.service.RoleService;
@Transactional
@Service("roleService")
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleDao roleDao;
	@Override
	public List<Role> roleList(Integer page, Integer limit, Object object) {
		// TODO Auto-generated method stub
		return roleDao.roleList(page,limit,object);
	}
	@Override
	public Integer countRole(Object object) {
		// TODO Auto-generated method stub
		return roleDao.countRole(object);
	}
	@Override
	public List<RoleViwe> quanxianList(Integer page, Integer limit, Object object){
		// TODO Auto-generated method stub
		return roleDao.quanxianList(page,limit,object);
	}
	@Override
	public Integer quanxianList(Object object) {
		// TODO Auto-generated method stub
		return roleDao.quanxianList(object);
	}

}
