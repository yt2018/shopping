package com.yitong.Estshopping.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.CategorySecondDao;
import com.yitong.Estshopping.entity.CategorySecond;

@Repository("categorySecondDao")
public class CategorySecondDaoImpl extends BaseDaoImpl<CategorySecond> implements CategorySecondDao {

	@Override
	public Integer countCategorySecond() {
		// TODO Auto-generated method stub
		String hql = "select count(*) from CategorySecond";
        return count(hql);
	}

	@Override
	public List<CategorySecond> findAll(Integer page) {
		// TODO Auto-generated method stub
		String hql = "from CategorySecond";
        int rows = 10;
        int page1 = page;
        return find(hql, page1, rows);
	}

	@Override
	public List<CategorySecond> findAll() {
		// TODO Auto-generated method stub
		String hql = "from CategorySecond";
		return find(hql);
	}

	@Override
	public CategorySecond categorySecondfind(String csname) {
		String hql ="from CategorySecond c where c.csName = :csname";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("csname", csname);
		return (CategorySecond) query.uniqueResult();
	}

	@Override
	public List<CategorySecond> findAllList(Integer cid) {
		String hql ="from CategorySecond c where c.category.cid = ?";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(0, cid);
		return (List<CategorySecond>)query.getResultList();
	}

	@Override
	public List<CategorySecond> findSearch(String key) {
		// TODO Auto-generated method stub
		String hql ="from CategorySecond c where c.csName = :key";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("key", "%"+key+"%");
		return query.list();
	}

}
