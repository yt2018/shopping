package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.entity.Walletlog;

/**
 * 订单的Dao接口
 * @author YT
 *
 */
public interface WalletLogDao extends BaseDao<Walletlog> {

	/**
	 * 查询记录列表
	 * @param user
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Walletlog> listorder(User user, Integer page, Integer limit);

	public Integer listorder(User user);
	/**
	 * 查询今日消费
	 * @param user
	 * @param states
	 * @return
	 */
	public Float usercount(User user, Integer states);
	
}
