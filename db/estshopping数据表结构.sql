/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 50715
Source Host           : localhost:3306
Source Database       : estshopping

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2018-03-18 10:16:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for adminuser
-- ----------------------------
DROP TABLE IF EXISTS `adminuser`;
CREATE TABLE `adminuser` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`aid`),
  KEY `FKoel1t4db3yrrgj3grtu97rikg` (`rid`) USING BTREE,
  KEY `FKsqly3pbdnqavbe4jgsvor5rah` (`uid`) USING BTREE,
  CONSTRAINT `FKoel1t4db3yrrgj3grtu97rikg` FOREIGN KEY (`rid`) REFERENCES `role` (`rid`),
  CONSTRAINT `FKsqly3pbdnqavbe4jgsvor5rah` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `adminuser_ibfk_1` FOREIGN KEY (`rid`) REFERENCES `role` (`rid`),
  CONSTRAINT `adminuser_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `adminuser_ibfk_3` FOREIGN KEY (`rid`) REFERENCES `role` (`rid`),
  CONSTRAINT `adminuser_ibfk_4` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `adminuser_ibfk_5` FOREIGN KEY (`rid`) REFERENCES `role` (`rid`),
  CONSTRAINT `adminuser_ibfk_6` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `bimg_alt` varchar(255) DEFAULT NULL,
  `bimg_color` varchar(255) DEFAULT NULL,
  `bimg_date` datetime DEFAULT NULL,
  `bimg_height` int(11) DEFAULT NULL,
  `bimg_num` int(11) DEFAULT NULL,
  `bimg_src` varchar(255) DEFAULT NULL,
  `bimg_states` int(11) DEFAULT NULL,
  `bimg_width` int(11) DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `datenum` int(11) DEFAULT NULL,
  `bimg_class` int(11) DEFAULT NULL,
  PRIMARY KEY (`bid`),
  KEY `FKdb5cv5erc4ssei36sgtb3wrum` (`uid`) USING BTREE,
  CONSTRAINT `FKdb5cv5erc4ssei36sgtb3wrum` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `banner_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `banner_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `banner_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(255) NOT NULL,
  `discount` float DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  `privilegetime` datetime DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for categorysecond
-- ----------------------------
DROP TABLE IF EXISTS `categorysecond`;
CREATE TABLE `categorysecond` (
  `csid` int(11) NOT NULL AUTO_INCREMENT,
  `csname` varchar(255) NOT NULL,
  `href` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  PRIMARY KEY (`csid`),
  KEY `FKh1rpx0qlb42dq81f2v1ow5oqh` (`cid`) USING BTREE,
  CONSTRAINT `FKh1rpx0qlb42dq81f2v1ow5oqh` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`),
  CONSTRAINT `categorysecond_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`),
  CONSTRAINT `categorysecond_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`),
  CONSTRAINT `categorysecond_ibfk_3` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `ccomment` varchar(255) DEFAULT NULL,
  `com_date` datetime DEFAULT NULL,
  `pcid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`cid`),
  KEY `FKimo6vqapa7rlfixeegmxv5mi8` (`uid`) USING BTREE,
  CONSTRAINT `FKimo6vqapa7rlfixeegmxv5mi8` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `imgid` int(11) NOT NULL AUTO_INCREMENT,
  `classify` int(11) DEFAULT NULL,
  `iheight` int(11) DEFAULT NULL,
  `imgsrc` varchar(255) DEFAULT NULL,
  `iwidth` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`imgid`),
  KEY `FKe00nk2eki5qrrehrtgnvyo8ss` (`pid`) USING BTREE,
  KEY `FKdcgnsh7g4o1u0oja4t1tqt8la` (`uid`) USING BTREE,
  CONSTRAINT `FKdcgnsh7g4o1u0oja4t1tqt8la` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `FKe00nk2eki5qrrehrtgnvyo8ss` FOREIGN KEY (`pid`) REFERENCES `product` (`pid`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `image_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `product` (`pid`),
  CONSTRAINT `image_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `image_ibfk_4` FOREIGN KEY (`pid`) REFERENCES `product` (`pid`),
  CONSTRAINT `image_ibfk_5` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `image_ibfk_6` FOREIGN KEY (`pid`) REFERENCES `product` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `coupon` varchar(255) DEFAULT NULL,
  `coupon_money` float DEFAULT NULL,
  `couponid` int(11) DEFAULT NULL,
  `kuaidi` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `oif` varchar(255) DEFAULT NULL,
  `order_time` datetime DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `pimg` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `FKt1qujix1enyynbjnlpr1s8oum` (`cid`) USING BTREE,
  KEY `FK58x4l9shxmkb7pismj4ilt7pj` (`uid`) USING BTREE,
  CONSTRAINT `FK58x4l9shxmkb7pismj4ilt7pj` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `FKt1qujix1enyynbjnlpr1s8oum` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orders_ibfk_6` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for orderslog
-- ----------------------------
DROP TABLE IF EXISTS `orderslog`;
CREATE TABLE `orderslog` (
  `ologid` int(11) NOT NULL AUTO_INCREMENT,
  `logobj` text,
  `logoif` varchar(255) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `pname` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `kid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`ologid`),
  KEY `FK7jjmifxc71wpne9l9ayw21aat` (`cid`) USING BTREE,
  KEY `FK6xok9mbvm3uax7lhnexv2xeyj` (`kid`) USING BTREE,
  KEY `FK9wwo0g95m004x2g6jknkf848j` (`uid`) USING BTREE,
  CONSTRAINT `FK6xok9mbvm3uax7lhnexv2xeyj` FOREIGN KEY (`kid`) REFERENCES `user` (`uid`),
  CONSTRAINT `FK7jjmifxc71wpne9l9ayw21aat` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`),
  CONSTRAINT `FK9wwo0g95m004x2g6jknkf848j` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_1` FOREIGN KEY (`kid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_4` FOREIGN KEY (`kid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_5` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_6` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_7` FOREIGN KEY (`kid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_8` FOREIGN KEY (`cid`) REFERENCES `user` (`uid`),
  CONSTRAINT `orderslog_ibfk_9` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for packet
-- ----------------------------
DROP TABLE IF EXISTS `packet`;
CREATE TABLE `packet` (
  `pacid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`pacid`),
  UNIQUE KEY `UK_th5acal46shrmg0fotogxoq3y` (`uid`) USING BTREE,
  CONSTRAINT `FKjdinriyg7imeuskihab64ldxi` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `packet_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `packet_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `packet_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `imgsrcs` text,
  `inventory` int(11) DEFAULT NULL,
  `is_hot` varchar(255) DEFAULT NULL,
  `is_state` varchar(255) DEFAULT NULL,
  `market_price` float DEFAULT NULL,
  `parameter` text,
  `pdate` datetime DEFAULT NULL,
  `pdesc` text,
  `pname` varchar(255) NOT NULL,
  `pnamedesc` varchar(255) DEFAULT NULL,
  `pnub` int(11) DEFAULT NULL,
  `shop_price` float DEFAULT NULL,
  `csid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `FKcobqeq387r4s6we6iyulx1xf6` (`csid`) USING BTREE,
  KEY `FKfx0e7ofgbomhiv61sy4mw8x4m` (`uid`) USING BTREE,
  CONSTRAINT `FKcobqeq387r4s6we6iyulx1xf6` FOREIGN KEY (`csid`) REFERENCES `categorysecond` (`csid`),
  CONSTRAINT `FKfx0e7ofgbomhiv61sy4mw8x4m` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`csid`) REFERENCES `categorysecond` (`csid`),
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `product_ibfk_3` FOREIGN KEY (`csid`) REFERENCES `categorysecond` (`csid`),
  CONSTRAINT `product_ibfk_4` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `product_ibfk_5` FOREIGN KEY (`csid`) REFERENCES `categorysecond` (`csid`),
  CONSTRAINT `product_ibfk_6` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `createdate` datetime DEFAULT NULL,
  `rolename` varchar(255) DEFAULT NULL,
  `rolenum` int(11) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ticket
-- ----------------------------
DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `consume` float DEFAULT NULL,
  `privilege` float DEFAULT NULL,
  `use_time` datetime DEFAULT NULL,
  `cid` int(11) NOT NULL,
  PRIMARY KEY (`tid`),
  UNIQUE KEY `UK_bveamlrms7iguexr6uteopdo9` (`cid`) USING BTREE,
  CONSTRAINT `FKnoxl6rvrrrn7oj5gd1jfowltg` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`),
  CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`),
  CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`),
  CONSTRAINT `ticket_ibfk_3` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `joindate` datetime DEFAULT NULL,
  `pwd_key` varchar(6) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `qquser` varchar(255) DEFAULT NULL,
  `rolelog` text,
  `sex` varchar(1) DEFAULT NULL,
  `sign` text,
  `state` int(11) DEFAULT NULL,
  `username` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wallet
-- ----------------------------
DROP TABLE IF EXISTS `wallet`;
CREATE TABLE `wallet` (
  `wid` int(11) NOT NULL AUTO_INCREMENT,
  `money` float DEFAULT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`wid`),
  UNIQUE KEY `UK_gd56wkq3b1wc4gd068p6iwsq` (`uid`) USING BTREE,
  CONSTRAINT `FKqpgqqo01nnlyat6maj8maskl1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `wallet_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `wallet_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for walletlog
-- ----------------------------
DROP TABLE IF EXISTS `walletlog`;
CREATE TABLE `walletlog` (
  `wlogid` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `log_time` datetime DEFAULT NULL,
  `money` float DEFAULT NULL,
  `oif` varchar(255) DEFAULT NULL,
  `states` int(11) DEFAULT NULL,
  `wif` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`wlogid`),
  KEY `FK5kxw9aj4s5th0jobqow9ag7lv` (`uid`) USING BTREE,
  CONSTRAINT `FK5kxw9aj4s5th0jobqow9ag7lv` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `walletlog_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `walletlog_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`),
  CONSTRAINT `walletlog_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- View structure for roleviwe
-- ----------------------------
DROP VIEW IF EXISTS `roleviwe`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `roleviwe` AS select `u`.`username` AS `username`,`u`.`name` AS `name`,`u`.`phone` AS `phone`,`u`.`qq` AS `qq`,`u`.`rolelog` AS `rolelog`,`u`.`uid` AS `uid`,`role`.`rid` AS `rid`,`role`.`rolename` AS `rolename`,`role`.`createdate` AS `createdate`,`role`.`rolenum` AS `rolenum` from ((`adminuser` `a` left join `role` on((`a`.`rid` = `role`.`rid`))) left join `user` `u` on((`a`.`uid` = `u`.`uid`))) ;
