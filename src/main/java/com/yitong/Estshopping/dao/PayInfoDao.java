package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.PayEntity;
import com.yitong.Estshopping.entity.User;

public interface PayInfoDao extends BaseDao<PayEntity>{
	/**
	 * 根据订单查询数据
	 * @param out_trade_no
	 * @return
	 */
	PayEntity getOut_trade_no(String out_trade_no);
	
	
	

}
