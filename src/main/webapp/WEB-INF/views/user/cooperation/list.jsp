<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<title>合作商</title>
	<%-- <link rel="stylesheet" href="${basePath}/res/css/categorylist.css" media="all" /> --%>
	<style type="text/css">
	</style>
</head>
<body>
  <div class="layui-tab">
  <ul class="layui-tab-title">
    <li class="layui-this">功能选择</li>
    <li>查询选择</li>
  </ul>
  <div class="layui-tab-content">
    <div class="layui-tab-item layui-show">
	  <div class="layui-btn-group">
			    <button class="layui-btn" data-type="tong">全部</button>
			    <button class="layui-btn" data-type="cshenhe">审核中</button>
			    <button class="layui-btn" data-type="ctongguo">通过</button>
			    <button class="layui-btn" data-type="cweitong">未通过</button>
			    <button class="layui-btn layui-btn-danger" data-type="btnDelAll">批量删除</button>
		    </div>
    </div>
    <div class="layui-tab-item">
    	<form name="searchform" id="searchform" class="layui-form layui-form-pane" method="post" action="">
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-inline">
					<input type="text" name="operCd" placeholder="请输入" autocomplete="off" class="layui-input" />
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">真实姓名</label>
				<div class="layui-input-inline">
					<input type="text" name="operNm" placeholder="请输入" autocomplete="off" class="layui-input" />
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">电话号码</label>
				<div class="layui-input-inline">
					<input type="text" name="telephone" placeholder="请输入" autocomplete="off" class="layui-input">
				</div>
			</div>
		</div>
		<button class="layui-btn" id="btnSearch" type="button">立即查询</button>
		<button type="reset" id="btnRetSet" type="button" class="layui-btn layui-btn-primary">重置</button>
	</form>

    </div>
  </div>
</div>
<table class="layui-hide" id="tableID" lay-filter="tableID" ></table>
<script>
layui.use(['laydate', 'laypage', 'layer', 'table', 'carousel','form', 'upload', 'element','jquery'], function(){
	  var layer = layui.layer; //弹层
	  var form = layui.form; //表单
	  var laydate = layui.laydate; //日期
	  var laypage = layui.laypage; //分页
	  var table = layui.table; //表格
	  var carousel = layui.carousel; //轮播
	  var upload = layui.upload; //上传
	  var element = layui.element; //元素操作
	  var $=layui.jquery;
	  
	  var index = layer.load(1);//开启进度条
	  var turl='${basePath}/admin/cooperationbannerlist';
	  var curr;
	 
	  //绑定table
	  var t=table.render({
		  id:"tableID",
		  elem: '#tableID' ,//table id
		  url: turl,
				method : 'POST', //方式
				page : true,//是否开启分页
				limits : [ 10, 20, 30, 60, 90, 100 ],
				limit : 10, //默认采用20
				cellMinWidth: 120,
				even : true, //开启隔行背景
				done: function(res, curr, count){
					//加载后回调
					layer.close(index);//关闭   
					//noAuthTip(res);//无权限提示
					curr=curr;
					console.info(curr)
				},
				cols : [ [ //标题栏
				{
					fixed : 'left',
					checkbox: true
				},{
					field : 'bid',
					fixed : 'left',
					title : 'ID',
					//width : '40',
					align : 'center',
					sort : true
				},{
					field : 'bimgAlt',
					title : '描述',
					fixed : 'left',
					width : '100',
					align : 'center'
				},{
					field : 'bimgColor',
					title : '主色',
					width : '100',
					align : 'center'
				},{
					field : 'bimgDate',
					title : '时间',
					width : '60',
					align : 'center',
					sort : true
				}, {
					field : 'bimgHeight',
					title : '高度',
					width : '50',
					align : 'center'
				},{
					field : 'bimgWidth',
					title : '宽度',
					width : '50',
					align : 'center'
				},{
					field : 'datenum',
					title : '合作天数',
					width : '100',
					align : 'center'
				},{
					field : 'href',
					title : '链接地址',
					width : '200',
					align : 'center',
					templet: '#imgsrc'
				},{
					fixed : 'right',
					field : 'bimgStates',
					title : '用户状态',
					align : 'center',
					width : '100',
					templet: '#operStTpl'
					
				}, {
					fixed : 'right',
					title : '操作',
					toolbar : '#toobar',
					width : '200'
				} ] ]
			});

			//监听工具条
			table.on('tool(tableID)', function(obj) { //注：tool是工具条事件名，table是table原始容器的属性 lay-filter="对应的值"
				var data = obj.data //获得当前行数据
				, layEvent = obj.event; //获得 lay-event 对应的值
				console.info(data)
				if (layEvent === 'detail') {
					openDetail(data);
				}else if(layEvent === 'update'){
					openUpdate(data);
				}else if(layEvent === 'del'){
					openDelete(data.bid);
				}
			});
			form.on('switch(switchTest)', function(data) {
				/* layer.msg('是否热门：' + (this.checked ? 'true' : 'false'), {
					offset: '6px'
				}); */
				console.info(this.checked);
				var dom =$(data.elem);
				var is
				this.checked ? is='1' : is='2';
				url="${basePath}/admin/cooperationbannerUpdate"
				var d={"bid":dom.data("bid"),"bimgStates":is}	
				$.post(url,d,function(r){
					layer.msg(r.msg)
					t.reload()
				})
			});


			//监听表格复选框选择
			  table.on('checkbox(tableID)', function(data){
				  //layer.alert(JSON.stringify(data));
			  });
			  
			
			//打开查看按钮
			function openDetail(data) {
				layer.open({
					type : 2,
					title : '查看',
					shadeClose : false,//点击遮罩关闭
					anim : 1,
					btnAlign : 'c',
					//shade :false,//是否有遮罩，可以设置成false
					//maxmin : true, //开启最大化最小化按钮
					area: ['700px', '400px'],
					//area : [ '100%', '100%' ],
					boolean : true,
					content : [ 'OperInfo?bid='+data.bid, 'yes'], //iframe的url，no代表不显示滚动条
					success : function(layero, lockIndex) {
						var body = layer.getChildFrame('body', lockIndex);
						//绑定解锁按钮的点击事件
						body.find('button#close').on('click', function() {
							layer.close(lockIndex);
							//location.reload();//刷新
						});

						body.find("input").attr("readonly", "readonly");  
					}
				});
			}
			
			
			//删除按钮
			function openDelete(ids){
				console.info(t)
				layer.open({
			        title: '确认删除' //显示标题栏
			        ,closeBtn: false
			        ,area: '300px;'
			        ,shade: 0.5
			        ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
			        ,btn: ['删除', '关闭']
			        ,content: '您是否要删除当前选中的记录？'
			        ,success: function(layero){
			          var btn = layero.find('.layui-layer-btn');
			          btn.css('text-align', 'center');//居中
			          btn.find('.layui-layer-btn0').on('click', function() {
			        	  var loadindex = layer.load(1);//开启进度条
			        	  $.ajax({
								url : '${basePath}/userinfo/cooperationbannerlist/del',
								data : {
									bid : ids
								},
								type:'POST',//默认以get提交，以get提交如果是中文后台会出现乱码
								dataType : 'json',
								success : function(r) {
									layer.close(loadindex);//关闭进程对话框
									if (r.code==0) {
										layer.msg(r.msg)
										
									} else {
										layer.msg(r.msg)
									}
								}
							});
			          });
			        }
			      });
			}
			
			//查询按钮
			$('#btnSearch').on('click', function() {
				index = layer.load(1);//开启进度条
				var searchform = pubUtil.serializeObject($("#searchform"));//查询页面表单ID
				//alert(JSON.stringify(searchform));
				table.reload('searchID', {
					where : searchform
				});
			});

			//重置按钮 
			$('#btnRetSet').on('click', function() {
				index = layer.load(1);//开启进度条
				table.reload('searchID', {
					where : ""
				});
			});
			
		   var active = { 
			btnDelAll: function(){ //批量删除
		      var checkStatus = table.checkStatus('tableID');
		      console.info(checkStatus)
		      var data = checkStatus.data;

		      console.info(data)
		      //layer.alert(JSON.stringify(data));
				var i,arr=[];
				for(i in data){
					//console.info(data[i].pid)
					arr.push(data[i].bid)
				}
				if(isEmpty(arr)){
					layer.msg('请选择删除的数据');
					return;
				}
		    	console.info(arr);
		    	console.info(arr.lenght);
		    	layer.confirm('请谨慎,删除数据不可恢复', function(index){
			    	$.ajax({
						url : "${basePath}/admin/cooperationbannerlist/del",
						type : "post",
						dataType : "json",
						data:{"bids":"["+arr+"]"},
						success : function(data){
							t.reload()
						    layer.close(index);
							layer.msg(data.msg);
						},
						error :function(){
							layer.msg("网络超时");
						}
					})
				})

		      //openDelete(ids);
		    }
		    ,btnAdd: function(){ //新增操作
		    	openAdd();
		    }
		    ,tong:function(){
				  t.reload({url:turl})
			  }
		    ,cshenhe:function(){
				  t.reload({where:{state:0}})
			  }
		    ,ctongguo:function(){
				  t.reload({where:{state:1}})
			 }
			,cweitong:function(){
				  t.reload({where:{state:2}})
			 }
		  };
		  
		  $('.layui-btn-group .layui-btn').on('click', function(){
		    var type = $(this).data('type');
		    active[type] ? active[type].call(this) : '';
		  });
			
		});
function isEmpty(obj) {
    // 本身为空直接返回true
    if (obj == null) return true;

    // 然后可以根据长度判断，在低版本的ie浏览器中无法这样判断。
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    //最后通过属性长度判断。
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}


	</script>
<script type="text/html" id="toobar">
{{#  if(d.bimgStates == 1){ }}
<input type="checkbox" data-bid="{{d.bid}}" checked name="is" lay-skin="switch" lay-filter="switchTest" lay-text="通过|停止" />
{{#  }else{ }} 
<input type="checkbox" data-bid="{{d.bid}}" name="is" lay-skin="switch" lay-filter="switchTest" lay-text="通过|停止" />
{{#  } }} 
<a class="layui-btn layui-btn-mini layui-btn-primary layui-btn-xs" lay-event="detail" data-id="{{d.bid}}">查看</a>
<a class="layui-btn layui-btn-mini layui-btn-danger layui-btn-xs" lay-event="del" data-id="{{d.bid}}">删除</a>


</script>
<script type="text/html" id="imgsrc">
<div style="background:{{d.bimgColor}}"><img src="{{d.bimgSrc}}" height=50 /></div>
</script>
<script type="text/html" id="operStTpl">
{{# if(d.bimgStates == 0||d.bimgStates ==''){}}
<span class="layui-badge ">审核中</span>
{{#}}}
{{# if(d.bimgStates == 2){}}
<span class="layui-badge ">被禁止</span>
{{#}}}
{{# if(d.bimgStates == 1){}}
<span class="layui-badge layui-bg-green ">审核通过</span>
{{#}}}
</script>
</body>
</html>