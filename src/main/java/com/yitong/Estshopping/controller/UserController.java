package com.yitong.Estshopping.controller;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.Adminuser;
import com.yitong.Estshopping.entity.Role;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.AdminService;
import com.yitong.Estshopping.service.UserService;

/**
 * 用户登录注册
 * @author YT
 *
 */

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private AdminService adminService;
	
	/**
	 * 跳转到用户登录
	 * @return
	 */
	@RequestMapping(value = "/userLogin")
    public String userLogin(HttpServletRequest request,HttpSession session,String uri) {
		if(uri!=null) {
			session.setAttribute("redirectUrl",uri);
		}
		Device device = DeviceUtils.getCurrentDevice(request);
		if (device.isMobile()) {
			return "mobile/login";
		}
        return "login";
    }
	
	/**
	 *  用户注册的跳转
	 * @return
	 */
	@RequestMapping("/userRegister")
    public String register(HttpServletRequest request) {
		Device device = DeviceUtils.getCurrentDevice(request);
		if (device.isMobile()) {
			return "mobile/regist";
		}
        return "regist";
    }
	/**
	 * 注册用户
	 */
	@RequestMapping(value = "/reg", method = RequestMethod.POST,produces = "application/json;charset=utf-8")
	public @ResponseBody Map<String,Object> reg(@RequestBody User user,HttpSession session) {
		Map<String,Object> resultMap = new HashMap<String,Object>();
		User existUser = userService.existUser(user.getUserName());
		if (existUser != null) {
			// 客户已存在
			resultMap.put("usereq",true);
			System.out.println("用户名已存在");
		} else {
			// 客户不存在
			resultMap.put("usereq",false);
			userService.registerUser(user);
			System.out.println("用户名不存在");
			session.setAttribute("user", user);
		}
		System.out.println(user.getUserName());
		
		return resultMap;
	}
	@RequestMapping(value = "/login/{checkcode}", method = RequestMethod.POST,produces = "application/json;charset=utf-8")
	public @ResponseBody Map<String, Object>  login(@RequestBody User user,
			@PathVariable("checkcode") String checkcode, HttpSession session,HttpServletRequest request,HttpServletResponse response) {
		Map<String,Object> map = new HashMap<String,Object>();
		System.out.println(user.getUserName());
		System.out.println("前端验证码" + checkcode);
		/**
		 * 校验验证码
		 */
		String checkCode = (String) session.getAttribute("checkcode");
		System.out.println("后端验证码" + checkCode);
		// 如果验证码不一致，直接返回
		if (checkCode == null || !checkCode.equalsIgnoreCase(checkcode)) {
			map.put("errorCheckCode",true);
			return map;
		}
		/**
		 * 判断是否存在该用户,用客户名称校验客户是否存
		 */
		System.out.println(user.getUserName());
		User existUser = userService.existUser(user.getUserName());
	
		if(existUser == null){
			map.put("state", 1);
			map.put("result", "用户不存在");
			return map;
		}
		/**
		 * 判断用户和密码是否正确
		 */
		User tUser = userService.findByUserNameAndPassword(user);
		System.out.println(tUser);
		if(tUser == null){
			map.put("state", 2);
			map.put("result", "密码不正确");
			return map;
		}
		
		/**
		 * 把user对象放入到session范围
		 */
		map.put("state", 0);
		session.setAttribute("user", tUser);
		map.put("result", "登录成功");
		Integer uid = tUser.getUid();
		System.out.println("----------------"+uid);
		Adminuser adminRole = adminService.adminRole(uid);
		String url =  (String) session.getAttribute("redirectUrl");
		if(url==null) {
			url=request.getContextPath();
		}
		map.put("url", url);
		if(adminRole!=null) {
			Role role = adminRole.getRole();
			System.out.println(role);
			session.setAttribute("role",role);
		}
		System.out.println("Session : " + session.getAttribute("user"));
		return map;
	}
	/**
	 * 解锁
	 */
	@RequestMapping(value = "/admin/jiesuo", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object>  loginjiesuo(HttpSession session,String pwd){
		Map<String,Object> map = new HashMap<String,Object>();
		User user = (User) session.getAttribute("user");
		user.setPassword(pwd);
		/**
		 * 判断用户和密码是否正确
		 */
		User tUser = userService.findByUserNameAndPassword(user);
		
		System.out.println(tUser);
		if(tUser == null){
			map.put("state", 2);
			map.put("result", "密码不正确");
			return map;
		}
		/**
		 * 把user对象放入到session范围
		 */
		map.put("state", 0);
		session.setAttribute("user", tUser);
		map.put("result", "解锁成功");
		return map;
	}
	
	/**
	 * 客户退出
	 */
	@RequestMapping(value = "/quit", method = RequestMethod.GET)
	public String quit(HttpSession session,HttpServletRequest request){
		User user = (User) session.getAttribute("user");
		//Integer uid = user.getUid();
		//String rootPath = request.getRealPath("upload/"+uid);
		//退出登录
		session.invalidate();
		/**
		 * 退出登录销毁未保存的文件
		 * 清除上传,未保存文件.
		 */
		//判断是否存在文件夹
		return "redirect:index";
	}
	
}
