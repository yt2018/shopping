layui.use(['form', 'table', 'layer', 'jquery','flow','element','layedit'], function() {
		var table = layui.table;
		var form = layui.form;
		var element = layui.element;
		var layer = parent.layer === undefined ? layui.layer : parent.layer;
		var $ = layui.jquery;
		var lcount;
		var ptool={
			tool: [
				'strong' //加粗
				, 'link' //超链接
				, 'unlink' //清除链接
				, 'face' //表情
				, 'image' //插入图片
			]
			,height:150
		}
		var layedit = layui.layedit;
		//获取hash来切换选项卡，假设当前地址的hash为lay-id对应的值
		  //Hash地址的定位
		
		var layid = location.hash.replace(/^#type=/, '');
		element.tabChange('type', layid);
	  element.on('tab(type)', function(elem){
	    //location.hash = 'type='+ $(this).attr('lay-id');
		  var flag=$(this).attr('lay-id')
		  
			if(flag=="p_pinglun"){
				initpl(1,20)
				//layer.close(index);  
			}
		window.location.href='#type='+ $(this).attr('lay-id');
	  });
      if(location.hash.indexOf('p_pinglun') != -1){
		  initpl(1,20)
      }
	  function initpl(p,l){
		  var index = layer.load(2)
		  /*初始化评论*/
		  if(p==1){
			  $(".pinglunqu").html('')
		  }
		  $.ajax({
				type:"post",
				url:path+"/p/plist/"+pid,
				async:true,
				data :{"page":p,"limit":l},
	            dataType : 'json',
				success:function(d){
					console.info(d)
					lcount=d.count
					console.info(lcount)
					if(d.code==1){
						
						layer.msg(d.msg);
						phtml(d.data)
						  layer.close(index);  
					}
				}
			});
	  }
	  function htm(v){
		  var fg=false;
		  	if(v.isni=='on'){
		  		fg=true;
		  	}
	      	  html='<li data-id="'+v.plid+'">'
					+'	<div class="detail-about detail-about-reply">'
					+'	<a class="fly-avatar" href="'+(fg?"javascrpit:;":path+'/u/'+v.uid)+'"> '
					+'		<img src="'+(fg?path+"/res/images/logo.png":v.icon)+'" alt="'+(fg?"匿名":v.name)+'">'
					+'	</a>'
					+'	<div class="fly-detail-user">'
					+'		<a href="'+(fg?"javascrpit:;":path+'/u/'+v.uid)+'" class="fly-link"> <cite>'+(fg?"匿名":v.name)+'</cite>'
					+'		</a>'
					+'	</div>'
					+'	<div class="detail-hits">'
					+'		<span>'+format(v.pldate,"yy-mm-dd HH:mm:ss")+'</span>'
					+'	</div>'
					+'</div>'
					+'<div class="detail-body layui-text pinglunqu-body photos">'
					+v.pltext
					+'</div>'
					+'<div class="pinglunqu-reply">'
					+'	<span class="pinglunqu-zan pl_zan" type="zan" data-id="'+v.plid+'"> '
					+'		<i class="iconfont icon-zan "></i> <em>'+(v.zan==null?v.zan=0:v.zan) +'</em>'
					+'	</span> '
					+'	<span type="reply" class="pl_zhuiping" data-id="'+v.plid+'" data-name="'+(fg?"匿名":v.name)+'"> '
					+'		<i class="iconfont icon-svgmoban53"></i> 回复'
					+'	</span>'
					+'</div>'
					+'</li>'
	      	  		$(".pinglunqu").append(html)
	  }
	  function phtml(d){
		  var html=''
			  $.each(d,function (i, v) {
				  	htm(v)
	          });
	  }
	  
		layedit.set({
			uploadImage: {
				url:  path+'/cloud/upload/watermark?fsrc=pinglun&uname='+uname+'&uid='+uid+'&title=' //上传接口
				,type: 'post' //默认post
				,size:2048
			}
		});
		
		var edit= layedit.build('yttpinglun',ptool); //建立编辑器
		form.on('submit(pinhlun)', function(data){
		    var dt=new Object();
		    var zid='';
		    var text=layedit.getContent(edit)
		    if(!text){
		    	layer.msg("请输入评论内容")
		    	return false;
		    }
		    //获取追评id
		    zid=$('#pingbtn').attr("data-id");
		    console.info(zid)
		    if(zid==undefined){zid=''}
		    dt.pinglun=text
		    //layer.msg(JSON.stringify(data.field));
		    dt.niming='off';
		    dt.zid=zid;
		    if(data.field['niming']=='on'){
		    	dt.niming='on'
		    	//alert('匿名');
		    }
		    console.info(JSON.stringify(dt));
		    $.ajax({
                url : path+"/userinfo/p/"+pid+"?pinglun="+dt.pinglun+"&niming="+dt.niming+"&zid="+dt.zid,
                type : "post",
                async : true,
                contentType : "application/json",
                //data : JSON.stringify(dt),
                //dataType : 'json',
                success : function(d) {
                	if(d.code==1){
    					parent.layer.msg(d.msg,{time: 2000});
    					//layedit.setContent(edit,"");
    					layer.closeAll();
    					//parent.location.reload(); 
    					//初始化评论
    					initpl(1,20)
    				}else if(d.code==3){
						layer.msg(d.msg,{
							  icon: 3,
							  time: 2000 
							}, function(){
								window.location.href = path+"/userinfo/set";
							})
					}else{
						parent.layer.msg("网络出现错误,刷新页面重试");
					}
                }
             })
		    return false;
		  });
		//回复评论
		$(document).on('click','.pl_zhuiping',function(){
			var id=$(this).data('id');
			var name=$(this).data('name');
			if(uname==""){
				layer.msg("你还没登录了,请登录后评论")
				return;
			}
			layer.open({
			  type: 1,
			  offset: 'b',
			  area: ['300px', '300px'],
			  title:"回复 @<b>"+name+"</b>:",
			  content: $('#zhuipingtext'),
			  //scrollbar: false,
			  success: function(layero, index){
				  edit= layedit.build('yttpinglun',ptool)
				  $("#pingbtn").attr("data-id",id)
				},
			  end:function(){
				  $("#pingbtn").removeAttr("data-id");
			  }
			})
			  
		})	
		//加载评论
		var i=0
		$('.pinglunlistbtn').on('click',function(){
			var chtml='<i class="layui-icon">&#xe61a;</i>  点击加载查看更多.....'
				,phtml='<i class="layui-icon">&#xe63d;</i> 正在加载中.....'
				,lhtml='<i class="layui-icon">&#xe60c;</i>到底了';
			console.info(lcount)
			$(this).html(phtml)
			var p=Math.ceil(lcount/20)
			console.info()
			if(p<=0){
				$(this).html(lhtml).removeClass("pinglunlistbtn")
			}else{
				if(p*20>=lcount){
					$(this).html(lhtml).removeClass("pinglunlistbtn")
				}else{
					initpl(p+i,20)
					$(this).html(chtml)
					i++
				}
			}
			 //initpl(1,20)
			  
		})	
		//点赞
		$(document).on('click','.pl_zan',function(){
			/*点赞class*/
			layer.msg("点赞功能正在开发中。。。。")
		})
		
		$("#cartbtn").on('click',function(){
			var pnub=$('#pnub').val()
			if(eval(pnub)>eval(pnum)){
				layer.msg('库存量不足',{
					  icon: 2,
					  time: 3000 
					})
				return;
			}
			$.get('${basePath}/cartadd/${product.pid}?count='+pnub,function(data){
				layer.msg(data.cartmap)
			})
			/* $.ajax({
				type:"post",
				contentType : "application/json ; charset=utf-8",
				url:"${basePath}/addCart",
				data:{"pid":${product.pid},"count":pnub},
				success:function(data){
					console.info(data.cartmap)
				}
			}) */
		})
		$(".nobtn").on("click",function(){
			layer.msg($(this).data("mrg"))
		}) 
		$("#buybtn").on('click',function(){ 
			layer.msg('正在排队下单.....',{time:3000},function(){
				var pnub=$('#pnub').val()
				if(eval(pnub)>eval(pnum)){
					var index=layer.msg('库存量不足',{
						  icon: 2,
						  time: 3000 
						})
					return index;
				}
				
				$.post(path+"/getorder",{"setid":pid,"num":pnub},function(d){
					if(d.code==0){
						layer.msg(d.msg,{
							  icon: 2,
							  time: 1000 
							}, function(){
								window.location.href = path+"/userLogin?uri="+window.location.href;
							})
					}else if(d.code==1){
						layer.msg(d.msg)
					}else if(d.code==3){
						layer.msg(d.msg,{
							  icon: 3,
							  time: 2000 
							}, function(){
								window.location.href = path+"/userinfo/set";
							})
					}else if(d.code==2){
						layer.msg(d.msg,{
							  icon: 1,
							  time: 2000 
							}, function(){
								window.location.href = path+"/userinfo/order";
							})
					}else{
						layer.msg("网络延时,请刷新网页",{
							  icon: 3,
							  time: 2000 
							}, function(){
								window.location.href();
							})
					}
				},"json")
			})
		}) 
		 
	})
	//检查是不是两位数字，不足补全
	function check(str){
	    str=str.toString();
	    if(str.length<2){
	        str='0'+ str;
	    }
	    return str;
	}
	function format(date,str){
		var date = new Date(date);
	    var mat={};
	    mat.M=date.getMonth()+1;//月份记得加1
	    mat.H=date.getHours();
	    mat.s=date.getSeconds();
	    mat.m=date.getMinutes();
	    mat.Y=date.getFullYear();
	    mat.D=date.getDate();
	    mat.d=date.getDay();//星期几
	    mat.d=check(mat.d);
	    mat.H=check(mat.H);
	    mat.M=check(mat.M);
	    mat.D=check(mat.D);
	    mat.s=check(mat.s);
	    mat.m=check(mat.m);
	    console.log(typeof mat.D)
	    if(str.indexOf(":")>-1){
	　　　　　//mat.Y=mat.Y.toString().substr(2,2);
	　　　　 return mat.Y+"-"+mat.M+"-"+mat.D+" "+mat.H+":"+mat.m+":"+mat.s;
	    }
	    if(str.indexOf("/")>-1){
	        return mat.Y+"/"+mat.M+"/"+mat.D+" "+mat.H+"/"+mat.m+"/"+mat.s;
	    }
	    if(str.indexOf("-")>-1){
	        return mat.Y+"-"+mat.M+"-"+mat.D+" "+mat.H+"-"+mat.m+"-"+mat.s;
	    }
	}