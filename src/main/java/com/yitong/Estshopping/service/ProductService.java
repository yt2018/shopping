package com.yitong.Estshopping.service;

import java.util.Date;
import java.util.List;

import com.yitong.Estshopping.entity.Product;

public interface ProductService {
	
	/**
	 * 查询商品的页数
	 */
	public Integer countProduct();
	/**
	 * 查询商品的页数
	 */
	public Integer countProductno(String on);
	
	
	/**
	 * 保存商品
	 */
	public void saveProduct(Product product);
	
	/**
	 * 得到一个商品
	 */
	public Product getProduct(Integer pid);
	
	/**
	 * 删除一个商品
	 */
	public void deleteProduct(Product product);
	
	/**
	 * 保存商品
	 */
	public void updateProduct(Product product);

	public Product getPrId(Date date);

	public List<Product> getProductList(Integer page, Integer limit);


	public void updateProductIsHot(String isHot, Integer pid);


	public void updateProductIsState(String isState, Integer pid);

	/**
	 * 查询被下架的商品
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> getProductListno(Integer page, Integer limit,String isState);
	/**
	 * 根据二级目录csid查询商品集合
	 * @param csid
	 * @param page
	 * @return
	 */
	public List<Product> getProductListcid(Integer csid, Integer page,Integer limit);
	/**
	 * 查询总数
	 * @param csid
	 * @return
	 */
	public Integer countProduct(Integer csid);
	/**
	 * 一级目录查询商品
	 * @param cid
	 * @return
	 */
	public List<Product> getProductListone(Integer cid, Integer page,Integer limit);
	/**
	 * 一级目录查询总数
	 * @param cid
	 * @return
	 */
	public Integer countProductone(Integer cid);
	/**
	 * 最新发布的商品
	 * @param i
	 * @return
	 */
	public List<Product> getProductdate(Integer page,Integer limit);
	public Integer countProductdate();
	/**
	 * 更新访问量
	 * @param f
	 * @param g 
	 */
	public void updateProductPnub(Integer pid, Integer pnub);
	/**
	 * 访问量最高的
	 */
	public List<Product> getProductPnub(Integer page, Integer limit);
	/**
	 * 热门商品
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> getProductPremen(Integer page, Integer limit);
	/**
	 * 热门数量
	 * @return
	 */
	public Integer countProductdatePremen();
	/**
	 * 随机查询
	 * @return
	 */
	public List<Product> getProductListRAND();
	/**
	 * 查询用户所发的所有商品
	 * @param uid
	 * @return
	 */
	public Integer getcount(Integer uid);
	/**
	 * 查询访问量总量
	 * @param uid
	 * @return
	 */
	public Integer sumnub(Integer uid);
	/**
	 * 查询访问量总量
	 * @param uid
	 * @return
	 */
	public List<Product> getuserProductList(Integer uid);
	/**
	 * 查询用户的所有商品
	 * @param uid
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> getMyAllproduct(Integer uid, Integer page, Integer limit,String on);
	/**
	 * 查询总数
	 * @param uid
	 * @return
	 */
	public Integer getMyAllproductCount(Integer uid,String on);
	/**
	 * 更新数量
	 * @param pid
	 * @param nume
	 */
	public void updateProductInventory(Integer pid, Integer nume);
	/**
	 * 模糊搜索
	 * @param key
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> listSearch(String key, Integer page, Integer limit);
	/**
	 * 模糊搜索数量
	 * @param key
	 * @return
	 */
	public Integer listSearchcount(String key);
	
}
