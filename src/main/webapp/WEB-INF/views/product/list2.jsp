<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<title>所有商品列表</title>
	<%-- <link rel="stylesheet" href="${basePath}/res/css/categorylist.css" media="all" /> --%>
	<link rel="stylesheet" href="${basePath}/res/css/productinfo.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<div class="productbody">
			<div id="wrap">
				<div class="pic">
					<div class="pic-cover">
						<img src="${basePath}/res/productinfo/img/1.jpg" width="400" height="400" />
						<div class="cover"></div>
						<div class="show"></div>
					</div>
				</div>
				<div class="tab">
					<ul>
						<li class="on"><img src="${basePath}/res/productinfo/img/1.jpg" /></li>
						<li ><img src="${basePath}/res/productinfo/img/2.jpg" /></li>
						<li><img src="${basePath}/res/productinfo/img/3.jpg" /></li>
						<li><img src="${basePath}/res/productinfo/img/4.jpg" /></li>
						<li><img src="${basePath}/res/productinfo/img/5.jpg" /></li>
					</ul>
				</div>
			</div>
			<div class="productinfo">
				<h1>抽褶分割丝绒棒球服外套有耳uare原创设计师17AW好好过系列</h1>
				<p>这一次丝绒的魔法，中和了温柔、帅气和质感。最后用抽褶工艺，放大绒面反光，让廓形抽紧——那叫一个潇洒利落！</p>
				<div class="layui-row promo-meta">
					<div class="layui-col-xs2">
						原价格
					</div>
					<div class="layui-col-xs10" style="color: red;">
						￥<del>6</del>
					</div>
					<div class="layui-col-xs2">
						促销价
					</div>
					<div class="layui-col-xs10" style="color: red;">
						登录即可享受优惠价
					</div>
				</div>
				<div class="layui-row promo-youhui">
					<div class="layui-col-xs2">
						优惠
					</div>
					<div class="layui-col-xs10" style="color: red;">
						本店满5件即可减3元
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						配送
					</div>
					<div class="layui-col-xs10" style="color: red;">
						本校内免费配送<br>
						校外根据店长情况,收取邮费
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						数量
					</div>
					<div class="layui-col-xs10">
						 <input type="text" name="number" lay-verify="required|number" autocomplete="off" class="layui-input"value="1" style="width: 60px;display: inline-table;">
						<span>件 (库存38件)</span>
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						声明
					</div>
					<div class="layui-col-xs10" style="color: red;">
						本站为了学生方便提供闲置物品存放网站<br>
						为了确实商品是否属实,建议当面交易<br>
						本站只提供商品上架，联系服务，不承担商品交易风险<br>
						若有商品虚假，进行举报，网站管理员尽快处理。<br>
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">&nbsp;
					</div>
					<div class="layui-col-xs10">
						<button class="layui-btn layui-btn-warm">立即购买</button>
						<button class="layui-btn layui-btn-danger">加入购物车</button>
						<button class="layui-btn layui-btn-primary">举报</button>
					</div>
				</div>
			</div>
			<div class="userinfo">
				<a href="#">
					<div class="user_icon">
						<img src="${basePath}/res/productinfo/img/2.jpg"/>
					</div>
					<div class="username">
						<h1>圓梦</h1>
						<p>个人店部</p>
					</div>
					<div class="layui-row">
						<div class="layui-col-xs4">
							商品
							<p>125件</p>
						</div>
						<div class="layui-col-xs4" style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
							访问量
							<p>125件</p>
						</div>
						<div class="layui-col-xs4">
							贡献
							<p>125件</p>
						</div>
					</div>
				</a>
				<div class="tauser">
					<button class="layui-btn layui-btn-warm layui-btn-mini">Ta的主页</button>
					<button class="layui-btn layui-btn-mini">发消息</button>
				</div>
				<div class="zuijin">
					<fieldset class="layui-elem-field layui-field-title">
						<legend>最近发布</legend>
					</fieldset>
					<ul>
						<li><a href="">冬衣</a></li>
						<li><a href="">冬衣</a></li>
						<li><a href="">冬衣</a></li>
						<li><a href="">冬衣</a></li>
						<li><a href="">冬衣</a></li>
						<li><a href="">冬衣</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="productbody">
			<div class="p_desc">
				<div class="layui-tab" lay-filter="test1">
					<ul class="layui-tab-title">
						<li class="layui-this">商品详情</li>
						<li>商品参数</li>
						<li class="pinglun " data-pid = "222">评论</li>
					</ul>
					<div class="layui-tab-content">
						<div class="layui-tab-item layui-show">
							1. 高度默认自适应，也可以随意固宽。
							<br>2. Tab进行了响应式处理，所以无需担心数量多少。
						</div>
						<div class="layui-tab-item">内容2</div>
						<div class="layui-tab-item">评论</div>
					</div>
				</div>
			</div>
			<div class="producbian">
				<p class="sousuo">
					<input type="text" placeholder="请输入关键字...">
					<a href="#" class="s_a">搜索</a>
				</p>
				<fieldset class="layui-elem-field layui-field-title">
						<legend>广告投放</legend>
					</fieldset>
				<a href="" class="ad">
					<img src="${basePath}/res/productinfo/img/1.jpg"/>
					<p>欧兰雅广告</p>
				</a>
				<a href="" class="ad">
					<img src="${basePath}/res/productinfo/img/2.jpg"/>
					<p>欧兰雅广告</p>
				</a>
				<a href="" class="ad">
					<img src="${basePath}/res/productinfo/img/3.jpg"/>
					<p>欧兰雅广告</p>
				</a>
			</div>
		</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<script src="${basePath}/res/js/productImg.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
