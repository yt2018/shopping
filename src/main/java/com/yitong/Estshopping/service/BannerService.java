package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.entity.Banner;
import com.yitong.Estshopping.entity.User;

public interface BannerService {
	/**
	 * banner保存
	 * @param banner
	 */
	void save(Banner banner);
	/**
	 * 根据信息获取banner
	 * @param banner
	 * @return
	 */
	Banner getbanner(Banner banner);
	/**
	 * 更新banner
	 * @param banner
	 */
	void update(Banner banner);
	/**
	 * 用户查询列表
	 * @param user
	 * @param state
	 * @param page
	 * @param limit
	 * @return
	 */
	List<Banner> list(User user, Integer state, Integer page, Integer limit);
	/**
	 * 用户查询列表数量
	 * @param user
	 * @param state
	 * @param page
	 * @param limit
	 * @return
	 */
	Integer listcount(User user, Integer state, Integer page, Integer limit);
	/**
	 * 广告ID查询
	 * @param bid
	 * @return
	 */
	Banner getbannerid(Integer bid);
	/**
	 * 修改广告状态
	 * @param bid
	 * @param bimgStates
	 */
	void updatebimgStates(Integer bid, Integer bimgStates);
	/**
	 * 删除数据
	 * @param valueOf
	 */
	void del(Integer valueOf);
	/**
	 * 页面展示
	 * @param i
	 * @return
	 */
	List<Banner> indexbannerlist(int i);

}
