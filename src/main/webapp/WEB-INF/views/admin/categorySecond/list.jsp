<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_metaadmin.jsp"></jsp:include>
	<title>二级类目列表</title>
	<%-- <link rel="stylesheet" href="${basePath}/res/css/categorylist.css" media="all" /> --%>
</head>
<body>
	<br/>
	<a class="layui-btn add">添加一级目录</a> <a class="layui-btn add2">添加二级目录</a>
	<br/>
	<br/>
	<div class="layui-collapse" lay-accordion  lay-filter="test">
		<c:forEach var="c" items="${categorys}" varStatus="vs">
			  <div class="layui-colla-item">
			    <h2 class="layui-colla-title">
				    ${vs.index+1} ${c.cname }
				</h2>
				
			    <div class="layui-colla-content">
			    	<c:if test="${empty c.categorySeconds}">
						该一级菜单没有二级菜单,去<a href="JavaScript:void(0)" class="add2" style="color:red">创建</a>吧!
					</c:if>
					<c:if test="${not empty c.categorySeconds}">
			    	<table class="layui-table">
					    <tbody class="news_content">
							<c:forEach items="${c.categorySeconds }" var="cat" varStatus="s">
								<tr>
									<td>${s.index+1}</td>
									<td>${cat.csName }</td>
									<td>${cat.href }</td>
									<td>
										<!-- 修改 -->
										<a data-id="${cat.csId}" data-cname="${cat.csName }" class="change" href="javascript:void(0)">
										<i class="layui-icon" style="font-size: 30px; color: #1E9FFF;">&#xe642;</i>  </a> 
										<!-- 删除 -->	
										<a data-id="${cat.csId}" class="delete" href="javascript:void(0)">
										<i class="layui-icon" style="font-size: 30px; color: #123;">&#xe640;</i> </a>
									</td>
								</tr>
							</c:forEach>
					    </tbody>
					</table>
				  	</c:if>
			    </div>
			  </div>
		</c:forEach>
	</div>
	<div id="pages"></div>
	<script type="text/javascript">
	layui.use(['form','layer','jquery','element','laypage'],function(){
		var form = layui.form(),
			layer = parent.layer === undefined ? layui.layer : parent.layer,
			laypage = layui.laypage,
			$ = layui.jquery,
			element = layui.element;
		var page=${page};
		laypage({ 
			cont : "pages",
			pages : ${count},//总页数    
			curr: page,//选中当前页
			jump: function(obj,first){
			    //obj包含了当前分页的所有参数，比如：
			    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
			    console.log(obj.limit); //得到每页显示的条数
			    if(page!=obj.curr){
			    	window.location.href="${basePath}/admin/listCategorySecond/"+obj.curr;
				}
			}
		})
		$(".downc").click(function(){
			layer.alert('正在开发者,请静候!', {icon: 3});
		})
		//删除
		$(".delete").click(function(){
			var id=$(this).data("id")
			$.get("${basePath}/admin/deleteCategorySecond/"+id+"/"+page+"",function(data){
				layer.msg(data.rest)
				window.location.reload();
			})
			//alert(id+"==="+page)
		})
		//修改
		$(".change").click(function(){
			var cname=$(this).data("cname")
			var csId=$(this).data("id")
			var index = layui.layer.open({
				title : "修改二级类目",
				type : 2,
				area: ['350px', '500px'],
				content : "${basePath}/admin/gotoEditCategorySecond/"+csId,
				success : function(layero, index){
					
				}
			})
		})

		//添加
		$(".add").click(function(){
			var index =layer.prompt({
				  formType: 0,
				  value: '请输入一级目录名称',
				  title: '新增一级目录',
				  resize:false,
				  area: ['300px', '20px'] //自定义文本域宽高
				}, function(value, index, elem){
					$.ajax({
						type:"post",
						url:"${basePath}/admin/addCategory",
						data:{"cname":value},
						async:true,
						success:function(data){
							if(data.flag){
								layer.msg(data.rest)
							}else{
								layer.msg(data.rest)
								layer.close(index);
							}
							//刷新当前页面
							window.location.reload();
						}
					});
				});	
		})
		//添加
		$(".add2").click(function(){
			var index = layui.layer.open({
				title : "新增二级类目",
				type : 2,
				area: ['350px', '500px'],
				content : "${basePath}/admin/gotoAddCategorySecond",
				success : function(layero, index){
					
				}
			})			
		})
			
	})		
	</script>
</body>
</html>
