package com.yitong.framework.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.yitong.Estshopping.entity.Role;
import com.yitong.Estshopping.entity.User;

public class AdminInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("进入拦截处理==="+this.getClass());
		String url = request.getRequestURI();
		HttpSession session = request.getSession();
		Role role = (Role) session.getAttribute("role");
		User user = (User) session.getAttribute("user");
		System.out.println(user);
		if(user!=null&&role!=null){
			System.out.println("----------2");
			return true;
		}else{
			System.out.println("----------3");
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("33333333333333333");
	}

}
