package com.yitong.Estshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.BannerDao;
import com.yitong.Estshopping.entity.Banner;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.BannerService;
@Service("BannerService")
@Transactional
public class BannerServiceImpl implements BannerService {
	@Autowired
	BannerDao bannerDao;
	@Override
	public void save(Banner banner) {
		// TODO Auto-generated method stub
		bannerDao.save(banner);
	}

	@Override
	public Banner getbanner(Banner banner) {
		// TODO Auto-generated method stub
		return bannerDao.getbanner(banner);
	}

	@Override
	public void update(Banner banner) {
		// TODO Auto-generated method stub
		bannerDao.update(banner);
	}

	@Override
	public List<Banner> list(User user, Integer state, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return bannerDao.list(user,state,page,limit);
	}

	@Override
	public Integer listcount(User user, Integer state, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return bannerDao.listcount(user,state,page,limit);
	}

	@Override
	public Banner getbannerid(Integer bid) {
		// TODO Auto-generated method stub
		return bannerDao.get(bid);
	}

	@Override
	public void updatebimgStates(Integer bid, Integer bimgStates) {
		// TODO Auto-generated method stub
		 Banner banner = new Banner();
        banner.setBimgStates(bimgStates);
        bannerDao.update(banner, bid);
		
	}

	@Override
	public void del(Integer bid) {
		// TODO Auto-generated method stub
		 bannerDao.delete(bid);
	}

	@Override
	public List<Banner> indexbannerlist(int i) {
		// TODO Auto-generated method stub
		return bannerDao.indexbannerlist(i);
	}

}
