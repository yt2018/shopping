<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
	<style type="text/css">
		blockquote img{width: 100%}
	</style>
</head>
<title>测试</title>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",11); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
				<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
					<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
					  	<legend>广告 ${empty banneredit ? "申请" : "修改"}</legend>
					</fieldset>
					<form class="layui-form layui-form-pane">
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">名称</label>
								<div class="layui-input-inline">
									<input type="hidden" name="bid"  autocomplete="off" value='${empty banneredit ? "":banneredit.bid}'>
									<input type="text" name="bimgAlt" lay-verify="required" autocomplete="off" class="layui-input" value='${empty banneredit ? "":banneredit.bimgAlt}'>
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">链接地址</label>
								<div class="layui-input-inline">
									<input type="text" name="href" lay-verify="url" autocomplete="off" class="layui-input" value='${empty banneredit ? basePath:banneredit.href}'>
								</div>
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">合作天数</label>
								<div class="layui-input-inline">
									<input type="number" name="datenum" lay-verify="number|datenum" autocomplete="off" class="layui-input" value=${empty banneredit ? 1:banneredit.datenum}>
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">上传图片</label>
								<div class="layui-input-inline">
									<button type="button" class="layui-btn" id="banner">
									  	<i class="layui-icon">&#xe67c;</i>上传图片
									</button>
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">图片主色</label>
								<div class="layui-input-inline">
									<input type="text" name='bimgColor' id="color" value=${empty banneredit ? "#ffffff":banneredit.bimgColor} class="layui-input">
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">广告类型</label>
						    	<div class="layui-input-inline">
							      <select name="bimgClass" lay-verify="required">
							        <option value="" ${not empty banneredit ?"": "selected"}>请选广告类型</option>
							        <option value="1" ${empty banneredit ?"" : banneredit.bimgClass==1 ? "selected" : ""}>banner</option>
							        <option value="2" ${empty banneredit ?"" : banneredit.bimgClass==2 ? "selected" : ""}>主页广告</option>
							        <option value="3" ${empty banneredit ?"" : banneredit.bimgClass==3 ? "selected" : ""}>子页广告</option>
							      </select>
							      </div>
						    </div>
						</div>
						<div class="layui-form-item">
							<div class="layui-inline">
								<div class="layui-input-inline">
									<a href="#" class="layui-btn" lay-submit lay-filter="addbanner">${empty banneredit ? "申请":"修改"}</a>
								</div>
							</div>
						</div>
						<div class="layui-form-item" style="text-align: center;" >
							<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
							  	<legend>图片预览</legend>
							</fieldset>
							<img id="bannerimg" alt='${empty banneredit ? "":banneredit.bimgAlt}' src='${empty banneredit ? "":banneredit.bimgSrc}'/>
						</div>
					</form>
				</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript">
		layui.config({
		  //base: 'res/js/jquery-zclip-master/' //假设这是你存放拓展模块的根目录
		}).use(['layer', 'laytpl', 'form', 'element', 'upload', 'util','jquery','laypage'], function(exports){
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer,
				laytpl = layui.laytpl,
				form = layui.form,
				element = layui.element,
				upload = layui.upload,
				util = layui.util,
				device = layui.device()
				
			var uploadInst = upload.render({
			    elem: '#banner' //绑定元素
			    ,url:  path+'/cloud/upload/watermark?fsrc=test/banner/Upload&uname=${user.name}&uid=${user.uid}&title=' //上传接口
			    ,before: function(obj) {
					layer.load()
				}
			    ,done: function(obj){
			      //上传完毕回调
			    	layer.closeAll('loading')
					console.info(obj)
					if(obj.code==0){
						$("#bannerimg").attr("src",obj.data.src).parent().css("background",obj.data.color);
						$("#color").val(obj.data.color).css("background",obj.data.color)
						layer.msg(obj.msg)
					}else{
						layer.msg(obj.msg)
					}
			    }
			    ,size:2048
			    ,error: function(){
			      //请求异常回调
			      layer.msg("上传失败")
			    }
			})
			form.on('submit(addbanner)', function(data){
				var src=$('#bannerimg').attr('src')
				if (!src){
					layer.msg("还没上传图片!!")
					 return false;
				}
				var bannerdata=data.field;
				bannerdata.bimgSrc=src
				form.render();
				console.info(data);
				
				$.ajax({
					type:"post",
					contentType : "application/json ; charset=utf-8",
					url:"${basePath}/userinfo/bannerSever",
					data:JSON.stringify(bannerdata),
					success:function(data){
						if(!data.code){
							layer.msg(data.msg)
							window.location.href="${basePath}/userinfo/cooperation"
						}else{
							layer.msg("添加失败")
						}
					}
					
				})
				return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
			});
		})
	</script>
</body>
</html>