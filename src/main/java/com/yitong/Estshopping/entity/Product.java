package com.yitong.Estshopping.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SelectBeforeUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 商品表，产品表
 * @author YT
 *
 */

@Table(name="product")
@Entity
@SelectBeforeUpdate
public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pid")
	public Integer pid;
	
	@Column(name="pname", nullable=false, length=255)
	public String pname;
	
	@Column(name="pnamedesc" )
	public String pnamedesc;
	
	public String getPnamedesc() {
		return pnamedesc;
	}

	public void setPnamedesc(String pnamedesc) {
		this.pnamedesc = pnamedesc;
	}
	@Column(name="market_price")
	public Float marketPrice;
	
	@Column(name="shop_price")
	public Float shopPrice;
	@Column(name="pnub")
	public Integer pnub;
	
	@Column(name="inventory")
	public Integer inventory;
	
	@Column(name="pdesc", columnDefinition="text")
	public String pdesc; 
	@Column(name="parameter", columnDefinition="text")
	public String parameter; 
	
	@Column(name="is_hot")
	public String isHot;
	
	@Column(name="is_state")
	public String isState;
	
	@Column(name="pdate")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  
	public Date pdate;
	
	
	@Column(name="imgsrcs", columnDefinition="text")
	public String imgsrcs;
	
	@JoinColumn(name = "csid")
    @ManyToOne
    @JsonIgnore
	public CategorySecond categorySecond;
	

	@JoinColumn(name = "uid")
	@ManyToOne
	@JsonIgnore
	public User user;
	
	


	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public Integer getPnub() {
		return pnub;
	}

	public void setPnub(Integer pnub) {
		this.pnub = pnub;
	}

	public String getIsState() {
		return isState;
	}

	public void setIsState(String isState) {
		this.isState = isState;
	}
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	public List<Image> image;
	

	public List<Image> getImage() {
		return image;
	}

	public void setImage(List<Image> image) {
		this.image = image;
	}

	public User getUser() {
		return user;
	}

	public String getImgsrcs() {
		return imgsrcs;
	}


	public void setImgsrcs(String imgsrcs) {
		this.imgsrcs = imgsrcs;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public Float getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(Float marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Float getShopPrice() {
		return shopPrice;
	}

	public void setShopPrice(Float shopPrice) {
		this.shopPrice = shopPrice;
	}

	public Integer getInventory() {
		return inventory;
	}


	public void setInventory(Integer inventory) {
		this.inventory = inventory;
	} 

	public String getPdesc() {
		return pdesc;
	}

	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}

	public String getIsHot() {
		return isHot;
	}

	public void setIsHot(String isHot) {
		this.isHot = isHot;
	}

	public Date getPdate() {
		return pdate;
	}

	public void setPdate(Date pdate) {
		this.pdate = pdate;
	}

	public CategorySecond getCategorySecond() {
		return categorySecond;
	}

	public void setCategorySecond(CategorySecond categorySecond) {
		this.categorySecond = categorySecond;
	}

	@Override
	public String toString() {
		return "Product [pid=" + pid + ", pname=" + pname + ", pnamedesc=" + pnamedesc + ", marketPrice=" + marketPrice
				+ ", shopPrice=" + shopPrice + ", pnub=" + pnub + ", inventory=" + inventory + ", pdesc=" + pdesc
				+ ", parameter=" + parameter + ", isHot=" + isHot + ", isState=" + isState + ", pdate=" + pdate
				+ ", imgsrcs=" + imgsrcs + ", categorySecond=" + categorySecond + ", user=" + user + ", image=" + image
				+ "]";
	}
	

}
