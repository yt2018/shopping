package com.test;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yitong.Estshopping.dao.AdminDao;
import com.yitong.Estshopping.dao.CategorySecondDao;
import com.yitong.Estshopping.dao.ImageDao;
import com.yitong.Estshopping.dao.ProductDao;
import com.yitong.Estshopping.dao.RoleDao;
import com.yitong.Estshopping.dao.UserDao;
import com.yitong.Estshopping.dao.WalletLogDao;
import com.yitong.Estshopping.service.AdminService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.ProductService;

//spring测试类的启动方式
/*@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-context.xml")*/
public class TestSpringService {
	/*//@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	protected Session getCurrentSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	@Autowired
	private AdminDao adminDao;
	@Autowired
	private CategorySecondDao categorySecondDao;
	@Autowired
	private AdminService adminService;
	@Autowired
	private ImageDao imageDao;
	@Autowired
	private ImageService imageService;
	@Autowired
	private ProductDao productDao;
	@Autowired
	private ProductService productService;
	@Autowired
	private UserDao userDao;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private WalletLogDao walletlogdao;*/
	/*@Test
	public void name() {
		String hql="from Image";
		List<Image> findByClassify = imageDao.find(hql);
		if(findByClassify.isEmpty()) {
			System.out.println(findByClassify.isEmpty());
		}
		if(findByClassify.size()==0) {
			System.out.println("2222222222222222222222222");
		}
		for (Image image : findByClassify) {
			System.out.println("-------"+image);
		}
	}
	@Test
	public void eq() {
		List<Image> classify = imageService.classify();
		List<String> imgsrclist = new ArrayList<String>(); 
		for (Image image : classify) {
			String imgsrc = image.getImgsrc();
			imgsrclist.add(imgsrc);
		}
		System.out.println("长度----"+imgsrclist.size());
		for (String string : imgsrclist) {
			System.out.println(string);
		}
		
		//ImageUtils.eq(new File("H:\\workspace\\ESTshopping\\src\\main\\webapp\\res\\images"), "404.gif");
		ImageUtils.eqs(new File("H:\\workspace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\ESTshopping\\upload"),imgsrclist);
		System.out.println("清除完毕");
	}
	@Test
	public void eqs() {
		Image image = imageDao.getimg("/ESTshopping/upload/3/1/18186b61-e60d-4b5d-a2cf-8dd691284b36.jpg");
		System.out.println(image.getImgid());
	}
	@Test
	public void sssss() {
		List<Product> productList = productDao.getProductList(6, 10);
		System.out.println(productList);
		for (Product product : productList) {
			System.out.println(product.getUser().getName());
		}
	}*/
	/*@Test
	public void sadfsdfgh() {
		List<RoleViwe> quanxianList = roleDao.quanxianList(1,10,null);
		System.out.println(quanxianList);
	}*/
	/*@Test
	public void sadfsdfghs() {
		boolean resetPassage = userDao.changePassage(8, "tong118.com", "123456");
		System.out.println(resetPassage);
	}*/
	/*@Test
	public void q() {
		//获取开始时间
		long startTime=System.currentTimeMillis();
		//查询数据表
		//String hql="show tables";
		//查询数据库
		String hql="show databases";
		//查看用户
		//String hql="select user()";
		//创建数据库
		//String hql="create database abcjong";
		//删除数据库
		//String hql="DROP DATABASE abcjong;";
		//查询当前数据库结构
		//String hql="select database()";
		//当前数据库版本
		//String hql="select version()";
		//当前时间
		//String hql="select now()";
		EntityManager createEntityManager = sessionFactory.createEntityManager();
		Query createNativeQuery = createEntityManager.createNativeQuery(hql);
		System.out.println(createNativeQuery);
		//List<RoleViwe> resultList = createNativeQuery.getResultList();
		//System.out.println(resultList);
		for (RoleViwe string : resultList) {
			System.out.println("数据表"+string);
		}
		long endTime=System.currentTimeMillis(); //获取结束时间
		 
		System.out.println("程序运行时间： "+(endTime-startTime)+"ms");
	}*/
	//@Test
	/*public void qsdfgs() {
		//获取开始时间
		long startTime=System.currentTimeMillis();
		//查询数据表
		//String hql="show tables";
		//查询数据库
		//String hql="show databases";
		//查看用户
		//String hql="select user()";
		//创建数据库
		//String hql="create database abcjong";
		//删除数据库
		String hql="select * from roleviwe";
		//查询当前数据库结构
		//String hql="select database()";
		//当前数据库版本
		//String hql="select version()";
		//当前时间
		//String hql="select now()";
		EntityManager createEntityManager = sessionFactory.createEntityManager();
		Query createNativeQuery = createEntityManager.createNativeQuery(hql);
		System.out.println(createNativeQuery);
		Iterator iterator = createNativeQuery.setFirstResult(0).setMaxResults(10).getResultList().iterator();
		//Object json = JSON.toJSON(iterator);
		//System.out.println(json);
		RoleViwe rViwe=null;
		List<RoleViwe> list= new ArrayList<>();
		while (iterator.hasNext()) {
			Object[] next = (Object[]) iterator.next();
			System.out.println(next[8]);
			rViwe=new RoleViwe(next[0], next[1], next[2],next[3], next[4], next[5], next[6], next[7], next[8], next[9]);
			list.add(rViwe);
		}
		Object json = JSON.toJSON(list);
		System.out.println(json);
		long endTime=System.currentTimeMillis(); //获取结束时间
		
		System.out.println("程序运行时间： "+(endTime-startTime)+"ms");
	}*/
	
	/*@Test
	public void hjgkhk() {
		// 驱动程序名  
        String driver = "com.mysql.jdbc.Driver";  
  
        // URL指向要访问的数据库名student  
        // String url = "jdbc:mysql://localhost/student";  
        String url = "jdbc:mysql://127.0.0.1:3306/estshopping";  
  
        // MySQL配置时的用户名  
        String user = "root";   
  
        // MySQL配置时的密码  
        String password = "123456";  
        PreparedStatement pstmt=null;
        ResultSet rs=null;
        try {   
            // 加载驱动程序  
            Class.forName(driver);  
  
            // 连续数据库  
            Connection conn = DriverManager.getConnection(url, user, password);  
            if(!conn.isClosed())   
                System.out.println("数据库连接成功");  
            // statement用来执行SQL语句  
            Statement statement = conn.createStatement();  
            //查询数据库
            //String hql="show databases";
           // String hql="select column_name, column_comment from information_schema.columns where table_schema ='estshopping' and table_name = 'user' ;";
            String hql="mysqldump estshopping> e : mydb. SQL - u root - p 123456";
            boolean t = statement.execute(hql);
            //rs= statement.executeQuery(hql);
            System.out.println(t);
            System.out.println(rs);
            while (rs.next()) {
            	System.out.println(rs.getString(1)); 
			}
            
            // 要执行的SQL语句  
            //String sql = "DROP database abcjong";  
            
            // 结果集  
            //boolean execute = statement.execute(sql);
            System.out.println("执行完成");
            //rs.close();  
            conn.close(); 
        } catch(ClassNotFoundException e) {  
            System.out.println("对不起没有连接到数据库");   
            e.printStackTrace();  
        } catch(SQLException e) {  
            e.printStackTrace();  
        } catch(Exception e) {  
            e.printStackTrace();  
        }   
	}*/
}
