package com.yitong.Estshopping.dao;

import java.util.Date;
import java.util.List;

import com.yitong.Estshopping.entity.Categorys;
import com.yitong.Estshopping.entity.Product;

public interface ProductDao extends BaseDao<Product>{
	
	/**
     * 查询二级分类的总的个数
     *
     * @return
     */
    public Integer countProduct();

    /**
     * 通过时间查询商品
     * @param date
     * @return
     */
	public Product getPrId(Date date);
	/**
	 * 
	 * @param page 当前页
	 * @param limit 查询数量
	 * @return
	 */
	public List<Product> getProductList(Integer page, Integer limit);
	/**
	 * 管理员设置商品是否热门
	 * @param isHot
	 * @param pid
	 */
	public void updateProductIsHot(String isHot, Integer pid);

	public void updateProductIsState(String isState, Integer pid);
	
	/**
	 * 根据二级目录csid查询商品集合
	 * @param csid
	 * @param page
	 * @return
	 */
	public List<Product> getProductListcid(Integer csid, Integer page,Integer limit);
	/**
	 * 查询商品
	 * @param csid
	 * @return
	 */
	public Integer countProduct(Integer csid);
	/**
	 * 一级菜单查询
	 * @param cid
	 * @param page
	 * @return
	 */
	public List<Product> findByCategoryId(Integer cid, Integer page ,Integer limit);
	/**
	 * 一级目录查询总数
	 * @param cid
	 * @return
	 */
	public Integer countProductone(Integer cid);
	/**
	 * 最新商品发布
	 * @param i
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> getProductdate(Integer page, Integer limit);
	/**
	 * 最新商品发布数量
	 * @param i
	 * @param page
	 * @param limit
	 * @return
	 */
	public Integer countProductdate();
	/**
	 * 跟新访问量
	 * @param f
	 * @param pnub 
	 */
	public void updateProductPnub(Integer pid, Integer pnub);
	/**
	 * 访问量最高
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> getProductPnub(Integer page, Integer limit);
	/**
	 * 
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> getProductPremen(Integer page, Integer limit);

	public Integer countProductdatePremen();
	/**
	 * 随机查询
	 * @return
	 */
	public List<Product> getProductListRAND();
	/**
	 * 查询用户的商品数量
	 * @param uid
	 * @return
	 */
	public Integer getcount(Integer uid);
	/**
	 * 访问量求和,按用户查询商品
	 * @param uid
	 * @return
	 */
	public List<Product> getcountsum(Integer uid);
	/**
	 * 查询用户的商品
	 * @param uid
	 * @param page
	 * @param limit
	 * @param on 
	 * @return
	 */
	public List<Product> getMyAllproduct(Integer uid, Integer page, Integer limit, String on);
	/**
	 * 查询自己发布的所有的商品
	 * @param uid
	 * @param on 
	 * @return
	 */
	public Integer getMyAllproductCount(Integer uid, String on);
	/**
	 * 更新数量
	 */
	public void updateProductInventory(Integer pid, Integer nume);
	/**
	 * 下架的商品管理
	 * @param page
	 * @param limit
	 * @param isState
	 * @return
	 */
	public List<Product> getProductListno(Integer page, Integer limit, String isState);
	/**
	 * 下架的总数量
	 * @param on 
	 * @return
	 */
	public Integer countProductno(String on);
	
	 /**
	  * 模糊搜索
	  * @param key
	  * @return
	  */
	 public List<Product> findSearch(String key,Integer page, Integer limit);
	/**
	 * 模糊搜索关键词
	 * @param key
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Product> listSearch(String key, Integer page, Integer limit);
	/**
	 * 模糊搜索关键词数量
	 * @param key
	 * @return
	 */
	public Integer listSearchcount(String key);
	
}
