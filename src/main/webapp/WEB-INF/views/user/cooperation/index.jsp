<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>与我合作</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
	<style type="text/css">
		blockquote img{width: 100%}
	</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",11); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
			<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
			  	<legend>公告</legend>
			</fieldset>
			<blockquote class="layui-elem-quote">
				与我们合作，线上合作 渠道合作 线上合作，广告合作，友情链接<br/>
				<span style="color:red">禁止上传违规广告,违规者两次直接封号</span><br/>
				申请流程:<br/><img alt="申请流程" src="${basePath}/res/images/liuc.jpg">
			</blockquote>
			<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
			 	 <legend>我要加入</legend>
			</fieldset>
			<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
				<div class="layui-row">
				    <div class="layui-col-xs3 layui-col-md3">
				     	 <div class="grid-demo grid-demo-bg1">
				     	 	<a href="${basePath}/userinfo/cooperation/addindex" class="layui-btn">申请加入</a>
				     	 </div>
				    </div>
				 </div>
			</div>
			<table class="layui-table">
				<colgroup>
					<col width="100">
					<col>
					<col width="150">
					<col width="150">
					<col width="100">
					<col width="150">
				</colgroup>
				<thead>
					<tr>
						<th>图片</th>
						<th>申请内容</th>
						<th>申请链接</th>
						<th>合作天数</th>
						<th>申请状态</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty banner }">
						<tr>
							<th colspan="6">暂无数据</th>
						</tr>
					</c:if>
					<c:if test="${not empty banner }">
					<c:forEach var="b" items="${banner }">
						<tr>
							<th style="background:${b.bimgColor};    text-align: center;"><img src="${b.bimgSrc }" height="30"></th>
							<th>${b.bimgAlt }</th>
							<th>${b.href }</th>
							<th>${b.datenum}</th>
							<th>${b.bimgStates==0?"<span class='layui-badge '>审核中</span>":b.bimgStates==1?"<span class='layui-badge layui-bg-green '>审核通过</span>":"<span class='layui-badge '>被禁止</span>"} </th>
							<th>
								<c:if test="${b.bimgStates==0}">
									<a href="">查看</a>
								</c:if>
								<c:if test="${b.bimgStates==1}">
									<a href="${basePath }/userinfo/cooperation/editindex/${b.bid}" class="layui-btn layui-btn-mini">修改</a>
								</c:if>
									<a href="javascript:;" class="layui-btn layui-btn-mini layui-btn-red bdel" data-bid="${b.bid}">删除</a>
							</th>
						</tr>
					</c:forEach>
					</c:if>
				</tbody>
			</table>
			<div id="pagedemo">
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript">
			layui.use(['form','layer','jquery','element','laypage'],function(){
				var form = layui.form,
					layer = parent.layer === undefined ? layui.layer : parent.layer,
					laypage = layui.laypage,
					$ = layui.jquery,
					element = layui.element;
				$(".bdel").on("click",function(){
					var bid=$(this).data("bid")
					console.info(bid)
					layer.confirm('你确定要删除吗?',{icon: 3, title:'提示'},function(index){
						$.get('${basePath}/userinfo/cooperationbannerlist/del',{"bid":bid},function(d){
							layer.msg(d.msg,{time: 4000},function(){
								layer.close(index);
								location.reload();
								
							})
						});
						})
				})
				//当前页
				var page=${page};
				var limit=${limit};
				laypage.render({ 
					 elem: 'pagedemo'
					,curr: page//选中当前页 
					,limit: limit
					,limits:[10, 20, 30, 40, 50]
  					,count: ${count} //数据总数，从服务端得到 
  					,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
					,jump: function(obj,first){
					    //obj包含了当前分页的所有参数，比如：
					    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
					    console.log(obj.limit); //得到每页显示的条数
					    if(page!=obj.curr){
					    	window.location.href="?page="+obj.curr+"&limit="+obj.limit;
						}
					}
				})
			})
		</script>
</body>
</html>