package com.yitong.Estshopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 客户的电子钱包，在创建客户的时候生成，用触发器
 * 
 * @author YT
 *
 */
@Entity
@Table(name = "walletlog")
public class Walletlog {

	public Walletlog() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Walletlog( Float money, Integer states, String wif, String oif, String content, Date logtime,
			User user) {
		super();
		this.money = money;
		this.states = states;
		this.wif = wif;
		this.oif = oif;
		this.content = content;
		this.logtime = logtime;
		this.user = user;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "wlogid")
	private Integer wlogid;
	/**
	 * 支付金额
	 */
	@Column(name = "money")
	private Float money;
	/**
	 * 支付状态,1消费,2收入,3充值(第三方),4,充值卡,5退货
	 */
	@Column(name = "states")
	private Integer states;
	/**
	 * 支付订单号
	 */
	@Column(name = "wif")
	private String wif;
	/**
	 * 数据单号
	 */
	@Column(name = "oif")
	private String oif;
	/**
	 * 支付内容
	 */
	@Column(name = "content",columnDefinition="text")
	private String content;
	/**
	 * 消费日期
	 */
	@Column(name="log_time")
    private Date logtime;
	/**
	 * 用户id
	 */
	@JoinColumn(name = "uid")
    @ManyToOne
    private User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getWlogid() {
		return wlogid;
	}
	public void setWlogid(Integer wlogid) {
		this.wlogid = wlogid;
	}
	public Float getMoney() {
		return money;
	}
	public void setMoney(Float money) {
		this.money = money;
	}
	public String getWif() {
		return wif;
	}
	public void setWif(String wif) {
		this.wif = wif;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getLogtime() {
		return logtime;
	}
	public void setLogtime(Date logtime) {
		this.logtime = logtime;
	}
	public Integer getStates() {
		return states;
	}
	public void setStates(Integer states) {
		this.states = states;
	}
	public String getOif() {
		return oif;
	}
	public void setOif(String oif) {
		this.oif = oif;
	}
}
