<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<title>所有商品列表</title>
	<%-- <link rel="stylesheet" href="${basePath}/res/css/categorylist.css" media="all" /> --%>
	<style type="text/css">
		tbody .layui-table-cell ,.layui-table-view .layui-table td {
		    height: 60px;
		}
	</style>
</head>
<body>
	<br/>
	<div class="layui-btn-group demoTable">
		<a class="layui-btn add">添加商品</a>
		<button class="layui-btn" data-type="getCheckData">批量删除</button>
		<!-- <button class="layui-btn" data-type="getCheckLength">获取选中数目</button>
		<button class="layui-btn" data-type="isAll">验证是否全选</button> -->
	</div>
	<table class="layui-table" id="test" lay-data="{
		id: 'idTest'
		,height:600
		, url:'${basePath}/admin/${url}'
		, page:true
		, id:'idTest'
		,limits: [10,20,30,60,100]
		,limit: 10
		,loading:true
		}"
		lay-filter="demo">
		<thead>
			<tr>
				<th lay-data="{checkbox:true, fixed: true}"></th>
				<th lay-data="{field:'pid', width:80, sort: true, fixed: true}">商品ID</th>
				<th lay-data="{field:'pname', width:300,templet:'#href' }">商品名称</th>
				<!-- <th lay-data="{field:'imgsrcs', width:300,heigth:60 ,templet:'#ingsrc'}">商品图片</th> -->
				<th lay-data="{field:'marketPrice', width:80}">市场价(元)</th>
				<th lay-data="{field:'shopPrice', width:177}">促销价(元)</th>
				<th lay-data="{field:'inventory', width:80}">库存量(件)</th>
				<th lay-data="{field:'isHot', width:100, sort: true,fixed: 'right',toolbar:'#isHot'}">是否热门</th>
				<th lay-data="{fixed: 'right', width:200, align:'center', toolbar: '#barDemo'}">操作</th>
			</tr>
		</thead>
	</table>
	<script type="text/html" id="ingsrc">
		  {{# layui.each((d.imgsrcs).split(','), function(index, item){}}
			<img width=80 src="{{item }}"/>
		{{# })}}
	</script>
	<script type="text/html" id="href">
		 <a target="_blank" href="${basePath}/Productinfo/{{d.pid}}" class="layui-table-link">{{d.pname}}</a>
	</script>
	<script type="text/html" id="isHot">
		 {{#  if(d.isHot == 'on'){ }}
    		<input type="checkbox" data-pid="{{d.pid}}" checked name="is" lay-skin="switch" lay-filter="switchTest" lay-text="是|否" />
  			{{#  }else{ }} 
    		<input type="checkbox" data-pid="{{d.pid}}" name="is" lay-skin="switch" lay-filter="switchTest" lay-text="是|否" />
  			{{#  } }} 
	</script>
	<script type="text/html" id="barDemo">
		{{#  if(d.isState == 'on'){ }}
    		<input type="checkbox" data-pid="{{d.pid}}" checked name="is" lay-skin="switch" lay-filter="switchTest2" lay-text="正常|停止" />
  			{{#  }else{ }} 
    		<input type="checkbox" data-pid="{{d.pid}}" name="is" lay-skin="switch" lay-filter="switchTest2" lay-text="正常|停止" />
  			{{#  } }} 
		<a class="layui-btn layui-btn-primary layui-btn-mini" href="${basePath}/Productinfo/{{d.pid}}" target="_blank">查看</a>
		<a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="del">删除</a>
	</script>
	<script>
	layui.use(['form','table','layer','jquery'], function(){
		var table = layui.table;
		var form=layui.form;
		var $=layui.jquery;
		
		//监听表格复选框选择
		table.on('checkbox(demo)', function(obj){
		    console.log(obj)
		});
		
		form.on('switch(switchTest)', function(data) {
			/* layer.msg('是否热门：' + (this.checked ? 'true' : 'false'), {
				offset: '6px'
			}); */
			var dom =$(data.elem);
			var is
			this.checked ? is='on' : is='off'
			updata(is,dom,"updateIsHot")
		});
		form.on('switch(switchTest2)', function(data) {
			/* layer.msg('是否热门：' + (this.checked ? 'true' : 'false'), {
				offset: '6px'
			}); */
			var dom =$(data.elem);
			var is
			this.checked ? is='on' : is='off'
			updata(is,dom,"updateIsState")
			if(is!='on'){
				$(data.elem).parents("tr").find('input[type="checkbox"]').removeAttr("checked");
			}
			form.render()
		});
		function updata(is,dom,url){
			
			var index  = layer.msg('修改中，请稍候',{icon: 16,time:false,shade:0.8});
	       	setTimeout(function(){
	        	$.ajax({
					url : "${basePath}/admin/"+url,
					type : "get",
					dataType : "json",
					data:{"is":is,"pid":dom.data("pid")},
					success : function(data){
						layer.close(index);
						layer.msg(data.msg);
					},
					error :function(){
						layer.msg("网络超时");
					}
				})
	        },200);	
		}
		  //监听工具条
		table.on('tool(demo)', function(obj){
			var data = obj.data;
			if(obj.event === 'del'){
				layer.confirm('真的删除行么', function(index){
				    $.ajax({
						url : "${basePath}/admin/delProduct",
						type : "post",
						dataType : "json",
						data:{"pid":data.pid},
						success : function(data){
							obj.del();
						    layer.close(index);
							layer.msg(data.msg);
						},
						error :function(){
							layer.msg("网络超时");
						}
					})
				});
			}
		});
		 
		var $ = layui.$, active = {
		    getCheckData: function(){ //获取选中数据
		    	var index  = layer.msg('批量删除中，请稍候',{icon: 16,time:false,shade:0.8});
				var checkStatus = table.checkStatus('idTest')
				,data = checkStatus.data;
				var i,arr=[];
				for(i in data){
					//console.info(data[i].pid)
					arr.push(data[i].pid)
				}
				if(isEmpty(arr)){
					layer.msg('请选择删除的数据');
					return;
				}
		    	console.info(arr);
		    	console.info(arr.lenght);
		    	layer.confirm('请谨慎,删除数据不可恢复', function(index){
			    	$.ajax({
						url : "${basePath}/admin/delProducts",
						type : "post",
						dataType : "json",
						data:{"pids":"["+arr+"]"},
						success : function(data){
							table.reload('idTest',{
								url:"${basePath}/admin/${url}"
							})
						    layer.close(index);
							layer.msg(data.msg);
						},
						error :function(){
							layer.msg("网络超时");
						}
					})
				})
			}
			/* ,getCheckLength: function(){ //获取选中数目
				var checkStatus = table.checkStatus('idTest')
				,data = checkStatus.data;
				layer.msg('选中了：'+ data.length + ' 个');
			}
			,isAll: function(){ //验证是否全选
				var checkStatus = table.checkStatus('idTest');
				layer.msg(checkStatus.isAll ? '全选': '未全选')
			} */
		};
		$('.demoTable .layui-btn').on('click', function(){
			var type = $(this).data('type');
			console.info(type)
			active[type] ? active[type].call(this) : '';
		});
		//添加
		$(".add").click(function(){
			var index = layer.open({
				title : "新增商品",
				type : 2,
				area: ['800px', '600px'],
				content : "${basePath}/admin/addProduct",
				cancel: function(index, layero){ 
					if(confirm('商品未保存确定要关闭么')){ //只有当点击confirm框的确定时，该层才会关闭
						//关闭后清除缓存文件
				    	$.get("${basePath}/imagesdelete");
						layer.close(index)
				  	}
				  	return false; 
				}    
			})			
		})
		/* window.onload =function(){ 
			$(".laytable-cell-1-imgsrcs").each(function(){
				var c=$(this).html();
				//$(this).html(c)
				console.info(c)
				/* var t=c.split(',');
				var html=""
				for(var i in arr){
					html+="<img width='60' src='"+arr[i]+"' />"
				} 
				
			})
		} */
	});
	function isEmpty(obj) {
	    // 本身为空直接返回true
	    if (obj == null) return true;

	    // 然后可以根据长度判断，在低版本的ie浏览器中无法这样判断。
	    if (obj.length > 0)    return false;
	    if (obj.length === 0)  return true;

	    //最后通过属性长度判断。
	    for (var key in obj) {
	        if (hasOwnProperty.call(obj, key)) return false;
	    }

	    return true;
	}
	</script>
</body>
</html>
