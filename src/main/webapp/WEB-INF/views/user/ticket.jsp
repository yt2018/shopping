<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>优惠券</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",9); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
			<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
				<div class="layui-row">
				    <div class="layui-col-xs12 layui-col-md4">
				     	 <div class="grid-demo grid-demo-bg1"><a href="${basePath}/userinfo/ticket/add" class="layui-btn">发行优惠券</a></div>
				    </div>
				    <div class="layui-col-xs6 layui-col-md4">
				      	<div class="grid-demo" style="font-size: 20px">总数量</div>
				    </div>
				    <div class="layui-col-xs6 layui-col-md4">
				      	<div class="grid-demo grid-demo-bg2" style="font-size: 20px">有效优惠券</div>
				    </div>
				 </div>
			</div>
			<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
			  	<legend>公告</legend>
			</fieldset>
			<blockquote class="layui-elem-quote">
				卖家只能发布自己的优惠券。<br/>
				平台为了方便大家,请勿大量发行优惠券。
			</blockquote>
			<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
			 	 <legend>优惠券</legend>
			</fieldset>
			<table class="layui-table">
				<colgroup>
					<col width="200">
					<col>
					<col width="150">
					<col width="150">
					<col width="100">
					<col width="100">
				</colgroup>
				<thead>
					<tr>
						<th>优惠劵单号</th>
						<th>内容</th>
						<th>起始日期</th>
						<th>结束日期</th>
						<th>金额</th>
						<th>状态</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th colspan="6">暂无数据</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
</body>
</html>