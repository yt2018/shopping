package com.yitong.Estshopping.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yitong.Estshopping.service.BannerService;
@Controller
public class AdminBannerController {
	
	@Autowired
	private BannerService bannerService;
	
	@RequestMapping(value="/admin/banner")
	public String showIndex(HttpSession session){
		return "admin/banner/index";
	}	
}
