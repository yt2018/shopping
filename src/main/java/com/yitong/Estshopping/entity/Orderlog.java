package com.yitong.Estshopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 订单日志表
 * @author YT
 *
 */
@Entity
@Table(name = "orderslog")
public class Orderlog {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ologid")
    private Integer ologid;
	@Column(name="log_time")
    private Date logtime;
	/**
	 * 订单号
	 */
	@Column(name="logoif")
    private String logoif;
	/**
	 * 商品标题
	 */
	@Column(name="pname")
	private String pname;
	/**
	 * 商品id
	 */
	@Column(name="pid")
	private Integer pid;
	@Column(name="logobj",columnDefinition="text")
	private String logobj;
	/**
	 * 1:未付款   
	 * 2:订单已经付款   
	 * 3:已经发货   
	 * 4:订单结束   
	 * 5:客户删除定单 
	 * 6:商家删除订单  
	 * 7:商家修改地址  
	 * 8:客户修改地址  
	 * 9:商家修改价格  
	 */
	@Column(name="state")
    private Integer state;
	/**
	 * 操作者
	 */
    @JoinColumn(name = "uid")
    @ManyToOne
    private User user;
    /**
     * 客户者
     */
    @JoinColumn(name = "cid")
    @ManyToOne
    private User cuser;
    /**
     * 操作者
     */
    @JoinColumn(name = "kid")
    @ManyToOne
    private User kuser;
	public void setOlogid(Integer ologid) {
		this.ologid = ologid;
	}
	public Date getLogtime() {
		return logtime;
	}
	public void setLogtime(Date logtime) {
		this.logtime = logtime;
	}
	public String getLogoif() {
		return logoif;
	}
	public void setLogoif(String logoif) {
		this.logoif = logoif;
	}
	public String getLogobj() {
		return logobj;
	}
	public void setLogobj(String logobj) {
		this.logobj = logobj;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Orderlog [ologid=" + ologid + ", logtime=" + logtime + ", logoif=" + logoif + ", logobj=" + logobj
				+ ", state=" + state + ", user=" + user + "]";
	}
	public User getCuser() {
		return cuser;
	}
	public void setCuser(User cuser) {
		this.cuser = cuser;
	}
	public User getKuser() {
		return kuser;
	}
	public void setKuser(User kuser) {
		this.kuser = kuser;
	}
	public Integer getOlogid() {
		return ologid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	
}
