<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>基本设置</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
<style type="text/css">
.red{color:red}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",2); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>

		<div class="fly-panel fly-panel-user" pad20>
				<div class="layui-tab layui-tab-brief" lay-filter="user">
					<ul class="layui-tab-title" id="LAY_mine">
						<li class="layui-this" lay-id="info">我的资料</li>
						<li lay-id="avatar">头像</li>
						<li lay-id="pass">密码</li>
						<li lay-id="bind">帐号绑定</li>
					</ul>
					<div class="layui-tab-content" style="padding: 20px 0;">
						<div class="layui-form layui-form-pane layui-tab-item layui-show">
							<form class="layui-form">
								<div class="layui-form-item">
									<label for="L_email" class="layui-form-label"><span class="red">* </span>QQ</label>
									<div class="layui-input-inline">
										<input type="text" id="L_QQ" name="qq" required lay-verify="number" autocomplete="off" value="${user.qq }" class="layui-input">
									</div>
									<div class="layui-form-mid layui-word-aux">为了方便联系请填写正确的QQ号
										<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=${user.qq }&site=qq&menu=yes" style="font-size: 12px; color: #4f99cf;">点击验证QQ号</a>。</div>
								</div>
								<div class="layui-form-item">
									<label for="L_username" class="layui-form-label"><span class="red">* </span>用户名</label>
									<div class="layui-input-inline">
										<input type="text" id="username" name="userName" required lay-verify="userName" autocomplete="off" value="${user.userName }" class="layui-input">
									</div>
									<div class="layui-form-mid layui-word-aux">
										作为登录账号
									</div>
								</div>
								<div class="layui-form-item">
									<label for="L_username" class="layui-form-label"><span class="red">* </span>昵称</label>
									<div class="layui-input-inline">
										<input type="text" id="name" name="name" required lay-verify="required" autocomplete="off" value="${user.name}" class="layui-input">
									</div>
									<div class="layui-inline">
										<div class="layui-input-inline">
											<c:if test="${user.sex=='男' }">
												<input type="radio" name="sex" value="男" checked title="男">
												<input type="radio" name="sex" value="女" title="女">
											</c:if>
											<c:if test="${user.sex=='女' }">
												<input type="radio" name="sex" value="男" title="男">
												<input type="radio" name="sex" value="女" checked title="女">
											</c:if>
										</div>
									</div>
								</div>
								<div class="layui-form-item">
									<label for="L_email" class="layui-form-label"><span class="red">* </span>邮箱</label>
									<c:if test="${empty user.email }">
									<div class="layui-input-inline">
										<input type="text" id="L_email" name="email" required lay-verify="email" autocomplete="off" value="${user.email}" class="layui-input">
									</div>
									</c:if>
									<c:if test="${not empty user.email }">
									<div class="layui-input-inline">
										<input type="text" id="L_email" name="email" required lay-verify="email" autocomplete="off" value="${user.email}" class="layui-input">
									</div>
									<div class="layui-form-mid layui-word-aux">如果您在邮箱已激活的情况下，变更了邮箱，需
										<a href="activate.html" style="font-size: 12px; color: #4f99cf;">重新验证邮箱</a>。</div>
									</c:if>
								</div>
								<div class="layui-form-item">
									<label for="L_city" class="layui-form-label"><span class="red">* </span>手机号码</label>
									<div class="layui-input-inline">
										<input type="text" id="L_phone" name="phone" autocomplete="off" lay-verify="phone" value="${user.phone}" class="layui-input">
									</div>
								</div>
								<div class="layui-form-item">
									<label for="L_city" class="layui-form-label">收货地址</label>
									<div class="layui-input-inline">
										<input type="text" id="L_city" name="addr" autocomplete="off" value="${user.addr}" class="layui-input">
									</div>
								</div>
								<div class="layui-form-item layui-form-text">
									<label for="L_sign" class="layui-form-label">签名</label>
									<div class="layui-input-block">
										<textarea placeholder="随便写些什么刷下存在感" id="L_sign" name="sign" autocomplete="off" class="layui-textarea" style="height: 80px;">${user.sign}</textarea>
									</div>
								</div>
								<div class="layui-form-item">
									<button class="layui-btn" key="set-mine" lay-filter="set-mine" lay-submit="set-mine">确认修改</button>
								</div>
							</form>
						</div>
						<div class="layui-form layui-form-pane layui-tab-item">
							<div class="layui-form-item">
								<div class="avatar-add">
									<p>建议尺寸168*168，支持jpg、png、gif，最大不能超过2MB</p>
									<button type="button" class="layui-btn upload-img" id="icon">
					                  <i class="layui-icon">&#xe67c;</i>上传头像
					                </button>
									<img src="${user.icon }" id="iconimg">
									<span class="loading"></span>
								</div>
							</div>
						</div>

						<div class="layui-form layui-form-pane layui-tab-item">
							<form  class="layui-form">
								<div class="layui-form-item">
									<label for="L_nowpass" class="layui-form-label">当前密码</label>
									<div class="layui-input-inline">
										<input type="password" id="L_nowpass" name="oldpassword" required lay-verify="required" autocomplete="off" class="layui-input">
									</div>
									<div class="layui-form-mid layui-word-aux">QQ绑定用户默认密码为tong118.com,忘记密码联系<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=1810258114&site=qq&menu=yes" style="font-size: 12px; color: #4f99cf;">网站管理员</a>初始登录密码</div>
								</div>
								<div class="layui-form-item">
									<label for="L_pass" class="layui-form-label">新密码</label>
									<div class="layui-input-inline">
										<input type="password" name="pass" id="pass" required lay-verify="pass" autocomplete="off" class="layui-input">
									</div>
									<div class="layui-form-mid layui-word-aux">6到16个字符</div>
								</div>
								<div class="layui-form-item">
									<label for="L_repass" class="layui-form-label">确认密码</label>
									<div class="layui-input-inline">
										<input type="password" name="newpassword"  id="newpassword" required lay-verify="newpassword" autocomplete="off" class="layui-input">
									</div>
								</div>
								<div class="layui-form-item">
									<button class="layui-btn" lay-filter="updatepassword" lay-submit="updatepassword">确认修改</button>
								</div>
							</form>
						</div>

						<div class="layui-form layui-form-pane layui-tab-item">
							<ul class="app-bind">
							<c:if test="${not empty user.qquser }">
								<li class="fly-msg app-havebind">
									<i class="iconfont icon-qq"></i>
									<span>已成功绑定，您可以使用QQ帐号直接登录Est商城，当然，您也可以</span>
									<a href="javascript:;" class="acc-unbind" id="relieveQQ">解除绑定</a>
								</li>
							</c:if>
							<c:if test="${empty user.qquser }">
								<li class="fly-msg app-havebind">
									<i class="iconfont icon-qq"></i>
									<span>没有绑定，您可以使用QQ帐号直接登录Est商城，当然，您也可以</span>
									<a href="${basePath}/qqlogin" class="acc-unbind" type="qq_id">立即绑定</a>
								</li>
							</c:if>
								<li class="fly-msg">
									<a href="javascript:;" class="layui-btn layui-btn-mini" style="color:#fff" id="cancellation">注销账号</a>
									<span>本站做了层层安全防护,网管不会泄露你的信息。注销账号会删除所有信息,请你冷静下。如果本网站做的不足请反馈<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=1810258114&site=qq&menu=yes" style="font-size: 12px; color: #4f99cf;">网站管理员</a>(qq:1810258114)，网管小哥一定会尽力建设美好的校园平台。</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
</body>
</html>