package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.PayEntity;
import com.yitong.Estshopping.entity.User;

public interface PayInfoService {
	
	/**
	 * 订单生成
	 * @param order
	 */
	public void save(PayEntity payEntity);
	/**
	 * 根据订单查询数据
	 * @param out_trade_no
	 * @return
	 */
	public PayEntity getOut_trade_no(String out_trade_no);
	/**
	 * 更新
	 * @param payEntity
	 */
	public void update(PayEntity payEntity);
	
	
	

}
