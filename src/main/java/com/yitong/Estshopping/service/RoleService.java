package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.bean.RoleViwe;
import com.yitong.Estshopping.entity.Role;

public interface RoleService {
	/**
	 * 查询角色列表
	 * @param page
	 * @param limit
	 * @param object
	 * @return
	 */
	List<Role> roleList(Integer page, Integer limit, Object object);
	/**
	 * 查询数量
	 * @param object
	 * @return
	 */
	Integer countRole(Object object);
	/**
	 * 权限列表
	 * @param page
	 * @param limit
	 * @param object
	 * @return
	 */
	List<RoleViwe> quanxianList(Integer page, Integer limit, Object object);
	/**
	 * 权限数量
	 * @param object
	 * @return
	 */
	Integer quanxianList(Object object);
	
}
