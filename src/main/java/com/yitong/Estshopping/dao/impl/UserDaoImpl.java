package com.yitong.Estshopping.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.UserDao;
import com.yitong.Estshopping.entity.User;
import com.yitong.framework.utils.MD5Utils;
import com.yitong.framework.utils.UpdateClass;

@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao{

	@Override
	public User findByUserName(String userName) {
		// TODO Auto-generated method stub
		String hql ="from User u where u.userName = :username";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("username", userName);
		return (User) query.uniqueResult();
	}

	@Override
	public User findByUserNameAndPassword(String userName, String password) {
		// TODO Auto-generated method stub
		String hql = "from User u where u.userName = :username and u.password = :password";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("username", userName);
		query.setParameter("password", password);
		return (User) query.uniqueResult();
	}

	@Override
	public User findByUserQQ(String openID) {
		// TODO Auto-generated method stub
		String hql ="from User u where u.qquser = :openID";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("openID", openID);
		return (User) query.uniqueResult();
	}

	@Override
	public void updateUser(User user) throws IllegalArgumentException, IllegalAccessException {
		// TODO Auto-generated method stub
		User user2 = this.get(user.getUid());
		UpdateClass updateClass = new UpdateClass();
		User userupdate = (User) updateClass.combineSydwCore(user, user2);
		this.update(userupdate);
	}

	@Override
	public boolean changePassage(Integer uid, String oldpassword, String newpassword) {
		// TODO Auto-generated method stub
		boolean flag = false;
		User user = this.get(uid);
		String oldpwd = MD5Utils.getMD5Str(oldpassword+ user.getKey());
		System.out.println(oldpwd);
		System.out.println(user.getPassword());
		if(oldpwd.equals(user.getPassword())) {
			String newpwd = MD5Utils.getMD5Str(newpassword+ user.getKey());
			user.setPassword(newpwd);
			this.update(user);
			flag=true;
		}
		return flag;
		
	}
	@Override
	public boolean resetPassage(Integer uid, String newpassword) {
		// TODO Auto-generated method stub
		boolean flag = false;
		User user = this.get(uid);
		String newpwd = MD5Utils.getMD5Str(newpassword+ user.getKey());
		user.setPassword(newpwd);
		this.update(user);
		flag=true;
		return flag;
		
	}

	@Override
	public void relieveQQ(Integer uid) {
		// TODO Auto-generated method stub
		User user = this.get(uid);
		user.setQquser(null);
		this.update(user);
		
	}

	@Override
	public List<User> userList(Integer page, Integer limit, Integer states) {
		// TODO Auto-generated method stub
		String hql = null;
		if(states==null) {
			hql ="from User u order by u.joindate desc";
		}else {
			hql ="from User u where u.state="+states+" order by u.joindate desc";
		}
		return find(hql,page, limit);
	}

	@Override
	public Integer countUsers(Integer states) {
		String hql = null;
		if(states==null) {
			hql ="select count(*) from User u order by u.joindate desc";
		}else {
			hql ="select count(*) from User u where u.state='"+states+"' order by u.joindate desc";
		}
		return count(hql);
	}

}
