package com.yitong.Estshopping.dao;

import com.yitong.Estshopping.entity.Packet;

/**
 * 卡包的Dao接口
 * @author YT
 *
 */
public interface PacketDao extends BaseDao<Packet> {

}
