<%@page import="com.yitong.Estshopping.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
	<title>登录--易通通</title>
	<meta name="keywords" content="登录,易通通,全国在校大学生创业平台,大学生创业平台,EST商城,易斯顿美术学院" />
	<meta name="description" content="登录,
	校园二手电商网站，实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	
	<jsp:include page="/WEB-INF/views/_meta/_meta.jsp"></jsp:include>
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/login.css" media="all" />
</head>
<body>
<% User u=(User)session.getAttribute("user"); 
	if(u!=null){
		response.sendRedirect("/");
	}
%>
<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<div class="login_bg">
		<div class="login_content">
			<div class="login">
				<h1>EST电商网站登录</h1>
				<form class="layui-form">
					<div class="layui-form-item">
						<input class="layui-input" name="userName" placeholder="用户名" lay-verify="required" type="text" autocomplete="off">
					</div>
					<div class="layui-form-item">
						<input class="layui-input" name="password" placeholder="密码" lay-verify="password" type="password" autocomplete="off">
					</div>
					<div class="layui-form-item form_code">
						<input class="layui-input" id="checkcode" placeholder="验证码" lay-verify="required" type="text" autocomplete="off">
						<div class="code" id="checkcode"><img id="codetf" src="${basePath}/getCheckCodeImage" width="116" height="38" style="background: url("${basePath}/res/css/img/loading-1.gif") no-repeat center center;"></div>
					</div>
					<div class="layui-form-item">
						<a href="#">忘了密码?</a>
						<a href="${basePath}/userRegister" class="fr">注册账号</a>
					</div>
					<button class="layui-btn login_btn" lay-submit="login" lay-filter="login">登录</button>
				</form>
				<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
					<legend>使用第三方登录</legend>
					<br>
					推荐使用QQ登录<a href="${basePath}/qqlogin"><img src="http://qzonestyle.gtimg.cn/qzone/vas/opensns/res/img/bt_white_76X24.png" alt="bt_white_76X24.png"></a>
					
				</fieldset>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	
	<script type="text/javascript">
	layui.use(['form','layer','jquery'],function(){
		var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		$ = layui.jquery;
		form.verify({
			password: [
			    /^[\S]{6,12}$/
			    ,'密码必须6到12位，且不能出现空格'
			]
		});
		function code(){
			var imgurl="${basePath}/getCheckCodeImage"
			//加随机数防止缓存不刷新
			$.get(imgurl, function(data){
				$("#codetf").attr("src",imgurl+"?"+Math.random(10));
				$("#checkcode").val("").focus();
			});
		}
		$("#codetf").on("click",function(){
			layer.msg("点击了")
			code()
		});
		
		
		form.on("submit(login)",function(data){
			//layer.msg(JSON.stringify(data.field));
			console.log(JSON.stringify(data.field))
			var checkcode= $("#checkcode").val()
			$.ajax({
				type:"post",
				contentType : "application/json ; charset=utf-8",
				url:"${basePath}/login/"+checkcode,
				data:JSON.stringify(data.field),
				success:function(data){
					console.log(data)
					if(data.errorCheckCode){
						layer.msg("验证码错误");
						code();
						return;
					}
					if(data.state==1){
						layer.msg(data.result);
						$("input[name='userName']").focus();
						code();
						return;
					}else if(data.state==2){
						layer.msg(data.result);	
						$("input[name='password']").focus();
						code();
						return;
					}else if(data.state==0){
						layer.msg(data.result);	
						layer.load(1);
						window.location.href = data.url;
						return;
					}
				}
			});
			return false;
		})
	})

	</script>
</body>
</html>