layui.use('jquery', function (url) {
    var $ = layui.jquery;
	//文章条数
	var html='';
	var url="../getIndex.json"
	d()
	function d(tag){
/*		console.info(tag)*/
		$.getJSON(url,function(data){
			//分类导航
			$.each(data.tag,function(infoIndex,info){
				html+='<a href="javascript:layer.msg(&#39;'+info+'&#39;)"  data-tag="'+info+'">'+info+'</a>'
				$('#tag').html(html)
				/*console.info(infoIndex)
				console.info(info)*/
			})
			//显示文章条数
			html='';
			$.each(data.art,function(infoIndex,info){
/*				console.info(infoIndex)*/
				if(tag==undefined||tag==(info['tag'])){
					html+='<div class="article shadow">'
							+'	<div class="article-left">'
							+'		<img src="'+info['img']+'" alt="'+info['title']+'" />'
							+'	</div>'
							+'	<div class="article-right">'
							+'		<div class="article-title">'
							+'			<a href="'+info['href']+'">'+info['title']+'</a>'
							+'		</div>'
							+'		<div class="article-abstract">'
							+			info['artic']
							+'		</div>'
							+'	</div>'
							+'	<div class="clear"></div>'
							+'	<div class="article-footer">'
							+'		<span><i class="fa fa-clock-o"></i>&nbsp;&nbsp;'+info['time']+'</span>'
							+'		<span class="article-author"><i class="fa fa-user"></i>&nbsp;&nbsp;'+info['author']+'</span>'
							+'		<span><i class="fa fa-tag"></i>&nbsp;&nbsp;<a href="javascript:layer.msg(\'启动标签。。。。正在修复中\')">'+info['tag']+'</a></span>'
							+'		<span class="article-viewinfo"><i class="fa fa-eye"></i>&nbsp;'+info['eye']+'</span>'
							+'		<span class="article-viewinfo"><a href="javascript:layer.msg(\'启动评论 。。。。正在修复中\')"><i class="fa fa-commenting"></i>&nbsp;'+info['commenting']+'</a></span>'
							+'	</div>'
							+'</div>';
				}else{
					html='<div class="article shadow">【'+tag+'】没上线！！！</div>';
				}
				$('.art').html(html)
			})
			
		})
		//复空
		html='';
		$.each(data.recommended,function(infoIndex,info){
			html+='<li><i class="fa-li fa fa-hand-o-right"></i>        '
					+'	<a href="'+info['href']+'">'+info['title']+'</a>'
					+'</li>';
			$('.recommended').html(html)
		})
		
	}
	$(document).on('click','#tag>a', function(){
	    var othis = $(this), tag = othis.data('tag');
	    d(tag)
	});
	
})