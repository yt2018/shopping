<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
		<div class="fly-header layui-bg-black">
			<div class="layui-container">
				<a class="fly-logo" href="${basePath}">
					<img src="${basePath}/res/images/logo.svg" alt="ESTshopping" height="45">
				</a>
				<ul class="layui-nav fly-nav layui-hide-xs">
					<li class="layui-nav-item layui-this">
						<a href="javascript:layer.msg('正在开发中,请静候.....')"><i class="iconfont icon-jiaoliu"></i>论坛</a>
					</li>
					<li class="layui-nav-item">
						<a href="javascript:layer.msg('正在开发中,请静候.....')"><i class="iconfont icon-iconmingxinganli"></i>社区</a>
					</li>
				</ul>
				<c:if test="${empty user }">
					<ul class="layui-nav fly-nav-user">
						<li class="layui-nav-item layui-this">
						<a href="${basePath}/userLogin">登录</a>
					</li>
					<li class="layui-nav-item">
						<a href="${basePath}/userRegister">注册</a>
					</li>
					</ul>
				</c:if>
				<c:if test="${not empty user }">
				<ul class="layui-nav fly-nav-user">

					<li class="layui-nav-item">
						<a class="fly-nav-avatar" href="javascript:;">
							<cite class="layui-hide-xs">${user.name }</cite>
							<i class="iconfont ${not empty user.phone ? "icon-renzheng":"" } layui-hide-xs" title="认证信息：${user.name }"></i>
							<c:if test="${empty user.phone }">
							<i class="layui-badge fly-badge-vip layui-hide-xs">未认证</i>
							</c:if>
							<c:if test="${not empty user.phone }">
							<i class="layui-badge fly-badge-vip layui-hide-xs" style="background: green;">认证</i>
							</c:if>
							<img src="${user.icon }">
						</a>
						<dl class="layui-nav-child">
							<c:if test="${not empty role.rolename }">
							<dd>
								<a href="${basePath}/admin"><i class="layui-icon">&#xe628;</i>${role.rolename }</a>
							</dd>
							</c:if>
							<dd>
								<a href="${basePath}/userinfo/set"><i class="layui-icon">&#xe620;</i>基本设置</a>
							</dd>
							<dd>
								<a href="${basePath}/userinfo/message"><i class="iconfont icon-tongzhi" style="top: 4px;"></i>我的消息</a>
							</dd>
							<dd>
								<a href="${basePath}/userinfo"><i class="layui-icon" style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>我的主页</a>
							</dd>
							<hr style="margin: 5px 0;">
							<dd>
								<a href="${basePath}/quit" style="text-align: center;">退出</a>
							</dd>
						</dl>
					</li>
				</ul>
				</c:if>
			</div>  
		</div>
		
		