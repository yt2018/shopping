package com.yitong.Estshopping.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.UserDao;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.UserService;
import com.yitong.framework.utils.MD5Utils;
import com.yitong.framework.utils.StringTools;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;

	@Override
	public void registerUser(User user) {
		// TODO Auto-generated method stub
		String key = StringTools.getStringRamdom(6);
		user.setState(1);
		user.setCode(StringTools.getUUID());
		user.setKey(key);
		String pwd = MD5Utils.getMD5Str(user.getPassword() + user.getKey());
		user.setPassword(pwd);
		user.setJoindate(new Date());
		userDao.save(user);
	}

	@Override
	public User existUser(String userName) {
		// TODO Auto-generated method stub
		return userDao.findByUserName(userName);
	}

	@Override
	public User findByUserNameAndPassword(User user) {
		// TODO Auto-generated method stub
		//user是多页面传入的参数的user对象
		//euser是数据库里存在user对象
		User euser = existUser(user.getUserName());
		String pwd = MD5Utils.getMD5Str(user.getPassword() + euser.getKey());
		return userDao.findByUserNameAndPassword(user.getUserName(), pwd);
	}

	@Override
	public User findByUserQQ(String openID) {
		// TODO Auto-generated method stub
		return userDao.findByUserQQ(openID);
	}

	@Override
	public User findUserId(Integer uid) {
		// TODO Auto-generated method stub
		return userDao.get(uid);
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		try {
			userDao.updateUser(user);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean changePassage(Integer uid, String oldpassword, String newpassword) {
		return userDao.changePassage(uid,oldpassword,newpassword);
	}

	@Override
	public void relieveQQ(Integer uid) {
		// TODO Auto-generated method stub
		userDao.relieveQQ(uid);
	}

	@Override
	public List<User> userList(Integer page, Integer limit, Integer states) {
		// TODO Auto-generated method stub
		return userDao.userList(page,limit,states);
	}

	@Override
	public Integer countUsers(Integer states) {
		// TODO Auto-generated method stub
		return userDao.countUsers(states);
	}

}
