package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.CategorySecond;
import com.yitong.Estshopping.entity.Categorys;

/**
 * 二级类目的Dao接口
 * @author YT
 *
 */
public interface CategorySecondDao extends BaseDao<CategorySecond> {

	/**
     * 查询二级分类的总的个数
     *
     * @return
     */
    public Integer countCategorySecond();

    /**
     * 分页查找所有二级分类
     *
     * @param page
     * @return
     */
    public List<CategorySecond> findAll(Integer page);
    
    
    /**
     * 获取所有的二级类目
     */
    public List<CategorySecond> findAll();
    /**
     * 根据一级菜单查询二级菜单
     */
    public List<CategorySecond> findAllList(Integer cid);
    /**
     * 查找是否存在
     * @param csname
     * @return
     */
	public CategorySecond categorySecondfind(String csname);
	/**
	  * 模糊搜索
	  * @param key
	  * @return
	  */
	 public List<CategorySecond> findSearch(String key);

}
