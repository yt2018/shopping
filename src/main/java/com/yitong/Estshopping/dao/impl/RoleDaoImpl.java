package com.yitong.Estshopping.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.yitong.Estshopping.bean.RoleViwe;
import com.yitong.Estshopping.dao.RoleDao;
import com.yitong.Estshopping.entity.Role;
@Service("roleDao")
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao  {
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<Role> roleList(Integer page, Integer limit, Object object) {
		// TODO Auto-generated method stub
		
		String hql = null;
		if(object==null) {
			hql ="from Role r";
		}else {
			//hql ="from Role r where u.state="+object+" order by u.joindate desc";
		}
		return find(hql,page, limit);
	}

	@Override
	public Integer countRole(Object object) {
		// TODO Auto-generated method stub
		String hql = null;
		if(object==null) {
			hql ="select count(*) from Role r";
		}else {
			//hql ="select count(*) from User u where u.state='"+object+"' order by u.joindate desc";
		}
		return count(hql);
	}

	@Override
	public List<RoleViwe> quanxianList(Integer page, Integer limit, Object object) {
		// TODO Auto-generated method stub
		String hql="";
		if(object==null) {
			hql="select * from roleviwe";
		}else {
			
		}
		EntityManager createEntityManager = sessionFactory.createEntityManager();
		Query q = createEntityManager.createNativeQuery(hql);
		Iterator iterator = q.setFirstResult((page - 1)*limit).setMaxResults(limit).getResultList().iterator();
		RoleViwe rViwe=null;
		List<RoleViwe> list= new ArrayList<>();
		while (iterator.hasNext()) {
			Object[] next = (Object[]) iterator.next();
			System.out.println(next[8]);
			rViwe=new RoleViwe(next[0], next[1], next[2],next[3], next[4], next[5], next[6], next[7], next[8], next[9]);
			list.add(rViwe);
		}
		System.out.println("查询数据"+JSON.toJSON(list));
		return list;
	}

	@Override
	public Integer quanxianList(Object object) {
		// TODO Auto-generated method stub
		String hql="";
		if(object==null) {
			hql="select * from roleviwe";
		}else {
			
		}
		EntityManager createEntityManager = sessionFactory.createEntityManager();
		Query q = createEntityManager.createNativeQuery(hql);
		Iterator iterator = q.getResultList().iterator();
		RoleViwe rViwe=null;
		List<RoleViwe> list= new ArrayList<>();
		while (iterator.hasNext()) {
			Object[] next = (Object[]) iterator.next();
			System.out.println(next[8]);
			rViwe=new RoleViwe(next[0], next[1], next[2],next[3], next[4], next[5], next[6], next[7], next[8], next[9]);
			list.add(rViwe);
		}
		//resultList.get(0);
		return list.size();
	}


}
