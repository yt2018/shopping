<%@page import="com.yitong.Estshopping.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
	<title>登录--易通通</title>
	<meta name="keywords" content="登录,易通通,全国在校大学生创业平台,大学生创业平台,EST商城,易斯顿美术学院" />
	<meta name="description" content="登录,
	校园二手电商网站，实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	
	<link rel="stylesheet" href="${basePath}/res/layui2/css/layui.css" media="all" />
	<jsp:include page="/WEB-INF/views/_meta/_metaadmin.jsp"></jsp:include>
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/login.css" media="all" />
</head>
<body>
<% User u=(User)session.getAttribute("user"); 
	if(u!=null){
		response.sendRedirect("/");
	}
%>

<div style="position: fixed; top:0;left:0;width: 100%;height:50px; background: #123">
	<a href="javascript:history.back()" class="layui-btn" style="position:absolute; top:5px;left:10px">返回上一页</a>
	<a href="${basePath}"  class="layui-btn" style="position: absolute; top:5px;right:10px">返回首页</a>
</div>
			<div class="mlogin">
				<div style="text-align: center;">
				<a href="${basePath}"><img alt="易通通" src="${basePath}/res/images/logo1.png" width="100"></a>
				</div>
				<form class="layui-form">
					<h1>EST电商网站注册</h1>
					<div class="layui-form-item">
							<input type="text" class="layui-input userName" placeholder="用户名" lay-verify="userName" name="userName" placeholder="请输入登录名">
					</div>
					<div class="layui-form-item">
							<input type="text" class="layui-input userEmail" placeholder="QQ" lay-verify="number" name="qq" placeholder="请输入QQ号">
					</div>
					<div class="layui-form-item">
							<input type="text" class="layui-input userEmail" placeholder="邮箱" lay-verify="email" name="email" placeholder="请输入邮箱">
					</div>
					<div class="layui-form-item">
							<input type="text" class="layui-input userEmail" placeholder="昵称" name="name" lay-verify="required" placeholder="请输入昵称">
					</div>
					<div class="layui-form-item">
							<input type="password"  id="pass"class="layui-input password" placeholder="设置密码" lay-verify="pass" placeholder="请设置密码">
					</div>
					<div class="layui-form-item">
							<input type="password" class="layui-input password" placeholder="确认密码" lay-verify="password" name="password" placeholder="请确认密码">
					</div>
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">性别<span class="layui-red">*</span></label>
							<div class="layui-input-block sex">
								<input type="radio" name="sex" value="男" title="男" checked>
								<input type="radio" name="sex" value="女" title="女">
							</div>
						</div>
					</div>
					<div class="layui-form-item">
							<input type="text" class="layui-input userEmail" placeholder="电话" lay-verify="phone" name="phone" placeholder="请输入电话">
					</div>
					
					<div class="layui-form-item">
							<input type="text" class="layui-input address" placeholder="地址" name="address"  placeholder="地址">
					</div>
					<div class="layui-form-item">
						<a href="${basePath}/userLogin" class="fr">我已经有账号，去登陆</a>
					</div>
					<div class="layui-form-item">
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
						<button class="layui-btn fr" lay-submit="" lay-filter="addUser">立即提交</button>
					</div>
				</form>
				<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
					<legend>使用第三方登录</legend>
					<br>
					推荐使用QQ登录<a href="${basePath}/qqlogin"><img src="http://qzonestyle.gtimg.cn/qzone/vas/opensns/res/img/bt_white_76X24.png" alt="bt_white_76X24.png"></a>
					
				</fieldset>
			</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<script type="text/javascript">
	layui.use(['form','layer','jquery'],function(){
		var form = layui.form(),
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		$ = layui.jquery;
		
		form.verify({
			userName:function(value, item){
				 if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
			      return '用户名不能有特殊字符';
			    }
			    if(/(^\_)|(\__)|(\_+$)/.test(value)){
			      return '用户名首尾不能出现下划线\'_\'';
			    }
			    if(/^\d+\d+\d$/.test(value)){
			      return '用户名不能全为数字';
			    }
			}
			,pass: [
			    /^[\S]{6,12}$/
			    ,'密码必须6到12位，且不能出现空格'
			]
			,password:function(value, item){
	            if(!new RegExp($("#pass").val()).test(value)){
	                return "两次输入密码不一致，请重新输入！";
	            }
	        }
		})
		
		form.on("submit(addUser)",function(data){
			//layer.msg(JSON.stringify(data.field));
			//console.log(data.field)
			$.ajax({
				type:"post",
				contentType : "application/json ; charset=utf-8",
				url:"${basePath}/reg",
				data:JSON.stringify(data.field),
				success:function(data){
					console.log(data)
					if(data.usereq){
						layer.msg("用户已存在");
						$(".userName").focus();
					}else{
						layer.msg("注册成功");
						window.location.href = "${basePath}";
					}
				}
			});
			return false;
		})
	})

	</script>
	<jsp:include page="/WEB-INF/views/_meta/m/_footer.jsp"></jsp:include>
	<canvas id="canvas-banner"></canvas>
	<script src="${basePath}/res/timeline/js/canvas.js"></script>
</body>
</html>