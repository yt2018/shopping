<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    					<li class="layui-col-sm4 layui-col-md3 layui-col-lg2">
								<a href="${basePath}/Productinfo/${p.pid }">
									<div class="">
										<c:if test="${not empty p.image}">
											<img lay-src="${p.image[0].src }"/>
										</c:if>
										<c:if test="${empty p.image}">
											<img lay-src="${basePath}/res/images/timg.jpg"/>
										</c:if>
										<p style="color: green">${p.pname }</p>
										<div class="layui-row">
											<div class="layui-col-xs3">
												<div class="picon">
													<img src="${p.user.icon }"/>
												</div>
											</div>
											<div class="layui-col-xs9">
												<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
												<c:if test="${not empty user }">
												<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
												</c:if>
												<c:if test="${empty user }">
												<span style="color: red;">现价: <b>登录即可查看优惠价</b></span>
											</div>
										</div>
										</c:if>
									</div>
								</a>
							</li>