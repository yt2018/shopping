<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>交易记录</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",7); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
			<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
				<blockquote class="layui-elem-quote layui-quote-nm">
					交易记录保存了双方的记录,不能修改,只能记录,方便记录购物记录清单.
				</blockquote>
				<div id="LAY_minemsg" style="margin-top: 10px;">
					<!-- <div class="fly-none">您暂时没有最新消息</div> -->
					<ul class="mine-msg">
						<c:forEach var="ol" items="${orderloglist }">
							<li >
								<blockquote class="layui-elem-quote">
									<c:if test="${user.uid== ol.cuser.uid}">
										<a href="${basePath}/u/${ol.cuser.uid}"><cite>您</cite></a>
									</c:if>
									<c:if test="${user.uid!= ol.cuser.uid}">
										<a href="${basePath}/u/${ol.cuser.uid}"><cite>1${ol.cuser.name }(客户)</cite></a>
									</c:if>
									<c:if test="${ol.state==1 }">拍下了<a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==2 }">订单已经付款<a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==3 }">已经发货 <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==4 }">订单结束 <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==5 }">客户删除定单  <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==6 }">商家删除订单  <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==7 }">商家修改地址  <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==8 }">客户修改地址  <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==9 }">商家修改价格  <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									<c:if test="${ol.state==10 }">未付款30分钟自动删除  <a  target="_blank" href="${basePath}/Productinfo/${ol.pid}"><cite>${ol.pname }</cite></a>的宝贝</c:if>
									订单号为:<a  target="_blank" href="javascript:layer.msg('正在开发中。。。');"><cite>${ol.logoif }</cite></a>
									商家:
									<c:if test="${user.uid== ol.kuser.uid}">
										<a href="${basePath}/u/${ol.cuser.uid}"><cite>您</cite></a>
									</c:if>
									<c:if test="${user.uid!= ol.kuser.uid}">
										<a href="${basePath}/u/${ol.cuser.uid}"><cite>${ol.kuser.name }(商家)</cite></a>
									</c:if>
									<span>时间:${ol.logtime }</span>
								</blockquote>
							</li>
						</c:forEach>
					</ul>
					
					<div id="pagedemo"></div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript">
	layui.config({
		  //base: 'res/js/jquery-zclip-master/' //假设这是你存放拓展模块的根目录
		}).use(['layer', 'laytpl', 'form', 'element', 'upload', 'util','jquery','laypage'], function(exports){
		  
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer,
				laytpl = layui.laytpl,
				form = layui.form,
				element = layui.element,
				upload = layui.upload,
				util = layui.util,
				device = layui.device(),
				page=${page}
				laypage.render({ 
					 elem: 'pagedemo'
					,curr: page//选中当前页  
					,limit: ${limit}
					,limits:[10, 20, 30, 40, 50]
					,count: ${count}//数据总数，从服务端得到 
					,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
					,jump: function(obj,first){
					    //obj包含了当前分页的所有参数，比如：
					    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
					    console.log(obj.limit); //得到每页显示的条数
					    if(page!=obj.curr){
					    	window.location.href="?page="+obj.curr+"&limit="+obj.limit;
						}
					}
				})
		})
	</script>
</body>
</html>