<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>我的宝贝</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
<link rel="stylesheet"
	href="${basePath}/res/css/ribbon.css"
	media="all" />
<style type="text/css">
.red{color:red}
.layui-row li{
	padding: 5px;
	position: relative;
	border: 10px solid transparent;
}
.layui-row li .productbox{
	display:block;
	width:100%;
	height:100%;
	overflow: hidden;
	
}
.layui-row li:hover img{
	transform: scale(1.05);
	 transition: all 0.6s;
}
.layui-row li p{
	position: absolute;
    bottom: 30px;
    left:0;
    background: #00000059;
    width: 94%;
    color:#fee;
    padding:1%;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}
.layui-row li .productbtn{
	position: absolute;
	bottom: 1px;
	right: 10px;
}
.layui-row img{
	position: absolute;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",4); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
			<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
				<c:if test="${empty user.name||empty user.phone||empty user.qq||empty user.addr}">
				 你的个人资料不全,无法发布个人商品,请补全个人资料<a class="layui-btn layui-btn-danger"  href='${basePath}/userinfo/set'>补全资料</a>
				</c:if>
				<c:if test="${not empty user.qq|| not empty user.addr}">
				<a class="layui-btn layui-btn-danger"  href='${basePath}/userinfo/ProductSet'>添加商品</a>
				</c:if>
				
				<div id="LAY_minemsg" style="margin-top: 10px;">
					<!--<div class="fly-none">您暂时没有最新消息</div>-->
					<ul class="layui-row ">
						<c:forEach var="mp" items="${myproduct }">
						
							<li class="layui-col-sm6 layui-col-md4" data-pid="${mp.pid }">
								<c:if test="${mp.isState=='on'}">
								<div class="wrap"><span class="ribbon6">正常</span></div>
								</c:if>
								<c:if test="${mp.isState=='off'}">
								<span class="ribbon1"><span>商品信息违规被下架</span></span>
								</c:if>
								<a target="_block" href="${basePath}/Productinfo/${mp.pid }" class="productbox">
									<img width="${mp.image[0].width }" height="${mp.image[0].height }" src="${mp.image[0].src }"/>
								</a>
								<p>${mp.pname }</p>
								<div class="productbtn">
									<span><i class="layui-icon">&#xe637;</i>:<b><fmt:formatDate value="${mp.pdate}" pattern="yyyy-MM-dd"/></b></span>
									<span>浏览次数:<b>${mp.pnub}</b></span>
									<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-mini productdel" data-dateleid="${mp.pid }">删除</a>
									<c:if test="${mp.isState=='on'}">
									<a target="_block" href="${basePath}/userinfo/ProductSet/${mp.pid}" class="layui-btn layui-btn-red layui-btn-mini">修改</a>
									</c:if>
								</div>
							</li>
						</c:forEach>
					</ul>
					<c:if test="${count==0}"> <c:set var="hide" value="layui-hide"/></c:if>
					<div id="pagedemo" style="clear: both;float: right;" class="${hide }"></div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript">
	var page=${page};
	var limit=${limit};
	var count=${count}
	</script>
	<script type="text/javascript" src="${basePath}/res/js/pli.js"></script>
</body>
</html>