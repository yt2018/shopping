<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
	 <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>请放过我，大神!</title>
<meta name="description" content="请放过我，大神!">
<meta name="keywords"
	content="请放过我，大神!">
<link rel="shortcut icon" href="">

<style type="text/css">
body {
	margin: 0;
	padding: 0;
	position: relative;
	background-image: url(${basePath}/res/images/db.gif);
	background-position: center; /*background-repeat: no-repeat;*/
	width: 100%;
	height: 100%;
	background-size: 100% 100%;
}

#jk {
	width: 30%;
	position: fixed;
	right: 10px;
	bottom: 40px;
}
</style>

</head>
<body id="body" onload="init()">

	<script type="text/javascript"
		src="${basePath}/res/js/threecanvas.js"></script>
	<script type="text/javascript" src="${basePath}/res/js/snow.js"></script>

	<script type="text/javascript">
		var SCREEN_WIDTH = window.innerWidth;//
		var SCREEN_HEIGHT = window.innerHeight;
		var container;
		var particle;//粒子

		var camera;
		var scene;
		var renderer;

		var starSnow = 1;

		var particles = [];

		var particleImage = new Image();
		particleImage.src = '${basePath}/res/images/db.gif';

		function init() {
			//alert("message3");
			container = document.createElement('div');//container：画布实例;
			document.body.appendChild(container);

			camera = new THREE.PerspectiveCamera(60, SCREEN_WIDTH
					/ SCREEN_HEIGHT, 1, 10000);
			camera.position.z = 1000;
			//camera.position.y = 50;

			scene = new THREE.Scene();
			scene.add(camera);

			renderer = new THREE.CanvasRenderer();
			renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
			var material = new THREE.ParticleBasicMaterial({
				map : new THREE.Texture(particleImage)
			});
			//alert("message2");
			for (var i = 0; i < 500; i++) {
				//alert("message");
				particle = new Particle3D(material);
				particle.position.x = Math.random() * 2000 - 1000;

				particle.position.z = Math.random() * 2000 - 1000;
				particle.position.y = Math.random() * 2000 - 1000;
				//particle.position.y = Math.random() * (1600-particle.position.z)-1000;
				particle.scale.x = particle.scale.y = 1;
				scene.add(particle);

				particles.push(particle);
			}

			container.appendChild(renderer.domElement);

			//document.addEventListener( 'mousemove', onDocumentMouseMove, false );
			document
					.addEventListener('touchstart', onDocumentTouchStart, false);
			document.addEventListener('touchmove', onDocumentTouchMove, false);
			document.addEventListener('touchend', onDocumentTouchEnd, false);

			setInterval(loop, 1000 / 60);

		}

		var touchStartX;
		var touchFlag = 0;//储存当前是否滑动的状态;
		var touchSensitive = 80;//检测滑动的灵敏度;
		//var touchStartY;
		//var touchEndX;
		//var touchEndY;
		function onDocumentTouchStart(event) {

			if (event.touches.length == 1) {

				//event.preventDefault();//取消默认关联动作;
				touchStartX = 0;
				touchStartX = event.touches[0].pageX;
				//touchStartY = event.touches[ 0 ].pageY ;
			}
		}

		function onDocumentTouchMove(event) {

			if (event.touches.length == 1) {
				//event.preventDefault();
				var direction = event.touches[0].pageX - touchStartX;
				if (Math.abs(direction) > touchSensitive) {
					if (direction > 0) {
						touchFlag = 1;
					} else if (direction < 0) {
						touchFlag = -1;
					}
					;
					//changeAndBack(touchFlag);
				}
			}
		}

		function onDocumentTouchEnd(event) {
			// if ( event.touches.length == 0 ) {
			// 	event.preventDefault();
			// 	touchEndX = event.touches[ 0 ].pageX ;
			// 	touchEndY = event.touches[ 0 ].pageY ;

			// }这里存在问题
			var direction = event.changedTouches[0].pageX - touchStartX;

			changeAndBack(touchFlag);
		}

		function changeAndBack(touchFlag) {
			var speedX = 25 * touchFlag;
			touchFlag = 0;
			for (var i = 0; i < particles.length; i++) {
				particles[i].velocity = new THREE.Vector3(speedX, -10, 0);
			}
			var timeOut = setTimeout(";", 2000);
			clearTimeout(timeOut);

			var clearI = setInterval(function() {
				if (touchFlag) {
					clearInterval(clearI);
					return;
				}
				;
				speedX *= 0.8;

				if (Math.abs(speedX) <= 1.5) {
					speedX = 0;
					clearInterval(clearI);
				}
				;

				for (var i = 0; i < particles.length; i++) {
					particles[i].velocity = new THREE.Vector3(speedX, -10, 0);
				}
			}, 1000);

		}

		function loop() {
			for (var i = 0; i < particles.length; i++) {
				var particle = particles[i];
				particle.updatePhysics();

				with (particle.position) {
					if ((y < -1000) && starSnow) {
						y += 2000;
					}

					if (x > 1000)
						x -= 2000;
					else if (x < -1000)
						x += 2000;
					if (z > 1000)
						z -= 2000;
					else if (z < -1000)
						z += 2000;
				}
			}

			camera.lookAt(scene.position);

			renderer.render(scene, camera);
		}
	</script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- <script type="text/javascript">
setInterval(function(){
var a = $("<a href='${basePath}/fgme' target='_blank' >test</a>").get(0);
var e = document.createEvent('MouseEvents');
e.initEvent('click', true, true);
a.dispatchEvent(e);
},10)
</script> -->



</body>
</html>
