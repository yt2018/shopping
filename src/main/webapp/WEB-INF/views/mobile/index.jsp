<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>易通通|tong118.com</title>
	<meta name="keywords" content="易斯顿,大学生,tong118,易通通,全国在校大学生创业平台,大学生创业,大学生创业平台,校园二手电商网站,EST商城,易斯顿美术学院" />
	<meta name="description" content="易斯顿校园二手电商网站，易通通实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	<jsp:include page="/WEB-INF/views/_meta/_meta_m.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/WEB-INF/views/_meta/m/_header.jsp"></jsp:include>
		<div class="wappost">
			<div class="ptitle">
				<h2>
					最热门商品 <a href="${basePath}/remen" class="fr" target="_blank">更多查看&gt;</a>
				</h2>
			</div>
			<ul class="layui-row ">
				<c:forEach var="p" items="${productsrm }" begin="0" end="9">
					<li class="layui-col-xs6 layui-col-sm4">
						<a href="${basePath}/Productinfo/${p.pid }">
							<div class="">
								<c:if test="${not empty p.image}">
									<img lay-src="${p.image[0].src }"/>
								</c:if>
								<c:if test="${empty p.image}">
									<img lay-src="${basePath}/res/images/timg.jpg"/>
								</c:if>
								<p style="color: green">${p.pname }</p>
								<div class="layui-row">
									<div class="layui-col-xs3">
										<div class="picon">
											<img src="${p.user.icon }"/>
										</div>
									</div>
									<div class="layui-col-xs9">
										<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
										<c:if test="${not empty user }">
										<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
										</c:if>
										<c:if test="${empty user }">
										<span style="color: red;font-size: 10px">现价: <b>登录即可查看优惠价</b></span>
										</c:if>
									</div>
								</div>
							</div>
						</a>
					</li>
				</c:forEach>
			</ul>
			<a class="back_href" href="${basePath}/remen" target="_blank">更多热门商品查看</a>
		</div>
		<div class="wappost">
			<div class="ptitle">
				<h2>
					最新商品 <a href="${basePath}/zuixin" class="fr" target="_blank">更多查看&gt;</a>
				</h2>
			</div>
			<ul class="layui-row ">
				<c:forEach var="p" items="${productszx }" begin="0" end="9">
					<li class="layui-col-xs6 layui-col-sm4">
						<a href="${basePath}/Productinfo/${p.pid }">
							<div class="">
								<c:if test="${not empty p.image}">
									<img lay-src="${p.image[0].src }"/>
								</c:if>
								<c:if test="${empty p.image}">
									<img lay-src="${basePath}/res/images/timg.jpg"/>
								</c:if>
								<p style="color: green">${p.pname }</p>
								<div class="layui-row">
									<div class="layui-col-xs3">
										<div class="picon">
											<img src="${p.user.icon }"/>
										</div>
									</div>
									<div class="layui-col-xs9">
										<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
										<c:if test="${not empty user }">
										<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
										</c:if>
										<c:if test="${empty user }">
										<span style="color: red;font-size: 10px">现价: <b>登录即可查看优惠价</b></span>
										</c:if>
									</div>
								</div>
							</div>
						</a>
					</li>
				</c:forEach>
			</ul>
			<a class="back_href" href="${basePath}/zuixin" target="_blank">更多最新商品查看</a>
		</div>
		<div class="wappost">
			<div class="ptitle">
				<h2>
					大家都在看商品 <a href="${basePath}/zuixin" class="fr" target="_blank">更多查看&gt;</a>
				</h2>
			</div>
			<ul class="layui-row ">
				<c:forEach var="p" items="${productszg }" begin="0" end="9">
					<li class="layui-col-xs6 layui-col-sm4">
						<a href="${basePath}/Productinfo/${p.pid }">
							<div class="">
								<c:if test="${not empty p.image}">
									<img lay-src="${p.image[0].src }"/>
								</c:if>
								<c:if test="${empty p.image}">
									<img lay-src="${basePath}/res/images/timg.jpg"/>
								</c:if>
								<p style="color: green">${p.pname }</p>
								<div class="layui-row">
									<div class="layui-col-xs3">
										<div class="picon">
											<img src="${p.user.icon }"/>
										</div>
									</div>
									<div class="layui-col-xs9">
										<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
										<c:if test="${not empty user }">
										<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
										</c:if>
										<c:if test="${empty user }">
										<span style="color: red;font-size: 10px">现价: <b>登录即可查看优惠价</b></span>
										</c:if>
									</div>
								</div>
							</div>
						</a>
					</li>
				</c:forEach>
			</ul>
			<a class="back_href" href="${basePath}/zuixin" target="_blank">更多大家都在看商品查看</a>
		</div>
		
		<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
		<jsp:include page="/WEB-INF/views/_meta/m/_footer.jsp"></jsp:include>
		
</body>
</html>