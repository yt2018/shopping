package com.yitong.Estshopping.dao;

import com.yitong.Estshopping.entity.Ticket;

/**
 * 优惠券的Dao接口
 * @author YT
 *
 */
public interface TicketDao extends BaseDao<Ticket> {

}
