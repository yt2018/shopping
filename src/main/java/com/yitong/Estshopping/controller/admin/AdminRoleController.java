package com.yitong.Estshopping.controller.admin;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yitong.Estshopping.bean.RoleViwe;
import com.yitong.Estshopping.entity.Role;
import com.yitong.Estshopping.service.AdminService;
import com.yitong.Estshopping.service.RoleService;

/**
 * 网站管理员--权限管理
 * @author YT
 *
 */

@Controller
@RequestMapping(value="/admin/role")
public class AdminRoleController {
	@Autowired
	private AdminService adminService;
	@Autowired
	private RoleService roleservice;
	
	/**
	 * 用户首页
	 * @return
	 */
	@RequestMapping(value= {"/","/index"})
	public String showIndex(HttpSession session){
		session.setAttribute("url", "getrolelist");
		return "admin/role/list";
	}
	/**
	 * 用户首页
	 * @return
	 */
	@RequestMapping(value= "/quanxian")
	public String quanxian(HttpSession session){
		session.setAttribute("url", "quanxiangetlist");
		return "admin/jurisdiction/list";
	}
	/**
	 * 查询所有用户
	 * @return
	 */
	@RequestMapping(value= "/getrolelist")
	public @ResponseBody Map<String,Object> getuserlist(HttpSession session, @RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit){
		Map<String,Object> map = new HashMap<String,Object>();
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		List<Role> users=roleservice.roleList(page,limit,null);
		map.put("code", 0);
		map.put("msg", "加载成功");
		//总数量
		map.put("count", roleservice.countRole(null));
		//System.out.println(productList);
		map.put("data", users);
		return map;
	}
	/**
	 * 查询用户权限
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value= "/quanxiangetlist")
	public @ResponseBody Map<String,Object> quanxiangetlist(HttpSession session, @RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit){
		Map<String,Object> map = new HashMap<String,Object>();
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		List<RoleViwe> users=roleservice.quanxianList(page,limit,null);
		System.out.println(JSON.toJSON(users));
		map.put("code", 0);
		map.put("msg", "加载成功");
		//总数量
		map.put("count", roleservice.quanxianList(null));
		//System.out.println(productList);
		map.put("data", users);
		return map;
	}
	
}
