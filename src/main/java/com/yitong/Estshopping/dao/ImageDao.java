package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.Image;
import com.yitong.Estshopping.entity.Product;

public interface ImageDao extends BaseDao<Image> {

	public List<Image> find();
	
	public Image getimg(String str);

	public void Updatepid(int iid,Product product );

	public List<Image> getPid(Integer pid);
	

}
