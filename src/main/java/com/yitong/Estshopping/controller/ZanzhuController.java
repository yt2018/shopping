package com.yitong.Estshopping.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class ZanzhuController {
	@RequestMapping(value="zanzhu")
	public String zanzhu(HttpSession session)
    {
        return "zanzhu";
    }
	@RequestMapping(value="timeline")
    public String timeline(HttpSession session)
    {
        return "timeline";
    }
}

