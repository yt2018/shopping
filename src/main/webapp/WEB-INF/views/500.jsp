<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<title>Sorry,你访问的页面出错!</title>
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
	<style type="text/css">
		.box_bg{
			width:100%;
			min-width: 1200px;
			background:#fff;
		}
		.box_content{
			width: 1200px;
			height: 600px;
			background: url(${basePath}/res/images/404.gif) no-repeat center center;
			margin: 0 auto;
		}
		.box_content p{
			text-align: center;
			font-size: 30px;
			padding-top: 520px;
		}
	</style>
</head>
<body>
<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<!--背景动画-->
	<div class="box_bg">
		<div class="box_content">
			<p>Sorry,你访问的页面出错!请联系<a href="http://wpa.qq.com/msgrd?v=3&uin=1810258114&site=qq&menu=yes">网站管理员</a>修复bug</p>
		</div>
	</div>
<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
</body>
</html>