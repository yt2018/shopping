<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
	<title>注册--易通通</title>
	<meta name="keywords" content="注册,易通通,全国在校大学生创业平台,大学生创业平台,EST商城,易斯顿美术学院" />
	<meta name="description" content="注册,
	校园二手电商网站，实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	
	<jsp:include page="/WEB-INF/views/_meta/_metaadmin.jsp"></jsp:include>
	<title>注册--EST电商网站</title>
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
</head>
<body>
<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<style type="text/css">
		.layui-form-item .layui-inline{ width:100%; float:none; }
		*{
			padding: 0;margin: 0;
		}
		.regbg{
			background: #e4e1dd;
			min-width: 1200px;
		}
		.regbox{
			margin: 0 auto;
			width: 1200px;
			height: 700px;
			position: relative;
			background: url(${basePath}/res/images/loginbg.png) no-repeat center left;
		}
		.layui-red{color: red;}
		.regbox h1{text-align: center;line-height: 30px;font-size: 20px;margin-bottom: 10px;}
	</style>
	<div class="regbg">
		<div class="regbox">
			<form class="layui-form" style="position:absolute;top:10px;right:10px;width:400px;background: #fff;padding: 20px 30px 20px 0;">
				<h1>EST电商网站注册</h1>
				
				<div class="layui-form-item">
					<label class="layui-form-label">用户名<span class="layui-red">*</span></label>
					<div class="layui-input-block">
						<input type="text" class="layui-input userName" lay-verify="userName" name="userName" placeholder="请输入登录名">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">QQ<span class="layui-red">*</span></label>
					<div class="layui-input-block">
						<input type="text" class="layui-input userEmail" lay-verify="number" name="qq" placeholder="请输入QQ号">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">邮箱<span class="layui-red">*</span></label>
					<div class="layui-input-block">
						<input type="text" class="layui-input userEmail" lay-verify="email" name="email" placeholder="请输入邮箱">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">昵称<span class="layui-red">*</span></label>
					<div class="layui-input-block">
						<input type="text" class="layui-input userEmail" name="name" lay-verify="required" placeholder="请输入昵称">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">设置密码<span class="layui-red">*</span></label>
					<div class="layui-input-block">
						<input type="password"  id="pass"class="layui-input password" lay-verify="pass" placeholder="请设置密码">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">确认密码<span class="layui-red">*</span></label>
					<div class="layui-input-block">
						<input type="password" class="layui-input password" lay-verify="password" name="password" placeholder="请确认密码">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">性别<span class="layui-red">*</span></label>
						<div class="layui-input-block sex">
							<input type="radio" name="sex" value="男" title="男" checked>
							<input type="radio" name="sex" value="女" title="女">
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">电话<span class="layui-red">*</span></label>
					<div class="layui-input-block">
						<input type="text" class="layui-input userEmail" lay-verify="phone" name="phone" placeholder="请输入电话">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label class="layui-form-label">地址</label>
					<div class="layui-input-block">
						<input type="text" class="layui-input address" name="address"  placeholder="地址">
					</div>
				</div>
				<div class="layui-form-item">
						<a href="${basePath}/userLogin" class="fr">我已经有账号，去登陆</a>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="addUser">立即提交</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script>
		layui.use(['form','layer','jquery'],function(){
			var form = layui.form(),
			layer = parent.layer === undefined ? layui.layer : parent.layer,
			$ = layui.jquery;
			
			form.verify({
				userName:function(value, item){
					 if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
				      return '用户名不能有特殊字符';
				    }
				    if(/(^\_)|(\__)|(\_+$)/.test(value)){
				      return '用户名首尾不能出现下划线\'_\'';
				    }
				    if(/^\d+\d+\d$/.test(value)){
				      return '用户名不能全为数字';
				    }
				}
				,pass: [
				    /^[\S]{6,12}$/
				    ,'密码必须6到12位，且不能出现空格'
				]
				,password:function(value, item){
		            if(!new RegExp($("#pass").val()).test(value)){
		                return "两次输入密码不一致，请重新输入！";
		            }
		        }
			})
			
			form.on("submit(addUser)",function(data){
				//layer.msg(JSON.stringify(data.field));
				//console.log(data.field)
				$.ajax({
					type:"post",
					contentType : "application/json ; charset=utf-8",
					url:"${basePath}/reg",
					data:JSON.stringify(data.field),
					success:function(data){
						console.log(data)
						if(data.usereq){
							layer.msg("用户已存在");
							$(".userName").focus();
						}else{
							layer.msg("注册成功");
							window.location.href = "${basePath}";
						}
					}
				});
				return false;
			})
		})
	</script>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	
</body>
</html>