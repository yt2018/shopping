<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>个人主页</title>
<link rel="stylesheet" href="${basePath}/res/css/global.css" media="all" />
<link rel="stylesheet"
	href="${basePath}/res/css/ribbon.css"
	media="all" />
<style type="text/css">
.pli li {
	width: 188px;
	height: 270px;
	overflow: hidden;
	display: inline-block;
	padding: 2px;
	float: left;
	margin:3px;
	border: 1px solid #eee;
}
.pli li:hover{
	box-shadow: #666 0px 0px 10px;
	transform:scale(0.98) /* rotate(360deg) */;
	transition:all 0.3s ease;
}
.pli li img{
    height: 200px;
    width: 100%;
    text-align: center;
    vertical-align: middle;
    display: table-cell;
}
.pli li p {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}
.pli li .cf {
	height: 255px;
	width: 199px;
	position: relative;
}

.pli li .cf p {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 22px;
	background: rgba(0, 0, 0, 0.4);;
	color: #EEEEEE;
	line-height: 22px;
	text-align: center;
}
.pli li .picon {
    width: 44px;
    height: 44px;
}
.pli li .picon img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="fly-home fly-panel" style="background-image: url();">
		<img src="${u.icon }" alt="${u.name }"> <i
			class='iconfont ${not empty u.phone ? "icon-renzheng":"" }'
			title="${u.name }"></i>
		<h1>
			${u.name } <i
				class="iconfont ${u.sex eq '男' ? 'icon-nan':'icon-nv' }"></i>
		</h1>
		<c:if test="${empty u.phone }">
			<p style="padding: 10px 0; color: red;">未认证</p>
		</c:if>
		<c:if test="${not empty u.phone }">
			<p style="padding: 10px 0; color: #5FB878;">认证信息：${u.name }</p>
		</c:if>
		<p class="fly-home-info">
			<i class="iconfont icon-shijian"></i><span>${u.joindate } 加入</span>
		</p>

		<p class="fly-home-sign">${empty u.sign ?"主人什么都没留下": u.sign  }</p>

		<div class="fly-sns" data-user="">
			<c:if test="${empty user }">
				<a href="javascript:layer.msg('请先登录，再联系卖家');" data-type="chat"
					class="layui-btn">发起会话</a>
			</c:if>
			<c:if test="${not empty user }">
				<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=${u.qq }&site=qq&menu=yes"
					data-type="chat" class="layui-btn">发起会话</a><span>QQ:${u.qq }<span>
			</c:if>

		</div>

	</div>
	<div class="layui-container">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md fly-home-jie">
				<div class="fly-panel">
					<h3 class="fly-panel-title">${u.name }发布的作品</h3>
					<ul class="layui-row pli">
						<c:forEach var="p" items="${myproduct }">
							<li class="layui-col-xs6 layui-col-sm4" data-pid="${mp.pid }">
								<a href="${basePath}/Productinfo/${p.pid }">
									<div class="">
										<c:if test="${not empty p.image}">
											<img class="p_imgone" lay-src="${p.image[0].src }"/>
										</c:if>
										<c:if test="${empty p.image}">
											<img class="p_imgone" lay-src="${basePath}/res/images/timg.jpg"/>
										</c:if>
										<p style="color: green">${p.pname }</p>
										<div class="layui-row">
											<div class="layui-col-xs3">
												<div class="picon">
													<img src="${p.user.icon }"/>
												</div>
											</div>
											<div class="layui-col-xs9">
												<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
												<c:if test="${not empty user }">
												<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
												</c:if>
												<c:if test="${empty user }">
												<span style="color: red;">现价: <b>登录即可查看优惠价</b></span>
											</div>
										</div>
										</c:if>
									</div>
								</a>
							</li>
						</c:forEach>
					</ul>
					<c:if test="${count==0}"> <c:set var="hide" value="layui-hide"/></c:if>
					<div id="pagedemo" style="clear: both;float: right;" class="${hide }"></div>
				</div>
			</div>

		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript">
	var page=${page};
	var limit=${limit};
	var count=${count}
	</script>
	<script type="text/javascript" src="${basePath}/res/js/pli.js"></script>
</body>
</html>