package com.yitong.Estshopping.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yitong.Estshopping.entity.Orderlog;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.OrderLogService;
import com.yitong.Estshopping.service.OrderService;
import com.yitong.Estshopping.service.ProductService;
import com.yitong.Estshopping.service.UserService;

/**
 * 订单日志
 * @author 18102
 *
 */
@Controller
public class OrderLogController {
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderLogService orderlogservice;
	
	
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	@Autowired
	private ImageService imageService;
	/**
	 * 删除订单
	 */
	@RequestMapping("/userinfo/orderlog")
    public String orderlog(Model model, HttpSession session,@RequestParam(required=false) Integer state,@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit) {
		User user = (User) session.getAttribute("user");
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=12;
		}
		if(state==null) {
			state=1;
		}
		List<Orderlog> listorder = orderlogservice.listorder(user, page, limit);
		Integer count = orderlogservice.listorder(user);
		session.setAttribute("orderloglist", listorder);
		session.setAttribute("page", page);
		session.setAttribute("limit", limit);
		session.setAttribute("state", state);
		session.setAttribute("count", count);
		
		return "user/orderlog";
		
	}
}
