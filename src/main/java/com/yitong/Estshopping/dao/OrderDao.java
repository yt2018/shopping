package com.yitong.Estshopping.dao;

import java.util.List;

import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.User;

/**
 * 订单的Dao接口
 * @author YT
 *
 */
public interface OrderDao extends BaseDao<Order> {
	/**
	 * 查询订单列表
	 * @param user
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Order> listorder(User user, Integer state, Integer page, Integer limit);
	/**
	 * 根据订单号查询
	 * @param iof
	 * @return
	 */
	public Order findOif(String iof);
	/**
	 * 查询订单数量
	 * @param user
	 * @param state
	 * @return
	 */
	public Integer count(User user, Integer state);
	/**
	 * 商家查询订单
	 * @param user
	 * @param state
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Order> listsell(User user, Integer state, Integer page, Integer limit);
	/**
	 * 商家查询订单数量
	 * @param user
	 * @param state
	 * @return
	 */
	public Integer countsell(User user, Integer state);

}
