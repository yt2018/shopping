<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>订单管理</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
<style type="text/css">
.red{color:red}
s{font-size: 12px;color: #666}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",6); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
			<blockquote class="layui-elem-quote">买家可以和卖家通过其他方式支付，买家可以支付交易平台，然后交易平台转入卖家支付账号</blockquote>
			<div class="layui-tab layui-tab-brief" lay-filter="type">
					<ul class="layui-tab-title" id="ytmine">
						<li class="layui-this" lay-id="noplay" lay-state="1">未付款<span class="layui-badge">${count1}</span></li>
						<li lay-id="yesplay" lay-state="2">已付款<span class="layui-badge">${count2}</span></li>
						<li lay-id="sendorder" lay-state="3">待收货<span class="layui-badge">${count3}</span></li>
						<li lay-id="finishorder" lay-state="4">已完成<span class="layui-badge">${count4}</span></li>
						<li lay-id="yichang" lay-state="10">异常处理<span class="layui-badge">${count10}</span></li>
					</ul>
					<div class="layui-tab-content" style="padding: 20px 0;">
						<div class="layui-form layui-form-pane layui-tab-item layui-show">
							<c:if test="${orderlist[0].state==1 }">
							<c:forEach var="o" items="${orderlist }">
								<div class="layui-row layui-hide-sm" style="border-bottom: 1px solid #eee; padding-bottom: 10px">
									<div class="layui-col-xs12">订单编号:${o.oif }</div>
									<div class="layui-col-xs12">时间:${o.ordertime }</div>
									<div class="layui-col-xs12">剩余时间:<span class="etimec" data-time="${o.ordertime }"></span> 自动取消订单</div>
									<div class="layui-col-xs12 layui-row">
										<div class="layui-col-xs3">
											<img src="${o.pimg }" style='width: 100% ;height: 100%'>
										</div>
										<div class="layui-col-xs9 layui-row">
											<div class="layui-col-xs12"><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></div>
											<div class="layui-col-xs12">${o.num }件 X ${o.price }元 = ${o.total }元</div>
											<div class="layui-col-xs12"><span class="uname">收货信息:${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</div>
										</div>
									</div>
									<div class="layui-col-xs12">
										<div class="layui-col-xs3">
											<a href="javascript:;" data-oif="${o.oif }" class="edit layui-btn layui-btn-mini">修改信息</a>
										</div>
										<div class="layui-col-xs3">
											<a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a>
										</div>
										<div class="layui-col-xs3">
											<a href="javascript:;" class="orderzhifu layui-btn layui-btn-mini" data-oif="${o.oif }">钱包支付</a>
										</div>
										<div class="layui-col-xs3">
											<a target="_blank" href="javascript:;"  class="layui-btn layui-btn-mini alipayzhifu" data-oif="${o.oif }">支付宝支付</a>
										</div>
									</div>
								</div>
								<table class="layui-table layui-hide-xs">
									<colgroup>
										<col width="100">
										<col>
										<col width="100">
										<col width="100">
										<col width="100">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th colspan="4">订单编号:${o.oif } <span class="etimec" data-time="${o.ordertime }"></span> 自动取消订单</th>
											<th colspan="2">时间:${o.ordertime }</th>
										</tr>
										<tr>
											<th>图片</th>
											<th>商品名</th>
											<th>单价</th>
											<th>数量</th>
											<th>小计</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td rowspan=2><img src="${o.pimg }" height="80"></td>
											<td><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></td>
											<td>${o.price }元</td>
											<td>${o.num }件</td>
											<td>${o.total }元</td>
											<td>
												<a  class="layui-btn layui-btn-mini orderdel" href="javascript:;" data-oid="${o.oif}">取消<i class="layui-icon" >&#xe640;</i></a>
											</td>
										</tr>
										<tr>
											<td colspan="5">收货地址: <span class="uname">${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</span> 
											<a href="javascript:;" style="float: right;" data-oif="${o.oif }" class="edit">修改收件信息<i class="layui-icon" style="font-size: 20px; color: #1E9FFF;">&#xe642;</i></a>
											</td>
										</tr>
										<tr>
											<td colspan="3">代金券</td>
											<td ><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a></td>
											<td ><a href="javascript:;" class="orderzhifu layui-btn layui-btn-mini" data-oif="${o.oif }">钱包支付</a></td>
											<td ><a target="_blank" href="javascript:;"  class="layui-btn layui-btn-mini alipayzhifu" data-oif="${o.oif }">支付宝支付</a></td>
										</tr>
									</tbody>
								</table>
							</c:forEach>
							</c:if>
						</div>
						<div class="layui-form layui-form-pane layui-tab-item" id="etime">
							<c:if test="${orderlist[0].state==2 }">
							<c:forEach var="o" items="${orderlist }">
								<div class="layui-row layui-hide-sm" style="border-bottom: 1px solid #eee; padding-bottom: 10px">
									<div class="layui-col-xs12">订单编号:${o.oif }</div>
									<div class="layui-col-xs12">时间:${o.ordertime }</div>
									<div class="layui-col-xs12 layui-row">
										<div class="layui-col-xs3">
											<img src="${o.pimg }" style='width: 100% ;height: 100%'>
										</div>
										<div class="layui-col-xs9 layui-row">
											<div class="layui-col-xs12"><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></div>
											<div class="layui-col-xs12">${o.num }件 X ${o.price }元 = ${o.total }元</div>
											<div class="layui-col-xs12"><span class="uname">收货信息:${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</div>
										</div>
									</div>
									<div class="layui-col-xs12">
										<div class="layui-col-xs4">
											<a href="javascript:;" data-oif="${o.oif }" class="edit layui-btn layui-btn-mini">修改信息</a>
										</div>
										<div class="layui-col-xs4">
											<a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a>
										</div>
										<div class="layui-col-xs4">
											<a href="javascript:;" class="layui-btn layui-btn-mini">催发货</a>
										</div>
									</div>
								</div>
								<table class="layui-table layui-hide-xs">
									<colgroup>
										<col width="100">
										<col>
										<col width="100">
										<col width="100">
										<col width="100">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th colspan="4">订单编号:${o.oif } </th>
											<th colspan="2">时间:${o.ordertime }</th>
										</tr>
										<tr>
											<th>图片</th>
											<th>商品名</th>
											<th>单价</th>
											<th>数量</th>
											<th>小计</th>
											<th>状态</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td rowspan=3><img src="${o.pimg }" height="80"></td>
											<td><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></td>
											<td>${o.price }元</td>
											<td>${o.num }件</td>
											<td>${o.total }元</td>
											<td>
												已支付
											</td>
										</tr>
										<tr>
											<td colspan="5">收货地址: <span class="uname">${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</span> 
												<a href="javascript:;" style="float: right;" data-oif="${o.oif }" class="edit">修改收件信息<i class="layui-icon" style="font-size: 20px; color: #1E9FFF;">&#xe642;</i></a>
											</td>
										</tr>
										<tr>
											<td colspan="3">代金券</td>
											<td colspan="1"><a href="javascript:;" class="layui-btn layui-btn-mini">催发货</a></td>
											<td ><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes" class="layui-btn layui-btn-mini">联系商家</a></td>
										</tr>
									</tbody>
								</table>
							</c:forEach>
							</c:if>
						</div>
						<div class="layui-form layui-form-pane layui-tab-item">
							<c:if test="${orderlist[0].state==3}">
							<c:forEach var="o" items="${orderlist }">
								<div class="layui-row layui-hide-sm" style="border-bottom: 1px solid #eee; padding-bottom: 10px">
									<div class="layui-col-xs12">订单编号:${o.oif }</div>
									<div class="layui-col-xs12">时间:${o.ordertime }</div>
									<div class="layui-col-xs12 layui-row">
										<div class="layui-col-xs3">
											<img src="${o.pimg }" style='width: 100% ;height: 100%'>
										</div>
										<div class="layui-col-xs9 layui-row">
											<div class="layui-col-xs12"><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></div>
											<div class="layui-col-xs12">${o.num }件 X ${o.price }元 = ${o.total }元</div>
											<div class="layui-col-xs12"><span class="uname">收货信息:${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</div>
										</div>
									</div>
									<div class="layui-col-xs12">
										<div class="layui-col-xs4">
											快递单号:${o.kuaidi }
										</div>
										<div class="layui-col-xs4">
											<a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a>
										</div>
										<div class="layui-col-xs4">
											<a class="layui-btn layui-btn-mini" href="${basePath}/userinfo/shouhuo/${o.oif}">确认收货</a>
										</div>
									</div>
								</div>
								<table class="layui-table layui-hide-xs">
									<colgroup>
										<col width="100">
										<col>
										<col width="100">
										<col width="100">
										<col width="100">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th colspan="4">订单编号:${o.oif }</th>
											<th colspan="2">时间:${o.ordertime }</th>
										</tr>
										<tr>
											<th>图片</th>
											<th>商品名</th>
											<th>单价</th>
											<th>数量</th>
											<th>小计</th>
											<th>状态</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td rowspan=3><img src="${o.pimg }" height="80"></td>
											<td><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></td>
											<td>${o.price }元</td>
											<td>${o.num }件</td>
											<td>${o.total }元</td>
											<td>
												已发货
											</td>
										</tr>
										<tr>
											<td colspan="5">收货地址: <span class="uname">${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</span></td>
										</tr>
										<tr>
											<td >代金券</td>
											<td colspan="2">快递单号:${o.kuaidi }</td>
											<td ><a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a></td>
											<td colspan="1"><a class="layui-btn layui-btn-mini" href="${basePath}/userinfo/shouhuo/${o.oif}">确认收货</a></td>
										</tr>
									</tbody>
								</table>
							</c:forEach>
							</c:if>
						</div>
						<div class="layui-form layui-form-pane layui-tab-item">
							<c:if test="${orderlist[0].state==4}">
							<c:forEach var="o" items="${orderlist }">
							<div class="layui-row layui-hide-sm" style="border-bottom: 1px solid #eee; padding-bottom: 10px">
									<div class="layui-col-xs12">订单编号:${o.oif }</div>
									<div class="layui-col-xs12">时间:${o.ordertime }</div>
									<div class="layui-col-xs12 layui-row">
										<div class="layui-col-xs3">
											<img src="${o.pimg }" style='width: 100% ;height: 100%'>
										</div>
										<div class="layui-col-xs9 layui-row">
											<div class="layui-col-xs12"><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></div>
											<div class="layui-col-xs12">${o.num }件 X ${o.price }元 = ${o.total }元</div>
											<div class="layui-col-xs12"><span class="uname">收货信息:${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</div>
										</div>
									</div>
									<div class="layui-col-xs12">
										<div class="layui-col-xs4">
											快递单号:${o.kuaidi }
										</div>
										<div class="layui-col-xs4">
											<a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a>
										</div>
										<div class="layui-col-xs4">
											<a href="javascrip:;" class="layui-btn layui-btn-mini">去评价</a>
										</div>
									</div>
								</div>
								<table class="layui-table layui-hide-xs">
									<colgroup>
										<col width="100">
										<col>
										<col width="100">
										<col width="100">
										<col width="100">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th colspan="4">订单编号:${o.oif }</th>
											<th colspan="2">时间:${o.ordertime }</th>
										</tr>
										<tr>
											<th>图片</th>
											<th>商品名</th>
											<th>单价</th>
											<th>数量</th>
											<th>小计</th>
											<th>状态</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td rowspan=3><img src="${o.pimg }" height="80"></td>
											<td><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></td>
											<td>${o.price }元</td>
											<td>${o.num }件</td>
											<td>${o.total }元</td>
											<td>
												已发货
											</td>
										</tr>
										<tr>
											<td colspan="5">收货地址: <span class="uname">${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</span></td>
										</tr>
										<tr>
											<td >代金券</td>
											<td colspan="2">快递单号:${o.kuaidi }</td>
											<td ><a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a></td>
											<td colspan="1"><a href="javascrip:;" class="layui-btn layui-btn-mini">评价</a></td>
										</tr>
									</tbody>
								</table>
							</c:forEach>
							</c:if>
						</div>
						<div class="layui-form layui-form-pane layui-tab-item">
							<c:if test="${orderlist[0].state==10}">
							<c:forEach var="o" items="${orderlist }">
								<div class="layui-row layui-hide-sm" style="border-bottom: 1px solid #eee; padding-bottom: 10px">
									<div class="layui-col-xs12">订单编号:${o.oif }</div>
									<div class="layui-col-xs12">时间:${o.ordertime }</div>
									<div class="layui-col-xs12 layui-row">
										<div class="layui-col-xs3">
											<img src="${o.pimg }" style='width: 100% ;height: 100%'>
										</div>
										<div class="layui-col-xs9 layui-row">
											<div class="layui-col-xs12"><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></div>
											<div class="layui-col-xs12">${o.num }件 X ${o.price }元 = ${o.total }元</div>
											<div class="layui-col-xs12"><span class="uname">收货信息:${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</div>
										</div>
									</div>
									<div class="layui-col-xs12">
										<div class="layui-col-xs4">
											快递单号:${o.kuaidi }
										</div>
										<div class="layui-col-xs4">
											<a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=${o.user.qq }&site=qq&menu=yes">联系商家</a>
										</div>
										<div class="layui-col-xs4">
											<a href="javascrip:;" class="layui-btn layui-btn-mini">等待处理</a>
										</div>
									</div>
								</div>
								<table class="layui-table layui-hide-xs">
									<colgroup>
										<col width="100">
										<col>
										<col width="100">
										<col width="100">
										<col width="100">
										<col width="100">
									</colgroup>
									<thead>
										<tr>
											<th colspan="4">订单编号:${o.oif }</th>
											<th colspan="2">时间:${o.ordertime }</th>
										</tr>
										<tr>
											<th>图片</th>
											<th>商品名</th>
											<th>单价</th>
											<th>数量</th>
											<th>小计</th>
											<th>状态</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td rowspan=3><img src="${o.pimg }" height="80"></td>
											<td><a href="${basePath}/Productinfo/${o.pid}">${o.name }</a></td>
											<td>${o.price }元</td>
											<td>${o.num }件</td>
											<td>${o.total }元</td>
											<td>
												等待处理
											</td>
										</tr>
										<tr>
											<td colspan="5">收货地址: <span class="uname">${o.uname}</span> <span class="phone">${o.phone}</span> <span class="addr">${o.addr }</span></td>
										</tr>
										<tr>
											<td colspan="2">代金券</td>
											<td colspan="2">快递单号:${o.kuaidi }</td>
											<td ><a target="_blank" class="layui-btn layui-btn-mini" href="http://wpa.qq.com/msgrd?v=3&uin=1810258114&site=qq&menu=yes">联系客户</a></td>
										</tr>
									</tbody>
								</table>
							</c:forEach>
							</c:if>
						</div>
					</div>
			</div>
			<script type="text/javascript">
				var page=${page};
				var limit=${limit};
				<% String state = request.getParameter("state"); %>
				var state=<%=state%>;
			</script>
			<div id="pagedemo"></div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript" src="${basePath}/res/js/orderpage.js"></script>
	
</body>
</html>