<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>我的消息</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
	<%application.setAttribute("thisclass",3); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>
		<div class="fly-panel fly-panel-user" pad20>
			<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
				<button class="layui-btn layui-btn-danger" id="LAY_delallmsg">清空全部消息</button>
				<div id="LAY_minemsg" style="margin-top: 10px;">
					<!--<div class="fly-none">您暂时没有最新消息</div>-->
					<ul class="mine-msg">
						<li data-id="123">
							<blockquote class="layui-elem-quote">
								<a href="/jump?username=Absolutely" target="_blank"><cite>易通</cite></a>回答了您的求解
								<a target="_blank" href="/jie/8153.html/page/0/#item-1489505778669"><cite>台式电脑吃鸡</cite></a>
							</blockquote>
							<p><span>1小时前</span>
								<a href="javascript:;" class="layui-btn layui-btn-mini">回复</a><a href="javascript:;" class="layui-btn layui-btn-mini layui-btn-danger">删除</a>
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
</body>
</html>