package com.yitong.Estshopping.dao.impl;

import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.TicketDao;
import com.yitong.Estshopping.entity.Ticket;

@Repository("ticketDao")
public class TicketDaoImpl extends BaseDaoImpl<Ticket> implements TicketDao{

}
