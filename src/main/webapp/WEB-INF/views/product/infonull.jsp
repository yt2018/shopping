<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_meta.jsp"></jsp:include>
	<title>Sorry,你访问的页面不存在!</title>
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
	<style type="text/css">
		.box_bg{
			width:100%;
			min-width: 1200px;
			background:#fff;
		}
		.box_content{
			width: 1200px;
			height: 600px;
			background: url(${basePath}/res/images/404.gif) no-repeat center center;
			margin: 0 auto;
		}
		.box_content p{
			text-align: center;
			font-size: 30px;
			padding-top: 520px;
		}
	</style>
</head>
<body>
<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<!--背景动画-->
	<div class="box_bg">
		<div class="box_content">
			<p>该商品不存在,或者已经被下架了</p>
		</div>
	</div>
<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
</body>
</html>