<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_metaadmin.jsp"></jsp:include>
	<title>添加二级级类目</title>
</head>
<body>
<br>
<br>
<div class="addcategory2" style="padding: 10px">
		<form class="layui-form" action="">
			<div class="layui-form-item">
				<label class="layui-form-label">一级目录</label>
				<div class="layui-input-block">
					<select name="cid" lay-filter="aihao">
						<option value=""></option>
						<c:forEach items="${categorys}" var="c">
							<option value="${c.cid }">${c.cname }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">二级目录名</label>
				<div class="layui-input-block">
					<input type="text" name="csname" lay-verify="required" autocomplete="off" placeholder="请输入标题" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
			    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="demo1" lay-filter="demo1">创建</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
			    </div>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		layui.use(['form','layer','jquery','element','laypage'],function(){
			var form = layui.form(),
				layer = parent.layer === undefined ? layui.layer : parent.layer,
				laypage = layui.laypage,
				$ = layui.jquery;
			var element = layui.element;
			form.on("submit(demo1)",function(data){
				var index = parent.layer.getFrameIndex(window.name);
				//layer.msg(JSON.stringify(data.field));
				console.log(JSON.stringify(data.field.cid))
				var cid=data.field.cid;
				if(cid!="") {
					$.get("${basePath}/admin/addCategorySecond/"+cid+"/"+data.field.csname+"",function(data){
						if(data.flag){
							layer.msg(data.rest)
							layer.close(index)
						}else{
							layer.msg(data.rest)
						}
					})
				}else{
					layer.msg("请选择一级菜单")
					return false;
					
				}
				return false;
			})
		})
	</script>
</body>
</html>
