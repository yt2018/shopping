package com.yitong.Estshopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 支付回调的参数实体类
 * 
 * @author yitongs
 */
@Table(name = "paysapi")
@Entity
public class PaySaPi {

	/**
	 * paysapi生成的订单ID号
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "paysapi_id")
	private Integer paysapi_id;

	/**
	 * 您的自定义订单号
	 */
	@Column(name ="orderid")
	private String orderid;

	/**
	 * 订单定价
	 */
	@Column(name ="price")
	private float price;

	/**
	 * 实际支付金额
	 */
	@Column(name ="realprice")
	private float realprice;

	/**
	 * 订单内容
	 */
	@Column(name ="ordername")
	private String ordername;

	/**
	 * 秘钥
	 */
	@Column(name ="key")
	private String key;
	/**
	 * 秘钥
	 */
	@Column(name ="orderbody",columnDefinition="text")
	private String orderbody;
	/**
	 * 支付时间
	 */
	@Column(name ="order_date")
	private Date orderDate;
	/**
     * 客户外键
     */
    @OneToOne
    @JoinColumn(name = "uid", unique = true, nullable = false, updatable = false, insertable = false)
    private User user;
	public Integer getPaysapi_id() {
		return paysapi_id;
	}
	public void setPaysapi_id(Integer paysapi_id) {
		this.paysapi_id = paysapi_id;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getRealprice() {
		return realprice;
	}
	public void setRealprice(float realprice) {
		this.realprice = realprice;
	}
	public String getOrdername() {
		return ordername;
	}
	public void setOrdername(String ordername) {
		this.ordername = ordername;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getOrderbody() {
		return orderbody;
	}
	public void setOrderbody(String orderbody) {
		this.orderbody = orderbody;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "PaySaPi [paysapi_id=" + paysapi_id + ", orderid=" + orderid + ", price=" + price + ", realprice="
				+ realprice + ", ordername=" + ordername + ", key=" + key + ", orderbody=" + orderbody + ", orderDate="
				+ orderDate + ", user=" + user + "]";
	}
    
}
