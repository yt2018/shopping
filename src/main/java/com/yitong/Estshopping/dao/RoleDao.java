package com.yitong.Estshopping.dao;

import java.text.ParseException;
import java.util.List;

import com.yitong.Estshopping.bean.RoleViwe;
import com.yitong.Estshopping.entity.Role;

/**
 * 角色管理
 * @author 18102
 *
 */
public interface RoleDao extends BaseDao<Role>{
	/**
	 * 查询角色列表
	 * @param page
	 * @param limit
	 * @param object
	 * @return
	 */
	List<Role> roleList(Integer page, Integer limit, Object object);
	/**
	 * 查询角色数量
	 * @param object
	 * @return
	 */
	Integer countRole(Object object);
	/**
	 * 权限
	 * @param page
	 * @param limit
	 * @param object
	 * @return
	 * @throws ParseException 
	 */
	List<RoleViwe> quanxianList(Integer page, Integer limit, Object object);
	Integer quanxianList(Object object);

}
