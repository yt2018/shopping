package com.yitong.Estshopping.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.CategorysDao;
import com.yitong.Estshopping.entity.Categorys;

@Repository("categoryDao")
public class CategorysDaoImpl extends BaseDaoImpl<Categorys> implements CategorysDao {

	@Override
	public Integer countCategory() {
		// TODO Auto-generated method stub
		String hql = "select count(*) from Categorys";
		return count(hql);
	}

	@Override
	public List<Categorys> findAll(Integer page) {
		// TODO Auto-generated method stub
		String hql = "from Categorys";
        int rows = 10;
        return find(hql, page, rows);
	}

	@Override
	public List<Categorys> findAll() {
		// TODO Auto-generated method stub
		String hql = "from Categorys";
		return find(hql);
	}

	@Override
	public Categorys findcname(String cname) {
		// TODO Auto-generated method stub
		String hql ="from Categorys c where c.cname = :cname";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("cname", cname);
		return (Categorys) query.uniqueResult();
	}

	@Override
	public List<Categorys> findSearch(String key) {
		// TODO Auto-generated method stub
		String hql ="from Categorys c where c.cname = :key";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("key", "%"+key+"%");
		return query.list();
	}
	

}
