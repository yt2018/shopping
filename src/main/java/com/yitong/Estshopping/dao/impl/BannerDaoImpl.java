package com.yitong.Estshopping.dao.impl;


import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.BannerDao;
import com.yitong.Estshopping.entity.Banner;
import com.yitong.Estshopping.entity.User;

@Repository("bannerDao")
public class BannerDaoImpl extends BaseDaoImpl<Banner> implements BannerDao {

	public Banner getbanner(Banner banner)
    {
        String hql = "";
        if(banner.getBid() != null)
            return (Banner)get(banner.getBid());
        if(banner.getBimgSrc() != null)
        {
            hql = "from Banner b where b.bimgSrc= :bimgSrc";
            Query query = getCurrentSession().createQuery(hql);
            query.setParameter("bimgSrc", banner.getBimgSrc());
            return (Banner)query.uniqueResult();
        } else
        {
            return banner;
        }
    }

    public List<Banner> list(User user, Integer state, Integer page, Integer limit)
    {
        String hql = "from Banner b ";
        if(user != null)
            hql += "where b.user.uid="+user.getUid()+" order by b.bimgDate desc";
        else
        if(state == null)
            hql += "order by b.bimgDate desc";
        else
        if(state != null)
            hql += "where b.bimgStates="+state+" order by b.bimgDate desc";
        return find(hql, page.intValue(), limit.intValue());
    }

    public Integer listcount(User user, Integer state, Integer page, Integer limit)
    {
        System.out.println(state);
        String hql = null;
        if(user != null)
            hql = "select count(*) from Banner b where b.user.uid="+user.getUid();
        else
        if(state == null)
            hql = "select count(*) from Banner";
        else
        if(state != null)
            hql = "select count(*) from Banner b where b.bimgStates="+state;
        return this.count(hql);
    }

    public List<Banner> indexbannerlist(int i)
    {
        String hql = "from Banner b where b.bimgStates=1 and b.bimgClass='"+i+"' order by b.bimgNum asc";
        return this.find(hql);
    }

}
