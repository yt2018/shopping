package com.yitong.Estshopping.dao;

import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.entity.Wallet;

/**
 * 电子钱包的Dao接口
 * @author YT
 *
 */
public interface WalletDao extends BaseDao<Wallet>{

	Float usercount(User user);

	

}
