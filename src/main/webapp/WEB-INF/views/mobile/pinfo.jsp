<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>易通通|tong118.com</title>
	<meta name="keywords" content="易斯顿,大学生,tong118,易通通,全国在校大学生创业平台,大学生创业,大学生创业平台,校园二手电商网站,EST商城,易斯顿美术学院" />
	<meta name="description" content="易斯顿校园二手电商网站，易通通实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	<jsp:include page="/WEB-INF/views/_meta/_meta_m.jsp"></jsp:include>
	<style type="text/css">
		#test1 img {
			text-align: center;
			line-height: 400px;
			width:100%;
		}
		.pimg{
			position: relative;
			
		}
		.iback{
			width:100%;
			height:100%;
			position:absolute;
			background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            -webkit-filter: blur(15px);
            -moz-filter: blur(15px);
            -o-filter: blur(15px);
            -ms-filter: blur(15px);
            filter: blur(15px);	
		}
		.pimg img{ position: absolute;}
		.productinfo{
			padding: 10px;
		}
		.productinfo h1{
			font-size: 16px;
			color: green;
		}
		.productinfo p{
			font-size: 14px;
		}
		.p_desc img,.pinglunqu-body img{
			max-width: 100%;
		}
		.pinglunqu-body img[src*='face']{
			width:auto;
		}
		
		/*评论*/
		.pinglunqu{margin-bottom: 30px;}
		.zhuiping{margin: 0 0 0 30px;}
		.pinglunqu>li{position: relative;border-bottom: 1px dotted #DFDFDF;margin: 10px 0;}
		.pinglunqu>li:last-child{border-bottom: none;}
		.pinglunqu .fly-none{height: 50px; min-height: 0;}
		.pinglunqu .icon-caina{position:absolute; right:10px; top:15px; font-size:60px; color: #58A571;}
		.detail-about-reply{padding: 0 0 0 55px; background: none;}
		.detail-about-reply .detail-hits{left: 0; bottom: 0;margin-top: 12px;}
		.detail-about-reply .fly-avatar{left: 0; top: 0;}
		
		.pinglunqu-body{margin: 5px 0; min-height: 0; line-height: 24px; font-size:14px;}
		.pinglunqu-body .s_pinglun{margin-bottom: 10px;background: #eee;}
		.pinglunqu-body a{color:#4f99cf}
		.pinglunqu-reply{position:relative;}
		.pinglunqu-reply span{padding-right:20px; color:#999; cursor:pointer;}
		.pinglunqu-reply span:hover{color:#666;}
		.pinglunqu-reply span i{margin-right:5px; font-size:16px;}
		.pinglunqu-reply span em{font-style: normal;}
		.pinglunqu-reply span .icon-zan{font-size: 22px;}
		.layui-layedit-tool .layedit-tool-face{float:right;}
		.layui-util-face ul{width:280px}
		.pinglunqu-reply .zanok,
		.pinglunqu-reply .pinglunqu-zan:hover{color:#c00}
		.pinglunqu-reply span .icon-svgmoban53{position: relative; top: 1px;}
		.fly-avatar{position: absolute; left: 15px; top: 15px;}
		.fly-avatar img{display: block; width: 45px; height: 45px; margin: 0; border-radius: 2px;}
		.fly-link{color: #01AAED;}
		.fly-link:hover{color: #5FB878;}
		.fly-grey{color: #999;}
		.layui-layedit {
		    border: 1px solid #009688;
		}
		.layui-layedit-tool {
		    padding: 0;
		    border-bottom: 1px solid #009688;
		 }
		 .layui-layedit-tool .layui-icon {
		    margin: 0;
		    padding: 5px;
		    color: #009688;
		    display: inline-block;
		    border-right: 1px solid #009688;
		}
		blockquote,.gray{
			-webkit-filter: grayscale(100%);
		    -moz-filter: grayscale(100%);
		    -ms-filter: grayscale(100%);
		    -o-filter: grayscale(100%);
		    filter: grayscale(100%);
		    filter: gray;
		}
	</style>
</head>
<body>
		<div style="position: fixed; top:0;left:0;width: 100%;height:50px;opacity: 0.5;z-index:999">
			<a href="javascript:history.back()" class="layui-btn" style="position:absolute; top:5px;left:10px"><i class="layui-icon">&#xe603;</i></a>
			<a href="${basePath}"  class="layui-btn" style="position: absolute; top:5px;right:10px"><i class="layui-icon">&#xe641;</i> </a>
		</div>
		<div class='layui-carousel ${product.isState!="on"?"gray":""}' id="test1" lay-filter="test1" >
			<div carousel-item>
				<c:forEach var="i" items="${product.image }">
					<div class="pimg" style="">
						<div class="iback" style="background-image: url('${i.src }');">
						</div>
						<img src="${i.src }"  w="${i.width }" h="${i.height }" />
					</div>
				</c:forEach>
			</div>
		</div>
		<div class="productinfo">
				<h1>${product.pname } <span style="float: right;font-size: 10px">(访问量 ${empty product.pnub?0:product.pnub })</span></h1>
				<p>${product.pnamedesc}</p>
				<div class="layui-row promo-meta">
					<div class="layui-col-xs2">
						原价格
					</div>
					<div class="layui-col-xs4" style="color: red;">
						￥<del> ${product.marketPrice } </del>
					</div>
					<div class="layui-col-xs2">
						秒杀价
					</div>
					<c:if test="${not empty user }">
					<div class="layui-col-xs4" style="color: red;font-size: 22px">
					￥<i> ${product.shopPrice} </i>
					</div>
					</c:if>
					<c:if test="${empty user }">
					<div class="layui-col-xs4" style="color: red;font-size: 12px">
						登录即可享受优惠价
					</div>
					</c:if>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						配送
					</div>
					<div class="layui-col-xs10" style="color: red;">
						本校内免费配送<br>
						校外根据卖家情况,收取邮费
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						数量
					</div>
					<div class="layui-col-xs10">
						 <input type="text" name="number" id="pnub" lay-verify="required|number" autocomplete="off" class="layui-input"value="1" style="width: 60px;display: inline-table;">
						<span>件 (库存${product.inventory }件)</span>
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						声明
					</div>
					<div class="layui-col-xs10" style="color: red;">
						本站为了学生方便提供闲置物品存放网站<br>
						为了确实商品是否属实,建议当面交易<br>
						本站只提供商品上架，联系服务，不承担商品交易风险<br>
						若有商品虚假，进行举报，<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=1810258114&amp;site=qq&amp;menu=yes">网站管理员</a>尽快处理。<br>
					</div>
				</div>
			</div>
			<div class="p_desc">
				<div class="layui-tab" lay-filter="type">
					<ul class="layui-tab-title">
						<li class="layui-this" lay-id="p_info">商品详情</li>
						<li lay-id="p_desc">商品参数</li>
						<li class="pinglun " lay-id="p_pinglun" data-pid = "222">评论</li>
					</ul>
					<div class="layui-tab-content">
						<div class="layui-tab-item layui-show p_img">
							${product.pdesc }
							<c:forEach var="i" items="${product.image }" >
							<div><img lay-src="${i.src }"/></div>
							</c:forEach>
						</div>
						<c:if test="${empty product.parameter}">
							<div class="layui-tab-item">商家没有提供该商品参数</div>
						</c:if>
						<c:if test="${not empty product.parameter}">
							<div class="layui-tab-item">${product.parameter}</div>
						</c:if>
						<div class="layui-tab-item" style="position: relative;">
						<form class="layui-form" action="" id="zhuipingtext">
							<textarea id="yttpinglun" style="display: none;" class="layui-textarea fly-editor"></textarea>
							<c:if test="${empty user }">
							<div style="position: absolute;top: 0;left: 0; width: 100%;height:234px;background: rgba(128, 128, 128, 0.56);text-align: center;line-height: 200px;">
							请<a class="layui-btn layui-btn-red layui-btn-mini" href="${basePath}/userLogin?uri=${basePath}/Productinfo/${product.pid }">登录</a>,登录后评论
							</div>
							</c:if>
							<div style="text-align: right;">
								<input type="checkbox" name="niming" title="匿名" lay-skin="primary">
								<button id="pingbtn" class="layui-btn layui-btn-red " href="javascript:;" lay-submit lay-filter="pinhlun">发表评论</button >
							</div>
						</form>	
							<fieldset class="layui-elem-field layui-field-title">
							  <legend>评论区</legend>
							</fieldset>
							<div>
								<ul class="pinglunqu">
								</ul>
							</div>
							<div><a style="display: block;text-align: center;background: #eee;padding: 10px;border-radius: 3px" href="javascript:;" class="pinglunlistbtn"><i class="layui-icon">&#xe61a;</i>  点击加载查看更多</a></div>
						</div>
					</div>
				</div>
			</div>
		<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
		<style>
			.footMenu{
				position: fixed;
			    bottom: 0;
			    left: 0;
			    width: 100%;
			    text-align: center;
			    height: 40px;
			    line-height: 40px;
			    font-size: 16px;
			    background: #fff;
			    color:#eee
			}
			.footMenu a{
				display: block;
				width: 100%;
				height:100%;
				border: 1px solid #eee;
			}
		</style>
		<div class="layui-row footMenu">
			<div class="layui-col-xs2"><a href="${basePath}">首页</a></div>
			<div class="layui-col-xs3"><a href="${basePath}/u/${ product.user.uid}">卖家店铺</a></div>
			<c:if test="${empty user }">
			
			<div class="layui-col-xs3"><a href='javascript:layer.msg("请先登录，再联系卖家");'>联系卖家</a></div>
			</c:if>
			<c:if test="${not empty user }">
			<div class="layui-col-xs3"><a href='http://wpa.qq.com/msgrd?v=3&uin=${ product.user.qq }&site=qq&menu=yes'>联系卖家</a></div>
			
			</c:if>
			<c:if test="${product.inventory<1 }">
				<div class="layui-col-xs4">
					<a class="layui-btn layui-btn-gray nobtn" data-mrg="你来晚了已经抢光了" href="javasript:layer.msg('你来晚了已经抢光了');">立即购买</a>
					<!-- <a class="layui-btn layui-btn-gray" href="javascript:layer.msg('你来晚了已经抢光了');"><i class="layui-icon">&#xe657;</i> 加入购物车</a> -->
				</div>
			</c:if>
			<c:if test="${product.inventory>=1 }">
				<c:if test="${product.user.uid==  user.uid }">
				
					<div class="layui-col-xs4">
						<a class="layui-btn layui-btn-gray" href="javascript:layer.msg('你很皮,这是自己的商品,不需要购买');" >立即购买</a>
						<!-- <a class="layui-btn layui-btn-gray" href="javascript:layer.msg('你很皮,这是自己的商品,不需要购买');"><i class="layui-icon">&#xe657;</i> 加入购物车</a> -->
					</div>
				</c:if>
				<c:if test="${product.user.uid != user.uid || empty product.user.uid}">
					<div class="layui-col-xs4">
						<a class="layui-btn layui-btn-red" id="buybtn" href="javascript:;"><i class="layui-icon">&#xe657;</i> 立即购买</a>
					</div>
				</c:if>
			</c:if>
		</div>
		<script type="text/javascript">
		var pid="${product.pid }",uname="${user.name}",uid="${user.uid}",pnum=${product.inventory }
		</script>
		<script type="text/javascript" src="${basePath}/res/js/pinfo.js"></script>
		<script type="text/javascript">
		layui.use(['carousel','flow','jquery'], function(){
			var carousel = layui.carousel;
			var flow = layui.flow;
			var $=layui.jquery;
				flow.lazyimg({
				    elem: 'body img'
				    //,scrollElem: '#LAY_demo3' //一般不用设置，此处只是演示需要。
				});
			  //建造实例
			carousel.render({
			    elem: '#test1'
			    ,width: $(window).width()//设置容器宽度
			    ,height:400
			    ,arrow: 'hover' //始终显示箭头
			    ,anim: 'fade' //切换动画方式
			}); 
			carousel.on('change(test1)',function(obj){
				var img=(obj.item).find('img')
				var w=img.attr("w")*1;
				var h=img.attr("h")*1;
				console.info(w)
				var ih=$(window).width()*400/w;
				if(w>=h){
					$(img).css({
						width:$(window).width(),
						height:ih,
						top:(400-ih)/2
					})
				}
				if(w<h){
					$(img).css({
						width:400*w/h,
						height:400,
						left:($(window).width()-400*w/h)/2
					})
				}
			})
		})
		</script>
</body>
</html>