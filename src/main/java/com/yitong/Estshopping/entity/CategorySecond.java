package com.yitong.Estshopping.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 二级类目的实体类
 * @author YT
 *
 */
@Table(name="categorysecond")
@Entity
public class CategorySecond {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="csid")
	private Integer csId;
	
	@Column(name="csname", nullable=false, length=255)
	private String csName;
	@Column(name = "href", length = 255)
	private String href;
	
	@JoinColumn(name = "cid")
    @ManyToOne
    @JsonIgnore
    private Categorys category;
	
	
	@OneToMany(fetch= FetchType.EAGER, mappedBy = "categorySecond")
	@JsonIgnore
	private Set<Product> products;

	public Integer getCsId() {
		return csId;
	}

	public void setCsId(Integer csId) {
		this.csId = csId;
	}
	
	public String getCsName() {
		return csName;
	}

	public void setCsName(String csName) {
		this.csName = csName;
	}

	public Categorys getCategory() {
		return category;
	}

	public void setCategory(Categorys category) {
		this.category = category;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

}
