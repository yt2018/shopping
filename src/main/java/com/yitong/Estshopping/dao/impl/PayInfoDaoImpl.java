package com.yitong.Estshopping.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.BaseDao;
import com.yitong.Estshopping.dao.PayInfoDao;
import com.yitong.Estshopping.entity.PayEntity;
import com.yitong.Estshopping.entity.Product;
@Repository("payInfoDao")
public class PayInfoDaoImpl extends BaseDaoImpl<PayEntity> implements PayInfoDao {

	@Override
	public PayEntity getOut_trade_no(String out_trade_no) {
		// TODO Auto-generated method stub
		String hql ="from PayEntity p where p.out_trade_no= :out_trade_no";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("out_trade_no", out_trade_no);
		return (PayEntity) query.uniqueResult();
	}

}
