package com.yitong.Estshopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 订单表
 * @author YT
 *
 */
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "oid")
    private Integer oid;
	//订单编号//UUID.randomUUID().toString().replaceAll("-", "");
	@Column(name="oif")
    private String oif;
	/**
	 * 数量
	 */
	@Column(name = "num")
    private Integer num;
	/**
	 * 单价
	 */
	@Column(name="price")
    private Float price;
	/**
	 * 总金额
	 */
	@Column(name="total")
	private Float total;
    
	@Column(name="order_time")
    private Date ordertime;
    
	/**
	 * 1:未付款   2:订单已经付款   3:已经发货   4:订单结束
	 */
	@Column(name="state")
    private Integer state;
	/**
	 * 商品id
	 */
	@Column(name="pid")
	private Integer pid;
    
	@Column(name="name", length=255)
    private String name;
	@Column(name="uname", length=255)
	private String uname;
	@Column(name="pimg", length=255)
	private String pimg;
    
	@Column(name="phone", length = 16)
    private String phone;
    
	
	@Column(name="address", length=255)
    private String addr;
	@Column(name="kuaidi")
	private String kuaidi;
	
	/**
	 * 优惠券id
	 */
	@Column(name = "couponid")
    private Integer couponid;
	/**
	 * 优惠券名称
	 */
	@Column(name="coupon", length=255)
	private String coupon;
	/**
	 * 优惠券金额
	 */
	@Column(name="coupon_money")
	private Float couponMoney;
	/**
	 * 商家订单
	 */
    @JoinColumn(name = "uid")
    @ManyToOne
    private User user;
    /**
     * 客户下单
     */
    @JoinColumn(name = "cid")
    @ManyToOne
    private User cuser;
    
    
    
    public String getPimg() {
		return pimg;
	}

	public void setPimg(String pimg) {
		this.pimg = pimg;
	}

	public Integer getCouponid() {
		return couponid;
	}

	public void setCouponid(Integer couponid) {
		this.couponid = couponid;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public Float getCouponMoney() {
		return couponMoney;
	}

	public void setCouponMoney(Float couponMoney) {
		this.couponMoney = couponMoney;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getOif() {
		return oif;
	}

	public void setOif(String oif) {
		this.oif = oif;
	}

	public User getCuser() {
		return cuser;
	}

	public void setCuser(User cuser) {
		this.cuser = cuser;
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Date getOrdertime() {
		return ordertime;
	}

	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}


	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getKuaidi() {
		return kuaidi;
	}

	public void setKuaidi(String kuaidi) {
		this.kuaidi = kuaidi;
	}


	
}
