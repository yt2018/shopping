<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class='description'>易斯顿校园二手电商网站，易通通实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。</div>
<div style="position: fixed;width: 100%;height: 50px;z-index: 9999">
<header class="header">
	<a href="${basePath}" class="logo"><img src="${basePath}/res/images/logo.png" alt="易通通" width="30"></a>
	<a href="#" class="search"><span>目的地搜索</span></a>
	<c:if test="${empty user }">
	<a href="${basePath}/userinfo" class="user-icon"><span>用户中心</span></a>
	</c:if>
	<c:if test="${not empty user }">
	<a href="${basePath}/userinfo" class="user-icon" style="background-image: url('${user.icon}');background-size:cover;border-radius:50%;top: 3px;width:44px;height:44px"><span>${user.name }</span></a>
	</c:if>
	<script type="text/javascript">
	layui.use(['layer','jquery'],function(){
		var $ = layui.jquery,
		layer = layui.layer;
		$(".search").on("click",function(){
			layer.prompt({
			  formType: 0,
			  value: '油画',
			  title: '搜索',
			  offset: 't',
			  shadeClose:true
			  //area: ['800px', '350px'] //自定义文本域宽高
			}, function(value, index, elem){
				var letters = "(!@#$%^&*;':)";
				var key= value;
				var c;
				var flag=false;
				if(key.trim()){
					for(var i = 0; i < key.length; i ++ )
				     {
				          c = key.charAt( i );
				          
				   			if (letters.indexOf(c)>0){
				   				flag=true;
				   				alert("不能有特殊字符"+c)
				   			}
				     }
					if(!flag){
						location.href=path+"/search/"+key
					}
				    
				}
			});
		})
	})
	</script>
	<div id="dl-menu" class="dl-menuwrapper">
		<button id="dl-menu-button">菜单</button>
		<ul class="dl-menu" >
			<li><a href="${basePath}">首页</a></li>
			<c:forEach var="c" items="${categorys}">
			<li>
				<c:if test="${empty c.href }">
				<a href='${basePath}/classify/${c.cid }'>${c.cname }</a>
				</c:if>
				<c:if test="${not empty c.href }">
				<a href='${c.href }'>${c.cname }</a>
				</c:if>
				<c:if test="${not empty c.categorySeconds }">
				<ul class="dl-submenu">
					<li class="dl-back"><a href="#">返回上一级</a></li>
					<li >
						<c:if test="${empty c.href }">
						<a href='${basePath}/classify/${c.cid }'>${c.cname }(全部)</a>
						</c:if>
						<c:if test="${not empty c.href }">
						<a href='${c.href }'>${c.cname }</a>
						</c:if>
					</li>
					<c:forEach items="${c.categorySeconds }" var="cat" varStatus="s">
					<li >
						<c:if test="${empty cat.href }">
							<a href='${basePath}/classify2/${cat.csId }'>${cat.csName }</a>
						</c:if>
						<c:if test="${not empty cat.href }">
							<a href='${cat.href }'>${cat.csName }</a>
						</c:if>
					</li>
					</c:forEach>
				</ul>
				</c:if>
			</li>
			</c:forEach>
		</ul>
	</div>
</header>
</div>
<div style="width: 100%;height: 50px">
</div>
<script type="text/javascript">
$(function(){
	$( '#dl-menu' ).dlmenu()
	$('body').other();
	
});

</script>
		<div class="layui-carousel" id="test1" lay-filter="test1">
			<div carousel-item>
				<c:forEach var="ad_b" items="${ad_banner }">
				<div><a target="_blank" href="${ad_b.href }"><img lay-src="${ad_b.bimgSrc}" alt="${ad_b.bimgAlt }" data-color="${ad_b.bimgColor }"></a></div>
				</c:forEach>
			</div>
		</div>
		<div class="img layui-row">
			<c:forEach var="ad_b" items="${ad_banner2 }" begin="0" end="2">
			<div class="layui-col-xs4" style="height: 40px">
				<a target="_blank" href="${ad_b.href }">
					<img lay-src="${ad_b.bimgSrc}" alt="${ad_b.bimgAlt }" data-color="${ad_b.bimgColor }" style="width:100%;height: 100%">
				</a>
			</div>
			</c:forEach>
		</div>
		<script type="text/javascript"src="${basePath}/res/js/bannerjs.js"></script>