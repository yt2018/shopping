package com.yitong.Estshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.OrderDao;
import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.OrderLogService;
import com.yitong.Estshopping.service.OrderService;
@Service("orderservice")
@Transactional
public class OrderServiceImpl implements OrderService{
	@Autowired
	private OrderDao orderDao;
	public OrderDao getOrderDao() {
		return orderDao;
	}
	public void setOrderDao(OrderDao orderDao) {
		this.orderDao = orderDao;
	}
	@Autowired
	private OrderLogService orderlogservice;
	@Override
	public void saveOrder(Order order) {
		// TODO Auto-generated method stub
		orderDao.save(order);
	}
	@Override
	public List<Order> listorder(User user,Integer state, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return orderDao.listorder(user,state,page,limit);
	}
	@Override
	public Order findOif(String iof) {
		// TODO Auto-generated method stub
		return orderDao.findOif(iof);
	}
	@Override
	public void delete(Order order) {
		// TODO Auto-generated method stub
		orderDao.delete(order);
	}
	@Override
	public Integer count(User user, Integer state) {
		// TODO Auto-generated method stub
		return orderDao.count(user,state);
	}
	@Override
	public boolean update(Order order) {
		// TODO Auto-generated method stub
		return orderDao.updateflag(order, order.getOid());
	}
	@Override
	public List<Order> listsell(User user, Integer state, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return orderDao.listsell(user,state,page,limit);
	}
	@Override
	public Integer countsell(User user, Integer state) {
		// TODO Auto-generated method stub
		return orderDao.countsell(user,state);
	}
	
	
	

}
