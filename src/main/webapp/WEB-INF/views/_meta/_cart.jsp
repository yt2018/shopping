<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    	<div class="cart">
			<div class="cart-title">
				<i class="layui-icon">&#xe657;</i> 购物车
			</div>
			<div class="cart-centcon">
				
				<ul style="font-size: 14px;" id='carthtml'>
				</ul>
				<div class="cart-jiesuan layui-row">
					<div class="layui-col-xs6" style="color: green;">
						<c:if test="${empty user }">登录查看</c:if>
						<c:if test="${not empty user }">
							<span>总计：￥</span><span id="total"></span>
						</c:if>
					</div>
					<div class="layui-col-xs6 layui-row">
						<div class="layui-col-xs6" id="clearcart" style="color: #DEDEDE;text-align: center;cursor:pointer;background:#FF5722;">
							清空
						</div>
						<div class="layui-col-xs6 layui-row" style="color: #DEDEDE;text-align: center;background: #c40000;cursor:pointer;">
							<a target="_blank" href="${basePath}/myCart">结算</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			layui.use('table', function(){
				var $ = layui.$;
				$('.cart-title').on('click',function(){
					if($('.cart-centcon').height()>100){
						$('.cart-centcon').animate({
							'height':0
						},1000);
						$('.cart-jiesuan,.cart-centcon>ul').fadeOut()
					}else{
						$('.cart-centcon').animate({
							'height':400
						},1000);
						$('.cart-jiesuan,.cart-centcon>ul').fadeIn()
						cartlist('${basePath}/getcart')
						
					}
				})
				//点击其他地方隐藏购物车
				$(document).on('click',function(e){
					 if($(e.target).parents(".cart").length == 0){
						$('.cart-centcon').animate({
							'height':0
						},1000);
						$('.cart-jiesuan,.cart-centcon>ul').fadeOut()
					 }
				})
				//清空购物车
				$("#clearcart").on('click',function(e){
					var index  = layer.msg('批量删除中，请稍候',{icon: 16,time:false,shade:0.8});
					layer.confirm('确定要删除吗?', function(index){
						cartlist("${basePath}/clearCart")
						layer.close(index);
					})
				})
				$(document).on('click','.pdelete',function(){
					var pid=$(this).data("id");
					var url='${basePath}/removeCart/'+pid
					cartlist(url)
				})
				function cartlist(url){
					$.get(url,function(d){
						var html;
						//console.info(d.carts)
						if(d.carts==null||d.carts.total==0){
							html="<li>购物车空空的,亲选择你喜欢的商品</li>"
							$("#carthtml").html(html);
							var t=0;
							$("#total").html(t.toFixed(2))
							html='';
							return ;
						}else{	
							html='';
							$.each(d.carts.cartItems,function(i,v,arr){
								html+= '<li class="layui-row">'
										+'	<div class="layui-col-xs2">'
										+'		<img width="48" height="48" src="'+v.imgsrc+'"/>'
										+'	</div>'
										+'	<div class="layui-col-xs9">'
										+'		<p><a href="${basePath}/Productinfo/'+v.product.pid+'" target="_blank">'+v.product.pname+'</a></p>'
										<c:if test="${not empty user }">	+'		<span class="picard">'+v.price+'元 X'+v.count+'件 小计:￥'+v.subtotal+'元</span>'</c:if>
										<c:if test="${empty user }">+'		<span class="picard">登录查看</span>'</c:if>
										+'	</div>'
										+'	<div class="layui-col-xs1 pdelete" data-id="'+v.product.pid+'">'
										+'		<i class="layui-icon" style="font-size: 30px; color: #1E9FFF;">&#xe640;</i>  '
										+'	</div>'
										+'</li>'
								//console.info(v)
								//console.info(v.product.imgsrcs.split(",")[0])
								//console.info(v.product)
							})
						}
						$("#carthtml").html(html);
						$("#total").html(d.carts.total.toFixed(2))
						html='';
						
					})
				}
			})
		</script>