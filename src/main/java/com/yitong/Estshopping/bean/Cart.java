package com.yitong.Estshopping.bean;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 购物车的操作类
 * @author YT
 *
 */
public class Cart {

	/**
	 *购物车的存放容 器  map key:pid, value :CartItem
	 */
	private Map<Integer, CartItem> map = new LinkedHashMap<Integer, CartItem>();
	
	/**
	 * 购物总计
	 */
	private float total;

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}
	
	/**
	 * Cart对象中有一个cartItems属性
	 */
	public Collection<CartItem> getCartItems(){
		return map.values();
	}
	
	/**
	 * 向购物车添加购物项
	 */
	public void addCart(CartItem cartItem){
		Integer pid = cartItem.getProduct().getPid();
		//判断购物车是否存在同类商品
		if(map.containsKey(pid)){
			CartItem _cartItem = map.get(pid);
			_cartItem.setCount(_cartItem.getCount() + cartItem.getCount());
		}else{
			map.put(pid, cartItem);
		}
		/**
		 * 计算总价格
		 */
		total += cartItem.getSubtotal();
	}
	
	/**
	 * 从购物车移除购物项
	 */
	public void removeCart(Integer pid){
		CartItem cartItem = map.remove(pid);
		
		total -= cartItem.getSubtotal();
	}
	
	/**
	 * 清空购物车
	 */
	public void clearCart(){
		map.clear();
		total = 0;
	}
	
}
