<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; Charset=gb2312">
<meta http-equiv="Content-Language" content="zh-CN">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="icon" rel="shortcut icon" type="image/x-icon" href="${basePath}/res/images/logo.png"/>
<title>支付详情</title>
<!--Layui-->
		<link href="${basePath}/res/timeline/plug/layui/css/layui.css" rel="stylesheet" />
		<!--font-awesome-->
		<link href="${basePath}/res/timeline/plug/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
		<!-- animate.css -->
		<link href="${basePath}/res/timeline/css/animate.min.css" rel="stylesheet" />
		<!--全局样式表-->
		<link href="${basePath}/res/timeline/css/global.css" rel="stylesheet" />
		<!-- 本页样式表 -->
		<link href="${basePath}/res/timeline/css/timeline.css" rel="stylesheet" />
<body>
	<!-- canvas -->
		<canvas id="canvas-banner"></canvas>
		<!-- 导航 -->
		<nav class="blog-nav layui-header">
			<div class="blog-container">
				<a href="${basePath}"><img src="${basePath}/res/images/logo1.png" height="50"/></a>
				<a href="${basePath}/timeline" class="layui-btn layui-btn-danger" style="float: right;margin-top: 12px">时光轴</a>
				<a href="${basePath}/resume" class="layui-btn layui-btn" style="float: right;margin-top: 12px">关于我</a>
			</div>
		</nav>
		<!-- 主体（一般只改变这里的内容） -->
		<style>
			.biaodan{
				background: rgba(255, 255, 255, 0.62)
			}
			h1{font-size: 22px}
			.biaodan b{
				display: inline-block;
				width: 100px
			}
			.biaodan div{
				border-bottom: 1px dashed #e5e5e5; 
			}
		</style>
		<div class="blog-body">
			<div class="blog-container">
				<div class="biaodan shadow" >
					<div style="max-width: 780px;min-height:600px;background: #fff;margin: 0 auto;padding:10px;line-height: 40px">
					<c:if test="${empty payentity }">
						<h1>没有查询到订单信息</h1>
					</c:if>
					<c:if test="${not empty payentity }">
					
						<h1>${not empty payentity.seller_id?"订单支付成功":"订单支付失败" }</h1>
						<div>
							<b>商家订单号:</b>
							${payentity.out_trade_no }
						</div>
						<div>
							<b>商品说明:</b>
							${payentity.subject }
						</div>
						<div>
							<b>商品金额:</b>
							${payentity.total_amount }元
						</div>
						<div>
							<b>支付宝订单号:</b>
							${payentity.trade_no }
						</div>
						<div>
							<b>收款账号:</b>
							易通通全国在校创业自主创业主办方
						</div>
						<div>
							<b>创建时间:</b>
							${payentity.timestamp}
						</div>
						<div>
							<b>支付方式:</b>
							${payentity.method=="alipay.trade.page.pay.return"?"电脑支付":"手机支付"}
						</div>
						<div>
							<b>订单详情:</b>
							${payentity.body}
						</div>
						<c:if test="${iShref==true }">
							<div>
								<a href="${path }" class="layui-btn layui-btn-warm layui-btn-mini">继续购物</a>
								<a href="${path }/userinfo/order?state=2#type=yesplay" class="layui-btn layui-btn-warm layui-btn-mini">返回我的订单管理</a>
							</div>
						</c:if>
					</c:if>
					
					</div>
				</div>
			</div>
		</div>
		<!-- 底部 -->
		<footer class="blog-footer">
			<p><span>Copyright</span><span>&copy;</span><span>2018</span>
				<a href="${basePath}">ESTshopping</a><span>Design By ESTshopping</span></p>
			<p>
				<a href="http://www.miibeian.gov.cn/" target="_blank">陇ICP备16001964号-1</a>
			</p>
		</footer>
		<!--背景-->
		<!-- layui.js -->
		<script src="${basePath}/res/layui2/layui.js"></script>
			<!-- 全局脚本 -->
		<script src="${basePath}/res/timeline/js/global.js"></script>
		<script type="text/javascript">
		layui.use(['jquery','form'], function() {
			var $ = layui.jquery;
			var form = layui.form;
		})
		</script>
		<script src="${basePath}/res/timeline/js/canvas.js"></script>
</body>
</html>