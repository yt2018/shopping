package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.User;

public interface OrderService {
	
	/**
	 * 订单生成
	 * @param order
	 */
	public void saveOrder(Order order);
	/**
	 * 查询订单
	 * @param user
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Order> listorder(User user, Integer state, Integer page, Integer limit);
	/**
	 * 根据订单号查询
	 * @param iof
	 * @return
	 */
	public Order findOif(String iof);
	/**
	 * 删除订单
	 * @param order
	 */
	public void delete(Order order);
	/**
	 * 查询数量
	 * @param user
	 * @param state
	 * @return
	 */
	public Integer count(User user, Integer state);
	/**
	 * 修改订单
	 * @param order
	 */
	public boolean update(Order order);
	/**
	 * 商家查询订单
	 * @param user
	 * @param state
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<Order> listsell(User user, Integer state, Integer page, Integer limit);
	/**
	 * 商家查询数量
	 * @param user
	 * @param i
	 * @return
	 */
	public Integer countsell(User user, Integer i);
	
	

}
