package com.yitong.framework;

import com.alibaba.fastjson.JSON;
import com.yitong.Estshopping.entity.Order;
import com.yitong.Estshopping.entity.Orderlog;
import com.yitong.Estshopping.entity.User;

public class Orderlogtext {
	
	public static Orderlog textlog(User user,String oif,Order order,Integer state) {
		//记录日志
		
				Orderlog orderlog =new Orderlog();
				//操作者
				orderlog.setUser(user);
				orderlog.setUser(user);
				orderlog.setCuser(order.getCuser());
				orderlog.setKuser(order.getUser());
				//订单号
				orderlog.setLogoif(oif);
				//商品标题
				orderlog.setPname(order.getName());
				//商品id
				orderlog.setPid(order.getPid());
				//时间
				//记录内容
				orderlog.setLogobj(JSON.toJSONString(order));
				//记录状态
						/**
						 * 1:未付款   
						 * 2:订单已经付款   
						 * 3:已经发货   
						 * 4:订单结束   
						 * 5:客户删除定单 
						 * 6:商家删除订单  
						 * 7:商家修改地址  
						 * 8:客户修改地址  
						 * 9:商家修改价格  
						 */
				orderlog.setState(state);
				return orderlog;
	}
}
