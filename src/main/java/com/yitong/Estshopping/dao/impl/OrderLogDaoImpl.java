package com.yitong.Estshopping.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.OrderLogDao;
import com.yitong.Estshopping.entity.Orderlog;
import com.yitong.Estshopping.entity.User;
@Repository("orderlogdao")
public class OrderLogDaoImpl extends BaseDaoImpl<Orderlog> implements OrderLogDao {

	@Override
	public List<Orderlog> listorder(User user, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		String hql = "from Orderlog o where o.user.uid='"+user.getUid() +"' or o.cuser.uid='"+user.getUid() +"' or o.kuser.uid='"+user.getUid() +"' order by o.logtime desc";
		return this.find(hql, page, limit);
	}

	@Override
	public Integer listorder(User user) {
		// TODO Auto-generated method stub
		String hql="select count(*) from Orderlog o where o.user.uid='"+user.getUid() +"' or o.cuser.uid='"+user.getUid() +"' or o.kuser.uid='"+user.getUid() +"' order by o.logtime desc";
		return this.count(hql);
	}



}
