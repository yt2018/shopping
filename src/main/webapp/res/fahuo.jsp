<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>基本设置</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
<style type="text/css">
.red{color:red}
</style>
</head>
<body>
	<form class="layui-form">
		<div class="layui-form-item">
			<label class="layui-form-label" for="male">当面交易</label>
			<div class="layui-input-inline">
				<input type="radio" name="kd" id = "male" value="1" checked title="当面交易">
			</div>
		</div>	
		<div class="layui-form-item">
			<label class="layui-form-label" for="male1">快递单号</label>
			<div class="layui-input-inline">
				<input type="radio" name="kd" id = "male1" value="2" title="快递单号">
				<input type="number" id="kuaidi" name="kuaidi" autocomplete="off" value="" class="layui-input" maxlength="16">
			</div>
		</div>	
		<div class="layui-form-item">
			<label class="layui-form-label"></label>
			<div class="layui-input-inline">
				<button class="layui-btn" lay-filter="fahuo" lay-submit="fahuo">发货</button>
			</div>
		</div>	
	</form>
	<% String iof = request.getParameter("iof"); %>
	<script type="text/javascript">var iof="<%= iof %>" </script>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
</body>
</html>