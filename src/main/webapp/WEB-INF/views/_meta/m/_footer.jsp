<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
<style>
	.footMenu{
		position: fixed;
	    bottom: 0;
	    left: 0;
	    width: 100%;
	    text-align: center;
	    height: 50px;
	    line-height: 50px;
	    font-size: 16px;
	    background:#404553;
	    color:#eee
	}
	.footMenu a{
		display: block;
		width: 100%;
		height:100%;
		color:#eee
	}
</style>
<div class="layui-row footMenu">
	<div class="layui-col-xs3"><a href="${basePath}">首页</a></div>
	<div class="layui-col-xs3"><a href="${basePath}/userinfo/sell">卖家中心</a></div>
	<div class="layui-col-xs3"><a href="${basePath}/userinfo/order">我的订单</a></div>
	<div class="layui-col-xs3"><a href="${basePath}/userinfo">个人中心</a></div>
</div>