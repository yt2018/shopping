package com.yitong.Estshopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 商城客户表
 * 
 * @author YT
 *
 */

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "uid")
	private Integer uid;

	@Column(name = "username", length = 32)
	private String userName;

	@Column(name = "password", length = 64)
	private String password;

	@Column(name = "name", length = 32)
	private String name;

	@Column(name = "email", length = 64)
	private String email;
	
	@Column(name = "sex", length = 1)
	private String sex;

	@Column(name = "phone", length = 16)
	private String phone;

	@Column(name = "pwd_key", length = 6)
	private String key;

	@Column(name = "address", length = 255)
	private String addr;
	@Column(name = "qq", length = 255)
	private String qq;
	@Column(name = "icon", length = 255)
	private String icon;
	@Column(name = "rolelog", columnDefinition="text")
	private String rolelog;
	/**
	 * 1正常
	 * 2被禁
	 */
	@Column(name = "state")
	private Integer state;
	@Column(name="sign", columnDefinition="text")
	private String sign;
	
	
	@Column(name="joindate")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  
	private Date joindate;
	

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public Date getJoindate() {
		return joindate;
	}

	public void setJoindate(Date joindate) {
		this.joindate = joindate;
	}

	@Column(name = "code", length = 255)
	private String code;
	@Column(name = "qquser")
	private String qquser;

	@OneToOne(mappedBy = "user")
	@JsonIgnore
	private Wallet wallet;
	
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public String getRolelog() {
		return rolelog;
	}

	public void setRolelog(String rolelog) {
		this.rolelog = rolelog;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getQquser() {
		return qquser;
	}

	public void setQquser(String qquser) {
		this.qquser = qquser;
	}
	
	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", userName=" + userName + ", password=" + password + ", name=" + name + ", email="
				+ email + ", sex=" + sex + ", phone=" + phone + ", key=" + key + ", addr=" + addr + ", icon=" + icon
				+ ", state=" + state + ", code=" + code + ", qquser=" + qquser + ", wallet=" + wallet + "]";
	}

}
