<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>${title }--易通通</title>
	<meta name="keywords" content="${title }易通通,全国在校大学生创业平台,大学生创业平台,EST商城,易斯顿美术学院" />
	<meta name="description" content="${title },校园二手电商网站，实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	<jsp:include page="/WEB-INF/views/_meta/_meta_m.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/WEB-INF/views/_meta/m/_header.jsp"></jsp:include>
		<div class="wappost">
			<div class="ptitle">
				<h2>
					${title } <a href="${basePath}/remen" class="fr" target="_blank">更多查看&gt;</a>
				</h2>
			</div>
			<c:if test="${not empty products}">
			<ul class="layui-row ">
				<c:forEach var="p" items="${products }" begin="0" end="9">
					<li class="layui-col-xs6 layui-col-sm4">
						<a href="${basePath}/Productinfo/${p.pid }">
							<div class="">
								<c:if test="${not empty p.image}">
									<img lay-src="${p.image[0].src }"/>
								</c:if>
								<c:if test="${empty p.image}">
									<img lay-src="${basePath}/res/images/timg.jpg"/>
								</c:if>
								<p style="color: green">${p.pname }</p>
								<div class="layui-row">
									<div class="layui-col-xs3">
										<div class="picon">
											<img src="${p.user.icon }"/>
										</div>
									</div>
									<div class="layui-col-xs9">
										<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
										<c:if test="${not empty user }">
										<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
										</c:if>
										<c:if test="${empty user }">
										<span style="color: red;font-size: 10px">现价: <b>登录即可查看优惠价</b></span>
										</c:if>
									</div>
								</div>
							</div>
						</a>
					</li>
				</c:forEach>
			</ul>
			</c:if>
			<c:if test="${count==0}"> <c:set var="hide" value="layui-hide"/></c:if>
			<div id="pagedemo" style="clear: both;float: right;" class="${hide }"></div>
			<c:if test="${empty products}">
			<blockquote class="layui-elem-quote layui-quote-nm" style="clear: both;">没有数据,请看看其他商品</blockquote>
			<ul class="layui-row ">
				<c:forEach var="p" items="${productssuiji }" begin="0" end="9">
					<li class="layui-col-xs6 layui-col-sm4">
						<a href="${basePath}/Productinfo/${p.pid }">
							<div class="">
								<c:if test="${not empty p.image}">
									<img lay-src="${p.image[0].src }"/>
								</c:if>
								<c:if test="${empty p.image}">
									<img lay-src="${basePath}/res/images/timg.jpg"/>
								</c:if>
								<p style="color: green">${p.pname }</p>
								<div class="layui-row">
									<div class="layui-col-xs3">
										<div class="picon">
											<img src="${p.user.icon }"/>
										</div>
									</div>
									<div class="layui-col-xs9">
										<span style="color:#23262E;">原价: ￥<s> ${p.marketPrice } </s>元</span><br>
										<c:if test="${not empty user }">
										<span style="color: red;">现价: ￥<b> ${p.shopPrice} </b>元</span>
										</c:if>
										<c:if test="${empty user }">
										<span style="color: red;font-size: 10px">现价: <b>登录即可查看优惠价</b></span>
										</c:if>
									</div>
								</div>
							</div>
						</a>
					</li>
				</c:forEach>
			</ul>
			</c:if>
			
		</div>
		
		<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
		<jsp:include page="/WEB-INF/views/_meta/m/_footer.jsp"></jsp:include>
		<script type="text/javascript">
			layui.use(['form','layer','jquery','element','flow','laypage'],function(){
				var form = layui.form,
					layer = parent.layer === undefined ? layui.layer : parent.layer,
					laypage = layui.laypage,
					$ = layui.jquery,
					element = layui.element;
				var flow = layui.flow;
				  flow.lazyimg({
				    elem: 'body img'
				    //,scrollElem: '#LAY_demo3' //一般不用设置，此处只是演示需要。
				  });
				//当前页
				var page=${page};
				var limit=${limit};
				laypage.render({ 
					 elem: 'pagedemo'
					,curr: page//选中当前页 
					,limit: limit
					,limits:[12, 24, 36, 48, 60]
  					,count: ${count} //数据总数，从服务端得到 
  					,layout: ['count', 'prev', 'page', 'next', 'limit']
					,jump: function(obj,first){
					    //obj包含了当前分页的所有参数，比如：
					    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
					    console.log(obj.limit); //得到每页显示的条数
					    if(page!=obj.curr){
					    	window.location.href="?page="+obj.curr+"&limit="+obj.limit;
						}
					}
				})
			})
		</script>
		
</body>
</html>