package com.yitong.Estshopping.bean;

import com.yitong.Estshopping.entity.Product;

/**
 * 购物项的实体类
 * @author YT
 *
 */
public class CartItem {
	
	/**
	 * 购物项中的商品信息
	 */
	private Product product;
	
	/**
	 * 价格
	 */
	private Float price = 1.0f;
	/**
	 * 图片显示
	 */
	private String imgsrc;
	/**
	 * 商品的数量
	 */
	private int count;
	
	/**
	 * 价格小计
	 */
	@SuppressWarnings("unused")
	private float subtotal;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public float getSubtotal() {
		return count * getPrice();
	}

	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}

	public String getImgsrc() {
		return imgsrc;
	}

	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}
	


}
