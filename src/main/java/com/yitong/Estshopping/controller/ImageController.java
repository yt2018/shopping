package com.yitong.Estshopping.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.yitong.Estshopping.entity.Image;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.UserService;
import com.yitong.framework.utils.ImageRemarkUtil;
import com.yitong.framework.utils.ImageZipUtil;

/**
 * 图片处理
 * @author YT
 *
 */

@Controller
public class ImageController {
	@Autowired
	private UserService userService;
	private String rootPath;
	private List<Image> ingids=new ArrayList<>();;
	@Autowired
	private ImageService imageService;


/*	*//**
	 * 预上传图片
	 * @return
	 *//*
	@RequestMapping(value="/upload/{classify}")
	public@ResponseBody Map<String, Object> showIndex(@PathVariable int classify,@RequestParam MultipartFile file,HttpServletRequest request,HttpSession session){
		*//**
		 * 图片上传分类
		 * 0====代表临时文件(默认设为0)
		 * 1====代表商品图片
		 * 2====代表商品描述
		 * 3====代表评论图片
		 * 4====代表头像图片
		 *//*
		
		System.out.println(file+"______"+this.getClass());
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> src = new HashMap<String,Object>();
		User user = (User) session.getAttribute("user");
		Integer uid = user.getUid();
		String fileupload="upload"+System.getProperty("file.separator")+uid+System.getProperty("file.separator")+classify;
		rootPath = request.getRealPath(fileupload);
		if(file==null||user==null) {
			map.put("code", 1);
			map.put("msg", "文件上传失败");
			map.put("data", src);
			return map;
		}
		
		//将上传的图片写入指定的文件
		if(file!=null){
			//获取上传文件的名称
			String fileName = file.getOriginalFilename();
			String suffix = fileName.substring(fileName.lastIndexOf("."));
			//为了保险起见,我们给上传的图片重新指定一个名称
			String tempFileName = UUID.randomUUID().toString()+suffix;
			if(classify==4) {
				tempFileName=uid+"icon"+suffix;
			}
			//获取上传上传的后缀
			File fileTemp = new File(rootPath);
			if(!fileTemp.exists()){
				fileTemp.mkdirs();
			}
			
			File filesrc = new File(rootPath+System.getProperty("file.separator")+tempFileName);
			try {
				
				//讲上传的文件写入指定路径
				String path = filesrc.getPath();
				System.out.println(path);
				file.transferTo(filesrc);
				long size = file.getSize();
				System.out.println("文件大小"+size);
				if(size>1048576) {
					//压缩图片处理
					ImageZipUtil.zipImageFile(fileTemp, fileTemp,0,0, 1f);
				}
				java.awt.Image read = ImageIO.read(filesrc);
				int height = read.getHeight(null);
				int width = read.getWidth(null);
				String srcstring=request.getContextPath()+System.getProperty("file.separator")+fileupload+System.getProperty("file.separator")+tempFileName;
				//上传头像
				if(classify==4) {
					User user2 = new User();
					user2.setUid(uid);
					user2.setIcon(srcstring);
					userService.updateUser(user2);
					map.put("code", 0);
					map.put("msg", "文件上传上传成功");
					map.put("data", srcstring+"?"+ UUID.randomUUID());
					return map;
				}
				if(height<400 && width<400) {
					map.put("code", 2);
					map.put("msg", "图片尺寸太小了,最小大于400px");
				}else {
					
					ImageRemarkUtil.markImageByText("@"+user.getUserName()+"--ESTshopping", path, path);
					
					Image image = new Image();
					System.out.println(srcstring);
					image.setImgsrc(srcstring);
					image.setName(fileName);
					image.setState(0);
					image.setClassify(classify);
					image.setIwidth(width);
					image.setIheight(height);
					image.setUser(user);
					
					imageService.save(image);
					map.put("src", srcstring);
					src.put("title", fileName);
					map.put("code", 0);
					map.put("msg", "文件上传上传成功");
					map.put("data", src);
				}
			} catch (IllegalStateException e) {
 				e.printStackTrace();
			} catch (IOException e) {
 				e.printStackTrace();
			}
		}
		

		return map;
	}
*/	
	
	/**
	 * 保存图片方法调用
	 * 先全部保存
	 * 调用图片id保存商品和图片表
	 * @param imgid 
	 * @param product 
	 */
	/*public List<Image> getIngids() {
		for (Image image : ingids) {
			System.out.println(image.getImgsrc());
		}
		return this.ingids;
	}
	public void setIngids(List<Image> ingids) {
		this.ingids = ingids;
	}*/


	/**
	 *  管理员操作删除文件缓存，释放内存
	 *  删除缓存文件
	 * @return
	 */
	/*@RequestMapping("admin/imagesdelete")
    public Map<String, Object> register() {
		List<Image> classify = imageService.classify();
		List<String> imgsrclist = new ArrayList<String>(); 
		for (Image image : classify) {
			String imgsrc = image.getImgsrc();
			imgsrclist.add(imgsrc);
		}
		System.out.println("长度----"+imgsrclist.size());
		for (String string : imgsrclist) {
			System.out.println(string);
		}
		
		//ImageUtils.eq(new File("H:\\workspace\\ESTshopping\\src\\main\\webapp\\res\\images"), "404.gif");
		ImageUtils.eqs(new File("H:\\workspace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\ESTshopping\\upload"),imgsrclist);
		System.out.println("清除完毕");
		return null;
    }*/

	@RequestMapping("/userinfo/removeimg")
	public @ResponseBody Map<String, Object> removeimg(@RequestParam String src){
		Image image = imageService.getImgid(src);
		imageService.updateImgid(image.getImgid(), null);
		return null;
	}
	

	
}
