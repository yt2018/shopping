package com.yitong.Estshopping.dao.impl;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.WalletLogDao;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.entity.Walletlog;
@Repository("walletlogdao")
public class WalletLogDaoImpl extends BaseDaoImpl<Walletlog> implements WalletLogDao {

	@Override 
	public List<Walletlog> listorder(User user, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		String hql = "from Walletlog w where w.user.uid='"+user.getUid() +"' order by w.logtime desc";
		return this.find(hql, page, limit);
	}

	@Override
	public Integer listorder(User user) {
		// TODO Auto-generated method stub
		String hql="select count(*) from Walletlog w where w.user.uid='"+user.getUid() + "' order by w.logtime desc";
		return this.count(hql);
	}

	@Override
	public Float usercount(User user, Integer states) {
		// TODO Auto-generated method stub
		String hql="select count(*) from Walletlog w where w.user.uid='"+user.getUid() + "' and to_days(w.logtime) = to_days(now()) and w.states = '"+states+"'";
		Integer count = this.count(hql);
		float parseFloat=0f;
		if(count==0) {
			parseFloat=0f;
		}else {
		String hql1="select sum(w.money) from Walletlog w where w.user.uid='"+user.getUid()+"' and to_days(w.logtime) = to_days(now()) and w.states = '"+states+"'";
		Query createQuery = this.getCurrentSession().createQuery(hql1);
		String string = createQuery.uniqueResult().toString();
		System.out.println("得到的值"+ string);
		parseFloat = Float.parseFloat(string);
		}
		return parseFloat;
	}



}
