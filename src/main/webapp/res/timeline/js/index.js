layui.use('jquery', function (url) {
    var $ = layui.jquery;
	//文章条数
	var html='';
	var url="../getIndex.json"
	$.getJSON(url,function(data){
		$.each(data.art,function(infoIndex,info){
			console.info(info['img'])
			//插入左边的文章
			html+='<div class="article shadow">'
					+'	<div class="article-left">'
					+'		<img src="'+info['img']+'" alt="'+info['title']+'" />'
					+'	</div>'
					+'	<div class="article-right">'
					+'		<div class="article-title">'
					+'			<a href="'+info['href']+'">'+info['title']+'</a>'
					+'		</div>'
					+'		<div class="article-abstract">'
					+			info['artic']
					+'		</div>'
					+'	</div>'
					+'	<div class="clear"></div>'
					+'	<div class="article-footer">'
					+'		<span><i class="fa fa-clock-o"></i>&nbsp;&nbsp;'+info['time']+'</span>'
					+'		<span class="article-author"><i class="fa fa-user"></i>&nbsp;&nbsp;'+info['author']+'</span>'
					+'		<span><i class="fa fa-tag"></i>&nbsp;&nbsp;<a href="javascript:layer.msg(\'启动标签。。。。正在修复中\')">'+info['tag']+'</a></span>'
					+'		<span class="article-viewinfo"><i class="fa fa-eye"></i>&nbsp;'+info['eye']+'</span>'
					+'		<span class="article-viewinfo"><a href="javascript:layer.msg(\'启动评论 。。。。正在修复中\')"><i class="fa fa-commenting"></i>&nbsp;'+info['commenting']+'</a></span>'
					+'	</div>'
					+'</div>'
			$('.art').html(html)
		})
		html=''
		$.each(data.desc,function(infoIndex,info){
			html+='<li><i class="fa-li fa fa-hand-o-right"></i>'
					+'	<a href="'+info['href']+'">'+info['title']+'</a>'
					+'</li>';
			$('.desc').html(html)
		})
		html=''
		$.each(data.res,function(infoIndex,info){
			html+='<li><i class="fa-li fa fa-hand-o-right"></i>'
					+'	<a href="'+info['href']+'" target="_blank">'+info['title']+'</a>'
					+'</li>';
			$('.res').html(html)
		})
		html=''
		$.each(data.weblog,function(infoIndex,info){
			html+='<dt>'+info['time']+'</dt>'
					+'<dd>'+info['title']+'</dd>';
			$('.weblog').html(html)
		})
		html=''
		$.each(data.backlog,function(infoIndex,info){
			html+='<dt>'+info['time']+'</dt>'
					+'<dd>'+info['title']+'</dd>';
			$('.backlog').html(html)
		})
		html=''
		$.each(data.friendship,function(infoIndex,info){
			html+='<li><a target="_blank" href="'+info['href']+'" title="'+info['title']+'">'+info['title']+'</a></li>';
			$('.friendship').html(html)
		})
	})
})
