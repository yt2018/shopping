layui.use(['carousel','flow','jquery'], function(){
			  var carousel = layui.carousel;
			  var flow = layui.flow;
			  var $=layui.jquery;
				  flow.lazyimg({
				    elem: 'body img'
				    //,scrollElem: '#LAY_demo3' //一般不用设置，此处只是演示需要。
				  });
				  var bf=$(window).width()>760
			  //建造实例
			  carousel.render({
			    elem: '#test1'
			    ,width: bf ?760: $(window).width()//设置容器宽度
			    ,height:bf?300:$(window).width()*300/760
			    ,arrow: 'hover' //始终显示箭头
			    ,anim: 'fade' //切换动画方式
			  }); 
			  carousel.on('change(test1)',function(obj){
			  		console.log(obj.index); //当前条目的索引
					console.log(obj.prevIndex); //上一个条目的索引
					console.log(obj.item); //当前条目的元素对象
					var color=(obj.item).find('img').data('color');
					console.info(color);
					console.info(color.colorRgb());
					$(".b_b_g,.header,.dl-menuwrapper button,.dl-menuwrapper ul,.footMenu").css({"background-color":color})
					$(".banner .b_nav .b_all .bb_all li").css({"background":color.colorRgb(20)})
					$(".oneall").css({"background":color.colorRgb(30),})
					$(".shopping-nav").css({"border-bottom": "3px solid "+color.colorRgb(30)})
					$(".banner .b_nav .b_all .bb_all li,.dl-menuwrapper li a").css({"border-bottom": "1px solid "+color.colorRgb(30)})
					$(".shopping-nav ul li a").css({"color": color.colorRgb(30)})
			  });
			});
			//十六进制颜色值的正则表达式  
			var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;  
			/*RGB颜色转换为16进制*/  
			String.prototype.colorHex = function(){  
			    var that = this;  
			    if(/^(rgb|RGB)/.test(that)){  
			        var aColor = that.replace(/(?:\(|\)|rgb|RGB)*/g,"").split(",");  
			        var strHex = "#";  
			        for(var i=0; i<aColor.length; i++){  
			            var hex = Number(aColor[i]).toString(16);  
			            if(hex === "0"){  
			                hex += hex;
			            }  
			            strHex += hex; 
			        }  
			        if(strHex.length !== 7){  
			            strHex = that;    
			        }  
			        return strHex;  
			    }else if(reg.test(that)){  
			        var aNum = that.replace(/#/,"").split("");  
			        if(aNum.length === 6){  
			            return that;      
			        }else if(aNum.length === 3){  
			            var numHex = "#";  
			            for(var i=0; i<aNum.length; i+=1){  
			                numHex += (aNum[i]+aNum[i]);  
			            }  
			            return numHex;  
			        }  
			    }else{  
			        return that;      
			    }  
			};  
			  
			//-------------------------------------------------  
			  
			/*16进制颜色转为RGB格式*/  
			String.prototype.colorRgb = function(val){  
				if(!val)val=0;
			    var sColor = this.toLowerCase();  
			    if(sColor && reg.test(sColor)){  
			        if(sColor.length === 4){  
			            var sColorNew = "#";  
			            for(var i=1; i<4; i+=1){  
			                sColorNew += sColor.slice(i,i+1).concat(sColor.slice(i,i+1));     
			            }  
			            sColor = sColorNew;  
			        }  
			        //处理六位的颜色值  
			        var sColorChange = [];  
			        for(var i=1; i<7; i+=2){ 
			        	
			            sColorChange.push(parseInt("0x"+sColor.slice(i,i+2))*1-val);    
			        }  
			        return "RGB(" + sColorChange.join(",") + ")";  
			    }else{  
			        return sColor;    
			    }  
			};