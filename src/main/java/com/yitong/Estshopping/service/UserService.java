package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.entity.User;

public interface UserService {
	
	/**
	 * 客户注册 
	 */
	public void registerUser(User user);
	
	/**
	 *判断客户是否存在
	 * @param userName
	 * @return
	 */
	public User existUser(String userName);
	
	/**
	 * 根据用户名和密码查询用户 
	 */
	public User findByUserNameAndPassword(User user);
	/**
	 * QQ登录查找用户
	 * @param openID
	 * @return
	 */
	public User findByUserQQ(String openID);
	/**
	 * 根据uid查询
	 * @param uid
	 * @return
	 */
	public User findUserId(Integer uid);
	/**
	 * 更新user
	 * @param user
	 */
	public void updateUser(User user);

	public boolean changePassage(Integer uid, String oldpassword, String newpassword);
	
	/**
	 * 解除绑定QQ
	 * @param uid 
	 */
	public void relieveQQ(Integer uid);
	/**
	 * 查询用户列表
	 * @param page
	 * @param limit
	 * @param object 
	 * @return
	 */
	public List<User> userList(Integer page, Integer limit, Integer states);
	/**
	 * 查询数量
	 * @param states
	 * @return
	 */
	public Object countUsers(Integer states);



}
