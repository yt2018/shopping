package com.yitong.Estshopping.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.Banner;
import com.yitong.Estshopping.service.BannerService;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.ProductService;

/**
 * 商城首页
 * @author YT
 *
 */

@Controller
public class IndexController {
	
	@RequestMapping(value= {"/","/index"})
	public String listProductoff(HttpServletRequest request,HttpSession session, HttpServletResponse response){
		Device device = DeviceUtils.getCurrentDevice(request);
		 if (device.isMobile()) {
			 System.out.println("手机端");
			 return "mobile/index";
        } else if (device.isNormal()) {
        	System.out.println("isNormal");
        	return "index";
        } else if (device.isTablet()) {
        	System.out.println("isTablet");
        	return "index";
        } else {
        	System.out.println("其他");
        	return "index";
        }
	}
	@RequestMapping(value= {"/db","/pma","/dbadmin","/phpmyadmin","/DB","/PMA","/DBADMIN","/PHPMYADMIN"})
	public String db(HttpServletRequest request,HttpSession session, HttpServletResponse response){
		
		return "db";
	}
	@RequestMapping(value= "/fgme")
	public String fgme(HttpServletRequest request,HttpSession session, HttpServletResponse response){
		
		return "fgme";
	}
	
	
}
