<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>该商品不存在,或者已经被下架了---易通通|tong118.com</title>
	<meta name="keywords" content="易斯顿,大学生,tong118,易通通,全国在校大学生创业平台,大学生创业,大学生创业平台,校园二手电商网站,EST商城,易斯顿美术学院" />
	<meta name="description" content="易斯顿校园二手电商网站，易通通实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	<jsp:include page="/WEB-INF/views/_meta/_meta_m.jsp"></jsp:include>
</head>
<body>
		<div style="position: fixed; top:0;left:0;width: 100%;height:50px;opacity: 0.5;z-index:999">
			<a href="javascript:history.back()" class="layui-btn" style="position:absolute; top:5px;left:10px"><i class="layui-icon">&#xe603;</i></a>
			<a href="${basePath}"  class="layui-btn" style="position: absolute; top:5px;right:10px"><i class="layui-icon">&#xe68e;	</i> </a>
		</div>
		
		<div class="box_content" style="text-align: center;">
			<img alt="该商品不存在,或者已经被下架了" src="${basePath}/res/images/404.gif" style="margin: 20px auto;">
			<p>该商品不存在,或者已经被下架了</p>
		</div>
		<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
		<jsp:include page="/WEB-INF/views/_meta/m/_footer.jsp"></jsp:include>
</body>
</html>