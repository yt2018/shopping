SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS adminUser;
DROP TABLE IF EXISTS orderitem;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS categorysecond;
DROP TABLE IF EXISTS ticket;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS packet;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS wallet;
DROP TABLE IF EXISTS user;




/* Create Tables */

-- 管理员表
CREATE TABLE adminUser
(
	adminid int NOT NULL AUTO_INCREMENT,
	-- 管理员权限,角色
	roleid int DEFAULT 1 NOT NULL COMMENT '管理员权限,角色',
	rid int NOT NULL,
	uid int NOT NULL,
	PRIMARY KEY (adminid),
	UNIQUE (uid)
) COMMENT = '管理员表';


CREATE TABLE category
(
	cid int NOT NULL AUTO_INCREMENT,
	cname varchar(255) NOT NULL,
	discount float DEFAULT 0 NOT NULL,
	-- 优惠时间
	privilegeTime datetime COMMENT '优惠时间',
	PRIMARY KEY (cid),
	UNIQUE (cid)
);


CREATE TABLE categorysecond
(
	csid int NOT NULL AUTO_INCREMENT,
	csname varchar(255) NOT NULL,
	cid int NOT NULL,
	PRIMARY KEY (csid),
	UNIQUE (cid)
);


CREATE TABLE orderitem
(
	itemid int NOT NULL AUTO_INCREMENT,
	-- 数量
	count int DEFAULT 0 NOT NULL COMMENT '数量',
	-- 总金额
	subtotal float DEFAULT 0 NOT NULL COMMENT '总金额',
	oid int NOT NULL,
	pid int NOT NULL,
	PRIMARY KEY (itemid),
	UNIQUE (itemid)
);


CREATE TABLE orders
(
	oid int NOT NULL AUTO_INCREMENT,
	addr varchar(255),
	name varchar(255),
	ordertime datetime,
	phone varchar(16) NOT NULL,
	state int DEFAULT 0 NOT NULL,
	total float DEFAULT 0 NOT NULL,
	uid int NOT NULL,
	PRIMARY KEY (oid),
	UNIQUE (uid)
);


CREATE TABLE packet
(
	-- 卡包编号
	pacid int NOT NULL AUTO_INCREMENT COMMENT '卡包编号',
	uid int NOT NULL,
	PRIMARY KEY (pacid),
	UNIQUE (uid)
);


CREATE TABLE product
(
	pid int NOT NULL AUTO_INCREMENT,
	-- 商品名称
	pname varchar(255) NOT NULL COMMENT '商品名称',
	-- 市场价
	market_price float DEFAULT 0 NOT NULL COMMENT '市场价',
	-- 成交价
	shop_price float DEFAULT 0 NOT NULL COMMENT '成交价',
	-- 存货量
	inventory int NOT NULL COMMENT '存货量',
	-- 图片地址
	image varchar(255) NOT NULL COMMENT '图片地址',
	-- 商品描述
	pdesc varchar(255) COMMENT '商品描述',
	-- 是否是热门商品
	is_hot int DEFAULT 0 NOT NULL COMMENT '是否是热门商品',
	pdate datetime,
	csid int NOT NULL,
	PRIMARY KEY (pid)
);


CREATE TABLE role
(
	rid int NOT NULL AUTO_INCREMENT,
	-- 权限名
	rolename char NOT NULL COMMENT '权限名',
	PRIMARY KEY (rid)
);


-- 优惠券表
CREATE TABLE ticket
(
	tid int NOT NULL AUTO_INCREMENT,
	-- 优惠券金额
	privilege float NOT NULL COMMENT '优惠券金额',
	-- 满减金额
	consume float DEFAULT 0 NOT NULL COMMENT '满减金额',
	-- 使用期限
	useTime datetime COMMENT '使用期限',
	cid int NOT NULL,
	-- 卡包编号
	pacid int NOT NULL COMMENT '卡包编号',
	PRIMARY KEY (tid),
	UNIQUE (cid)
) COMMENT = '优惠券表';


CREATE TABLE user
(
	uid int NOT NULL AUTO_INCREMENT,
	-- 客户名称
	username varchar(255) NOT NULL COMMENT '客户名称',
	-- 密码
	password varchar(255) NOT NULL COMMENT '密码',
	-- 客户昵称
	name varchar(255) NOT NULL COMMENT '客户昵称',
	-- 邮箱地址
	email varchar(255) COMMENT '邮箱地址',
	-- 电话 
	phone varchar(16) COMMENT '电话 ',
	-- 年龄
	age int DEFAULT 0 NOT NULL COMMENT '年龄',
	-- 地址
	addr varchar(255) COMMENT '地址',
	-- 状态
	state int DEFAULT 0 NOT NULL COMMENT '状态',
	code varchar(64),
	PRIMARY KEY (uid),
	UNIQUE (uid)
);


CREATE TABLE wallet
(
	wid int NOT NULL AUTO_INCREMENT,
	money float DEFAULT 0 NOT NULL,
	uid int NOT NULL,
	PRIMARY KEY (wid),
	UNIQUE (wid),
	UNIQUE (uid)
);



/* Create Foreign Keys */

ALTER TABLE categorysecond
	ADD FOREIGN KEY (cid)
	REFERENCES category (cid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ticket
	ADD FOREIGN KEY (cid)
	REFERENCES category (cid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE product
	ADD FOREIGN KEY (csid)
	REFERENCES categorysecond (csid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE orderitem
	ADD FOREIGN KEY (oid)
	REFERENCES orders (oid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ticket
	ADD FOREIGN KEY (pacid)
	REFERENCES packet (pacid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE orderitem
	ADD FOREIGN KEY (pid)
	REFERENCES product (pid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE adminUser
	ADD FOREIGN KEY (rid)
	REFERENCES role (rid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE adminUser
	ADD FOREIGN KEY (uid)
	REFERENCES user (uid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE orders
	ADD FOREIGN KEY (uid)
	REFERENCES user (uid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE packet
	ADD FOREIGN KEY (uid)
	REFERENCES user (uid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE wallet
	ADD FOREIGN KEY (uid)
	REFERENCES user (uid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



