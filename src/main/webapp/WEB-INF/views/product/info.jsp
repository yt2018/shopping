<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>${product.pname }--易通通</title>
	<meta name="keywords" content="${product.pname }易通通,全国在校大学生创业平台,大学生创业平台,EST商城,易斯顿美术学院" />
	<meta name="description" content="${product.pnamedesc},
易通通,校园二手电商网站，实现物质资源节约，环境保护的作用。 当前大学生生活水品层次不齐，爱追赶时尚潮流，而且购买行为往往缺乏计性，常常因一时冲动买下某物，之后又发现不喜欢有没用。在这种情况下，对学生而言，闲置物品，扔掉简直是弃如敝屣，每到毕业季是校园环境犹如一片狼藉，造成环境的污染，资源的浪费。利于互联网信息资源提供方便的通道，提倡大学生创新能力，和思考能力，与时俱进。" />
	<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
	<link rel="stylesheet" href="${basePath}/res/css/productinfo.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/_header.css" media="all" />
	<link rel="stylesheet" href="${basePath}/res/css/ribbon.css" media="all" />
	
	
	<style type="text/css">
		.productbody .p_img img{background: url("${basePath}/res/css/img/logo-03.png") no-repeat center center;background-size: cover;}
		.clear{clear:both;}
		.pinglunqu{margin-bottom: 30px;}
		.zhuiping{margin: 0 0 0 30px;}
		.pinglunqu>li{position: relative;border-bottom: 1px dotted #DFDFDF;margin: 10px 0;}
		.pinglunqu>li:last-child{border-bottom: none;}
		.pinglunqu .fly-none{height: 50px; min-height: 0;}
		.pinglunqu .icon-caina{position:absolute; right:10px; top:15px; font-size:60px; color: #58A571;}
		.detail-about-reply{padding: 0 0 0 55px; background: none;}
		.detail-about-reply .detail-hits{left: 0; bottom: 0;margin-top: 12px;}
		.detail-about-reply .fly-avatar{left: 0; top: 0;}
		
		.pinglunqu-body{margin: 5px 0; min-height: 0; line-height: 24px; font-size:14px;}
		.pinglunqu-body .s_pinglun{margin-bottom: 10px;background: #eee;}
		.pinglunqu-body a{color:#4f99cf}
		.pinglunqu-reply{position:relative;}
		.pinglunqu-reply span{padding-right:20px; color:#999; cursor:pointer;}
		.pinglunqu-reply span:hover{color:#666;}
		.pinglunqu-reply span i{margin-right:5px; font-size:16px;}
		.pinglunqu-reply span em{font-style: normal;}
		.pinglunqu-reply span .icon-zan{font-size: 22px;}
		.pinglunqu-reply .zanok,
		.pinglunqu-reply .pinglunqu-zan:hover{color:#c00}
		.pinglunqu-reply span .icon-svgmoban53{position: relative; top: 1px;}
		.fly-avatar{position: absolute; left: 15px; top: 15px;}
		.fly-avatar img{display: block; width: 45px; height: 45px; margin: 0; border-radius: 2px;}
		.fly-link{color: #01AAED;}
		.fly-link:hover{color: #5FB878;}
		.fly-grey{color: #999;}
		.layui-layedit {
		    border: 1px solid #009688;
		}
		.layui-layedit-tool {
		    padding: 0;
		    border-bottom: 1px solid #009688;
		 }
		 .layui-layedit-tool .layui-icon {
		    margin: 0;
		    padding: 5px;
		    color: #009688;
		    display: inline-block;
		    border-right: 1px solid #009688;
		}
		.p_desc .p_img img,.pinglunqu-body img{
			max-width: 100%;
		}
		.pinglunqu-body img[src*='face']{
			width:auto;
		}
		blockquote,.gray{
			-webkit-filter: grayscale(100%);
		    -moz-filter: grayscale(100%);
		    -ms-filter: grayscale(100%);
		    -o-filter: grayscale(100%);
		    filter: grayscale(100%);
		    filter: gray;
		}
	</style>
</head>
<body>
	<jsp:include page="/WEB-INF/views/_meta/_header.jsp"></jsp:include>
	<div class='productbody'>
		<c:if test="${product.inventory<1 }">
			<span class="ribbon2"><span>商品已售尽</span></span>
		</c:if>
		<c:if test='${product.isState=="off" }'>
			<span class="ribbon3"><span>商品被禁止</span></span>
		</c:if>
			<div id="wrap" class='${product.isState!="on"?"gray":""}'>
				<div class="pic">
					<div class="pic-cover">
						<c:forEach var="i" items="${product.image }" begin="0" end="0" >
							<img src="${i.src }" width="400" height="400"/>
						</c:forEach>
						<div class="cover"></div>
						<div class="show"></div>
					</div>
				</div>
				<div class="tab">
					<ul>
						<c:forEach var="i" items="${product.image }" begin="0" end="0" >
							<li class="on"><img src="${i.src }" w="${i.width }" h="${i.height }"/></li>
						</c:forEach>
						<c:forEach var="i" items="${product.image }" begin="1" end="4" >
							<li><img src="${i.src }" w="${i.width }" h="${i.height }"/></li>
						</c:forEach>
						<%-- <li class="on"><img src="${basePath}/res/productinfo/img/1.jpg" /></li>
						<li ><img src="${basePath}/res/productinfo/img/2.jpg" /></li>
						<li><img src="${basePath}/res/productinfo/img/3.jpg" /></li>
						<li><img src="${basePath}/res/productinfo/img/4.jpg" /></li>
						<li><img src="${basePath}/res/productinfo/img/5.jpg" /></li> --%>
					</ul>
					<span style="position: absolute;bottom: 22px">(访问量 ${product.pnub })</span>
				</div>
			</div>
			<div class="productinfo">
				<h1>${product.pname }</h1>
				<p>${product.pnamedesc}</p>
				<div class="layui-row promo-meta">
					<div class="layui-col-xs2">
						原价格
					</div>
					<div class="layui-col-xs10" style="color: red;">
						￥<del> ${product.marketPrice } </del>
					</div>
					<div class="layui-col-xs2">
						促销价
					</div>
					<c:if test="${not empty user }">
					<div class="layui-col-xs10" style="color: red;">
						￥<b> ${product.shopPrice} </b>元
					</div>
					</c:if>
					<c:if test="${empty user }">
					<div class="layui-col-xs10" style="color: red;">
						登录即可享受优惠价
					</div>
					</c:if>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						配送
					</div>
					<div class="layui-col-xs10" style="color: red;">
						本校内免费配送<br>
						校外根据卖家情况,收取邮费
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						数量
					</div>
					<div class="layui-col-xs10">
						 <input type="text" name="number" id="pnub" lay-verify="required|number" autocomplete="off" class="layui-input"value="1" style="width: 60px;display: inline-table;">
						<span>件 (库存${product.inventory }件)</span>
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">
						声明
					</div>
					<div class="layui-col-xs10" style="color: red;">
						本站为了学生方便提供闲置物品存放网站<br>
						为了确实商品是否属实,建议当面交易<br>
						本站只提供商品上架，联系服务，不承担商品交易风险<br>
						若有商品虚假，进行举报，<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=1810258114&amp;site=qq&amp;menu=yes">网站管理员</a>尽快处理。<br>
					</div>
				</div>
				<div class="layui-row">
					<div class="layui-col-xs2">&nbsp;
					</div>
					<c:if test="${product.inventory<1 }">
						<div class="layui-col-xs10">
							<a class="layui-btn layui-btn-gray nobtn" data-mrg="你来晚了已经抢光了" href="javasript:;">立即购买</a>
							<!-- <a class="layui-btn layui-btn-gray" href="javascript:layer.msg('你来晚了已经抢光了');"><i class="layui-icon">&#xe657;</i> 加入购物车</a> -->
						</div>
					</c:if>
					<c:if test="${product.inventory>=1 }">
					<c:if test="${user.key == product.user.key}">
					
						<div class="layui-col-xs10">
							<a class="layui-btn layui-btn-gray" href="javascript:;" data-mrg='你很皮,这是自己的商品,不需要购买'>立即购买</a>
							<!-- <a class="layui-btn layui-btn-gray" href="javascript:layer.msg('你很皮,这是自己的商品,不需要购买');"><i class="layui-icon">&#xe657;</i> 加入购物车</a> -->
						</div>
					</c:if>
					<c:if test="${user.key != product.user.key}">
					<div class="layui-col-xs10">
						<a class="layui-btn layui-btn-red" id="buybtn" href="javascript:;"><i class="layui-icon">&#xe657;</i> 立即购买</a>
						<!-- <button class="layui-btn layui-btn-danger cartbtn" id="cartbtn"><i class="layui-icon">&#xe657;</i> 加入购物车</button> -->
						<button class="layui-btn layui-btn-primary">举报</button>
					</div>
					</c:if>
					</c:if>
				</div>
			</div>
			<div class="userinfo">
				<a href="${basePath}/u/${product.user.uid }">
					<div class="user_icon">
						<img src="${product.user.icon }"/>
					</div>
					<div class="username">
						<h1>${product.user.userName }</h1>
						<p>个人店部</p>
					</div>
					<div class="layui-row">
						<div class="layui-col-xs4">
							商品
							<p>${count }件</p>
						</div>
						<div class="layui-col-xs4" style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
							访问量
							<p>${sumnub }次</p>
						</div>
						<div class="layui-col-xs4">
							贡献
							<p>125件</p>
						</div>
					</div>
				</a>
				<div class="tauser">
					<a class="layui-btn layui-btn-warm layui-btn-mini" href="${basePath}/u/${product.user.uid }">Ta的主页</a>
					<button class="layui-btn layui-btn-mini">发消息</button>
				</div>
				<div class="zuijin">
					<fieldset class="layui-elem-field layui-field-title">
						<legend>最近发布</legend>
					</fieldset>
					<ul>
						<c:forEach var="p" items="${userProduct }" begin="0" end="5">
						<li><a href="${basePath}/Productinfo/${p.pid }">${p.pname}</a></li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
		<div class="productbody">
			<div class="p_desc">
				<div class="layui-tab" lay-filter="type">
					<ul class="layui-tab-title">
						<li class="layui-this" lay-id="p_info">商品详情</li>
						<li lay-id="p_desc">商品参数</li>
						<li class="pinglun " lay-id="p_pinglun" data-pid = "222">评论<span class="layui-badge-dot layui-bg-orange"></span></li>
					</ul>
					<div class="layui-tab-content">
						<div class="layui-tab-item layui-show p_img">
							${product.pdesc }
							<c:forEach var="i" items="${product.image }" >
							<div><img lay-src="${i.src }"/></div>
							</c:forEach>
						</div>
						<c:if test="${empty product.parameter}">
							<div class="layui-tab-item">商家没有提供该商品参数</div>
						</c:if>
						<c:if test="${not empty product.parameter}">
							<div class="layui-tab-item">${product.parameter}</div>
						</c:if>
						<div class="layui-tab-item" style="position: relative;">
						<form class="layui-form" action="" id="zhuipingtext">
							<textarea id="yttpinglun" style="display: none;" class="layui-textarea fly-editor"></textarea>
							<c:if test="${empty user }">
							<div style="position: absolute;top: 0;left: 0; width: 100%;height:234px;background: rgba(128, 128, 128, 0.56);text-align: center;line-height: 200px;">
							请<a class="layui-btn layui-btn-red layui-btn-mini" href="${basePath}/userLogin?uri=${basePath}/Productinfo/${product.pid }">登录</a>,登录后评论
							</div>
							</c:if>
							<div style="text-align: right;">
								<input type="checkbox" name="niming" title="匿名" lay-skin="primary">
								<button id="pingbtn" class="layui-btn layui-btn-red " href="javascript:;" lay-submit lay-filter="pinhlun">发表评论</button >
							</div>
						</form>	
							<fieldset class="layui-elem-field layui-field-title">
							  <legend>评论区</legend>
							</fieldset>
							<div>
								<ul class="pinglunqu">
								</ul>
							</div>
							<div><a style="display: block;text-align: center;background: #eee;padding: 10px;border-radius: 3px" href="javascript:;" class="pinglunlistbtn"><i class="layui-icon">&#xe61a;</i>  点击加载查看更多</a></div>
						</div>
					</div>
				</div>
			</div>
			<%-- <div class="producbian">
				<p class="sousuo">
				</p>
				<fieldset class="layui-elem-field layui-field-title">
						<legend>广告投放</legend>
					</fieldset>
				<a href="" class="ad">
					<img src="${basePath}/res/productinfo/img/1.jpg"/>
					<p>欧兰雅广告</p>
				</a>
				<a href="" class="ad">
					<img src="${basePath}/res/productinfo/img/2.jpg"/>
					<p>欧兰雅广告</p>
				</a>
				<a href="" class="ad">
					<img src="${basePath}/res/productinfo/img/3.jpg"/>
					<p>欧兰雅广告</p>
				</a>
			</div> --%>
		</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<script type="text/javascript">
	var pid="${product.pid }",uname="${user.name}",uid="${user.uid}",pnum=${product.inventory }
	</script>
	<script src="${basePath}/res/js/productImg.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="${basePath}/res/js/pinfo.js"></script>
	<!-- 购物车功能取消 2018年1月28日-->
	<%-- <jsp:include page="/WEB-INF/views/_meta/_cart.jsp"></jsp:include> --%>
</body>
</html>
