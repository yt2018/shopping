package com.yitong.Estshopping.dao;

import java.util.List;
import java.util.Map;

import com.yitong.Estshopping.entity.Pinglun;

public interface PinglunDao extends BaseDao<Pinglun>{
	/**
	 * 评论加载
	 * @param pid
	 * @param page
	 * @param limit
	 * @return
	 */
	List<Map> maplist(Integer pid, Integer page, Integer limit);

	List<Map> getUidlist(Integer uid, String string);
	/**
	 * 评论总数量
	 * @param pid
	 * @return
	 */
	Integer countlist(Integer pid);
	

}
