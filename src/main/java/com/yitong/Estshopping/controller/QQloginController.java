package com.yitong.Estshopping.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qq.connect.QQConnectException;
import com.qq.connect.api.OpenID;
import com.qq.connect.api.qzone.UserInfo;
import com.qq.connect.javabeans.AccessToken;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import com.qq.connect.oauth.Oauth;
import com.yitong.Estshopping.entity.Adminuser;
import com.yitong.Estshopping.entity.Role;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.AdminService;
import com.yitong.Estshopping.service.UserService;

/**
 * qq登陆
 * 
 * @author YT
 *
 */

@Controller
public class QQloginController {
	@Autowired
	private UserService userService;
	@Autowired
	private AdminService adminService;
	/**
	 * QQ第三方登录
	 * @throws QQConnectException 
	 */
	@RequestMapping("/qqlogin")
	public void qqlogin(HttpServletRequest request,HttpSession session, HttpServletResponse response) throws IOException, QQConnectException {
		
		response.sendRedirect(new Oauth().getAuthorizeURL(request));  
	}

	@RequestMapping("/login/qq")
	public String qqfanhui(HttpServletRequest request,HttpSession session, HttpServletResponse response)
			throws IOException, QQConnectException {
		String accessToken = null, openID = null;
		long tokenExpireIn = 0L;
		// --------------官方代码-----------------------------------------------
		// 以下代码由腾讯官方Java SDK给出，详见:http://wiki.connect.qq.com/sdk下载，所用到的
		// 类也均在腾讯官方给的Sdk4J.jar中给出
		// 根据返回的request解析出AccessToken对象
		AccessToken accessTokenObj = (new Oauth()).getAccessTokenByRequest(request);

		if (accessTokenObj.getAccessToken().equals("")) {
			// 我们的网站被CSRF攻击了或者用户取消了授权
			// 做一些数据统计工作
			System.out.print("没有获取到响应参数");
		} else {
			// 获取accessToken信息
			accessToken = accessTokenObj.getAccessToken();

			tokenExpireIn = accessTokenObj.getExpireIn();

			// 利用获取到的accessToken 去获取当前用的openid -------- start
			OpenID openIDObj = new OpenID(accessToken);
			// 获取openid
			openID = openIDObj.getUserOpenID();

			System.out.println("欢迎你，代号为 " + openID + " 的用户!");

			// 根据accessToken和openID获取UserInfo对象

			UserInfo qzoneUserInfo = new UserInfo(accessToken, openID);

			// 获取bean
			UserInfoBean userInfoBean = qzoneUserInfo.getUserInfo();
			System.out.println(userInfoBean.toString());
			System.out.println(userInfoBean.getGender());
			System.out.println(userInfoBean.getAvatar());
			System.out.println(userInfoBean.getAvatar().getAvatarURL100());
			/*
			 * 注入数据库
			 * 判断是否存在
			 */
			User userqq= userService.findByUserQQ(openID);
			if(userqq==null) {
				
				User user = new User();
				//user.setUserName(userInfoBean.getNickname());
				
				user.setQquser(openID);
				user.setName(userInfoBean.getNickname());
				user.setIcon(userInfoBean.getAvatar().getAvatarURL100());
				user.setSex(userInfoBean.getGender());
				User usersession = (User) session.getAttribute("user");
				System.out.println("==========="+usersession);
				if(usersession==null) {
					user.setPassword("tong118.com");
					user.setJoindate(new Date());
					userService.registerUser(user);
					session.setAttribute("user", user);
				}else{
					Integer uid = ((User) session.getAttribute("user")).getUid();
					user.setUid(uid);
					userService.updateUser(user);
					session.setAttribute("user", user);
				}
				
			}else {
				Adminuser adminRole = adminService.adminRole(userqq.getUid());
				
				if(adminRole!=null) {
					Role role = adminRole.getRole();
					session.setAttribute("role",role);
				}
				session.setAttribute("user", userqq);
			}
		}
		String url =  (String) session.getAttribute("redirectUrl");
		if(url==null) {
			url="/";
		}
		return "redirect:" + url;
		//return url;
	}

}
