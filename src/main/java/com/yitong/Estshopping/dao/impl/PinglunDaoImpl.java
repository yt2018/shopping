package com.yitong.Estshopping.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.PinglunDao;
import com.yitong.Estshopping.entity.Pinglun;
@Repository("pinglunDao") 
@Transactional
public class PinglunDaoImpl extends BaseDaoImpl<Pinglun> implements PinglunDao {

	@Override
	public List<Map> maplist(Integer pid, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		Integer status=1;
		List<Map> pList = new ArrayList<Map>();
		String hql="SELECT "
				+ "p.plid, p.isni, p.pldate, p.pltext, p.state, p.pid,"
				+ " p.zan, p.zid, u.uid, u.name, u.icon FROM pinglun AS p INNER JOIN `user` "
				+ "AS u ON p.uid = u.uid WHERE p.pid = '"+pid+"' AND p.state = '"+status+"' "
				+ "ORDER BY p.zan DESC, p.pldate DESC LIMIT "+(page - 1)*limit+", "+limit+"";
		
		Query cq = (Query) this.getCreateEntityManager().createNativeQuery(hql);
		Iterator iter = cq.getResultList().iterator();
		while (iter.hasNext()) {
			Map<String,Object> map = new HashMap<String,Object>();
			List<Map> jsonObjects = new ArrayList<Map>();
			Object[] next = (Object[]) iter.next();
			for (int i = 0; i < next.length; i++) {
				Object string = next[i];
				if(string==null) {
					string="";
				}
				System.out.print("【"+i+"】:"+string.toString()+"\t");
				
				
			}
			Object zid = next[7];
			String pltext = next[3].toString();
			if(zid!=null) {
				Pinglun pl = this.get(Integer.parseInt(zid.toString()));
				String name = pl.getUser().getName();
				if(pl.getIsni()=="on") {
					name="匿名";
				}
				
				String pltext2 = pl.getPltext();
				System.out.println("追评内容"+pltext2);
				pltext="回复 <b>@"+name+"</b> :"+pltext+"<blockquote class=\"layui-elem-quote layui-quote-nm\"><b>@"+name+"</b>:"+pltext2+"</blockquote>";
			}
			map.put("plid", next[0]);
			map.put("isni", next[1]);
			map.put("pldate", next[2]);
			map.put("pltext", pltext);
			map.put("state", next[4]);
			map.put("pid", next[5]);
			map.put("zan", next[6]);
			map.put("zid", zid);
			map.put("uid", next[8]);
			map.put("name", next[9]);
			map.put("icon", next[10]);
			pList.add(map);
			System.out.println(next);
		}
		return pList;
	}

	@Override
	public List<Map> getUidlist(Integer uid, String string) {
		return null;
		// TODO Auto-generated method stub
	}

	@Override
	public Integer countlist(Integer pid) {
		// TODO Auto-generated method stub
		String hql="select count(*) from Pinglun p where p.state = 1 and p.product.pid= "+pid;
				
		return this.count(hql);
	}
}
