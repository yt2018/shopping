package com.yitong.Estshopping.controller;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.yitong.Estshopping.entity.Banner;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.BannerService;
import com.yitong.framework.utils.ImageZipUtil;
import com.yitong.framework.utils.ListArray;
import com.yitong.framework.utils.MainColor;
import com.yitong.framework.utils.UpdateClass;

/**
 * 合作商注入
 * 
 * @author 18102
 *
 */
@Controller
public class CooperationController {
	private String rootPath;
	@Autowired
	private BannerService bannerService;

	@RequestMapping(value = "/userinfo/cooperation")
	public String userIndex(HttpSession session, Integer state, Integer page, Integer limit) {
		User user = (User) session.getAttribute("user");
		if (page == null)
			page = Integer.valueOf(1);
		if (limit == null)
			limit = Integer.valueOf(10);
	   List<Banner> banner = bannerService.list(user, state, page, limit);
		Integer count = bannerService.listcount(user, state, page, limit);
		session.setAttribute("banner", banner);
		session.setAttribute("count", count);
		session.setAttribute("page", page);
		session.setAttribute("limit", limit);
		return "user/cooperation/index";
	}

	@RequestMapping(value = "/userinfo/cooperation/addindex")
	public String bannerAdd(HttpSession session) {
		return "user/cooperation/c_add";
	}


	/**
	 * banner保存信息
	 */
	@RequestMapping(value = "/userinfo/bannerSever")
	public @ResponseBody Map<String, Object> showbanner(HttpSession session, @RequestBody Banner banner) {
		Map<String, Object> map = new HashMap<String, Object>();
		User user = (User) session.getAttribute("user");
		banner.setUser(user);
		// 状态 0审核中
		banner.setBimgStates(0);
		// 查询号
		if(banner.getBid()==null) {
			bannerService.save(banner);
		}else {
			bannerService.update(banner);
		}
		map.put("code", 0);
		map.put("msg", "申请成功");
		return map;
	}

	/**
	 * 修改
	 * 
	 * @param session
	 * @param bid
	 * @return
	 */
	@RequestMapping(value="/userinfo/cooperation/editindex/{bid}")
	public String editbanner(HttpSession session,@PathVariable("bid") Integer bid) {
		Banner banner1 = bannerService.getbannerid(bid);
		session.setAttribute("banneredit", banner1);
		return "user/cooperation/c_add";
	}
	/**
	 * 管理员控制页面
	 * @param session
	 * @param start
	 * @return
	 */
	@RequestMapping(value="/admin/cooperation")
	public String cooperationbanner(HttpSession session, Integer start) {
		return "user/cooperation/list";
	}
	/**
	 * 管理员查询数据
	 * @param session
	 * @param state
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping(value="/admin/cooperationbannerlist")
	public @ResponseBody Map<String, Object> cooperationbannerlist(HttpSession session, Integer state, Integer page, Integer limit) {
		Map<String, Object> map = new HashMap<>();
		List<Banner> list= bannerService.list(null, state, page, limit);
		map.put("code", 0);
		map.put("msg", "加载成功");
		map.put("count", bannerService.listcount(null, state, page, limit));
		map.put("data", list);
		return map;
	}
	@RequestMapping(value="/admin/cooperationbannerUpdate")
	public @ResponseBody Map<String, Object> cooperationbannerUpdate(Integer bid, Integer bimgStates) {
		Map<String, Object> map = new HashMap<>();
		if (bid == null) {
			map.put("code", Integer.valueOf(1));
			map.put("msg", "操作失败");
			return map;
		} else {
			bannerService.updatebimgStates(bid, bimgStates);
			map.put("code", Integer.valueOf(0));
			map.put("msg", "操作成功");
			return map;
		}
	}
	/**
	 * 单个删除
	 * @param bid
	 * @return
	 */
	@RequestMapping(value="/userinfo/cooperationbannerlist/del")
	public @ResponseBody Map<String, Object>  delcooperationbannerlist(Integer bid) {
		Map<String, Object> map = new HashMap<>();
		if (bid == null) {
			map.put("code", Integer.valueOf(1));
			map.put("msg", "\u5220\u9664\u5931\u8D25");
			return map;
		} else {
			bannerService.del(bid);
			map.put("code", Integer.valueOf(0));
			map.put("msg", "\u5220\u9664\u6210\u529F");
			return map;
		}
	}
	/**
	 * 批量删除
	 * @param bids
	 * @return
	 */
	@RequestMapping(value="/admin/cooperationbannerlist/del")
	public  @ResponseBody Map<String, Object> delcooperationbannerlists(String bids) {
		Map<String, Object> map = new HashMap<>();
		if (bids == null) {
			map.put("code", Integer.valueOf(1));
			map.put("msg", "操作失败");
			return map;
		}
		String stringshuzu[] = ListArray.Stringshuzu(bids);
		String as[];
		int j = (as = stringshuzu).length;
		for (int i = 0; i < j; i++) {
			String string = as[i];
			int bid = Integer.parseInt(string);
			bannerService.del(Integer.valueOf(bid));
		}

		map.put("code", Integer.valueOf(0));
		map.put("msg", "操作成功");
		return map;
	}
}
