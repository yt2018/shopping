package com.yitong.Estshopping.controller.admin;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.entity.CategorySecond;
import com.yitong.Estshopping.entity.Categorys;
import com.yitong.Estshopping.service.CategorysService;

/**
 * 菜单管理
 * @author YT
 *
 */

@Controller
@RequestMapping("/admin")
public class CategorysController {
	@Autowired
	private CategorysService categorysService;
	/**
	 * 一级目录页面
	 * @return
	 */
	
	@RequestMapping(value = "/listCategory/{page}")
	public String listCategorys(@PathVariable("page") Integer page, Map<String, Object> map){
		List<Categorys> categoryPage = categorysService.listCategory(page);
		map.put("categorys", categoryPage);
		map.put("page", page);
		//查询一级分类的页数
		Integer count = categorysService.countCategory();
		map.put("count", count);
		return "admin/category/list";
	 }
	
	/**
	 * 添加
	 */
	@RequestMapping(value = "/addCategory", method = RequestMethod.POST)
    public@ResponseBody Map<String, Object> addCategory(@RequestParam(value = "cname", required = true) String cname) {
		Map<String, Object> map =new HashMap<>();
		
		//查找是否存在
		Categorys existCategory = categorysService.existCategory(cname);
		if(existCategory!=null) {
			map.put("flag", true);
			map.put("rest", "该目录已存在,请进行修改");
			return map;
		}
		//创建一级分类的对象
    	Categorys category = new Categorys();
        category.setCname(cname);
        category.setPrivilegeTime(new Date());;
        categorysService.addCategory(category);
        map.put("flag", false);
        map.put("rest", "添加成功");
        return map;
    }
	/**
	 * 删除一级分类
	 */
	@RequestMapping(value = "/deleteCategory/{cid}/{page}")
    public @ResponseBody Map<String, Object> deleteCategory(@PathVariable("cid") Integer cid, @PathVariable("page") Integer page) {
		System.out.println(this.getClass()+"==="+cid+"----"+page);
		Map<String, Object> map =new HashMap<>();
		Categorys findCategory = categorysService.findCategory(cid);
		Set<CategorySecond> categorySeconds = findCategory.getCategorySeconds();
		System.out.println(categorySeconds.size());
		if (categorySeconds.size()!=0) {
			map.put("rest", "此菜单有二级菜单,不能直接删除");
			System.out.println(categorySeconds);
	        return map;
		}
		categorysService.deleteCategory(cid);
		map.put("rest", "删除成功");
        return map;
    }
	/**
	 * 修改一级分类
	 */
	@RequestMapping(value = "/updateCategory", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> updateCategory(Categorys category) {
		Map<String, Object> map =new HashMap<>();
		categorysService.updateCategory(category);
		map.put("rest", "修改成功");
        return map;
    }
}
