package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.entity.Image;
import com.yitong.Estshopping.entity.Product;

public interface ImageService {
	
	/**
	 * 查询数据库存放的文件
	 */
	public List<Image> classify();

	public void save(Image image);
	/**
	 * 查询图片ID
	 * @param imgsrc 
	 * @return
	 */
	public Image getImgid(String imgsrc);
	/**
	 * 更新
	 * @param string
	 * @param pid
	 * @return
	 */
	public void updateImgid(int iid,Product product);
	/**
	 * 根据关联的商品ID查询关联图片
	 * @param pid
	 * @return
	 */
	public List<Image> getPid(Integer pid);
	/**
	 * 删除图片
	 * @param image
	 */
	public void delete(Image image);

}
