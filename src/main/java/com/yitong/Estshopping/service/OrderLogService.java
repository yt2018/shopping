package com.yitong.Estshopping.service;

import java.util.List;

import com.yitong.Estshopping.entity.Orderlog;
import com.yitong.Estshopping.entity.User;

/**
 * 订单的Dao接口
 * @author YT
 *
 */

public interface OrderLogService{
	public void seve(Orderlog orderlog);
	/**
	 * 查询记录列表
	 * @param user
	 * @param page
	 * @return
	 */

	public List<Orderlog> listorder(User user, Integer page, Integer limit);
	public Integer listorder(User user);
	
}
