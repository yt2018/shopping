package com.yitong.Estshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.ImageDao;
import com.yitong.Estshopping.entity.Image;
import com.yitong.Estshopping.entity.Product;
import com.yitong.Estshopping.service.ImageService;

@Service("imageService")
@Transactional
public class ImageServiceImpl implements ImageService {
	@Autowired
	private ImageDao imageDao;
	@Override
	public List<Image> classify() {
		// TODO Auto-generated method stub
		
		return imageDao.find();
	}
	@Override
	public void save(Image image) {
		// TODO Auto-generated method stub
		imageDao.save(image);
	}
	@Override
	public Image getImgid(String imgsrc){
		// TODO Auto-generated method stub
		return imageDao.getimg(imgsrc);
	}
	@Override
	public void updateImgid(int iid,Product product){
		// TODO Auto-generated method stub
		imageDao.Updatepid(iid,product);;
	}
	@Override
	public List<Image> getPid(Integer pid) {
		// TODO Auto-generated method stub
		return imageDao.getPid(pid);
	}
	@Override
	public void delete(Image image) {
		// TODO Auto-generated method stub
		imageDao.delete(image);
	}
	
	
}
