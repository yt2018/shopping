<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
    <jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/_meta/_metaadmin.jsp"></jsp:include>
	<title>所有商品列表</title>
	<%-- <link rel="stylesheet" href="${basePath}/res/css/categorylist.css" media="all" /> --%>
</head>
<body>
	<br/>
	<a class="layui-btn add">添加商品</a>
	<br/>
	<div class="layui-form news_list">
	  	<table class="layui-table">
		    <colgroup>
				<col width="50">
				<col width="100">
				<col width="100">
		    </colgroup>
		    <thead>
				<tr>
					<th style="text-align:left;">序号</th>
					<th>一级分类</th>
					<th>操作</th>
				</tr> 
		    </thead>
		    <tbody class="news_content">
		    	<c:forEach var="c" items="${categorys}" varStatus="vs">
					<tr>
						<td>${vs.index+1}</td>
						<td>${c.cname }</td>
						<td>
						<!-- 修改 -->
						<a data-id="${c.cid}" data-cname="${c.cname }" class="change" href="javascript:void(0)">
						<i class="layui-icon" style="font-size: 30px; color: #1E9FFF;">&#xe642;</i>  </a> 
						<!-- 显示首页导航栏 -->
						<a data-id="${c.cid}" class="downc iconfont icon-menu1" href="javascript:void(0)"></a>
						<!-- 删除 -->	
						<a data-id="${c.cid}" class="delete" href="javascript:void(0)">
						<i class="layui-icon" style="font-size: 30px; color: #123;">&#xe640;</i> </a>
						</td>
					</tr>
				</c:forEach>
		    </tbody>
		</table>
	</div>
	<div id="pages"></div>
	<script type="text/javascript">
	layui.config({
		base : "js/"
	}).use(['form','layer','jquery','laypage'],function(){
		var form = layui.form(),
			layer = parent.layer === undefined ? layui.layer : parent.layer,
			laypage = layui.laypage,
			$ = layui.jquery;
		var page=${page};
		/* laypage({
			cont : "pages",
			pages : ${count}//总页数
			,curr:page//选中当前页
			,jump: function(obj,first){
			    //obj包含了当前分页的所有参数，比如：
			    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
			    console.log(obj.limit); //得到每页显示的条数
			    if(page!=obj.curr){
			    	window.location.href="${basePath}/admin/listCategory/"+obj.curr;
				}
			}
		}) */
		$(".downc").click(function(){
			layer.alert('正在开发者,请静候!', {icon: 3});
		})
		//删除
		$(".delete").click(function(){
			var id=$(this).data("id")
			$.get("${basePath}/admin/deleteCategory/"+id+"/"+page+"",function(data){
				layer.msg(data.rest)
				window.location.reload();
			})
			//alert(id+"==="+page)
		})
		//修改
		$(".change").click(function(){
			var cname=$(this).data("cname")
			var id=$(this).data("id")
			
			var index =layer.prompt({
				formType: 0,
			  	value: cname,
			  	title: '修改一级目录',
			  	resize:false,
			  	area: ['300px', '20px'] //自定义文本域宽高
			}, function(value, index, elem){
				$.ajax({
					type:"post",
					url:"${basePath}/admin/updateCategory",
					data:{"cid":id,"cname":value},
					async:true,
					success:function(data){
						if(data.flag){
							layer.msg(data.rest)
						}else{
							layer.msg(data.rest)
							layer.close(index);
						}
						//刷新当前页面
						window.location.reload();
					}
				});
			});
		})

		//添加
		$(".add").click(function(){
			var index = layer.open({
				title : "新增商品",
				type : 2,
				area: ['800px', '600px'],
				content : "${basePath}/admin/addProduct",
				cancel: function(index, layero){ 
					if(confirm('商品未保存确定要关闭么')){ //只有当点击confirm框的确定时，该层才会关闭
						//关闭后清除缓存文件
				    	$.get("${basePath}/imagesdelete");
						layer.close(index)
				  	}
				  	return false; 
				}    
			})			
		})
			
	})		
	</script>
</body>
</html>
