package com.yitong.Estshopping.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.entity.Walletlog;
import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.ProductService;
import com.yitong.Estshopping.service.WalletLogService;

/**
 * 钱包
 * @author YT
 *
 */

@Controller
public class PacketController {
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private WalletLogService walletLogService;
	
	@RequestMapping(value= "/userinfo/packet")
	public String listProductoff(HttpServletRequest request,HttpSession session, HttpServletResponse response,@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer limit){
		User user = (User) session.getAttribute("user");
		if(page==null) {
			page=1;
		}
		if(limit==null) {
			limit=10;
		}
		//**查询钱包总金额
		Float money=walletLogService.usercount(user);
		//查询消费记录
		List<Walletlog> listorder = walletLogService.listorder(user, page, limit);
		//数量
		Integer count = walletLogService.listorder(user);
		//今日消费
		Float money1=walletLogService.usercount(user,1);
		Float money2=walletLogService.usercount(user,2);
		session.setAttribute("listorder", listorder);
		session.setAttribute("money", money);
		session.setAttribute("money1", money1);
		session.setAttribute("money2", money2);
		session.setAttribute("page", page);
		session.setAttribute("limit", limit);
		session.setAttribute("count", count);
		return "user/packet";
	}
	
}
