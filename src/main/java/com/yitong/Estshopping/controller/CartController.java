package com.yitong.Estshopping.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yitong.Estshopping.bean.Cart;
import com.yitong.Estshopping.bean.CartItem;
import com.yitong.Estshopping.entity.Product;
import com.yitong.Estshopping.service.ProductService;

@Controller
public class CartController {
	@Autowired
	private ProductService productService;
	/**
	 * 购物车功能取消掉,客户与客户之间交易没有购物车,以免网络报错
	 * 跳转到购物车
	 */
	@RequestMapping(value = "/myCart")
	public String myCart() {

		return "cart/list";
	}

	/**
	 * 添加商品到购物车
	 */
	@RequestMapping(value="/cartadd/{pid}",method =RequestMethod.GET)
	public@ResponseBody Map<String, Object> addCarts(@PathVariable Integer pid,@RequestParam("count") Integer count, HttpSession session) {
		Map<String,Object> map = new HashMap<String,Object>();
		Product product = productService.getProduct(pid);
		CartItem cartItem = new CartItem();
		cartItem.setProduct(product);
		cartItem.setCount(count);
		cartItem.setImgsrc(product.getImage().get(0).getSrc());
		cartItem.setPrice(product.getShopPrice());

		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
		cart.addCart(cartItem);
		map.put("cartmap", "加入购物车成功");
		return map;
	}
	/**
	 * 获取购物车
	 */
	@RequestMapping(value="/getcart",method =RequestMethod.GET)
	public@ResponseBody Map<String, Object> getcart(HttpSession session) {
		Map<String,Object> map = new HashMap<String,Object>();
		Cart cart = (Cart) session.getAttribute("cart");
		map.put("carts", cart);
		return map;
	}
	/**
	 * 删除购物车商品
	 */
	@RequestMapping(value = "/removeCart/{pid}")
	public @ResponseBody Map<String, Object> removeCart(@PathVariable("pid") Integer pid, HttpSession session) {
		Map<String,Object> map = new HashMap<String,Object>();
		// 获取购物车对象
		Cart cart = (Cart) session.getAttribute("cart");
		cart.removeCart(pid);
		map.put("carts", cart);
		return map;
	}
	/**
	 * 清空购物车商品
	 */
	@RequestMapping(value = "/clearCart")
	public @ResponseBody Map<String, Object>  clearCart(HttpSession session) {
		Map<String,Object> map = new HashMap<String,Object>();
		// 获取购物车对象
		Cart cart = (Cart) session.getAttribute("cart");
		cart.clearCart();
		map.put("carts", cart);
		return map;
	}
	/**
	 * 生成订单
	 */
	@RequestMapping(value = "/userinfo/mycart")
	public @ResponseBody Map<String, Object>  orderCart(HttpSession session) {
		Map<String,Object> map = new HashMap<String,Object>();
		// 获取购物车对象
		Cart cart = (Cart) session.getAttribute("cart");
		Collection<CartItem> cartItems = cart.getCartItems();
		for (CartItem cartItem : cartItems) {
			
		}
		map.put("carts", cart);
		return map;
	}
	
}
