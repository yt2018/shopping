<%@page import="com.yitong.framework.utils.Dingdan"%>
<%@page import="java.util.UUID"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; Charset=gb2312">
<meta http-equiv="Content-Language" content="zh-CN">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="icon" rel="shortcut icon" type="image/x-icon" href="${basePath}/res/images/logo.png"/>
<title>我要赞助</title>
<!--Layui-->
		<link href="${basePath}/res/timeline/plug/layui/css/layui.css" rel="stylesheet" />
		<!--font-awesome-->
		<link href="${basePath}/res/timeline/plug/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
		<!-- animate.css -->
		<link href="${basePath}/res/timeline/css/animate.min.css" rel="stylesheet" />
		<!--全局样式表-->
		<link href="${basePath}/res/timeline/css/global.css" rel="stylesheet" />
		<!-- 本页样式表 -->
		<link href="${basePath}/res/timeline/css/timeline.css" rel="stylesheet" />
<body>
	<body>
		<!-- canvas -->
		<canvas id="canvas-banner"></canvas>
		<!-- 导航 -->
		<nav class="blog-nav layui-header">
			<div class="blog-container">
				<a href="${basePath}"><img src="${basePath}/res/images/logo1.png" height="50"/></a>
				<a href="${basePath}/timeline" class="layui-btn layui-btn-danger" style="float: right;margin-top: 12px">时光轴</a>
				<a href="${basePath}/resume" class="layui-btn layui-btn" style="float: right;margin-top: 12px">关于我</a>
			</div>
		</nav>
		<!-- 主体（一般只改变这里的内容） -->
		<style>
			.zanzhu{
				background: rgba(255, 255, 255, 0.62)
			}
		</style>
		<div class="blog-body">
			<div class="blog-container">
				<div class="zanzhu shadow" >
					<form class="layui-form layui-form-pane" style="max-width: 644px ;margin: 0 auto;" action="${basePath}/playzhifu" method="post">
						 <div class="layui-form-item">
							<div class="layui-inline">
								<label class="layui-form-label">赞助者</label>
								<div class="layui-input-inline">
									<% Dingdan dingdan=new Dingdan();
									String diof = dingdan.diof();request.setAttribute("out_trade_no", diof); %>
									<input type="hidden" name="out_trade_no" value="${out_trade_no }">
									<input type="text" name="uname" lay-verify="required" autocomplete="off" class="layui-input">
									<input type="hidden" name="subject" value="赞助">
								</div>
							</div>
							<div class="layui-inline">
								<label class="layui-form-label">赞助金额</label>
								<div class="layui-input-inline">
									<input type="number" name="total_amount" lay-verify="number" autocomplete="off" class="layui-input">
								</div>
							</div>
						</div>
						<div class="layui-form-item layui-form-text">
							
					          <label class="layui-form-label">留言</label>
					          <div class="layui-input-block">
					            <textarea placeholder="给我一个留言,励志我的梦" class="layui-textarea" name="body"></textarea>
					          </div>
				        </div>
						<div class="layui-form-item">
					          <div class="layui-input-block">
					            <button class="layui-btn layui-btn-normal" lay-submit="*" lay-filter="*">支付宝支付</button>
					          </div>
				        </div>
					</form>
				</div>
			</div>
		</div>
		<!-- 底部 -->
		<footer class="blog-footer">
			<p><span>Copyright</span><span>&copy;</span><span>2018</span>
				<a href="${basePath}">ESTshopping</a><span>Design By ESTshopping</span></p>
			<p>
				<a href="http://www.miibeian.gov.cn/" target="_blank">陇ICP备16001964号-1</a>
			</p>
		</footer>
		<!-- layui.js -->
		<script src="${basePath}/res/layui2/layui.js"></script>
		<!-- 全局脚本 -->
		<script src="${basePath}/res/timeline/js/global.js"></script>
		<!-- 本页脚本 -->
		<script src="https://open.mobile.qq.com/sdk/qqapi.js?_bid=152"></script>
		<script type="text/javascript" src="${basePath}/ap.js"></script> 
		<script type="text/javascript">
		layui.use(['jquery','form'], function() {
			var $ = layui.jquery;
			var form = layui.form;
		           
			form.on('submit(*)', function(data){
				 
				  var t=data.field;
				  console.info(t);
				  var queryParam='';
				 for( var attr in t ){
						queryParam+=attr+"="+t[attr]+"&"
				 } 
				var url="${basePath}/playzhifu" +"?";		
		        // var url=  ${basePath}/playzhifu +"?"+queryParam.substr(0, basic.length - 1);
				  //t.istype=$(data.elem).data("key")
				if(isWeiXin()){
					 queryParam += 'bizcontent=' + encodeURIComponent(bizStr);
					 for( var attr in t ){
							queryParam+=attr+"="+t[attr]+"&"
					 } 
						 alert("微信版"+url+queryParam.substr(0, basic.length - 1))
				         _AP.pay(url+queryParam.substr(0, basic.length - 1))
				         return false;
				 }
				/* if(isSjQQ()){
					for( var attr in t ){
						queryParam+=attr+"="+t[attr]+"&"
					 } 
					 void (void 0 !== mqq.ui.openUrl && mqq.ui.openUrl({
	                        target: 2,
	                        url: a
	                    }));
		                setTimeout(function() {
		                    window.location.href = url
		             }, 500)
		             return false;
				 } */
				 $.ajax({
						type:"post",
						dataType:"html",
		                contentType: "application/json",
						url:"${basePath}/playzhifu",
						data:JSON.stringify(t),
						success:function(data){
							//window.location.href="${basePath}/playzhifu"
							var url=data.substring(data.indexOf('action="')+8,data.indexOf('">'))
							console.info(data)
							console.info(data.indexOf('action="'))
							console.info(data.indexOf('">'))
							console.info(data.substring(data.indexOf('action="')+8,data.indexOf('">')) )
							//$("body").html(data)
							
						}
				})
				  //window.location.href="${basePath}/playzhifu?";
				return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
			});
			
		});
		/**
		* 判断是否在微信里打开
		*/
		function isWeiXin()
		{
			var ua = window.navigator.userAgent.toLowerCase();
			if(ua.match(/MicroMessenger/i) == 'micromessenger')
			{
				return true;

			}else
			{
				return false;
			}
		}

		/**
		* 判断是否在QQ里打开
		*/
		function isQQ()
		{
			var ua = window.navigator.userAgent.toLowerCase();
			if(ua.match(/QQ/i)=='qq'){
				return true;
			}else{
				return false;
			}
		}
		</script>
		<!--背景-->
		<script src="${basePath}/res/timeline/js/canvas.js"></script>
	</body>
</html>