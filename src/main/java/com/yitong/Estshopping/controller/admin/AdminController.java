package com.yitong.Estshopping.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yitong.Estshopping.entity.Role;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.service.AdminService;

/**
 * 网站管理员
 * @author YT
 *
 */

@Controller
public class AdminController {
	@Autowired
	private AdminService adminService;
	
	/**
	 * 商城首页
	 * @return
	 */
	@RequestMapping(value="/admin")
	public String showIndex(HttpSession session){
		System.out.println(session);
		User user= (User) session.getAttribute("user");
		if(user==null) {
			return "index";
		}
		Integer uid = user.getUid();
		System.out.println(uid);
		Role role = adminService.adminRole(uid).getRole();
		if(role==null) {
			return "norole";
		}
		return "admin/index";
	}
	
	/**
	 * 跳转到用户登录
	 * @return
	 */
	/*@RequestMapping(value = "/userLogin")
    public String userLogin() {
        return "login";
    }*/
	
	/**
	 *  用户注册的跳转
	 * @return
	 */
	/*@RequestMapping("/userRegister")
    public String register() {
        return "regist";
    }*/
	
}
