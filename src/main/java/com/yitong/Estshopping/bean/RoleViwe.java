package com.yitong.Estshopping.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;


public class RoleViwe {
	/**
	 * 用户id
	 */
	Integer uid;
	/**
	 * 用户名
	 */
	String username;
	/**
	 * 昵称
	 */
	String name;
	/**
	 * 手机号
	 */
	String phone;
	/**
	 * qq
	 */
	String qq;
	/**
	 * rid角色id
	 */
	Integer rid;
	/**
	 * 角色名称
	 */
	String rolename;
	/**
	 * 角色日志
	 */
	String rolelog;
	/**
	 * 角色级别
	 */
	Integer rolenum;
	/**
	 * 创建日期
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  
	Date createdate;
	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public Integer getRid() {
		return rid;
	}
	public void setRid(Integer rid) {
		this.rid = rid;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getRolelog() {
		return rolelog;
	}
	public void setRolelog(String rolelog) {
		this.rolelog = rolelog;
	}
	public Integer getRolenum() {
		return rolenum;
	}
	public void setRolenum(Integer rolenum) {
		this.rolenum = rolenum;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public RoleViwe(
			Object username, 
			Object name, 
			Object phone,
			Object qq,
			Object rolelog, 
			Object uid,
			Object rid,
			Object rolename,
			Object createdate,
			Object rolenum
			){
		super();
		this.uid = uid!=null?Integer.parseInt(uid.toString()):null;
		this.username = username!=null?username.toString():null;
		this.name = name!=null?name.toString():null;
		this.phone = phone!=null?phone.toString():null;
		this.qq = qq!=null?qq.toString():null;
		this.rid = rid!=null?Integer.parseInt(rid.toString()):null;
		this.rolename = rolename!=null?rolename.toString():null;
		this.rolelog = rolelog!=null?rolelog.toString():null;
		this.rolenum = rolenum!=null?Integer.parseInt(rolenum.toString()):null;
		try {
			this.createdate = createdate!=null?new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(createdate.toString()):null;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public RoleViwe() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
