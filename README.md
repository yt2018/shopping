# shopping

#### 项目介绍
使用layui组件做的模拟电商平台,做了支付宝支付功能,QQ一键登录功能,钱包管理功能,日志记录,商品添加功能
使用 layui+spring+hibernate+springmvc 做的模拟电商平台,作者qq1810258114

#### 软件架构
layui+spring+hibernate+springmvc


#### 安装教程

1. 导入Eclipse
2. 配置数据源
3. 配置QQ登录,配置支付宝接口
4. 上传图片资源需要重写

#### 使用说明

1. 尊重个人作品,做的不好仅供参考


#### 参与贡献

1. 使用layui组件做的模拟电商平台
2. QQ一键登录功能
3. 支付宝支付功能
4. 上传图片资源


