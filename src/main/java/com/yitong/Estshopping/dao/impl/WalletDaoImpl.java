package com.yitong.Estshopping.dao.impl;

import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.WalletDao;
import com.yitong.Estshopping.entity.User;
import com.yitong.Estshopping.entity.Wallet;

@Repository("walletDao")
public class WalletDaoImpl extends BaseDaoImpl<Wallet> implements WalletDao{

	@Override
	public Float usercount(User user) {
		// TODO Auto-generated method stub
		Wallet wallet = user.getWallet();
		 Float money=0.00f;
		if (wallet!=null) {
			 Wallet wallet2 = this.get(wallet.getWid());
			 money = wallet2.getMoney();
		}
		return money;
	}
 

}
