package com.yitong.Estshopping.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yitong.Estshopping.dao.PinglunDao;
import com.yitong.Estshopping.entity.Pinglun;
import com.yitong.Estshopping.service.PinglunService;
@Service("pinglunService")
@Transactional
public class PinglunServiceImpl implements PinglunService {
	@Autowired
	private PinglunDao pinglunDao;
	/**
	 * 评论
	 * @param pinglun
	 */
	public void save(Pinglun pinglun) {
		pinglunDao.save(pinglun);
	}
	@Override
	public List<Map> maplist(Integer pid, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		return pinglunDao.maplist(pid,page,limit);
	}
	@Override
	public Pinglun getpingid(int zid) {
		// TODO Auto-generated method stub
		return pinglunDao.get(zid);
	}
	@Override
	public List<Map> getUidlist(Integer uid, String string) {
		// TODO Auto-generated method stub
		return pinglunDao.getUidlist(uid,string);
	}
	@Override
	public Integer countlist(Integer pid) {
		// TODO Auto-generated method stub
		return pinglunDao.countlist(pid);
	};

}
