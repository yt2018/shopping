<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %> 
    <%application.setAttribute("layui_this","layui-this"); application.setAttribute("layui_no",null); %>
    	<ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
			<li class='layui-nav-item ${thisclass eq 1 ? layui_this : layui_no}'><a href="${basePath}/userinfo">
					<i class="layui-icon">&#xe609;</i> 我的主页
			</a></li>

			<li class='layui-nav-item ${thisclass eq 2 ? layui_this :layui_no}'><a href="${basePath}/userinfo/set"> <i
					class="layui-icon">&#xe620;</i> 基本设置
			</a></li>
			<li class='layui-nav-item ${thisclass eq 3 ? layui_this : layui_no}'><a href="${basePath}/userinfo/message"> <i
					class="layui-icon">&#xe611;</i> 我的消息
			</a></li>
			<li class="layui-nav-item ${thisclass eq 4 ? layui_this : layui_no}"><a href="${basePath}/userinfo/allproduct"> <i
					class="layui-icon">&#xe857;</i> 我的宝贝
			</a></li>
			<li class="layui-nav-item ${thisclass eq 5 ? layui_this : layui_no}"><a href="${basePath}/userinfo/sell"> <i
					class="layui-icon">&#xe62a;</i> 卖出宝贝<span class="layui-badge-dot layui-bg-orange"></span>
			</a></li>
			<li class="layui-nav-item ${thisclass eq 6 ? layui_this : layui_no}"><a href="${basePath}/userinfo/order"> <i
					class="layui-icon">&#xe63c;</i> 订单管理
			</a></li>
			<li class="layui-nav-item ${thisclass eq 7 ? layui_this : layui_no}"><a href="${basePath}/userinfo/orderlog"> <i
					class="layui-icon">&#xe60e;</i> 交易记录
			</a></li>
			<li class="layui-nav-item ${thisclass eq 8 ? layui_this : layui_no}"><a href="${basePath}/userinfo/packet"> <i
					class="layui-icon">&#xe65e;</i> 钱包
			</a></li>
			<li class="layui-nav-item ${thisclass eq 9 ? layui_this : layui_no}"><a href="${basePath}/userinfo/ticket"> <i
					class="layui-icon">&#xe60a;</i> 优惠券
			</a></li>
			<li class="layui-nav-item ${thisclass eq 10 ? layui_this : layui_no}"><a href="${basePath}/userinfo/help"> <i
					class="layui-icon">&#xe607;</i> 寻物启事
			</a></li>
			
			<li class="layui-nav-item ${thisclass eq 11 ? layui_this : layui_no}"><a href="${basePath}/userinfo/cooperation"> <i
					class="layui-icon">&#xe62e;</i> 与我合作
			</a></li>
		</ul>


		<div class="site-tree-mobile layui-hide">
			<i class="layui-icon">&#xe602;</i>
		</div>
		<div class="site-mobile-shade"></div>