<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
	 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<jsp:include page="/WEB-INF/views/_meta/path.jsp"></jsp:include>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/_meta/_meta2.jsp"></jsp:include>
<title>个人主页</title>
<link rel="stylesheet"
	href="${basePath}/res/css/global.css"
	media="all" />
</head>
<link rel="stylesheet"
	href="${basePath}/res/css/ribbon.css"
	media="all" />
<body>

	<jsp:include page="/WEB-INF/views/_meta/user/header.jsp"></jsp:include>
	<div class="layui-container fly-marginTop fly-user-main">
		<%application.setAttribute("thisclass",1); %>
		<jsp:include page="/WEB-INF/views/_meta/_menu.jsp"></jsp:include>

		<div class="fly-panel fly-panel-user" pad20>
			<div class="layui-tab layui-tab-brief" lay-filter="user">
				<div class="fly-home fly-panel" style="background-image: url();">
					<img
						src="${user.icon }"
						alt="${user.name }"> <i class='iconfont ${not empty user.phone ? "icon-renzheng":"" }' title="${user.name }"></i>
					<h1>
						${user.name } 
						<i class="iconfont ${user.sex eq '男' ? 'icon-nan':'icon-nv' }"></i>
					    <span style="color:#c00;">${role.rolename }</span>
					</h1>
					<c:if test="${empty user.phone }">
						<p style="padding: 10px 0; color: red;">未认证</p>
					</c:if>
					<c:if test="${not empty user.phone }">
						<p style="padding: 10px 0; color: #5FB878;">认证信息：${user.name }</p>
					</c:if>
					<p class="fly-home-info">
						<i class="iconfont icon-shijian"></i><span>${user.joindate } 加入</span>
					</p>

					<div class="fly-sns" data-user="">
						<span>个人主页:<input id="text" type ='text' value='http:${basePath}/u/${user.uid }'/></span> <a id="copy" href="javascript:;"
							class="layui-btn layui-btn-normal layui-btn-mini copy" data-type="copy">复制</a>
					</div>
					<!-- <div class="fly-sns" data-user="">
						<a href="javascript:;"
							class="layui-btn layui-btn-primary fly-imActive"
							data-type="addFriend">加为好友</a> <a href="javascript:;"
							class="layui-btn layui-btn-normal fly-imActive" data-type="chat">发起会话</a>
					</div> -->
					<script type="text/javascript">
					window.onload=function(event){
						var copy=document.getElementById("copy");
						copy.onclick=function(event){
							document.getElementById("text").select();
							document.execCommand("copy",false,null);
							alert("复制成功")
						};
					};	
					</script>

				</div>

				<div class="layui-row layui-col-space15">
					<div class="layui-col-md6 fly-home-jie">
						<div class="fly-panel">
							<h3 class="fly-panel-title">${user.name } 最近的发布的宝贝</h3>
							<ul class="layui-row pli">
								<c:forEach var="mp" items="${myproduct }">
									<li class="layui-col-xs6" data-pid="${mp.pid }">
										<c:if test="${mp.isState=='on'}">
										<div class="wrap"><span class="ribbon6">正常</span></div>
										</c:if>
										<c:if test="${mp.isState=='off'}">
										<span class="ribbon1"><span>商品信息违规被下架</span></span>
										</c:if>
										<a target="_block" href="${basePath}/Productinfo/${mp.pid }" class="productbox">
											<img width="${mp.image[0].width }" height="${mp.image[0].height }" src="${mp.image[0].src }"/>
										</a>
										<p>${mp.pname }</p>
										<div class="productbtn">
											<span><i class="layui-icon">&#xe637;</i>:<b><fmt:formatDate value="${mp.pdate}" pattern="yyyy-MM-dd"/></b></span>
											<span>浏览次数:<b>${mp.pnub}</b></span>
											<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-mini productdel" data-dateleid="${mp.pid }">删除</a>
											<c:if test="${mp.isState=='on'}">
											<a target="_block" href="${basePath}/userinfo/ProductSet/${mp.pid}" class="layui-btn layui-btn-red layui-btn-mini">修改</a>
											</c:if>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>

					<div class="layui-col-md6 fly-home-da">
						<div class="fly-panel">
							<h3 class="fly-panel-title">${user.name } 最近的回答</h3>
							<ul class="home-jieda">
								<li>
									<p>
										<span>1分钟前</span> 在 <a href="" target="_blank">闲置物品韩式衣服</a>中评论：
									</p>
									<div class="home-dacontent">
										有几成新,多少码?
									</div>
								</li>
								<li>
									<p>
										<span>5分钟前</span> 在 <a href="" target="_blank">闲置物品电脑</a>中评论：
									</p>
									<div class="home-dacontent">鲁大师跑分多少?</div>
								</li>

								<!-- <div class="fly-none" style="min-height: 50px; padding:30px 0; height:auto;"><span>没有回答任何问题</span></div> -->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/_meta/_footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/views/_meta/_u_footer.jsp"></jsp:include>
	<script type="text/javascript" src="${basePath}/res/js/userinfo.js"></script>
	<script type="text/javascript">
	var page=${page};
	var limit=${limit};
	var count=${count}
	</script>
	<script type="text/javascript" src="${basePath}/res/js/pli.js"></script>
</body>
</html>