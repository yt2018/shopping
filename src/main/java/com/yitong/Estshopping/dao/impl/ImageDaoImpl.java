package com.yitong.Estshopping.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.yitong.Estshopping.dao.ImageDao;
import com.yitong.Estshopping.entity.Image;
import com.yitong.Estshopping.entity.Product;
@Repository("imageDao")
public class ImageDaoImpl extends BaseDaoImpl<Image> implements ImageDao {

	public List<Image> find() {
		String hql ="from Image i where i.classify=0";
		Query query = this.getCurrentSession().createQuery(hql);
		return query.getResultList();
	}
	public Image getimg(String imgsrc) {
		// TODO Auto-generated method stub
		String hql ="from Image i where i.src= :imgsrc";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("imgsrc", imgsrc);
		return (Image) query.uniqueResult();
	}
	@Override
	public void Updatepid(int iid,Product product ) {
		// TODO Auto-generated method stub
		/*String hql="update Image i set i.product.pid = :pid where i.imgsrc = :imgsrc";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("pid", id);
		query.setParameter("imgsrc", src);*/
		Session session = this.getCurrentSession();
		Image image = session.get(Image.class, iid);
		image.setProduct(product);
		session.update(image);
	}
	@Override
	public List<Image> getPid(Integer pid) {
		// TODO Auto-generated method stub
		String hql="from Image i where i.product.pid="+pid;
		return this.find(hql);
	}
}
