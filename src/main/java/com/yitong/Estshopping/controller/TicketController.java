package com.yitong.Estshopping.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yitong.Estshopping.service.CategorySecondService;
import com.yitong.Estshopping.service.CategorysService;
import com.yitong.Estshopping.service.ImageService;
import com.yitong.Estshopping.service.ProductService;

/**
 * 优惠券
 * @author YT
 *
 */

@Controller
public class TicketController {
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategorysService categorysService;
	@Autowired
	private CategorySecondService categorySecondService;
	@Autowired
	private ImageService imageService;
	
	@RequestMapping(value= "/userinfo/ticket")
	public String listProductoff(HttpServletRequest request,HttpSession session, HttpServletResponse response){
		return "user/ticket";
	}
	@RequestMapping(value= "/userinfo/ticket/add")
	public String ticketadd(HttpServletRequest request,HttpSession session, HttpServletResponse response){
		return "user/ticketAdd";
	}
	
}
