layui.config({
  //base: 'res/js/jquery-zclip-master/' //假设这是你存放拓展模块的根目录
}).use(['layer', 'laytpl', 'form', 'element', 'upload', 'util','jquery','laypage'], function(exports){
  
	var $ = layui.jquery,
		laypage = layui.laypage,
		layer = layui.layer,
		laytpl = layui.laytpl,
		form = layui.form,
		element = layui.element,
		upload = layui.upload,
		util = layui.util,
		device = layui.device()
	//手机设备的简单适配
	var treeMobile = $('.site-tree-mobile'),
		shadeMobile = $('.site-mobile-shade')

	treeMobile.on('click', function() {
		$('body').addClass('site-mobile');
	});
	shadeMobile.on('click', function() { 
		$('body').removeClass('site-mobile');
	});
	//修改基本信息
	form.on('submit(set-mine)', function(data){
		//console.log(JSON.stringify(data.field))
			$.ajax({
				type:"post",
				contentType : "application/json ; charset=utf-8",
				url:path+"/userinfo/update",
				data:JSON.stringify(data.field),
				success:function(data){
					if(data.state==1){
						layer.msg(data.result)
					}
				}
			})
		return false;
	})
	form.verify({
		userName:function(value, item){
			 if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
		      return '用户名不能有特殊字符';
		    }
		    if(/(^\_)|(\__)|(\_+$)/.test(value)){
		      return '用户名首尾不能出现下划线\'_\'';
		    }
		    if(/^\d+\d+\d$/.test(value)){
		      return '用户名不能全为数字';
		    }
		}
		,pass: [
		    /^[\S]{6,12}$/
		    ,'密码必须6到12位，且不能出现空格'
		]
		,newpassword:function(value, item){
           if(!new RegExp($("#pass").val()).test(value)){
               return "两次输入密码不一致，请重新输入！";
           }
       }
		,datenum:[/^\+?[1-9]\d*$/,'大于0的整数']
	})
	//修改基本信息
	form.on('submit(updatepassword)', function(data){
		//console.log(JSON.stringify(data.field))
		var oldpwd=$("#L_nowpass").val();
		var newpwd=$("#newpassword").val();
		$.ajax({
			type:"get",
			contentType : "application/json ; charset=utf-8",
			url:path+"/userinfo/password",
			data:{"oldpassword":oldpwd,"newpassword":newpwd},
			success:function(data){
				if(data.state==1){
					layer.msg(data.result)
				}
				if(data.state==2){
					layer.msg(data.result)
				}
			}
		})
		return false;
	})
	//上传文件
	  var uploadInst = upload.render({
	    elem: '#icon' //绑定元素
	    ,url: path+'/cloud/upload/icon?fsrc=test/banner/Upload&uname='+uname+'&uid='+uid+'&title=易通通-'+uname+'头像'//上传接口
	    ,before: function(obj) {
			layer.load()
		}
	    ,done: function(obj){
	      //上传完毕回调
	    	layer.closeAll('loading')
			console.info(obj)
			if(obj.code==0){
				layer.msg(obj.msg)
				$.post(path+"/userinfo/icon",{"icon":obj.data.src},function(d){
					$("#iconimg").attr("src",obj.data.src);
					layer.msg(d.result)
				})
				$("#iconimg").attr("src",obj.data.src);
				layer.msg(obj.msg)
				fileobj=obj;
			}
	    }
	    ,size:2048
	    ,error: function(){
	      //请求异常回调
	    }
	  });
	$("#relieveQQ").on("click",function(){
		
		layer.confirm('您确定要解绑QQ吗?', {icon: 3, title:'温馨提示'}, function(index){
			//do something
			layer.msg("此功能已取消")
//			$.get(path+"/userinfo/relieveQQ",function(d){
//				if(d.state==0){
//					layer.msg(d.result)
//					window.location.href=path+"/userinfo/set"
//				}else{
//					layer.msg("网络故障")
//				}
//			})
			layer.close(index);
		});
	})
	$("#cancellation").on("click",function(){
		
		layer.confirm('您确定要注销账号吗?', {icon: 3, title:'温馨提示'}, function(index){
			//do something
//			$.get(path+"/userinfo/relieveQQ",function(d){
//				if(d.state==0){
//					layer.msg(d.result)
//					window.location.href=path+"/userinfo/set"
//				}else{
//					layer.msg("网络故障")
//				}
//			})
			layer.msg("此功能暂时不开放。")
			layer.close(index);
		});
	})
	$(".orderzhifu").on("click",function(){
		$.post(path+"/userinfo/zhifuorder/"+$(this).data("oif"),function(d){
			
			layer.msg("正在支付中......",{
				icon: 1,
				time: 2000 
			}, function(){
				if(d.code==1){
					window.location.href="?state=2#type=yesplay"
				}else if(d.code==2){
					layer.alert(d.msg,{icon:2})
				}else{
					layer.alert("网络出现错误,请刷新网页",{icon:2})
				}
				//window.location.href="?state="+d.code+"#type=yesplay"
			})
		})
	  })
	$(".edit_f").on("click",function(){
		var oif=$(this).data("oif")
		 var html =' <div class="editinfo">'
		  		+'			<div class="layui-form-item">'
				+'				<label class="layui-form-label">件数</label>'
				+'				<div class="layui-input-inline">'
				+'					<input type="number" id="num" autocomplete="off" value="'+$(this).data("num")+'" class="layui-input">'
				+'				</div>'
				+'			</div>'	
				+'			<div class="layui-form-item">'
				+'				<label class="layui-form-label">总金额</label>'
				+'				<div class="layui-input-inline">'
				+'					<input type="number" id="money" name="phone" autocomplete="off" value="'+$(this).data("money")+'" class="layui-input" maxlength="16">'
				+'				</div>'
				+'			</div>'	
				+'</div>'	
		layer.open({
			  content: html
			  ,title:'修改订单信息'
			  ,btn: ['修改','取消']
			  ,yes: function(index, layero){
			    //按钮【按钮一】的回调
				  var num=$("#num").val();
				  var money=$("#money").val();
				  var data={
					  "num":num,
					  "money":money
				  }
				  $.post(path+"/userinfo/editsell/"+oif,data,function(d){
					  if(d.code==1){
						  layer.msg(d.msg,{icon:1,time:2000},function(){
							  window.location.reload()
						  })
					  }else{
						  layer.msg(d.msg,{icon:2,time:2000},function(){
							  window.location.reload()
						  }) 
					  }
				  },"json")
			  }
		  		
		  	  ,btn2: function(index, layero){
			    //按钮【按钮二】的回调
			    
			    //return false 开启该代码可禁止点击该按钮关闭
			  }
		  });
	})
	$(".fahuo").on("click",function(){
		var oif=$(this).data("oif")
			layer.open({
				type:2
				,content:path+"/res/fahuo.jsp?iof="+oif
				,title:'发送方式'
				,area: ['400px', '300px']
			});
	})
	form.on('submit(fahuo)', function(data){
		console.log(JSON.stringify(data.field))
		/**
		 * 快递交易方式
		 */
		if(data.field.kd==1){
			data.field.kuaidi="当面交易";
			console.info(JSON.stringify(data.field));
		}
		if(data.field.kd==2){
			var kd=$("#kuaidi").val()
			if(kd==""||kd==null){
				layer.msg("快递单号不为空")
				return false;
			}
		}
		$.post(path+"/userinfo/kuaidi/"+iof,{"kuaidi":data.field.kuaidi},function(d){
			// parent.location.reload(); 
			if(d.code==1){
				parent.layer.msg(d.msg);
				parent.location.reload(); 
			}else{
				parent.layer.msg("网络错误!请刷新");
			}
		})
		
		return false;
	})
	var element = layui.element;
	//获取hash来切换选项卡，假设当前地址的hash为lay-id对应的值
	  //Hash地址的定位
	
	var layid = location.hash.replace(/^#type=/, '');
	element.tabChange('type', layid);
	  element.on('tab(type)', function(elem){
	    //location.hash = 'type='+ $(this).attr('lay-id');
		window.location.href='?state='+$(this).attr('lay-state')+'#type='+ $(this).attr('lay-id');
	  });
	  
		
	  //修改订单
	  $(".edit").on('click',function(){
		  var oif=$(this).data("oif")
		  var html =' <div class="editinfo">'
			  		+'			<div class="layui-form-item">'
					+'				<label class="layui-form-label">收件人</label>'
					+'				<div class="layui-input-inline">'
					+'					<input type="text" id="L_uname" name="uname" autocomplete="off" value="'+$(this).siblings('.uname').text()+'" class="layui-input">'
					+'				</div>'
					+'			</div>'	
					+'			<div class="layui-form-item">'
					+'				<label class="layui-form-label">手机号码</label>'
					+'				<div class="layui-input-inline">'
					+'					<input type="number" id="L_phone" name="phone" autocomplete="off" value="'+$(this).siblings('.phone').text()+'" class="layui-input" maxlength="16">'
					+'				</div>'
					+'			</div>'	
					+'			<div class="layui-form-item">'
					+'				<label class="layui-form-label">收件地址</label>'
					+'				<div class="layui-input-inline">'
					+'					<input type="text" id="L_addr" name="addr" autocomplete="off" value="'+$(this).siblings('.addr').text()+'" class="layui-input">'
					+'				</div>'
					+'			</div>'	
					+'</div>'	
		  layer.open({
			  content: html
			  ,title:'修改收件信息'
			  ,btn: ['修改','取消']
			  ,yes: function(index, layero){
			    //按钮【按钮一】的回调
				  var uname=$("#L_uname").val();
				  var phone=$("#L_phone").val();
				  var addr=$("#L_addr").val();
				  var data={
						  "uname":uname
						  ,"phone":phone
						  ,"addr":addr
				  }
				  $.post(path+"/userinfo/editorder/"+oif,data,function(d){
					  if(d.code==0){
						  layer.msg("修改成功",{icon:1,time:2000},function(){
							  window.location.reload()
						  })
					  }else{
						  layer.msg("修改失败",{icon:2,time:2000},function(){
							  window.location.reload()
						  }) 
					  }
				  },"json")
			  }
		  		
		  	  ,btn2: function(index, layero){
			    //按钮【按钮二】的回调
			    
			    //return false 开启该代码可禁止点击该按钮关闭
			  }
		  });
	  })
	 
})




